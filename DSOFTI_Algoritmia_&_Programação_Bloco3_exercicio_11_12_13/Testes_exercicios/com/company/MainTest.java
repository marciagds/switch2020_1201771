package com.company;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    //Testes exercicio11_v1
    @org.junit.jupiter.api.Test
    void getContagemHipotesesSomadeDoisAlgarismos_valor3() {
        //arrange
        int numero = 3;
        int expected =2 ;

        //act
        int resultado = Main.getContagemHipotesesSomadeDoisAlgarismos(numero);

        //assert
        assertEquals(expected, resultado);
    }
    @org.junit.jupiter.api.Test
    void getContagemHipotesesSomadeDoisAlgarismos_valor20() {
        //arrange
        int numero = 20;
        int expected =1 ;

        //act
        int resultado = Main.getContagemHipotesesSomadeDoisAlgarismos(numero);

        //assert
        assertEquals(expected, resultado);
    }
    @org.junit.jupiter.api.Test
    void getContagemHipotesesSomadeDoisAlgarismos_valorSuperior20() {
        //arrange
        int numero = 24;
        int expected =0 ;

        //act
        int resultado = Main.getContagemHipotesesSomadeDoisAlgarismos(numero);

        //assert
        assertEquals(expected, resultado);
    }
    @org.junit.jupiter.api.Test
    void getContagemHipotesesSomadeDoisAlgarismos_valorInferior0() {
        //arrange
        int numero = -1;
        int expected =0 ;

        //act
        int resultado = Main.getContagemHipotesesSomadeDoisAlgarismos(numero);

        //assert
        assertEquals(expected, resultado);
    }

    //Testes Exercicio12
    @org.junit.jupiter.api.Test
    void getRaizesPolinomio2ºgrau_teste_Equação2ºgrau_2raizes() {
        //arrange
        double coeficiente_a = 5;
        double coeficiente_b = 2;
        double coeficiente_c = -3;
        double raiz_1 = 0.6;
        double raiz_2 = -1;
        String expected ="A equação de 2º grau tem 2 raízes. \n r1=" + String.format( "%.2f",raiz_1) + " e r2=" + String.format( "%.2f",raiz_2);

        //act
        String resultado = Main.getRaizesPolinomio2ºgrau(coeficiente_a,coeficiente_b,coeficiente_c);

        //assert
        assertEquals(expected, resultado);
    }
    @org.junit.jupiter.api.Test
    void getRaizesPolinomio2ºgrau_teste_Equação1ºgrau() {
        //arrange
        double coeficiente_a = 0;
        double coeficiente_b = 2;
        double coeficiente_c = -3;
        double raiz_1 = 1.5;
        String expected = "A equação não é de 2º grau. A solução é: " + String.format( "%.2f",raiz_1);
        //act
        String resultado = Main.getRaizesPolinomio2ºgrau(coeficiente_a,coeficiente_b,coeficiente_c);

        //assert
        assertEquals(expected, resultado);
    }
    @org.junit.jupiter.api.Test
    void getRaizesPolinomio2ºgrau_teste_Equação2ºgrau_raizDupla() {
        //arrange
        double coeficiente_a = 2;
        double coeficiente_b = 4;
        double coeficiente_c = 2;
        double raiz_1 = -1;
        String expected = "A equação de 2º grau tem raíz múltipla. r=" + String.format( "%.2f",raiz_1);
        //act
        String resultado = Main.getRaizesPolinomio2ºgrau(coeficiente_a,coeficiente_b,coeficiente_c);

        //assert
        assertEquals(expected, resultado);
    }
    @org.junit.jupiter.api.Test
    void getRaizesPolinomio2ºgrau_teste_Equação2ºgrau_raizesImaginárias() {
        //arrange
        double coeficiente_a = 1;
        double coeficiente_b = 2;
        double coeficiente_c = 5;
        double parte_real = -1;
        double parte_imaginaria_raiz_1 = -2;        //valor negativo
        double parte_imaginaria_raiz_2 = 2;         //valor positivo (simétrico)
        String expected ="Equação de 2º grau com raizes imaginárias. \n r1="+ String.format( "%.2f", parte_real) + " + " + String.format( "%.2f",parte_imaginaria_raiz_1) + "i e r2=" + String.format( "%.2f", parte_real) + " + " +  String.format( "%.2f",parte_imaginaria_raiz_2) + "i";
        //act
        String resultado = Main.getRaizesPolinomio2ºgrau(coeficiente_a,coeficiente_b,coeficiente_c);

        //assert
        assertEquals(expected, resultado);
    }

    //Testes Exercicio13
    @org.junit.jupiter.api.Test
    void getRespostaCodigo_valor5() {
        //arrange
        int codigo = 5;
        String expected ="Vestuário \n";

        //act
        String resultado = Main.getRespostaCodigo(codigo);

        //assert
        assertEquals(expected, resultado);
    }
    @org.junit.jupiter.api.Test
    void getRespostaCodigo_valor7() {
        //arrange
        int codigo = 7;
        String expected ="Higiene pessoal \n";

        //act
        String resultado = Main.getRespostaCodigo(codigo);

        //assert
        assertEquals(expected, resultado);
    }
    @org.junit.jupiter.api.Test
    void getRespostaCodigo_valorNegativo2() {
        //arrange
        int codigo = -5;
        String expected ="Código Inválido \n";

        //act
        String resultado = Main.getRespostaCodigo(codigo);

        //assert
        assertEquals(expected, resultado);
    }
    @org.junit.jupiter.api.Test
    void getRespostaCodigo_valorSuperior15() {
        //arrange
        int codigo = 23;
        String expected ="Código Inválido \n";

        //act
        String resultado = Main.getRespostaCodigo(codigo);

        //assert
        assertEquals(expected, resultado);
    }
}