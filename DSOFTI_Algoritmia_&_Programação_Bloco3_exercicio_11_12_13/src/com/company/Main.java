package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        //exercicio11();
        exercicio11v_2();
        //exercicio12();
        //exercicio13();
    }

    public static void exercicio11() {                   //Este método mostra me as opções. Não consegui modelizar. Tinha de criar array?
/*Elabore uma solução em Java que, dado um número N de 1 a 20, apresente todas as maneiras possíveis
de obter esse número N, somando dois números de 0 a 10, independentemente da ordem desses dois
números. No final deve indicar quantas maneiras diferentes foram encontradas.  */

        //Variáveis envolvidas
        int numero, soma;
        String resultado;
        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        do {
            System.out.print("Qual o valor do Número (entre 1 - 20)?");
            numero = ler.nextInt();
        } while (numero <= 0);

        //Processamento
        int contaHipotese =0;
        for (int i=0; i<=10; i++){
            for (int j=0; j<=i; j++){
                soma = j + i;
                if(soma == numero){
                    resultado = "(" + j + "+" + i+")";
                    System.out.print(resultado);
                    contaHipotese += 1;
                }
            }
        }
        System.out.print("\n Total hipoteses:" + contaHipotese);
    }         //Este método mostra me as opções. Não consegui modelizar. Tinha de criar array?

    public static void exercicio11v_2() {            //Este método apenas contabiliza as hipoteses. Está modelizado.
/*Elabore uma solução em Java que, dado um número N de 1 a 20, apresente todas as maneiras possíveis
de obter esse número N, somando dois números de 0 a 10, independentemente da ordem desses dois
números. No final deve indicar quantas maneiras diferentes foram encontradas.  */

        //Variáveis envolvidas
        int numero, soma;
        int contaHipoteses;
        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        do {
            System.out.print("Qual o valor do Número (entre 1 - 20)?");
            numero = ler.nextInt();
        } while (numero <= 0);

        //Processamento
        contaHipoteses = getContagemHipotesesSomadeDoisAlgarismos(numero);

        System.out.print("Hipoteses: " + contaHipoteses);
    }       //Este método apenas contabiliza as hipoteses. Está modelizado.

    public static int getContagemHipotesesSomadeDoisAlgarismos(int numero){
        int contaHipotese =0;
        int soma;
        for (int i=0; i<=10; i++){
            for (int j=0; j<=i; j++){
                soma = j + i;
                if(soma == numero){
                    contaHipotese += 1;
                }
            }
        }
        return contaHipotese;
    }

    public static void exercicio12(){
/*Obter as raizes de uma função quadrática.
Hipoteses:
1. Não é equação do segundo grau (a=0);
2. A equação tem duas raízes reais (x1 e x2);
3. A equação tem uma raiz dupla (x1 = x2);
4. A equação tem raízes imaginárias. */

        //Variáveis envolvidas
        double coeficiente_a, coeficiente_b, coeficiente_c, delta;
        double raiz_1, raiz_2;
        String resultado;
        int resposta;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);


        //Entrada dos dados//Processamento
        do {
            System.out.print("Se pretender executar o programa colocque 0 ?" + "\n");
            resposta = ler.nextInt();
            if (resposta == 0) {
                System.out.print("Valor do coeficiente de 2º grau: ");
                coeficiente_a = ler.nextDouble();
                System.out.print("Valor do coeficiente de 1º grau: ");
                coeficiente_b = ler.nextDouble();
                System.out.print("Valor do coeficiente de 0º grau: ");
                coeficiente_c = ler.nextDouble();

                //Processamento
                resultado = getRaizesPolinomio2ºgrau(coeficiente_a, coeficiente_b, coeficiente_c);

                //Saída dos dados
                System.out.print(resultado + "\n");
            }

        } while(resposta == 0);

    }

    public static String getRaizesPolinomio2ºgrau(double coeficiente_a, double coeficiente_b, double coeficiente_c){
        double delta, raiz_1, raiz_2;
        String resultado;
        //Hipotese1.
        if(coeficiente_a == 0){
            raiz_1 = - coeficiente_c / coeficiente_b;
            resultado = "A equação não é de 2º grau. A solução é: " + String.format( "%.2f",raiz_1);
            return resultado;
        }
        else{
            delta = Math.pow(coeficiente_b,2)-4*coeficiente_a*coeficiente_c;

            if(delta >=0){
                raiz_1 = (-coeficiente_b + Math.sqrt(delta))/(2*coeficiente_a);
                raiz_2 = (-coeficiente_b - Math.sqrt(delta))/(2*coeficiente_a);

                if(raiz_1 == raiz_2){       //Hipotese 3.
                    resultado = "A equação de 2º grau tem raíz múltipla. r=" + String.format( "%.2f",raiz_1);
                }
                else {                      //Hipotese 2.
                    resultado = "A equação de 2º grau tem 2 raízes. \n r1=" + String.format( "%.2f",raiz_1) + " e r2=" + String.format( "%.2f",raiz_2);
                }
            }
            else {                          //Hipotese 4.
                double parte_real = -coeficiente_b / (2*coeficiente_a);
                double parte_imaginaria_raiz_1 = -Math.sqrt(-delta)/ (2*coeficiente_a);
                double parte_imaginaria_raiz_2 = Math.sqrt(-delta)/ (2*coeficiente_a);

                resultado = "Equação de 2º grau com raizes imaginárias. \n r1="+ String.format( "%.2f", parte_real) + " + " + String.format( "%.2f",parte_imaginaria_raiz_1) + "i e r2=" + String.format( "%.2f", parte_real) + " + " +  String.format( "%.2f",parte_imaginaria_raiz_2) + "i";
            }

        }
        return resultado;
    }

    public static void exercicio13(){
        //Variáveis envolvidas
        String classification;
        int codigo;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados

        do {
            do {
                System.out.print("Insira o código: ");
                codigo = ler.nextInt();
                if (codigo<0){
                    System.out.print("Código Inválido \n");
                }
            } while (codigo < 0);

            classification = getRespostaCodigo(codigo);
            System.out.print(classification);
        }while(codigo > 0);

    }

    public static String getRespostaCodigo(int codigo){
    String classification;
        if (codigo == 1) {
            classification = "Alimento não perecível \n";
            return classification;
        }
        else {
            if (codigo >= 2 && codigo <= 4) {
                classification = "Alimento perecível \n";
            } else {
                if (codigo == 5 || codigo == 6) {
                    classification = "Vestuário \n";
                } else {
                    if (codigo == 7) {
                        classification = "Higiene pessoal \n";
                    } else {
                        if (codigo >= 8 && codigo <= 15) {
                            classification = "Limpeza e utensílios domesticos \n";
                        } else {
                            classification = "Código Inválido \n";
                        }
                    }
                }
            }
        }
        return classification;
    }
}















