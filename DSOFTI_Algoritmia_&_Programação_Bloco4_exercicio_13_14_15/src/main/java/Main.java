import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        // write your code here
        //exercicio13();
        //exercicio14();
        //exercicio15();
        //getCalculoAuxiliarMatrizInversa();
    }


    public static void exercicio13() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}};
        boolean matrizChecked = getSeMatrizQuadrada(matriz);
        if (matrizChecked == true) {
            System.out.println("A matriz é quadrada.");
        } else {
            System.out.println("A matriz não é quadrada.");
        }
    }

    public static boolean getSeMatrizQuadrada(int[][] matriz) {
        if (matriz == null || matriz.length ==0) {
            return false;
        } else {
            boolean matrizQuadrada = true;
            int nLinhas = matriz.length;
            int nColunas;

            for (int i = 0; i < matriz.length && matrizQuadrada == true; i++) {
                nColunas = matriz[i].length;
                if (nColunas != nLinhas) {
                    //return false;
                    matrizQuadrada = false;
                }
            }
            //return true;
            return matrizQuadrada;
        }
    }

    public static void exercicio14() {
        int[][] matriz = {{1, 2, 3}, {4, 5, 6}};
        boolean matrizChecked = getSeMatrizQuadrada(matriz);
        if (matrizChecked == true) {
            System.out.println("A matriz é retangular.");
        } else {
            System.out.println("A matriz não é é retangular.");
        }
    }

    public static boolean getSeMatrizRetangular(int[][] matriz) {
        if (matriz == null || matriz.length ==0) {
            return false;
        } else {
            boolean matrizRetangular = true;
            int nColunasReferencia = matriz[0].length;
            int nLinhas = matriz.length;
            int nColunas;
            for (int i = 0; i < matriz.length && matrizRetangular == true; i++) {
                nColunas = matriz[i].length;
                if (nColunas != nColunasReferencia) {  //a matriz tem de ter as mesmas colunas em todas as linhas
                    //return false;
                    matrizRetangular = false;
                }
                if (nColunas == nLinhas) {
                    matrizRetangular = false;           //a matriz é quadrada e não retangular
                }
            }
            //return true;
            return matrizRetangular;
        }
    }

    public static void exercicio15() {
        int[][] matriz = {{2,1,3},{2,2,3},{2,1,2}};
        System.out.println("Exercicio 15.a: " + getMenorValordaMatriz(matriz));
        System.out.println("Exercicio 15.b: " + getMaiorValordaMatriz(matriz));
        System.out.println("Exercicio 15.c: " + getMediaElementosDaMatriz(matriz));
        System.out.println("Exercicio 15.d: " + getProdutoElementosDaMatriz(matriz));
        System.out.println("Exercicio 15.e: " + Arrays.toString(getConjuntoElementosNaoRepetidosDaMatriz(matriz)));
        System.out.println("Exercicio 15.f: " + Arrays.toString(getElementosPrimosdaMatriz(matriz)));
        System.out.println("Exercicio 15.g: " + Arrays.toString(getElementosDaDiagonalPrincipalDaMatriz(matriz)));
        System.out.println("Exercicio 15.h: " + Arrays.toString(getElementosDaDiagonalSecundariaDaMatriz(matriz)));
        System.out.println("Exercicio 15.i: " + getVerificarMatrizIdentidade(matriz));
        System.out.println("Exercicio 15.j: " + Arrays.toString(getMatrizInversa(matriz)));
        System.out.println("Exercicio 15.k: " + Arrays.toString(getMatrizTransposta(matriz)));
    }

    public static Integer getMenorValordaMatriz(int[][] matriz) {
        if (matriz == null || matriz.length == 0) {
            return null;
        } else {
            int menorValor = matriz[0][0];  //valor de referência
            for (int i = 0; i < matriz.length; i++) {
                for (int k = 0; k < matriz[i].length; k++) {
                    if (matriz[i][k] < menorValor) {
                        menorValor = matriz[i][k];
                    }
                }
            }
            return menorValor;
        }
    }

    public static Integer getMaiorValordaMatriz(int[][] matriz) {
        if (matriz == null || matriz.length == 0) {
            return null;
        } else {
            int maiorValor = matriz[0][0];  //valor de referência
            for (int i = 0; i < matriz.length; i++) {
                for (int k = 0; k < matriz[i].length; k++) {
                    if (matriz[i][k] > maiorValor) {
                        maiorValor = matriz[i][k];
                    }
                }
            }
            return maiorValor;
        }
    }

    public static Double getMediaElementosDaMatriz(int[][] matriz) {
        if (matriz == null || matriz.length == 0) {
            return null;
        } else {
            double somaElmentos = 0;
            int contaElementos = 0;
            double mediaELementos;
            for (int i = 0; i < matriz.length; i++) {
                for (int k = 0; k < matriz[i].length; k++) {
                    somaElmentos += matriz[i][k];
                    contaElementos += 1;
                }
            }
            mediaELementos = somaElmentos / contaElementos;
            return mediaELementos;
        }
    }

    public static Double getProdutoElementosDaMatriz(int[][] matriz) {
        if (matriz == null || matriz.length == 0) {
            return null;
        } else {
            double produtoElmentos = 1;
            for (int i = 0; i < matriz.length; i++) {
                for (int k = 0; k < matriz[i].length; k++) {
                    produtoElmentos *= matriz[i][k];
                }
            }
            return produtoElmentos;
        }
    }

    public static int[] getConjuntoElementosNaoRepetidosDaMatriz(int[][] matriz) {
        if (matriz == null) {
            return null;
        }
        int[] vetorElementosNaoRepetidos = new int[0];
        boolean existeNoVetorResultante = false;
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz[linha].length; coluna++) {
                existeNoVetorResultante = getSeExisteNoVetor(vetorElementosNaoRepetidos, matriz[linha][coluna]);
                if (existeNoVetorResultante == false) {
                    vetorElementosNaoRepetidos = getAdicionarNoVetor(vetorElementosNaoRepetidos, matriz[linha][coluna]);
                }
            }
        }
        return vetorElementosNaoRepetidos;
    }

    public static boolean getSeExisteNoVetor(int[] v, int number) {
        if (v == null) {              //caso o vetor seja null
            return false;
        }
        boolean result = false;
        int position = 0;
        while (position < v.length && result == false) {     //assim que se verificar que existe repetição. retorna true e acaba o ciclo
            if (v[position] == number) {
                result = true;
            }
            position++;
        }
        return result;
    }

    public static int[] getAdicionarNoVetor(int[] v, int number) {
        int size = 1;
        if (v != null) {
            size += v.length;
        }
        int[] vetorResultante = new int[size];
        for (int position = 0; position < size - 1; position++) {   //garante que o vetor resultante guarda os elementos do vetor inicial
            vetorResultante[position] = v[position];
        }
        vetorResultante[size - 1] = number;     //insere o valor do elemento adicionado ao vetor resultante.
        return vetorResultante;
    }

    public static int[] getElementosDaDiagonalPrincipalDaMatriz(int[][] matriz) {
        boolean matrizQuadrada = getSeMatrizQuadrada(matriz);
        boolean matrizRetangular = getSeMatrizRetangular(matriz);
        int[] vetorElementosDiagonalDaMatriz;
        if (matrizQuadrada == true || matrizRetangular == true) { //definir tamanho do vetor
            if (matriz.length <= matriz[0].length) {
                vetorElementosDiagonalDaMatriz = new int[matriz.length];
            } else {
                vetorElementosDiagonalDaMatriz = new int[matriz[0].length];
            }
            int k = 0;
            for (int indexLinha = 0; indexLinha < matriz.length; indexLinha++) {
                for (int indexColuna = 0; indexColuna < matriz[indexLinha].length; indexColuna++) {
                    if (indexLinha == indexColuna) {
                        vetorElementosDiagonalDaMatriz[k] = matriz[indexLinha][indexColuna];
                        k++;
                    }
                }
            }
            return vetorElementosDiagonalDaMatriz;
        } else {
            return null;
        }
    }

    public static int[] getElementosDaDiagonalSecundariaDaMatriz(int[][] matriz) {
        boolean matrizQuadrada = getSeMatrizQuadrada(matriz);
        boolean matrizRetangular = getSeMatrizRetangular(matriz);
        int[] vetorElementosDiagonalDaMatriz;
        if (matrizQuadrada == true || matrizRetangular == true) { //definir tamanho do vetor
            if (matriz.length <= matriz[0].length) {
                vetorElementosDiagonalDaMatriz = new int[matriz.length];
            } else {
                vetorElementosDiagonalDaMatriz = new int[matriz[0].length];
            }
            int k = 0;
            int indexUltimaColuna = matriz[0].length - 1;
            for (int indexLinha = 0; indexLinha < vetorElementosDiagonalDaMatriz.length; indexLinha++) {
                int indexcoluna = indexUltimaColuna - indexLinha;
                vetorElementosDiagonalDaMatriz[k] = matriz[indexLinha][indexcoluna];
                k++;
            }
            return vetorElementosDiagonalDaMatriz;
        } else {
            return null;
        }
    }

    public static int[] getElementosPrimosdaMatriz(int[][] matriz) {
        if (matriz == null) {
            return null;
        }
        boolean numberPrime;
        int[] vetorElementosPrimosDaMatriz = new int[0];
        for (int indexLinha = 0; indexLinha < matriz.length; indexLinha++) {
            for (int indexColuna = 0; indexColuna < matriz[indexLinha].length; indexColuna++) {
                int number = matriz[indexLinha][indexColuna];
                numberPrime = getCheckIfNumberIsPrime(number);
                if (numberPrime) {
                    vetorElementosPrimosDaMatriz = getAdicionarNoVetor(vetorElementosPrimosDaMatriz, number);
                }
            }
        }
        return vetorElementosPrimosDaMatriz;
    }

    public static boolean getCheckIfNumberIsPrime(int number) {
        boolean primeNumber = true;
        int i;
        if (number < 2) {
            primeNumber = false;
        }
        for (i = 2; i <= (number / 2) && primeNumber == true; i++) {
            if (number % i == 0) {
                primeNumber = false;
            }
        }
        return primeNumber;
    }

    public static boolean getVerificarMatrizIdentidade(int[][] matriz) {
        boolean matrizQuadrada = getSeMatrizQuadrada(matriz);
        boolean matrizIdentidade = true;
        if (matrizQuadrada == true) {
            for (int i = 0; i < matriz.length && matrizIdentidade == true; i++) {
                if (matriz[i][i] != 1) {
                    matrizIdentidade = false;
                }
                for (int j = 0; j < matriz[0].length && matrizIdentidade == true; j++) {
                    if ((i != j) && (matriz[i][j] != 0)) {
                        matrizIdentidade = false;
                    }
                }
            }
        }
        else {
            matrizIdentidade=false;
        }
        return matrizIdentidade;
    }

    public static double[][] getMatrizInversa(int[][] matriz){
        boolean matrizQuadradaCheck = getSeMatrizQuadrada(matriz);
        if(!matrizQuadradaCheck){
        return null;
        }
        else {
            double[][] matrizAuxiliar = getMatrizAuxiliarMetodoMatrizInversa(matriz); //criar matriz auxiliar
            double [][] matrizAuxiliarIntermedia = getCalculoAuxiliarMatrizInversa(matrizAuxiliar);
            if(matrizAuxiliarIntermedia == null){
                return null;            //a matriz não pode ser invertida
            }
            else {
                int indexElementoPivoFinal = matrizAuxiliarIntermedia.length-1;
                double elementoPivoFinal = matrizAuxiliarIntermedia[indexElementoPivoFinal][indexElementoPivoFinal];
                double[][]matrizAuxiliarFinal = getMatrizAuxiliarFinal(matrizAuxiliarIntermedia);
                double [][] matrizInvertida = getMultiplicarMatrizPorConstante(matrizAuxiliarFinal,(1/elementoPivoFinal));
                return  matrizInvertida;
            }
        }
    }

    public static double[][] getMatrizAuxiliarMetodoMatrizInversa(int[][]matriz) {
        //criação de matriz auxiliar - matriz + matriz identidade
        if (matriz == null || matriz.length == 0){
            return null;
        } else {
            int tamanhoMatriz = matriz.length;
            int nColunasMatrizAuxiliar = tamanhoMatriz * 2;
            int nLinhasMatrizAuxilizar = tamanhoMatriz;
            double[][] matrizAuxiliar = new double[nLinhasMatrizAuxilizar][nColunasMatrizAuxiliar];
            for (int i = 0; i < matrizAuxiliar.length; i++) {
                for (int k = 0; k < matrizAuxiliar[i].length; k++) {
                    if (k < matriz.length) {
                        matrizAuxiliar[i][k] = matriz[i][k];
                    } else {
                        if (k == matriz.length + i) {
                            matrizAuxiliar[i][k] = 1;
                        } else {
                            matrizAuxiliar[i][k] = 0;
                        }
                    }
                }
            }
            return matrizAuxiliar;
        }
    }

   public static double [][] getCalculoAuxiliarMatrizInversa(double [][]matrizAuxiliar){
       //double [][]matrizAuxiliar = {{1,2,3,1,0,0},{1,3,5,0,1,0},{2,3,3,0,0,1}};
        int m=0;
        double elementoPivo = 1;
        int nLinhasMatrizResultante = matrizAuxiliar.length;
        int nColunasMatrizResultante = matrizAuxiliar.length * 2;
        double[][]matrizAuxiliarResultante = new double[nLinhasMatrizResultante][nColunasMatrizResultante];
        boolean matrizInversaPossivel = true;
        while(m<matrizAuxiliar.length && matrizInversaPossivel==true && matrizAuxiliar!=null){
            for (int i = 0; i < matrizAuxiliarResultante.length; i++) {
                for (int k = 0; k < matrizAuxiliarResultante[i].length; k++) {
                    if (i == m) {
                        matrizAuxiliarResultante[i][k] = matrizAuxiliar[i][k];
                    } else {
                        matrizAuxiliarResultante[i][k] = ((matrizAuxiliar[m][m] * matrizAuxiliar[i][k]) - (matrizAuxiliar[i][m] * matrizAuxiliar[m][k])) / elementoPivo;
                    }
                    if(matrizAuxiliarResultante[i][k] == -0.0){                 ////Duvida. forço o valor a ficar zero quando me dá -0.0        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        matrizAuxiliarResultante[i][k] = 0;
                    }
                }
            }
            elementoPivo = matrizAuxiliarResultante[m][m];
            if(m<matrizAuxiliar.length-2){
                if(matrizAuxiliarResultante[m+1][m+1] == 0){            //caso seja necessário trocar de linha
                    matrizAuxiliarResultante = getTrocarLinhasDaMatrizNoCalculoMatrizInversa(matrizAuxiliarResultante,m+1, m+1);
                    if(matrizAuxiliarResultante == null){       // a matriz tem determinante zero, pelo que não pode ser invertida
                    matrizInversaPossivel = false;
                    }
                }
            }
            matrizAuxiliar = matrizAuxiliarResultante;
            matrizAuxiliarResultante = new double[nLinhasMatrizResultante][nColunasMatrizResultante];
            m++;
        }
       int indexElementoFinal = matrizAuxiliar.length -1;
        if(matrizAuxiliar[indexElementoFinal][indexElementoFinal] ==0){
            matrizAuxiliar = null;
        }
        return matrizAuxiliar;
    }

    public static double [][] getTrocarLinhasDaMatrizNoCalculoMatrizInversa(double[][]matrizAuxilizar, int linha , int coluna){
        boolean linhaDiferentezero=false;
        double[]linha1Matriz;
        double[]linha2Matriz = new double[matrizAuxilizar[0].length];
        int j=0;
        int linhaSubstituida=0;
        linha1Matriz = getGuardarLinhadaMatriz(matrizAuxilizar,linha);
        for(int i = linha+1; i < matrizAuxilizar[0].length && linhaDiferentezero == false; i++){
            if(matrizAuxilizar[i][coluna]!=0){
                linha2Matriz = getGuardarLinhadaMatriz(matrizAuxilizar, i);
                linhaDiferentezero=true;
                linhaSubstituida = i;
            }
        }
        if(linhaSubstituida!=0) {
            for (int k = 0; k < matrizAuxilizar[0].length; k++) {
                matrizAuxilizar[linha][k] = linha2Matriz[j];
                matrizAuxilizar[linhaSubstituida][k] = linha1Matriz[j];
                j++;
            }
            return matrizAuxilizar;
        }
        else {
            return null;
        }
    }

    public static double[] getGuardarLinhadaMatriz(double[][]matriz, int i){
        int j=0;
        if(matriz == null || matriz.length==0 || i <0) {
            return null;
        }
        else{
            double[]linhaMatriz = new double[matriz[0].length];
            for(int k=0; k<matriz[0].length; k++){
            linhaMatriz[j]= matriz[i][k];         //guardar linha
            j++;
            }
            return linhaMatriz;
        }
    }

    public static double[][] getMatrizAuxiliarFinal(double[][]matrizAuxiliar){
        if(matrizAuxiliar == null || matrizAuxiliar.length ==0 ){
           return null;
        }
        else{
            double [][] matrizAuxiliarFinal = new double[matrizAuxiliar.length][matrizAuxiliar.length];
            for(int i=0; i< matrizAuxiliar.length; i++){
                int j = 0;
                for (int k=matrizAuxiliar.length; k<matrizAuxiliar[i].length; k++){
                matrizAuxiliarFinal[i][j]=matrizAuxiliar[i][k];
                j++;
                }
            }
            return matrizAuxiliarFinal;
        }
    }

    public static double [][] getMultiplicarMatrizPorConstante(double[][] matriz, double number) {
        if (matriz == null) {
            return null;
        } else {
            for (int i = 0; i < matriz.length; i++) {
                for (int k = 0; k < matriz[i].length; k++) {
                    matriz[i][k] = matriz[i][k] * number;
                    if(matriz[i][k] == -0.0){                 ////Duvida. forço o valor a ficar zero quando me dá -0.0        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        matriz[i][k] = 0;
                    }
                }
            }
            return matriz;
        }
    }

    public static int[][] getMatrizTransposta(int[][]matriz){
        boolean matrizQuadrada = getSeMatrizQuadrada(matriz);
        boolean matrizRetangular = getSeMatrizRetangular(matriz);
        int [][]matrizTransposta;

        if(matrizQuadrada==true || matrizRetangular==true){
           matrizTransposta = new int[matriz[0].length][matriz.length];
            for(int i=0; i<matrizTransposta.length; i++){
                for(int k=0; k<matrizTransposta[i].length; k++){
                    matrizTransposta[i][k] = matriz[k][i];
                }
            }
        }
        else {
            matrizTransposta = null;
        }
       return matrizTransposta;
    }
}
