public class HELP {
    public static void main(String[] args) {

       // getCalculoAuxiliarMatrizInversa();
        getMatrizInversa();

    }

    public static void getCalculoAuxiliarMatrizInversa(){
        double [][]matrizAuxiliar ={{2,3,1,3,1,0,0,0},{2,3,4,1,0,1,0,0},{3,4,2,4,0,0,1,0},{2,3,4,2,0,0,0,1}};
        int m=0;
        double elementoPivo = 1;
        int nLinhasMatrizResultante = matrizAuxiliar.length;
        int nColunasMatrizResultante = matrizAuxiliar.length * 2;
        double[][]matrizAuxiliarResultante = new double[nLinhasMatrizResultante][nColunasMatrizResultante];
        boolean matrizInversaPossivel = true;

        for (int l = 0; l < matrizAuxiliar.length; l++)  {
            for (int c = 0; c < matrizAuxiliar[0].length; c++)     {
                System.out.print( matrizAuxiliar[l][c] + " "); //imprime caracter a caracter
            }
            System.out.println(" "); //muda de linha
        }


        while(m<matrizAuxiliar.length && matrizInversaPossivel==true){
            for (int i = 0; i < matrizAuxiliarResultante.length; i++) {
                for (int k = 0; k < matrizAuxiliarResultante[i].length; k++) {
                    if (i == m) {
                        matrizAuxiliarResultante[i][k] = matrizAuxiliar[i][k];
                    } else {
                        matrizAuxiliarResultante[i][k] = ((matrizAuxiliar[m][m] * matrizAuxiliar[i][k]) - (matrizAuxiliar[i][m] * matrizAuxiliar[m][k])) / elementoPivo;
                    }
                    if(matrizAuxiliarResultante[i][k] == -0.0){
                        matrizAuxiliarResultante[i][k] = 0;
                    }
                }
            }

            System.out.println(" "); //muda de linha
            for (int l = 0; l < matrizAuxiliarResultante.length; l++)  {
                for (int c = 0; c < matrizAuxiliarResultante[0].length; c++)     {
                    System.out.print( matrizAuxiliarResultante[l][c] + " "); //imprime caracter a caracter
                }
                System.out.println(" "); //muda de linha
            }
            System.out.println(" ");


            elementoPivo = matrizAuxiliarResultante[m][m];
            if(m<matrizAuxiliar.length-1){
                if(matrizAuxiliarResultante[m+1][m+1] == 0){
                    matrizAuxiliarResultante = Main.getTrocarLinhasDaMatrizNoCalculoMatrizInversa(matrizAuxiliarResultante,m+1, m+1);
                    if(matrizAuxiliarResultante == null){       // a matriz tem determinante zero, pelo que não pode ser invertida
                        matrizInversaPossivel = false;
                    }
                }
            }

            System.out.println(" "); //muda de linha
            for (int l = 0; l < matrizAuxiliarResultante.length; l++)  {
                for (int c = 0; c < matrizAuxiliarResultante[0].length; c++)     {
                    System.out.print( matrizAuxiliarResultante[l][c] + " "); //imprime caracter a caracter
                }
                System.out.println(" "); //muda de linha
            }
            System.out.println(" ");
            System.out.println("p=" + elementoPivo+ "\n" + "m=" + m);
            System.out.println(matrizAuxiliarResultante[1][0]);

            matrizAuxiliar = matrizAuxiliarResultante;
            matrizAuxiliarResultante = new double[nLinhasMatrizResultante][nColunasMatrizResultante];
            m++;
        }

       /*

        if(matrizAuxiliar[indexElementoFinal][indexElementoFinal] == 0){      //ultimo elemento da matriz principal ser zero.
            matrizAuxiliar = null;;
        }

        */
        //return matrizAuxiliar;
    }

    public static void getMatrizInversa() {
        int[][] matriz = {{2,2,4,2},{2,3,2,1,3},{2,3,1,2},{2,3,4,1}};
        for (int l = 0; l < matriz.length; l++)  {
            for (int c = 0; c < matriz[0].length; c++)     {
                System.out.print( matriz[l][c] + " "); //imprime caracter a caracter
            }
            System.out.println(" "); //muda de linha
        }
        System.out.println(" "); //muda de linha
        boolean matrizQuadradaCheck = Main.getSeMatrizQuadrada(matriz);
        double[][] matrizAuxiliar = Main.getMatrizAuxiliarMetodoMatrizInversa(matriz); //criar matriz auxiliar
        for (int l = 0; l < matrizAuxiliar.length; l++)  {
            for (int c = 0; c < matrizAuxiliar[0].length; c++)     {
                System.out.print( matrizAuxiliar[l][c] + " "); //imprime caracter a caracter
            }
            System.out.println(" "); //muda de linha
        }
        System.out.println(" "); //muda de linha

        double[][] matrizAuxiliarIntermedia = Main.getCalculoAuxiliarMatrizInversa(matrizAuxiliar);
        for (int l = 0; l < matrizAuxiliarIntermedia.length; l++)  {
            for (int c = 0; c < matrizAuxiliarIntermedia[0].length; c++)     {
                System.out.print( matrizAuxiliarIntermedia[l][c] + " "); //imprime caracter a caracter
            }
            System.out.println(" "); //muda de linha
        }
        System.out.println(" "); //muda de linha

            if (matrizAuxiliarIntermedia == null || matrizQuadradaCheck == false) {
               System.out.println("Matriz não invertivel");
            } else {
                int indexElementoPivoFinal = matrizAuxiliarIntermedia.length - 1;
                double elementoPivoFinal = matrizAuxiliarIntermedia[indexElementoPivoFinal][indexElementoPivoFinal];

                double[][] matrizAuxiliarFinal = Main.getMatrizAuxiliarFinal(matrizAuxiliarIntermedia);
                for (int l = 0; l < matrizAuxiliarFinal.length; l++)  {
                    for (int c = 0; c < matrizAuxiliarFinal[0].length; c++)     {
                        System.out.print( matrizAuxiliarFinal[l][c] + " "); //imprime caracter a caracter
                    }
                    System.out.println(" "); //muda de linha
                }
                System.out.println(" "); //muda de linha

                double[][] matrizInvertida = Main.getMultiplicarMatrizPorConstante(matrizAuxiliarFinal, (1 / elementoPivoFinal));
                for (int l = 0; l < matrizInvertida.length; l++)  {
                    for (int c = 0; c < matrizInvertida[0].length; c++)     {
                        System.out.print( matrizInvertida[l][c] + " "); //imprime caracter a caracter
                    }
                    System.out.println(" "); //muda de linha
                }
                System.out.println(" "); //muda de linha
            }
    }
}
