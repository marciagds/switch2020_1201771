import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    /**
     * Testes Exercicio13
     */
    @org.junit.jupiter.api.Test
    void getSeMatrizQuadrada_True() {
        //arrange
        int[][]matriz = {{1,3},{2,4}};
        //act
        boolean result = Main.getSeMatrizQuadrada(matriz);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizQuadrada_False() {
        //arrange
       int [][]matriz = {{1,3,2},{2,4,2}};
        //act
        boolean result = Main.getSeMatrizQuadrada(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizQuadrada_MatrizNula_True() {
        //arrange
        int [][]matriz = {{0,0},{2,4}};
        //act
        boolean result = Main.getSeMatrizQuadrada(matriz);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizQuadrada_MatrizNula_False() {
        //arrange
        int [][]matriz = {{0,0,0},{0,0,0}};
        //act
        boolean result = Main.getSeMatrizQuadrada(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizQuadrada_MatrizVazia_True() {
        //arrange
        int [][]matriz = {};
        //act
        boolean result = Main.getSeMatrizQuadrada(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizQuadrada_matrizVazia_False() {
        //arrange
        int [][]matriz = {{}};
        //act
        boolean result = Main.getSeMatrizQuadrada(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizQuadradaElementosNegativos_True() {
        //arrange
        int [][]matriz = {{-1,3},{-2,4}};
        //act
        boolean result = Main.getSeMatrizQuadrada(matriz);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizQuadradaElementosNegativos_False() {
        //arrange
        int [][]matriz = {{1,3,-2},{2,4,2}};
        //act
        boolean result = Main.getSeMatrizQuadrada(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizQuadradaMatrizNull_False() {
        //arrange
        int[][]matriz=null;
        //act
        boolean result = Main.getSeMatrizQuadrada(matriz);
        //assert
        assertFalse(result);
    }

    /**
     * Testes Exercicio14
     */
    @org.junit.jupiter.api.Test
    void getSeMatrizRetangular_True() {
        //arrange
        int [][]matriz = {{1,3,4},{2,4,5}};
        //act
        boolean result = Main.getSeMatrizRetangular(matriz);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizRetangular_False_nColunasDiferente() {
        //arrange
        int [][]matriz = {{1,3,2},{2,4}};
        //act
        boolean result = Main.getSeMatrizRetangular(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizRetangular_False_MatrizQuadrada() {
        //arrange
        int [][]matriz = {{1,3},{2,4}};
        //act
        boolean result = Main.getSeMatrizRetangular(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizRetangular_MatrizNula_True() {
        //arrange
        int [][]matriz = {{0,0,0},{2,4,6}};
        //act
        boolean result = Main.getSeMatrizRetangular(matriz);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizRetangular_MatrizNula_False() {
        //arrange
        int [][]matriz = {{0,0},{0,0}};
        //act
        boolean result = Main.getSeMatrizRetangular(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizRetangular_MatrizVazia_True() {
        //arrange
        int [][]matriz = {{}};
        //act
        boolean result = Main.getSeMatrizRetangular(matriz);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizRetangularElementosNegativos_True() {
        //arrange
        int [][]matriz = {{-1,3,-2},{-2,4,0}};
        //act
        boolean result = Main.getSeMatrizRetangular(matriz);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizRetangularElementosNegativos_False() {
        //arrange
        int [][]matriz = {{1,3},{2,0}};
        //act
        boolean result = Main.getSeMatrizRetangular(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizRetangularMatrizNull_False() {
        //arrange
        int[][]matriz=null;
        //act
        boolean result = Main.getSeMatrizRetangular(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizRetangularMatriz_MatrizVazia_True() {
        //arrange
        int [][]matriz = {};
        //act
        boolean result = Main.getSeMatrizRetangular(matriz);
        //assert
        assertFalse(result);
    }

    /**
     * Testes Exercicio15.a
     */
    @org.junit.jupiter.api.Test
    void getMenorValordaMatriz_teste1MatrizRetangular(){
        //arrange
        int[][]matriz = {{1,-3,6},{4,7,-8},{1,0,0}};
        int expected = -8;
        //act
        int result = Main.getMenorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMenorValordaMatriz_teste2MatrizRetangular(){
        //arrange
        int[][]matriz = {{1,-3,-16},{4,17,-8},{1,0,0}};
        int expected = -16;
        //act
        int result = Main.getMenorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMenorValordaMatriz_teste1MatrizQuadrada(){
        //arrange
        int[][]matriz = {{1,-3},{4,7},{0,0}};
        int expected = -3;
        //act
        int result = Main.getMenorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMenorValordaMatriz_teste2MatrizQuadrada(){
        int[][]matriz = {{1,-16},{17,-8},{1,0}};
        int expected = -16;
        //act
        int result = Main.getMenorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMenorValordaMatriz_teste1MatrizNaoUniforme(){       //matriz com diferentes colunas
        //arrange
        int[][]matriz = {{1,-3},{4,7,-5},{0,0,0,-6}};
        int expected = -6;
        //act
        int result = Main.getMenorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMenorValordaMatriz_teste2MatrizNaoUniforme(){
        int[][]matriz = {{1,-16,0,-4},{17,-8,0},{1,0}};
        int expected = -16;
        //act
        int result = Main.getMenorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMenorValordaMatriz_testeMatrizNula(){
        //arrange
        int[][]matriz = {{0},{0},{0,0,0}};
        int expected = 0;
        //act
        int result = Main.getMenorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMenorValordaMatriz_testeMatrizVazia(){
        //arrange
        int[][]matriz = {};
        Integer expected = null;
        //act
        Integer result = Main.getMenorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMenorValordaMatriz_testeMatrizNull(){
        //arrange
        int[][]matriz = null;
        Integer expected = null;
        //act
        Integer result = Main.getMenorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }

    /**
     * Testes Exercicio15.b
     */
    @org.junit.jupiter.api.Test
    void getMaiorValordaMatriz_teste1MatrizRetangular(){
        //arrange
        int[][]matriz = {{1,-3,6},{4,7,-8},{1,0,0}};
        int expected = 7;
        //act
        int result = Main.getMaiorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void  getMaiorValordaMatriz_teste2MatrizRetangular(){
        //arrange
        int[][]matriz = {{1,-3,-16},{4,17,-8},{1,0,0}};
        int expected = 17;
        //act
        int result = Main.getMaiorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMaiorValordaMatriz_teste1MatrizQuadrada(){
        //arrange
        int[][]matriz = {{1,-3},{4,7},{0,0}};
        int expected = 7;
        //act
        int result = Main.getMaiorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMaiorValordaMatriz_teste2MatrizQuadrada(){
        int[][]matriz = {{1,-16},{17,-8},{1,0}};
        int expected = 17;
        //act
        int result = Main.getMaiorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMaiorValordaMatriz_teste1MatrizNaoUniforme(){       //matriz com diferentes colunas
        //arrange
        int[][]matriz = {{1,-3},{4,7,-5},{0,0,0,-6}};
        int expected = 7;
        //act
        int result = Main.getMaiorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMaiorValordaMatriz_teste2MatrizNaoUniforme(){
        int[][]matriz = {{1,-16,0,-4},{17,-8,0},{1,0}};
        int expected = 17;
        //act
        int result = Main.getMaiorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMaiorValordaMatriz_testeMatrizNula(){
        //arrange
        int[][]matriz = {{0},{0},{0,0,0}};
        int expected = 0;
        //act
        int result = Main.getMaiorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMaiorValordaMatriz_testeMatrizVazia(){
        //arrange
        int[][]matriz = {};
        Integer expected = null;
        //act
        Integer result = Main.getMaiorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMaiorValordaMatriz_testeMatrizNull(){
        //arrange
        int[][]matriz = null;
        Integer expected = null;
        //act
        Integer result = Main.getMaiorValordaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }

    /**
     * Testes Exercicio15.c
     */
    @org.junit.jupiter.api.Test
    void getMediaElementosDaMatriz_teste1MatrizRetangular(){
        //arrange
        int[][]matriz = {{1,-3,6},{4,7,-8},{1,0,0}};
        double expected = 0.88;
        //act
        double result = Main.getMediaElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void  getMediaElementosDaMatriz_teste2MatrizRetangular(){
        //arrange
        int[][]matriz = {{1,-3,-16},{4,2,-8},{1,0,0}};
        double expected = -2.11;
        //act
        double result = Main.getMediaElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getMediaElementosDaMatriz_teste1MatrizQuadrada(){
        //arrange
        int[][]matriz = {{1,-3},{4,7},{0,0}};
        double expected = 1.5;
        //act
        double result = Main.getMediaElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void getMediaElementosDaMatriz_teste2MatrizQuadrada(){
        int[][]matriz = {{1,-16},{17,-8},{1,0}};
        double expected = -0.83;
        //act
        double result = Main.getMediaElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void getMediaElementosDaMatriz_teste1MatrizNaoUniforme(){       //matriz com diferentes colunas
        //arrange
        int[][]matriz = {{1,-3},{4,7,-5},{0,0,0,-6}};
        double expected = -0.22;
        //act
        double result = Main.getMediaElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void getMediaElementosDaMatriz_teste2MatrizNaoUniforme(){
        int[][]matriz = {{12,16,10,12},{17,19,20},{10,20}};
        double expected = 15.11;
        //act
        double result = Main.getMediaElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void getMediaElementosDaMatriz_testeMatrizNula(){
        //arrange
        int[][]matriz = {{0},{0},{0,0,0}};
        double expected = 0;
        //act
       double result = Main.getMediaElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void getMediaElementosDaMatriz_testeMatrizVazia(){
        //arrange
        int[][]matriz = {};
        Double expected = null;
        //act
        Double result = Main.getMediaElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMediaElementosDaMatriz_testeMatrizNull(){
        //arrange
        int[][]matriz = null;
        Double expected = null;
        //act
        Double result = Main.getMediaElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }

    /**
     * Testes Exercicio15.d
     */
    @org.junit.jupiter.api.Test
    void getProdutoElementosDaMatriz_teste1MatrizRetangular(){
        //arrange
        int[][]matriz = {{1,-3,6},{4,7,-8},{1,1,1}};
        double expected = 4032;
        //act
        double result = Main.getProdutoElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void  getProdutoElementosDaMatriz_teste2MatrizRetangular(){
        //arrange
        int[][]matriz = {{1,-3,-2},{4,2,-8},{1,2,0}};
        double expected = 0;
        //act
        double result = Main.getProdutoElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getProdutoElementosDaMatriz_teste1MatrizQuadrada(){
        //arrange
        int[][]matriz = {{1,-3},{4,7},{3,4}};
        double expected = -1008;
        //act
        double result = Main.getProdutoElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getProdutoElementosDaMatriz_teste2MatrizQuadrada(){
        int[][]matriz = {{1,-16},{17,-8},{1,0}};
        double expected = 0;
        //act
        double result = Main.getProdutoElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void getProdutoElementosDaMatriz_teste1MatrizNaoUniforme(){       //matriz com diferentes colunas
        //arrange
        int[][]matriz = {{1,-3},{4,7,5},{1,1,1,-6}};
        double expected = 2520;
        //act
        double result = Main.getProdutoElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void getProdutoElementosDaMatriz_teste2MatrizNaoUniforme(){
        int[][]matriz = {{12,16,0,12},{17,19,20},{10,20}};
        double expected =0 ;
        //act
        double result = Main.getProdutoElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void getProdutoElementosDaMatriz_testeMatrizNula(){
        //arrange
        int[][]matriz = {{0},{0},{0,0,0}};
        double expected = 0;
        //act
        double result = Main.getProdutoElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result,0.01);
    }
    @org.junit.jupiter.api.Test
    void getProdutoElementosDaMatriz_testeMatriVazia(){
        //arrange
        int[][]matriz = {};
        Double expected = null;
        //act
        Double result = Main.getProdutoElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getProdutoElementosDaMatriz_testeMatrizNull(){
        //arrange
        int[][]matriz = null;
        Double expected = null;
        //act
        Double result = Main.getProdutoElementosDaMatriz(matriz);
        //assert
        assertEquals(expected, result);
    }

    /**
     * Testes Exercicio15.e
     */
    @org.junit.jupiter.api.Test
    void getSeExisteNoVetor_testeTrue(){
        //arrange
        int[]vetor={1,2,4,5};
        int number= 4;
        //act
        boolean result = Main.getSeExisteNoVetor(vetor, number);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeExisteNoVetor_teste_False(){
        //arrange
        int[]vetor={1,2,4,5};
        int number= 3;
        //act
        boolean result = Main.getSeExisteNoVetor(vetor, number);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeExisteNoVetor_teste_VetorNull(){
        //arrange
        int[]vetor=null;
        int number= 3;
        //act
        boolean result = Main.getSeExisteNoVetor(vetor, number);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeExisteNoVetor_teste_VetorVazio(){
        //arrange
        int[]vetor={};
        int number= 3;
        //act
        boolean result = Main.getSeExisteNoVetor(vetor, number);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeExisteNoVetor_teste_VetorNulo(){
        //arrange
        int[]vetor={0,0};
        int number= 3;
        //act
        boolean result = Main.getSeExisteNoVetor(vetor, number);
        //assert
        assertFalse(result);
    }

    @org.junit.jupiter.api.Test
    void getAdicionarNoVetor_teste(){
        //arrange
        int[]vetor = {1,4,5};
        int number = 7;
        int [] expected = {1,4,5,7};
        //act
        int [] result = Main.getAdicionarNoVetor(vetor,number);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getAdicionarNoVetor_vetorNull(){
        //arrange
        int[]vetor = null;
        int number = 7;
        int [] expected = {7};
        //act
        int [] result = Main.getAdicionarNoVetor(vetor,number);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getAdicionarNoVetor_vetorVazio(){
        //arrange
        int[]vetor = {};
        int number = 7;
        int [] expected = {7};
        //act
        int [] result = Main.getAdicionarNoVetor(vetor,number);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getAdicionarNoVetor_vetorNulo(){
        //arrange
        int[]vetor = {0};
        int number = 7;
        int [] expected = {0,7};
        //act
        int [] result = Main.getAdicionarNoVetor(vetor,number);
        //assert
        assertArrayEquals(expected,result);
    }

    @org.junit.jupiter.api.Test
    void getConjuntoElementosNaoRepetidosDaMatriz_teste1(){
        //arrange
        int[][]matriz = {{1,2,3},{1,6,4},{2,5,4},{-1,3,-5}};
        int []expected = {1,2,3,6,4,5,-1,-5};
        //act
        int[]result = Main.getConjuntoElementosNaoRepetidosDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getConjuntoElementosNaoRepetidosDaMatriz_teste2(){
        //arrange
        int[][]matriz = {{1,2,3},{1,6,0},{2,5,4}};
        int []expected = {1,2,3,6,0,5,4};
        //act
        int[]result = Main.getConjuntoElementosNaoRepetidosDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getConjuntoElementosNaoRepetidosDaMatriz_teste3(){
        //arrange
        int[][]matriz = {{1,2,3},{1,2,3},{1,2,3}};
        int []expected = {1,2,3};
        //act
        int[]result = Main.getConjuntoElementosNaoRepetidosDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getConjuntoElementosNaoRepetidosDaMatriz_testeMatrizNull(){
        //arrange
        int[][]matriz = null;
        int []expected = null;
        //act
        int[]result = Main.getConjuntoElementosNaoRepetidosDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getConjuntoElementosNaoRepetidosDaMatriz_testeMatrizVazia(){
        //arrange
        int[][]matriz = {};
        int []expected = {};
        //act
        int[]result = Main.getConjuntoElementosNaoRepetidosDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }

    /**
     * Testes Exercicio15.f
     */
    @org.junit.jupiter.api.Test
    void getCheckIfNumberIsPrime_FalseNumber1(){
        //arrange
        int number = 4;
        //act
        boolean result = Main.getCheckIfNumberIsPrime(number);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getCheckIfNumberIsPrime_TrueNumber2(){
        //arrange
        int number = 2;
        //act
        boolean result = Main.getCheckIfNumberIsPrime(number);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getCheckIfNumberIsPrime_TrueNumber3(){
        //arrange
        int number = 3;
        //act
        boolean result = Main.getCheckIfNumberIsPrime(number);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getCheckIfNumberIsPrime_TrueNumber11(){
        //arrange
        int number = 11;
        //act
        boolean result = Main.getCheckIfNumberIsPrime(number);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getCheckIfNumberIsPrime_FalseNumber0(){
        //arrange
        int number = 4;
        //act
        boolean result = Main.getCheckIfNumberIsPrime(number);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getCheckIfNumberIsPrime_FalseNumberNeg2(){
        //arrange
        int number = -2;
        //act
        boolean result = Main.getCheckIfNumberIsPrime(number);
        //assert
        assertFalse(result);
    }

    @org.junit.jupiter.api.Test
    void getElementosPrimosdaMatriz_testeMatrizQuadrada(){
        //arrange
        int[][]matriz={{1,2,3},{4,7,5},{3,11,6}};
        int[]expected={2,3,7,5,3,11};
        //act
        int[]result=Main.getElementosPrimosdaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosPrimosdaMatriz_testeMatrizRetangular1(){
        //arrange
        int[][]matriz={{1,3,4},{3,7,5},{3,5,6},{2,5,8}};
        int []expected={3,3,7,5,3,5,2,5};
        //act
        int[]result=Main.getElementosPrimosdaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosPrimosdaMatriz_testeMatrizRetangular2(){
        //arrange
        int[][]matriz={{1,3,4,6},{3,7,5,7},{3,5,8,6}};
        int []expected={3,3,7,5,7,3,5};
        //act
        int[]result=Main.getElementosPrimosdaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosPrimosdaMatriz_testeMatrizNaoUniforme(){
        //arrange
        int[][]matriz={{1,3,4,6},{3,7,5,7},{3,5,8,6,7}};
        int []expected= {3,3,7,5,7,3,5,7};
        //act
        int[]result=Main.getElementosPrimosdaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosPrimosdaMatriz_testeMatrizNull(){
        //arrange
        int[][]matriz=null;
        int[]expected=null;
        //act
        int[]result=Main.getElementosPrimosdaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosPrimosdaMatriz_testeMatrizVazia(){
        //arrange
        int[][]matriz= {};
        int[]expected={};
        //act
        int[]result=Main.getElementosPrimosdaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosPrimosdaMatriz_testeMatrizNula(){
        //arrange
        int[][]matriz={{0},{0,0}};
        int[]expected= {};
        //act
        int[]result=Main.getElementosPrimosdaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }

    /**
     * Testes Exercicio15.g
     */
    @org.junit.jupiter.api.Test
    void getElementosDaDiagonalDaMatriz_testeMatrizQuadrada(){
        //arrange
        int[][]matriz={{1,3,4},{3,7,5},{3,5,6}};
        int []expected={1,7,6};
        //act
        int[]result=Main.getElementosDaDiagonalPrincipalDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosDaDiagonalDaMatriz_testeMatrizRetangular1(){
        //arrange
        int[][]matriz={{1,3,4},{3,7,5},{3,5,6},{2,5,8}};
        int []expected={1,7,6};
        //act
        int[]result=Main.getElementosDaDiagonalPrincipalDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosDaDiagonalDaMatriz_testeMatrizRetangular2(){
        //arrange
        int[][]matriz={{1,3,4,6},{3,7,5,7},{3,5,8,6}};
        int []expected={1,7,8};
        //act
        int[]result=Main.getElementosDaDiagonalPrincipalDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosDaDiagonalDaMatriz_testeMatrizNaoUniforme(){
        //arrange
        int[][]matriz={{1,3,4,6},{3,7,5,7},{3,5,8,6,7}};
        int []expected=null;
        //act
        int[]result=Main.getElementosDaDiagonalPrincipalDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosDaDiagonalDaMatriz_testeMatrizNula(){
        //arrange
        int[][]matriz=null;
        int[]expected=null;
        //act
        int[]result=Main.getElementosDaDiagonalPrincipalDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }

    /**
     * Testes Exercicio15.h
     */
    @org.junit.jupiter.api.Test
    void getElementosDaDiagonalSecundariaDaMatriz_testeMatrizQuadrada(){
        //arrange
        int[][]matriz={{1,3,4},{3,7,5},{3,5,6}};
        int []expected={4,7,3};
        //act
        int[]result=Main.getElementosDaDiagonalSecundariaDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosDaDiagonalSecundariaDaMatriz_testeMatrizRetangular1(){
        //arrange
        int[][]matriz={{1,3,4},{3,7,5},{3,5,2},{2,5,8}};
        int []expected={4,7,3};
        //act
        int[]result=Main.getElementosDaDiagonalSecundariaDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosDaDiagonalSecundariaDaMatriz_testeMatrizRetangular2(){
        //arrange
        int[][]matriz={{1,3,4,6},{3,7,5,7},{3,5,8,6}};
        int []expected={6,5,5};
        //act
        int[]result=Main.getElementosDaDiagonalSecundariaDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosDaDiagonalSecundariaDaMatriz_testeMatrizNaoUniforme(){
        //arrange
        int[][]matriz={{1,3,4,6},{3,7,5,7},{3,5,8,6,7}};
        int []expected=null;
        //act
        int[]result=Main.getElementosDaDiagonalSecundariaDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getElementosDaDiagonalSecundariaDaMatriz_testeMatrizNula(){
        //arrange
        int[][]matriz=null;
        int[]expected=null;
        //act
        int[]result=Main.getElementosDaDiagonalSecundariaDaMatriz(matriz);
        //assert
        assertArrayEquals(expected,result);
    }

    /**
     * Testes Exercicio15.i
     */
    @org.junit.jupiter.api.Test
    void getVerificarMatrizIdentidade_testeTrue(){
        //arrange
        int[][]matriz={{1,0,0},{0,1,0},{0,0,1}};
        //act
        boolean result = Main.getVerificarMatrizIdentidade(matriz);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getVerificarMatrizIdentidade_testeFalseMatrizQuadrada(){
        //arrange
        int[][]matriz={{1,1,0},{0,1,0},{0,0,1}};
        //act
        boolean result = Main.getVerificarMatrizIdentidade(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getVerificarMatrizIdentidade_testeFalseMatrizRetangular1(){
        //arrange
        int[][]matriz={{1,1,0},{0,1,0},{0,0,1},{0,1,0}};
        //act
        boolean result = Main.getVerificarMatrizIdentidade(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getVerificarMatrizIdentidade_testeFalseMatrizRetangular2(){
        //arrange
        int[][]matriz={{1,1,0,1},{0,1,0,1},{0,0,1,0}};
        //act
        boolean result = Main.getVerificarMatrizIdentidade(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getVerificarMatrizIdentidade_testeFalseMatrizNaoUniforme(){
        //arrange
        int[][]matriz={{1,1,0,1},{0,1,0,1,1},{0,0,1,0}};
        //act
        boolean result = Main.getVerificarMatrizIdentidade(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getVerificarMatrizIdentidade_testeFalseMatrizNull(){
        //arrange
        int[][]matriz= null;
        //act
        boolean result = Main.getVerificarMatrizIdentidade(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getVerificarMatrizIdentidade_testeTrueMatrizQuadrada2(){
        //arrange
        int[][]matriz= {{1}};
        //act
        boolean result = Main.getVerificarMatrizIdentidade(matriz);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getVerificarMatrizIdentidade_testeFalseMatrizVazia(){
        //arrange
        int[][]matriz={};
        //act
        boolean result = Main.getVerificarMatrizIdentidade(matriz);
        //assert
        assertFalse(result);
    }

    /**
     * Testes Exercicio15.j
     */
    @org.junit.jupiter.api.Test
    void getMatrizAuxiliarMetodoMatrizInversa_teste1_Matriz3x3(){
        //arrange
        int[][] matriz = {{1,2,3},{1,3,5},{2,3,3}};
        double[][]expected = {{1,2,3,1,0,0},{1,3,5,0,1,0},{2,3,3,0,0,1}};
        //act
        double[][]result = Main.getMatrizAuxiliarMetodoMatrizInversa(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizAuxiliarMetodoMatrizInversa_teste2_Matriz3x3(){
        //arrange
        int[][] matriz = {{2,2,3},{-1,3,5},{2,5,3}};
        double[][]expected = {{2,2,3,1,0,0},{-1,3,5,0,1,0},{2,5,3,0,0,1}};
        //act
        double[][]result = Main.getMatrizAuxiliarMetodoMatrizInversa(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizAuxiliarMetodoMatrizInversa_teste_Matriz2x2(){
        //arrange
        int[][] matriz = {{2,2},{3,5}};
        double[][]expected = {{2,2,1,0},{3,5,0,1}};
        //act
        double[][]result = Main.getMatrizAuxiliarMetodoMatrizInversa(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizAuxiliarMetodoMatrizInversa_teste_MatrizNull(){
        //arrange
        int[][] matriz = null;
        double[][]expected =null;
        //act
        double[][]result = Main.getMatrizAuxiliarMetodoMatrizInversa(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizAuxiliarMetodoMatrizInversa_teste_MatrizVazia(){
        //arrange
        int[][] matriz = {};
        double[][]expected =null;
        //act
        double[][]result = Main.getMatrizAuxiliarMetodoMatrizInversa(matriz);
        //assert
        assertArrayEquals(expected,result);
    }

    @org.junit.jupiter.api.Test
    void getGuardarLinhadaMatriz_testeGuardarlinha1Matriz3x3(){
        //arrange
        double [][]matriz = {{1,2,3},{1,3,5},{2,3,3}};
        int linha = 1;
        double[]expected = {1,3,5};
        //act
        double[]result = Main.getGuardarLinhadaMatriz(matriz,linha);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getGuardarLinhadaMatriz_testeGuardarlinha1Matriz3x6(){
        //arrange
        double [][]matriz = {{1,2,3,4,5,6},{0,0,2,6,2,2},{0,1,4,5,3,4},{0,1,3,4,2,3}};
        int linha = 1;
        double[]expected = {0,0,2,6,2,2};
        //act
        double[]result = Main.getGuardarLinhadaMatriz(matriz,linha);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getGuardarLinhadaMatriz_testelinhaNegativa(){
        //arrange
        double [][]matriz = {{1,2,3},{1,3,5},{2,3,3}};
        int linha = -2;
        double[]expected =null;
        //act
        double[]result = Main.getGuardarLinhadaMatriz(matriz,linha);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getGuardarLinhadaMatriz_testeNull(){
        //arrange
        double [][]matriz = null;
        int linha = 1;
        double[]expected = null;
        //act
        double[]result = Main.getGuardarLinhadaMatriz(matriz,linha);
        //assert
        assertArrayEquals(expected,result);
    }

    @org.junit.jupiter.api.Test
    void getTrocarLinhasDaMatrizNoCalculoMatrizInversa_testepossivelMatriz3x3(){
        //arrange
        double[][]matriz={{1,2,3},{0,0,2},{0,1,4}};
        int linha = 1;
        int coluna = 1;
        double [][]expected={{1,2,3},{0,1,4},{0,0,2}};
        //act
        double[][]result = Main.getTrocarLinhasDaMatrizNoCalculoMatrizInversa(matriz,linha,coluna);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getTrocarLinhasDaMatrizNoCalculoMatrizInversa_testeimpossivel(){
        //arrange
        double[][]matriz={{1,2,3},{0,0,2},{0,0,4}};
        int linha = 1;
        int coluna = 1;
        double [][]expected=null;
        //act
        double[][]result = Main.getTrocarLinhasDaMatrizNoCalculoMatrizInversa(matriz,linha,coluna);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getTrocarLinhasDaMatrizNoCalculoMatrizInversa_testepossivelMatriz4x4(){
        //arrange
        double[][]matriz={{1,2,3,4},{0,0,2,6},{0,0,4,5},{0,1,3,4}};
        int linha = 1;
        int coluna = 1;
        double [][]expected={{1,2,3,4},{0,1,3,4},{0,0,4,5},{0,0,2,6}};
        //act
        double[][]result = Main.getTrocarLinhasDaMatrizNoCalculoMatrizInversa(matriz,linha,coluna);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getTrocarLinhasDaMatrizNoCalculoMatrizInversa_teste2possivelMatriz4x4(){
        //arrange
        double[][]matriz={{1,2,3,4},{0,0,2,6},{0,1,4,5},{0,1,3,4}};
        int linha = 1;
        int coluna = 1;
        double [][]expected={{1,2,3,4},{0,1,4,5},{0,0,2,6},{0,1,3,4}};
        //act
        double[][]result = Main.getTrocarLinhasDaMatrizNoCalculoMatrizInversa(matriz,linha,coluna);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getTrocarLinhasDaMatrizNoCalculoMatrizInversa_teste2possivelMatriz3x6(){
        //arrange
        double[][]matriz={{1,2,3,4,5,6},{0,0,2,6,2,2},{0,1,4,5,3,4},{0,1,3,4,2,3}};
        int linha = 1;
        int coluna = 1;
        double [][]expected={{1,2,3,4,5,6},{0,1,4,5,3,4},{0,0,2,6,2,2},{0,1,3,4,2,3}};
        //act
        double[][]result = Main.getTrocarLinhasDaMatrizNoCalculoMatrizInversa(matriz,linha,coluna);
        //assert
        assertArrayEquals(expected,result);
    }

    @org.junit.jupiter.api.Test
    void getCalculoAuxiliarMatrizInversa_possivelTrocarLinhas3x3(){
        //arrange
        double[][]matrizAuxiliar = {{2,1,3,1,0,0},{2,1,2,0,1,0},{3,2,3,0,0,1}};
        double[][]expected = {{-1,0,0,1,-3,1},{0,-1,0,0,3,-2},{0,0,-1,-1,1,0}};
        //act
        double[][]result = Main.getCalculoAuxiliarMatrizInversa(matrizAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getCalculoAuxiliarMatrizInversa_possivelSemTrocarLinhas3x3(){
        //arrange
        double[][]matrizAuxiliar = {{1,2,3,1,0,0},{1,3,5,0,1,0},{2,3,3,0,0,1}};
        double[][]expected = {{-1,0,0,-6,3,1},{0,-1,0,7,-3,-2},{0,0,-1,-3,1,1}};
        //act
        double[][]result = Main.getCalculoAuxiliarMatrizInversa(matrizAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getCalculoAuxiliarMatrizInversa_possivelTrocarLinhas4x4(){
        //arrange
        double[][]matrizAuxiliar = {{2,3,1,3,1,0,0,0},{2,3,4,1,0,1,0,0},{3,4,2,4,0,0,1,0},{2,3,4,2,0,0,0,1}};
        double[][]expected = {{-3,0,0,0,10,-2,-9,4},{0,-3,0,0,-8,-2,6,1},{0,0,-3,0,1,1,0,-2},{0,0,0,-3,0,3,0,-3}};
        //act
        double[][]result = Main.getCalculoAuxiliarMatrizInversa(matrizAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getCalculoAuxiliarMatrizInversa_matrizNaoInvertivel(){
        //arrange
        double[][]matrizAuxiliar = {{2,1,3,1,0,0},{2,1,3,0,1,0},{2,3,3,0,0,1}};
        double[][]expected = null;
        //act
        double[][]result = Main.getCalculoAuxiliarMatrizInversa(matrizAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }

    @org.junit.jupiter.api.Test
    void getMatrizAuxiliarFinal_testeMatriz3x6(){
        //arrange
        double [][] matrizAuxiliar = {{-1,0,0,1,-3,1},{0,-1,0,0,3,-2},{0,0,-1,-1,1,0}};
        double [][] expected = {{1,-3,1},{0,3,-2},{-1,1,0}};
        //act
        double [][] result =Main.getMatrizAuxiliarFinal(matrizAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizAuxiliarFinal_testeMatriz4x8(){
        //arrange
        double [][] matrizAuxiliar = {{-1,0,0,0,1,-3,1,2},{0,-1,0,0,0,3,-2,1},{0,0,-1,0,-1,1,0,2},{0,0,0,-1,1,1,1,1}};
        double [][] expected = {{1,-3,1,2},{0,3,-2,1},{-1,1,0,2},{1,1,1,1}};
        //act
        double [][] result =Main.getMatrizAuxiliarFinal(matrizAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizAuxiliarFinal_testeMatrizNull(){
        //arrange
        double [][] matrizAuxiliar = null;
        double [][] expected = null;
        //act
        double [][] result = Main.getMatrizAuxiliarFinal(matrizAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }

    @org.junit.jupiter.api.Test
    void getMultiplicarMatrizPorConstante_MatrizQuadrada(){
        //arrange
        double [][] matriz = {{1,2,3},{2,4,3},{1,3,4}};
        double number = 2;
        double[][] expected = {{2,4,6},{4,8,6},{2,6,8}};
        //act
        double [][]result = Main.getMultiplicarMatrizPorConstante(matriz,number);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMultiplicarMatrizPorConstante_MatrizRetangular(){
        //arrange
        double [][] matriz = {{1,2,3},{2,4,3}};
        double number = 2;
        double[][] expected = {{2,4,6},{4,8,6}};
        //act
        double [][]result = Main.getMultiplicarMatrizPorConstante(matriz,number);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMultiplicarMatrizPorConstante_MatrizNaoUniforme(){
        //arrange
        double [][] matriz = {{1,2,3},{2,4,3,2}};
        double number = 2;
        double[][] expected = {{2,4,6},{4,8,6,4}};
        //act
        double [][]result = Main.getMultiplicarMatrizPorConstante(matriz,number);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMultiplicarMatrizPorConstante_MatrizVazia(){
        //arrange
        double [][] matriz = {};
        double number = 2;
        double[][] expected = {};
        //act
        double [][]result = Main.getMultiplicarMatrizPorConstante(matriz,number);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMultiplicarMatrizPorConstante_MatrizNull(){
        //arrange
        double [][] matriz = null;
        double number = 2;
        double[][] expected = null;
        //act
        double [][]result = Main.getMultiplicarMatrizPorConstante(matriz,number);
        //assert
        assertArrayEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getMatrizInversa_testepossivelMatriz3x3(){
        //arrange
        int [][] matriz = {{2,1,3},{2,2,3},{2,1,2}};
        double [][] expected = {{-0.5,-0.5,1.5},{-1,1,0},{1,0,-1}};
        //act
        double [][] result = Main.getMatrizInversa(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizInversa_testepossivelMatriz4x4(){
        //arrange
        int [][] matriz = {{2,2,4,2},{2,3,2,1},{2,3,1,2},{2,3,4,1}};
        double [][] expected = {{1.5,4,-2,-3},{-1,-1.5,1,1.5},{0,-0.5,0,0.5},{0,-1.5,1,0.5}};
        //act
        double [][] result = Main.getMatrizInversa(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizInversa_testeMatrizNaoInvertivel(){
        //arrange
        int [][] matriz = {{2,2,2},{2,2,2},{2,3,1}};
        double [][] expected = null;
        //act
        double [][] result = Main.getMatrizInversa(matriz);
        //assert
        assertArrayEquals(expected,result);
    }

    /**
     * Testes Exercicio15.k
     */
    @org.junit.jupiter.api.Test
    void getMatrizTransposta_matrizQuadrada(){
        //arrange
        int[][]matriz = {{1,2,3},{4,5,6},{7,8,9}};
        int[][]expected = {{1,4,7},{2,5,8},{3,6,9}};
        //act
        int[][] result = Main.getMatrizTransposta(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizTransposta_MatrizQuadrangular(){
        //arrange
        int[][]matriz = {{1,2,3},{4,5,6}};
        int[][]expected = {{1,4},{2,5},{3,6}};
        //act
        int[][] result = Main.getMatrizTransposta(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizTransposta_MatrizNaoUniforme(){
        //arrange
        int[][]matriz = {{1,2,3,2},{4,5,6}};
        int[][]expected = null;
        //act
        int[][] result = Main.getMatrizTransposta(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizTransposta_MatrizNull(){
        //arrange
        int[][]matriz =null;
        int[][]expected = null;
        //act
        int[][] result = Main.getMatrizTransposta(matriz);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getMatrizTransposta_MatrizVazia(){
        //arrange
        int[][]matriz = {{}};
        int[][]expected = {};
        //act
        int[][] result = Main.getMatrizTransposta(matriz);
        //assert
        assertArrayEquals(expected,result);
    }

}