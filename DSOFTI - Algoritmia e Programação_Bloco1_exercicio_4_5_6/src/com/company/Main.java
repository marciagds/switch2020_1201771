package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //exercicio4();
        //exercicio5();
        exercicio6();
    }
    public static void exercicio4(){
        //Qual a distância a que se encontra uma trovoada?

        // Variáveis envolvidas
        int velocidadeluz, velocidadesom;
        double intervalotempo, distancia;

        // Iniciaização da leitura dos dados requeridos
        Scanner ler = new Scanner(System.in);

        // Leitura dos dados
        System.out.println("Qual o intervalo de tempo entre o relâmpago e o trovão, em segundos?");
        intervalotempo = ler.nextDouble();

        // Processamento
        velocidadesom = 1224;   // km/h
        distancia = velocidadesom * (intervalotempo/3600);

        // Saída dos dados
        System.out.println("A trovoada encontra-se a " + String.format("%.2f", distancia)+" km de distância");

    }

        public static void exercicio5(){
        // Calcular a altura de um edifício usando a gravidade

        // Variáveis envolvidas
        double alturapredio, gravidade, tempo, v_inicial;

        // Inicialização da leitura dos dados requeridos
        Scanner ler = new Scanner(System.in);

        // Leitura dos dados
        System.out.println("Tempo que demorou a chegar ao solo, em segundos");
        tempo = ler.nextDouble();

        // Processamento
        v_inicial= 0;
        gravidade = 9.8;
        alturapredio = (v_inicial * tempo)+((gravidade * Math.pow(tempo,2))/2);

        // Saída dos dados
        System.out.println("O prédio tem "+String.format("%.2f",alturapredio)+" metros de altura");

        }

        public static void exercicio6(){
        // Calcular a altura de um edifício usando o teorema de Tales

       // Variáveis envolvidas
       double sombrapessoa, sombraedificio, alturapessoa, alturaedificio;

       // Inicialização da leitura dos dados requeridos
       Scanner ler = new Scanner(System.in);

       // Leitura dos dados
       System.out.println("Altura da pessoa, em metros");
       alturapessoa = ler.nextDouble();
       System.out.println("Comprimento da sombra da pessoa, em metros");
       sombrapessoa=ler.nextDouble();
       System.out.println("Comprimento da sombra do edificio, em metros");
       sombraedificio = ler.nextDouble();

       // Processamento
       alturaedificio = (sombraedificio * alturapessoa) / sombrapessoa;

        // Saída dos dados
       System.out.println("O edificio mede " + String.format("%.2f", alturaedificio) + " metros");

    }

}
