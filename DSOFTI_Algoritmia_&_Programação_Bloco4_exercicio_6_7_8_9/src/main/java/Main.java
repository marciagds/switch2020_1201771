import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // write your code here
        exercicio6();
        //exercicio7();
        //exercicio8();
        //exercicio9();
    }

    public static void exercicio6() {
/*Construa uma solução em Java que dado um vetor de números inteiros, retorne um outro vetor com
apenas os primeiros N elementos do vetor recebido*/

        //Variáveis envolvidas
        int tamanho, n_retornar;
        int[] vetor_entrada, vetor_saida;
        int[] vetorPares, vetorImpares;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura do array
        System.out.println("Qual o tamanho do vetor?");
        tamanho = ler.nextInt();
        vetor_entrada = new int[tamanho];
        System.out.println("Inserir valor dos elementos: ");
        for (int indice = 0; indice < tamanho; indice++) {
            vetor_entrada[indice] = ler.nextInt();
        }
        System.out.println("Qual a quantidade de elementos a retornar:");
        n_retornar = ler.nextInt();

        //Processamento
        vetor_saida = getVetorComOsPrimeirosN_Elementos(vetor_entrada, n_retornar);

        //Saída dos elementos
        System.out.println(Arrays.toString(vetor_saida));   //Imprime valores do array
    }

    public static int[] getVetorComOsPrimeirosN_Elementos(int[] vetor_entrada, int n_retornar) {
        int[] vetor_saida;
        vetor_saida = new int[n_retornar];
        if (vetor_entrada.length >= n_retornar) {
            for (int i = 0; i < vetor_saida.length; i++) {
                vetor_saida[i] = vetor_entrada[i];
            }
        } else {
            for (int i = 0; i < vetor_saida.length; i++) {      //O numero de valores a retornar não pode ser superior ao tamanho do array prinicipal
                vetor_saida[i] = -1;
            }
        }
        return vetor_saida;
    }

    public static void exercicio7() {
        //Variáveis envolvidas
        int lim_min, lim_max, divisor;
        int[] vetorMultiplos;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura do array
        System.out.println("Qual o valor minimo do intervalo: ");
        lim_min = ler.nextInt();
        System.out.println("Qual o valor maximo do intervalo: ");
        lim_max = ler.nextInt();
        System.out.println("Qual o valor do divisor: ");
        divisor = ler.nextInt();

        //Processamento
        vetorMultiplos = getVetordosMultiplosnumDadoIntervalo(lim_min, lim_max, divisor);

        //Saída dos resultados
        System.out.println("Vetor com os elementos multiplos de " + divisor + " :" + Arrays.toString(vetorMultiplos));   //Imprime valores do array
    }

    public static int[] getVetordosMultiplosnumDadoIntervalo(int lim_min, int lim_max, int divisor) {
        int[] vetorMultiplos;
        int contaMultiplos;
        double resto;
        int indiceVetor = 0;

        contaMultiplos = getNumeroMultiplosdeUmNumero(lim_min, lim_max, divisor);
        vetorMultiplos = new int[contaMultiplos];

        for (int i = lim_min; i <= lim_max; i++) {
            resto = getRestodaDivisão(i, divisor);
            if (resto == 0) {
                vetorMultiplos[indiceVetor] = i;
                indiceVetor++;
            } else {
            }
        }
        return vetorMultiplos;
    }

    public static int getNumeroMultiplosdeUmNumero(int lim_min, int lim_max, int numero) {   //Código reutiliizável.
        double resto;
        int contaMultiplo = 0;
        if (lim_min >= 0 && numero > 0) {
            for (int i = lim_min; i <= lim_max; i++) {
                resto = getRestodaDivisão(i, numero);
                if (resto == 0) {
                    contaMultiplo += 1;
                } else {
                }
            }
        } else {
            contaMultiplo = -1;
        }
        return contaMultiplo;
    }

    public static double getRestodaDivisão(int numero, int divisor) {
        double resto;
        resto = numero % divisor;
        return resto;
    }

    public static void exercicio8() {
        //Variáveis envolvidas
        int lim_min, lim_max, tamanho;
        int []divisores;
        int []vetorMultiplosComuns;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual o valor minimo do intervalo: ");
        lim_min = ler.nextInt();
        System.out.println("Qual o valor maximo do intervalo: ");
        lim_max = ler.nextInt();
        System.out.println("Quantos divisores?");
        tamanho = ler.nextInt();
        divisores = new int[tamanho];
        System.out.println("Inserir valor dos divisores: ");
        for (int indice = 0; indice < tamanho; indice++) {
            divisores[indice] = ler.nextInt();
        }

        //Processamento
        vetorMultiplosComuns = getVetorMultiplosComunsdeVariosDivisoresNumDadoIntervalo(lim_min,lim_max,divisores);

        //Saída dos resultados
        System.out.println(Arrays.toString(vetorMultiplosComuns));
    }

    public static int[] getVetorMultiplosComunsdeVariosDivisoresNumDadoIntervalo(int lim_min, int lim_max, int[] divisores) {
        int[] vetorMultiplosComuns;
        double resto;
        int n_elementos = 0;      //numero de elementos do 1º array
        if (lim_min >= 0 && lim_max > lim_min) {
            for (int i = lim_min; i <= lim_max; i++) {       //criação do 1º array com os multiplos do primeiro divisor
                resto = i % divisores[0];
                if (resto == 0) {
                    n_elementos += 1;
                }
            }
            int[] arrayMultiplos_PrimeiroDivisor = new int[n_elementos];     //defenir tamanho do meu 1º array

            int j = 0;
            for (int i = lim_min; i <= lim_max; i++) {               //colocar os numeros multiplos dentro do 1º array.
                resto = i % divisores[0];
                if (resto == 0) {
                    arrayMultiplos_PrimeiroDivisor[j] = i;
                    j++;
                }
            }
            for (int i = 1; i < divisores.length; i++) {          //Começa em i=1, pois o primeiro divisor já foi considerado na criação do 1º array
                for (j = 0; j < n_elementos; j++) {
                    if (arrayMultiplos_PrimeiroDivisor[j] != -1) {        //não considera valores dos elementos =-1
                        resto = arrayMultiplos_PrimeiroDivisor[j] % divisores[i];
                        if (resto != 0) {
                            arrayMultiplos_PrimeiroDivisor[j] = -1;
                        }
                    }
                }
            }
            int n_elementos_final = 0;
            for (int i = 0; i < arrayMultiplos_PrimeiroDivisor.length; i++) {        //contagem dos elementos do vetor =-1, ou seja, contagem dos multiplos comuns.
                if (arrayMultiplos_PrimeiroDivisor[i] != -1) {
                    n_elementos_final += 1;
                }
            }
            vetorMultiplosComuns = new int[n_elementos_final];
            int k = 0;
            for (int i = 0; i < arrayMultiplos_PrimeiroDivisor.length; i++) {       //colocar dentro do vetor dos elementos finais os respetivos valores
                if (arrayMultiplos_PrimeiroDivisor[i] != -1) {
                    vetorMultiplosComuns[k] = arrayMultiplos_PrimeiroDivisor[i];
                    k++;
                }

            }
            return vetorMultiplosComuns;
        }
        else{                           // se não respeitar o lim_min >0 ou lim_min < lim_max deve retornar conjunto vazio.
            vetorMultiplosComuns = new int[]{};
            return vetorMultiplosComuns;
        }

    }

    public static void exercicio9(){
    }

    public static double getNumeroEspelhado(int numero) {
        int unidades;
        double resultado;
        resultado = 0;
        while (numero > 0) {
            unidades = numero % 10;
            resultado = resultado * 10 + unidades;
            numero = numero / 10;
        }
        return resultado;
    }

    public static boolean getVerificarSeNumeroCapicua(int numero) {
        boolean capicua = false;
        int numero_inverso = (int) getNumeroEspelhado(numero);
        if (numero == numero_inverso) {
            capicua = true;
        } else {
            capicua = false;
        }
        return capicua;
    }

}
