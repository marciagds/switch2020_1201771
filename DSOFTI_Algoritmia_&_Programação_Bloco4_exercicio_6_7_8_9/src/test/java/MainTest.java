import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    //Testes exercicio6
    @org.junit.jupiter.api.Test
    void getVetorComOsPrimeirosN_Elementos_valoresPositivos(){
        //arrange
        int[]vetor_entrada = {1,5,4,7,9,8};
        int n_retorno= 3;
        int[]expected = {1,5,4};
        //act
        int[] result = Main.getVetorComOsPrimeirosN_Elementos(vetor_entrada, n_retorno);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getVetorComOsPrimeirosN_Elementos_valoresNegativos(){
        //arrange
        int[]vetor_entrada = {-1,5,4,-7,9,8};
        int n_retorno= 5;
        int[]expected = {-1,5,4,-7,9};
        //act
        int[] result = Main.getVetorComOsPrimeirosN_Elementos(vetor_entrada, n_retorno);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getVetorComOsPrimeirosN_Elementos_valoresNulos(){
        //arrange
        int[]vetor_entrada = {0,0,4,-7,9,8};
        int n_retorno= 4;
        int[]expected = {0,0,4,-7};
        //act
        int[] result = Main.getVetorComOsPrimeirosN_Elementos(vetor_entrada, n_retorno);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getVetorComOsPrimeirosN_Elementos_valoresRetornoSuperioraoTamanhodoVetorEntrada(){
        //arrange
        int[]vetor_entrada = {-7,9,8};
        int n_retorno= 4;
        int[]expected = {-1,-1,-1,-1};
        //act
        int[] result = Main.getVetorComOsPrimeirosN_Elementos(vetor_entrada, n_retorno);
        //assert
        assertArrayEquals(expected,result);
    }

    //Testes exercicio7
    @org.junit.jupiter.api.Test
    void getVetordosMultiplosnumDadoIntervalo_teste_valoresPositivos1() {
        //arrange
        int lim_min = 4;
        int lim_max = 10;
        int divisor = 3;
        int[] expected = {6, 9};

        //act
        int[] result = Main.getVetordosMultiplosnumDadoIntervalo(lim_min, lim_max, divisor);

        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getVetordosMultiplosnumDadoIntervalo_teste_valoresPositivos2() {
        //arrange
        int lim_min = 3;
        int lim_max = 29;
        int divisor = 5;
        int[] expected = {5, 10,15,20,25};

        //act
        int[] result = Main.getVetordosMultiplosnumDadoIntervalo(lim_min, lim_max, divisor);

        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getVetordosMultiplosnumDadoIntervalo_teste_valoresNulos() {
        //arrange
        int lim_min = 0;
        int lim_max = 10;
        int divisor = 2;
        int[] expected = {0, 2, 4, 6, 8, 10};

        //act
        int[] result = Main.getVetordosMultiplosnumDadoIntervalo(lim_min, lim_max, divisor);

        //assert
        assertArrayEquals(expected, result);
    }

    //Testes exercicio8
    @org.junit.jupiter.api.Test
    void getVetorMultiplosComunsdeVariosDivisoresNumDadoIntervalo_teste_limSuperior0_1(){
        //arrange
        int lim_min = 5;
        int lim_max = 12;
        int[]divisores = {2,3,4};
        int [] expected = {12};

        //act
        int []result = Main.getVetorMultiplosComunsdeVariosDivisoresNumDadoIntervalo(lim_min,lim_max,divisores);

        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getVetorMultiplosComunsdeVariosDivisoresNumDadoIntervalo_teste_limSuperior0_2(){
        //arrange
        int lim_min = 5;
        int lim_max = 20;
        int[]divisores = {2,4,8};
        int [] expected = {8,16};

        //act
        int []result = Main.getVetorMultiplosComunsdeVariosDivisoresNumDadoIntervalo(lim_min,lim_max,divisores);

        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getVetorMultiplosComunsdeVariosDivisoresNumDadoIntervalo_teste_limInferior0(){
        //arrange
        int lim_min = -1;
        int lim_max = 20;
        int[]divisores = {2,4,8};
        int [] expected = {};

        //act
        int []result = Main.getVetorMultiplosComunsdeVariosDivisoresNumDadoIntervalo(lim_min,lim_max,divisores);

        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getVetorMultiplosComunsdeVariosDivisoresNumDadoIntervalo_teste_limTrocados(){
        //arrange
        int lim_min = 20;
        int lim_max = 3;
        int[]divisores = {2,4,8};
        int [] expected = {};

        //act
        int []result = Main.getVetorMultiplosComunsdeVariosDivisoresNumDadoIntervalo(lim_min,lim_max,divisores);

        //assert
        assertArrayEquals(expected, result);
    }


    //Testes exercicio9

}
