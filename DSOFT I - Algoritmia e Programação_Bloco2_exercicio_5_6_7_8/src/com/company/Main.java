package com.company;

import java.util.Scanner;
import java.util.SplittableRandom;

public class Main {

    public static void main(String[] args) {
        // write your code here
        //exercicio5();
        //getVolume(23);
        //getClassification(12);
        //exercicio6();
        //exercicio7();
        exercicio8();
    }

    public static void exercicio5() {
        // Cálculo do volume do cubo.

        //Variáveis envolvidas
        double area, aresta, volume;
        String resultado, classification;

        //Inicialização da Leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual é a área do cubo ?");
        area = ler.nextDouble();

        //Procedimento
        volume = getVolume(area);
        classification = getClassification(area);

        //Saída dos dados
        System.out.println("O volume do cubo é " + String.format("%.3f", volume));
        System.out.println("É um cubo " + classification);
    }


    public static double getVolume(double area) { //Em caso de uma área inválida, retorna -1
        if (area > 0) {
            double aresta = Math.sqrt(area / 6); // ATENÇÂO: a area é dada em cm2
            double volume = Math.pow(aresta, 3) / 1000; //// ATENÇÂO: o volume é em dm3
            return volume;
        } else {
            return -1;
        }
    }

    public static String getClassification(double area) {

        double volume = getVolume(area);

        if (volume < 0) {
            return "Área/Volume incorretos.";
        } else {
            if (volume <= 1) {
                String classification = "Pequeno";
                return classification;
            } else {
                if (volume > 1 && volume <= 2) {
                    String classification = "Médio";
                    return classification;
                } else {
                    String classification = "Grande";
                    return classification;
                }
            }
        }
    }

    public static void exercicio6() {
        //Saudação durante o dia

        // Variáveis
        int hours, seconds, total_segundos, minuts;
        String result;

        //Inicialização da Leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Total Segundos ?");
        total_segundos = ler.nextInt();
        String greeting;

        //Procedimento
        hours = total_segundos / 3600;
        minuts = (total_segundos - hours * 3600) / 60;
        seconds = (total_segundos - hours * 3600 - minuts * 60);
        result = getConversorTime_H_M_S(total_segundos);

        greeting = getGretting(total_segundos);

        //Saída de resultados
        System.out.println(result);
        System.out.println(greeting);
    }

    public static String getConversorTime_H_M_S(int total_segundos) {
        String result;
        int hours, seconds, minuts;

        if (total_segundos < 0) {
            result = "Valor de segundos incorreto. Valor mínimo: 0";
            return result;
        } else {
            if (total_segundos > 86399) {
                result = "Valor de segundos incorreto. Valor máximo:86399";
                return result;
            } else {
                hours = total_segundos / 3600;
                minuts = (total_segundos - hours * 3600) / 60;
                seconds = (total_segundos - hours * 3600 - minuts * 60);
                result = hours + ":" + minuts + ":" + seconds;
                return result;
            }
        }
    }

    public static String getGretting(int total_segundos) {
        String greeting;

        int hours = total_segundos / 3600;

        if (total_segundos < 0) {
            greeting = "Valor de segundos incorreto. Valor mínimo: 0";
            return greeting;
        } else {
            if (hours >= 0 && hours <= 6) {
                greeting = "Boa noite";
                return greeting;
            } else {
                if (hours > 6 && hours < 12) {
                    greeting = "Bom dia";
                    return greeting;
                } else {
                    if (hours >= 12 && hours < 20) {
                        greeting = "Boa tarde";
                        return greeting;
                    } else {
                        if (hours >= 20 & hours < 24) {
                            greeting = "Boa noite";
                            return greeting;
                        } else {
                            greeting = "Valor de segundos incorreto. Valor máximo:86399";
                            return greeting;
                        }
                    }
                }
            }
        }

    }

    public static void exercicio7() {
        // Custo da pintura de um edificio

        //Variáveis envolvidas
        double area, salario_pintor, $tinta, $total, rendimento_litro, rendimento_pintor, $material, $mao_de_obra;
        int n_pintores;

        //Inicialização da Leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual é a área do edifício ?");
        area = ler.nextDouble();
        System.out.println("Qual é o custo da tinta/L ?");
        $tinta = ler.nextDouble();
        System.out.println("Qual é o rendimento do litro da tinta ?");
        rendimento_litro = ler.nextDouble();
        System.out.println("Qual é o salário diário do pintor ?");
        salario_pintor = ler.nextDouble();

        //Procedimento
        int horas_trabalho = 8;
        rendimento_pintor = 2;

        //nºpintores
            $material = get$material(area, $tinta, rendimento_litro);
            $mao_de_obra = get$mao_de_obra(area, salario_pintor, rendimento_pintor);

            $total = $material + $mao_de_obra;

            // Saída dos dados
            System.out.println("O custo total é de: " + String.format("%.2f", $total));

    }

    public static double get$mao_de_obra(double area, double salario_pintor, double rendimento_pintor) {
        double $mao_de_obra;
        int n_pintores;
        double  horas_trabalho = 8;

        if (area <= 0) {
            $mao_de_obra = -1;
            return $mao_de_obra;
        }
        else {
            if (area > 0 && area < 100) {
                n_pintores = 1;
            }
            else {
                if (area >= 100 && area < 300) {
                    n_pintores = 2;
                }
                else {
                    if (area >= 300 && area < 1000) {
                        n_pintores = 3;
                    }
                    else {
                        n_pintores = 4;
                    }
                }
            }
            double total_dias_trabalho = area / (rendimento_pintor * horas_trabalho * n_pintores);
            $mao_de_obra = n_pintores * total_dias_trabalho * salario_pintor;
            return $mao_de_obra;
        }
    }

    public static double get$material(double area, double $tinta, double rendimento_litro) {
        if (area <=0   || $tinta <= 0 || rendimento_litro <= 0){
            double $material = -1;
            return $material;
        }
        else {
            double $material;
            $material = area * (1 / rendimento_litro) * $tinta;
            return $material;
        }
    }

    public static void exercicio8() {
        // Saber se dois números são multiplos

        //Variáveis envolvidas
        int x, y;
        String resultado;

        //Inicialização da Leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual é valor do 1º número ?");
        x = ler.nextInt();
        System.out.println("Qual é valor do 2º número ?");
        y = ler.nextInt();

        //Processamento
        resultado = getSaber_se_Multiplo(x,y);

        //Saída dos dados
        System.out.println(resultado);
    }

    public static String getSaber_se_Multiplo(int x, int y) {
        String resultado;

        if (x == 0 || y == 0) {
            resultado = "0 e multiplo de todos os numeros.";
            return resultado;
        } else {
            double resto_x_por_y = x % y;
            double resto_y_por_x = y % x;
            if (resto_x_por_y == 0) {
                resultado = x + " é multiplo de " + y;
                return resultado;
            } else {
                if (resto_y_por_x == 0) {
                    resultado = y + " é multiplo de " + x;
                    return resultado;
                } else {
                    resultado = x + " não é nem multiplo nem divisor de " + y;
                    return resultado;
                }
            }
        }
    }
}

