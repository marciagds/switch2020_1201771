package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

//Testes do Exercicio 5
    @Test
    void getVolume_teste_area_negativa() {
        //arrange
        double area = -3;
        double expected =-1;

        //act
        double result = Main.getVolume(area);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getVolume_teste_area_positiva() {
        //arrange
        double area = 24;
        double expected =0.008;

        //act
        double result = Main.getVolume(area);

        //assert
        assertEquals(expected, result, 0.001);
    }

    @Test
    void getVolume_teste_area_nula() {
        //arrange
        double area = 0;
        double expected =-1;

        //act
        double result = Main.getVolume(area);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void getClassification_teste_area_nula() {
        //arrange
        double area = 0;
        String expected = "Área/Volume incorretos.";

        //act
        String result = Main.getClassification(area);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getClassification_teste_area_negativa() {
        //arrange
        double area = -1;
        String expected = "Área/Volume incorretos.";

        //act
        String result = Main.getClassification(area);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getClassification_teste_area_grande() {
        //arrange
        double area = 2560;
        String expected = "Grande";

        //act
        String result = Main.getClassification(area);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getClassification_teste_area_media() {
        //arrange
        double area = 607;
        String expected = "Médio";

        //act
        String result = Main.getClassification(area);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getClassification_teste_area_pequena() {
        //arrange
        double area = 20;
        String expected = "Pequeno";

        //act
        String result = Main.getClassification(area);

        //assert
        assertEquals(expected, result);
    }

//Testes do Exercicio 6
    @Test
    void getConversorTime_H_M_S_teste_valor_positivo1() {
    //arrange
    int total_segundos = 45936;
    String expected = "12:45:36";

    //act
    String result = Main.getConversorTime_H_M_S(total_segundos);

    //assert
    assertEquals(expected, result);
}
    @Test
    void getConversorTime_H_M_S_teste_valor_positivo2() {
        //arrange
        int total_segundos = 75300;
        String expected = "20:55:0";

        //act
        String result = Main.getConversorTime_H_M_S(total_segundos);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getConversorTime_H_M_S_teste_valor_nulo() {
        //arrange
        int total_segundos = 0;
        String expected = "0:0:0";

        //act
        String result = Main.getConversorTime_H_M_S(total_segundos);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getConversorTime_H_M_S_teste_valor_25h() {
        //arrange
        int total_segundos = 90000;
        String expected = "Valor de segundos incorreto. Valor máximo:86399";

        //act
        String result = Main.getConversorTime_H_M_S(total_segundos);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getConversorTime_H_M_S_teste_valor_negativo() {
        //arrange
        int total_segundos = -18900;
        String expected = "Valor de segundos incorreto. Valor mínimo: 0";

        //act
        String result = Main.getConversorTime_H_M_S(total_segundos);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getGretting_teste_valor_0() {
        //arrange
        int total_segundos = 0;
        String expected = "Boa noite";

        //act
        String result = Main.getGretting(total_segundos);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getGretting_teste_valor_27600() {
        //arrange
        int total_segundos = 27600;
        String expected = "Bom dia";

        //act
        String result = Main.getGretting(total_segundos);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getGretting_teste_valor_82564() {
        //arrange
        int total_segundos = 82564;
        String expected = "Boa noite";

        //act
        String result = Main.getGretting(total_segundos);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getGretting_teste_valor_negativo() {
        //arrange
        int total_segundos = -1000;
        String expected = "Valor de segundos incorreto. Valor mínimo: 0";

        //act
        String result = Main.getGretting(total_segundos);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getGretting_teste_valor_26h() {
        //arrange
        int total_segundos = 93600;
        String expected = "Valor de segundos incorreto. Valor máximo:86399";

        //act
        String result = Main.getGretting(total_segundos);

        //assert
        assertEquals(expected, result);
    }

//Testes Exercicio 7
    @Test
    void get$material_valor_positivo() {
        //arrange
        double area = 400;
        double $tinta = 12;
        double rendimento_litro = 3;
        double expeted =1600 ;

        //act
        double result = Main.get$material(area, $tinta, rendimento_litro);

        //assert
        assertEquals(expeted, result, 0.01);
    }

    @Test
    void get$material_valor_$tinat_nula() {
        //arrange
        double area = 800;
        double $tinta = 0;
        double rendimento_litro = 3;
        double expeted =-1 ;

        //act
        double result = Main.get$material(area, $tinta, rendimento_litro);

        //assert
        assertEquals(expeted, result, 0.01);
    }

    @Test
    void get$material_valor_area_nula() {
        //arrange
        double area = 0;
        double $tinta = 12;
        double rendimento_litro = 3;
        double expeted =-1 ;

        //act
        double result = Main.get$material(area, $tinta, rendimento_litro);

        //assert
        assertEquals(expeted, result, 0.01);
    }

    @Test
    void get$material_valor_rendimento_litro_nula() {
        //arrange
        double area = 12222;
        double $tinta = 12;
        double rendimento_litro = 0;
        double expeted =-1 ;

        //act
        double result = Main.get$material(area, $tinta, rendimento_litro);

        //assert
        assertEquals(expeted, result, 0.01);
    }

    @Test
    void get$mao_de_obra_valor_area_nulo() {
        //arrange
        double area = 0;
        double salario_pintor = 45;
        double rendimento_pintor = 2;
        double expeted =-1 ;

        //act
        double result = Main.get$mao_de_obra(area, salario_pintor, rendimento_pintor);

        //assert
        assertEquals(expeted, result, 0.01);
    }

    @Test
    void get$mao_de_obra_valor_positivo1() {
        //arrange
        double area = 100;
        double salario_pintor = 45;
        double rendimento_pintor = 2;
        double expeted =281.25 ;

        //act
        double result = Main.get$mao_de_obra(area, salario_pintor, rendimento_pintor);

        //assert
        assertEquals(expeted, result, 0.01);
    }

    @Test
    void get$mao_de_obra_valor_positivo2() {
        //arrange
        double area = 1020;
        double salario_pintor = 42;
        double rendimento_pintor = 3;
        double expeted =1785;

        //act
        double result = Main.get$mao_de_obra(area, salario_pintor, rendimento_pintor);

        //assert
        assertEquals(expeted, result, 0.01);
    }

// Testes do Exercicio 8
    @Test
    void get$mao_de_obra_valor_teste1() {
    //arrange
    int x = 14;
    int y = 7;
    String expeted =  x + " é multiplo de " + y;

    //act
    String result = Main.getSaber_se_Multiplo(x,y);

    //assert
    assertEquals(expeted, result);
}
    @Test
    void get$mao_de_obra_valor_teste2() {
        //arrange
        int x = 3;
        int y = 24;
        String expeted =  y + " é multiplo de " + x;

        //act
        String result = Main.getSaber_se_Multiplo(x,y);

        //assert
        assertEquals(expeted, result);
    }

    @Test
    void get$mao_de_obra_valor_teste3() {
        //arrange
        int x = 0;
        int y = 24;
        String expeted =  "0 e multiplo de todos os numeros.";

        //act
        String result = Main.getSaber_se_Multiplo(x,y);

        //assert
        assertEquals(expeted, result);
    }

    @Test
    void get$mao_de_obra_valor_teste4() {
        //arrange
        int x = 12;
        int y = 35;
        String expeted =  x + " não é nem multiplo nem divisor de " + y;

        //act
        String result = Main.getSaber_se_Multiplo(x,y);

        //assert
        assertEquals(expeted, result);
    }

}



