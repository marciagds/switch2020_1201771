package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        //exercicio1();
        //exercicio2();
        //exercicio3();
        //exercicio4();

    }

    public static void exercicio1() {
        //Cálculo da média das notas

        //Variáveis envolvidas
        double nota1, nota2, nota3, peso1, peso2, peso3;
        double media_pesada;

        //Inicialização da leituda
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual o valor da nota1 ?");
        nota1 = ler.nextDouble();
        System.out.println("Qual o valor da nota2 ?");
        nota2 = ler.nextDouble();
        System.out.println("Qual o valor da nota3 ?");
        nota3 = ler.nextDouble();
        System.out.println("Qual o peso da nota1 ?");
        peso1 = ler.nextDouble();
        System.out.println("Qual o peso da nota2 ?");
        peso2 = ler.nextDouble();
        System.out.println("Qual o peso da nota3 ?");
        peso3 = ler.nextDouble();

        //Processamento
        media_pesada = getMedia_pesada(nota1, nota2, nota3, peso1, peso2, peso3);

        //Saída dos resultados
        System.out.println(String.format("%.2f", media_pesada));
        if (media_pesada >= 8) {
            System.out.println("O aluno apresenta nota mínima exigida");
        } else {
            System.out.println("O aluno não apresenta nota mínima exigida");
        }

    }

    public static double getMedia_pesada(double nota1, double nota2, double nota3, double peso1, double peso2, double peso3) {
        double media_pesada;
        media_pesada = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);
        return media_pesada;
    }

    public static void exercicio2() {
        //Separação dos algaristmos que constituem um número

        // Variáveis envolvidas
        int numero, digito1, digito2, digito3, par_ou_impar;
        String tipo_numero;

        //Inicialização da leituda
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Escolha um número de 3 algarismos ?");
        numero = ler.nextInt();

        //Processamento
        tipo_numero = getPar_ou_Impar(numero);
        if (numero < 100 || numero > 999) {
            System.out.println("O número não tem 3 digitos");
        } else {
            digito3 = getDigit3(numero);
            digito2 = getDigit2(numero);
            digito1 = getDigit1(numero);

            System.out.println(digito1 + " " + digito2 + " " + digito3 + " ");
            System.out.println("O número que escolheu é " + tipo_numero);
        }
    }

    public static String exercicio2_teste(int numero) {
        String result;
        if (numero < 100 || numero > 999) {
            result = "Não tem 3 digitos";
        } else {
            int digito3 = getDigit3(numero);
            int digito2 = getDigit2(numero);
            int digito1 = getDigit1(numero);

            result = digito1 + " " + digito2 + " " + digito3;
        }
        return result;
    }

    public static String getPar_ou_Impar(int numero) {
        int par_ou_impar;
        String tipo_numero;
        par_ou_impar = numero % 2;
        if (par_ou_impar == 0) {
            tipo_numero = "PAR";
        } else {
            tipo_numero = "IMPAR";
        }
        return tipo_numero;
    }

    public static int getDigit1(int numero) {
        int digito1;
        digito1 = (numero / 100) % 10;
        return digito1;
    }

    public static int getDigit2(int numero) {
        int digito2;
        digito2 = (numero / 10) % 10;
        return digito2;
    }

    public static int getDigit3(int numero) {
        int digito3;
        digito3 = numero % 10;
        return digito3;
    }

    public static void exercicio3() {
        // Cálculo da distância entre dois pontos

        // Variáveis envolvidas
        double x_1, x_2, y_1, y_2, distancia;

        //Inicialização da leituda
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Abcissa do Ponto 1 (x1) ?");
        x_1 = ler.nextDouble();
        System.out.println("Ordenada do Ponto 1 (y1) ?");
        y_1 = ler.nextDouble();
        System.out.println("Abcissa do Ponto 2 (x2) ?");
        x_2 = ler.nextDouble();
        System.out.println("Ordenada do Ponto 2 (y2) ?");
        y_2 = ler.nextDouble();

        //Processamento
        distancia = getDistanceBetweenTwoPoints(x_1, x_2, y_1, y_2);

        // Saída dos dados
        System.out.println("A distância entre os dois pontos é " + String.format("%.2f", distancia));
    }

    public static double getDistanceBetweenTwoPoints(double x_1, double x_2, double y_1, double y_2) {
        double distancia;
        distancia = Math.sqrt(Math.pow(x_2 - x_1, 2) + Math.pow(y_2 - y_1, 2));
        return distancia;
    }

    public static void exercicio4() {
        // Cálculo da função F(x)

        // Variáveis envolvidas
        double x, result;

        //Inicialização da leituda
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Valor de x ?");
        x = ler.nextDouble();

        //Processamento
        result = getResultF_x(x);

        //Saída dos dados
        System.out.println("O resultado é " + String.format("%.2f", result));
    }

    public static double getResultF_x(double x) {
        double result;
        if (x < 0) {
            result = x;
        } else {
            if (x == 0) {
                result = 0;
            } else {
                result = Math.pow(x, 2) - 2 * x;
            }
        }
        return result;
    }
}


