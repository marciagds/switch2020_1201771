package com.company;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

//Testes exercicio1
    @org.junit.jupiter.api.Test
    void getMedia_pesada_teste1() {
        //arrange
        double nota1 = 12;
        double nota2 = 13;
        double nota3 = 8;
        double peso1 = 3;
        double peso2 = 6;
        double peso3 = 5;
        double expected = 11;

        //act
        double result = Main.getMedia_pesada(nota1, nota2, nota3, peso1, peso2, peso3);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getMedia_pesada_teste2() {
        //arrange
        double nota1 = 7;
        double nota2 = 4;
        double nota3 = 16;
        double peso1 = 2;
        double peso2 = 7;
        double peso3 = 8;
        double expected = 10;

        //act
        double result = Main.getMedia_pesada(nota1, nota2, nota3, peso1, peso2, peso3);

        //assert
        assertEquals(expected, result, 0.01);

    }

//Testes exercicio2
    @org.junit.jupiter.api.Test
    void getDigit3_teste_numero_positivo1() {
        //arrange
        int numero = 134;
        int expected = 4;

        //act
        int result = Main.getDigit3(numero);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getDigit3_teste_numero_positivo2() {
        //arrange
        int numero = 235;
        int expected = 5;

        //act
        int result = Main.getDigit3(numero);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getDigit3_teste_numero_nulo() {
        //arrange
        int numero = 0;
        int expected = 0;

        //act
        int result = Main.getDigit3(numero);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getDigit2_teste_numero_positivo1() {
        //arrange
        int numero = 235;
        int expected = 3;

        //act
        int result = Main.getDigit2(numero);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getDigit2_teste_numero_positivo2() {
        //arrange
        int numero = 467;
        int expected = 6;

        //act
        int result = Main.getDigit2(numero);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getDigit1_teste_numero_postivo1() {
        //arrange
        int numero = 467;
        int expected = 4;

        //act
        int result = Main.getDigit1(numero);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getDigit1_teste_numero_positivo2() {
        //arrange
        int numero = 679;
        int expected = 6;

        //act
        int result = Main.getDigit1(numero);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test //Em vez de testar cada função dos digitos, testa-se todas simultaneamente. Método mais correto.
    void exercicio2_teste_teste_numero_positivo1() {
        //arrange
        int numero = 679;
        String expected = "6 7 9";

        //act
        String result = Main.exercicio2_teste(numero);

        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void exercicio2_teste_teste_numero_positivo2() {
        //arrange
        int numero = 120;
        String expected = "1 2 0";

        //act
        String result = Main.exercicio2_teste(numero);

        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void exercicio2_teste_teste_numero_de_2_digitos() {
        //arrange
        int numero = 22;
        String expected = "Não tem 3 digitos";

        //act
        String result = Main.exercicio2_teste(numero);

        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void par_ou_impar_teste_numero_impar() {
        //arrange
        int numero = 679;
        String expected = "IMPAR";

        //act
        String result = Main.getPar_ou_Impar(numero);

        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void par_ou_impar_teste_numero_par() {
        //arrange
        int numero = 200;
        String expected = "PAR";

        //act
        String result = Main.getPar_ou_Impar(numero);

        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void par_ou_impar_teste_numero_nulo() {
        //arrange
        int numero = 0;
        String expected = "PAR";

        //act
        String result = Main.getPar_ou_Impar(numero);

        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void par_ou_impar_teste_numero_negativo_par() {
        //arrange
        int numero = -10;
        String expected = "PAR";

        //act
        String result = Main.getPar_ou_Impar(numero);

        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void par_ou_impar_teste_numero_negativo_impar() {
        //arrange
        int numero = -55;
        String expected = "IMPAR";

        //act
        String result = Main.getPar_ou_Impar(numero);

        //assert
        assertEquals(expected, result);

    }

    //Testes exercicio3
    @org.junit.jupiter.api.Test
    void getDistanceBetweenTwoPoints_teste_numeros_positivos() {
        //arrange
        double x_1 = 2;
        double y_1 = 3;
        double x_2 = 4;
        double y_2 = 1;
        double expected = 2.828;

        //act
        double result = Main.getDistanceBetweenTwoPoints(x_1, x_2, y_1, y_2);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getDistanceBetweenTwoPoints_teste_numeros_negativos() {
        //arrange
        double x_1 = -2;
        double y_1 = 3;
        double x_2 = 4;
        double y_2 = -1;
        double expected = 7.211;

        //act
        double result = Main.getDistanceBetweenTwoPoints(x_1, x_2, y_1, y_2);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getDistanceBetweenTwoPoints_teste_numeros_pos_neg_nulo() {
        //arrange
        double x_1 = -3;
        double y_1 = -6;
        double x_2 = 0;
        double y_2 = -4;
        double expected = 3.605;

        //act
        double result = Main.getDistanceBetweenTwoPoints(x_1, x_2, y_1, y_2);

        //assert
        assertEquals(expected, result, 0.01);

    }

    //Testes exercicio4
    @org.junit.jupiter.api.Test
    void getResultF_x_teste1() {
        //arrange
        double x = 3;
        double expected = 3;

        //act
        double result = Main.getResultF_x(x);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getResultF_x_teste2() {
        //arrange
        double x = -3;
        double expected = -3;

        //act
        double result = Main.getResultF_x(x);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getResultF_x_teste3() {
        //arrange
        double x = 0;
        double expected = 0;

        //act
        double result = Main.getResultF_x(x);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void getResultF_x_teste4() {
        //arrange
        double x = -1.6;
        double expected = -1.6;

        //act
        double result = Main.getResultF_x(x);

        //assert
        assertEquals(expected, result, 0.01);

    }
}