public class parte2 {
    public static void main(String[] args) {
        int[][] matriz = {{1, 2, 3, 4, 5}, {1, 2, 3, 5, 6}, {1, 2, 3, 6, 4}, {1, 2, 3, 7, 6}, {2, 3, 4, 5, 2}};

    }

    public static double detLaplace(int n, double a[][]) {
        a = new double[n][n];
        if (n == 1) {
            //Caso base: matriz 1x1
            //System.out.println(a[0][0]);
            return a[0][0];
        } else {
            double det = 0;
            int i, row, col, j_aux, i_aux;
            double[][] aux;
            //Escolhe a primeira linha para calcular os cofatores
            for (i = 0; i < n; i++) {
                //ignora os zeros (zero vezes qualquer número é igual zero)
                if (a[0][i] != 0) {
                    aux = new double[n - 1][n - 1];
                    i_aux = 0;
                    j_aux = 0;
                    //Gera as matrizes para calcular os cofatores
                    for (row = 1; row < n; row++) {
                        for (col = 0; col < n; col++) {
                            if (col != i) {
                                aux[i_aux][j_aux] = a[row][col];
                                j_aux++;
                            }
                        }
                        i_aux++;
                        j_aux = 0;
                    }
                    double factor = (i % 2 == 0) ? a[0][i] : -a[0][i];
                    det = det + factor * detLaplace(n - 1, aux);
                }
            }
            return det;
            //System.out.println("determinante= " + det);
        }
    }
}