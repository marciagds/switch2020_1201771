public class Main {
    public static void main(String[] args) {
        exercicio16();
        //exercicio17();
    }

    public static void exercicio16(){
    }

    public static Integer obterDeterminanteMatrizPorTeoremaLaplace(int[][]matriz){
        boolean matrizQuadrada = getSeMatrizQuadrada(matriz);
            int determinanteMatriz=0;
            for(int i=0; i<matriz.length; i++) {
                //passo1
                int termoLinha = matriz[i][0];
                int[][]matrizAuxiliar = criarMatrizAuxiliarLaPlace(matriz,i);
                matrizAuxiliar=multiplicarTermoLinhaPelaMatriz(matrizAuxiliar,termoLinha);
                //passo2


                for (int k = 0; k < matriz.length - 2; k++) {


                }
            }
            return determinanteMatriz;
    }

    public static void getcalcularDeterminanteAuxiliar(int[][]matriz){
        int i=0;
        while(matriz.length>2){
            int termoLinha = matriz[i][0];
            int[][]matrizAuxiliar = criarMatrizAuxiliarLaPlace(matriz,i);
            matrizAuxiliar=multiplicarTermoLinhaPelaMatriz(matrizAuxiliar,termoLinha);
        }
        int determinanteAuxiliar=(matriz[0][0]*matriz[1][1]) - (matriz[0][1]*matriz[0][1]);
        //return  determinanteAuxiliar;
        System.out.println(determinanteAuxiliar);
    }


    public static int[][] criarMatrizAuxiliarLaPlace(int[][]matriz, int i) {
        int sizeLinha = matriz.length;
        int sizeColuna = matriz.length;
        int[][] matrizAuxiliar = new int[sizeLinha - 1][sizeColuna - 1];
        boolean linhaEliminadaEncontrada = false;
        for (int linha = 0; linha < matrizAuxiliar.length; linha++) {
            if (linha >= i) {
                linhaEliminadaEncontrada = true;
            }
            for (int coluna = 0; coluna < matrizAuxiliar.length; coluna++) {
                if (!linhaEliminadaEncontrada) {
                    matrizAuxiliar[linha][coluna] = matriz[linha][coluna + 1];
                } else {
                    matrizAuxiliar[linha][coluna] = matriz[linha + 1][coluna + 1];
                }
            }
        }
        return matrizAuxiliar;
    }

    public static int[][] multiplicarTermoLinhaPelaMatriz(int[][]matriz, int termo) {
        for(int i=0; i<matriz.length; i++){
            for(int k=0; k<matriz.length; k++){
                if(k==0){
                    matriz[i][k] = matriz[i][k] * termo;
                }
            }
        }
        return matriz;
    }








    public static boolean getSeMatrizQuadrada(int[][] matriz) {
        if (matriz == null || matriz.length ==0) {
            return false;
        } else {
            boolean matrizQuadrada = true;
            int nLinhas = matriz.length;
            int nColunas;

            for (int i = 0; i < matriz.length && matrizQuadrada == true; i++) {
                nColunas = matriz[i].length;
                if (nColunas != nLinhas) {
                    //return false;
                    matrizQuadrada = false;
                }
            }
            //return true;
            return matrizQuadrada;
        }
    }

    public static void exercicio17() {
        int[][] matriz1 = {{2, 1, 3}, {2, 2, 3}, {2, 1, 2}};
        int[][] matriz2 = {{2, 3, 4}, {1, 2, 1}, {4, 4, 4}};
        //alinea 17.a
        System.out.println("Exercicio 17.a: ");
        int[][] matrizResultado_a = getMultiplicarMatrizPorConstante(matriz1, 3);
        for (int l = 0; l < matrizResultado_a.length; l++) {
            for (int c = 0; c < matrizResultado_a[l].length; c++) {
                System.out.print(matrizResultado_a[l][c] + " "); //imprime caracter a caracter
            }
            System.out.println(" "); //muda de linha
        }
        //alinea 17.b
        System.out.println("Exercicio 17.b: ");
        int[][] matrizResultado_b = getSomaDeDuasMatrizes(matriz1, matriz2);
        for (int l = 0; l < matrizResultado_b.length; l++) {
            for (int c = 0; c < matrizResultado_b[l].length; c++) {
                System.out.print(matrizResultado_b[l][c] + " "); //imprime caracter a caracter
            }
            System.out.println(" "); //muda de linha
        }
        //alinea 17.c
        System.out.println("Exercicio 17.c: ");
        int[][] matrizResultado_c = getProdutoMatrizesInteiras(matriz1, matriz2);
        for (int l = 0; l < matrizResultado_c.length; l++) {
            for (int c = 0; c < matrizResultado_c[l].length; c++) {
                System.out.print(matrizResultado_c[l][c] + " "); //imprime caracter a caracter
            }
            System.out.println(" "); //muda de linha
        }
     }

    public static int[][] getMultiplicarMatrizPorConstante(int[][] matriz, int number) {
        if (matriz == null) {
            return null;
        } else {
            int[][] matrizResultante;
            matrizResultante = new int[matriz.length][];     //tamanho de coluna varia com a linha - cria-se a matriz sem valor

            for (int l = 0; l < matriz.length; l++) {        //criação da matrizSoma
                matrizResultante[l] = new int[matriz[l].length];
            }
            for (int i = 0; i < matriz.length; i++) {
                for (int k = 0; k < matriz[i].length; k++) {
                    matrizResultante[i][k] = matriz[i][k] * number;
                    if (matrizResultante[i][k] == -0.0) {                 //Tenho de fazer este algoritmo.
                        matrizResultante[i][k] = 0;
                    }
                }
            }
            return matrizResultante;
        }
    }

    public static boolean getSeDuasMatrizesTemAMesmaOrdem(double[][] matriz1, double[][] matriz2) {
        boolean matrizesMesmaOrdem = true;
        if (matriz2.length == matriz1.length && matriz2 != null && matriz1 != null) {
            for (int i = 0; i < matriz1.length && matrizesMesmaOrdem == true; i++) {
                if (matriz1[i].length != matriz2[i].length) {
                    matrizesMesmaOrdem = false;
                }
            }
        }
        return matrizesMesmaOrdem;
    }

    public static int[][] getSomaDeDuasMatrizes(int[][] matriz1, int[][] matriz2){
        boolean matrizesTamanhoIgual = getSeDuasMatrizComTamanhoIguais(matriz1, matriz2);
        int[][] matrizSoma;
            if (matrizesTamanhoIgual == true) {
                matrizSoma = new int[matriz1.length][];     //tamanho de coluna varia com a linha - cria-se a matriz sem valor
                for (int l = 0; l < matriz1.length; l++) {        //criação da matrizSoma
                    matrizSoma[l] = new int[matriz1[l].length];
                }
                for (int i = 0; i < matrizSoma.length; i++) {
                    for (int k = 0; k < matrizSoma[i].length; k++) {
                        matrizSoma[i][k] = matriz1[i][k] + matriz2[i][k];
                    }
                }
                return matrizSoma;
        } else {
            return null;
        }
    }

    public static int[][] getProdutoMatrizesInteiras(int[][] matriz1, int[][] matriz2){
        boolean possivelMultiplicarMatrizes = getSePossivelMultiplocarMatrizes(matriz1,matriz2);
        if(possivelMultiplicarMatrizes){
            int[][]matrizProduto=new int[matriz1.length][matriz2[0].length];
            for(int l=0; l<matrizProduto.length; l++){
                for(int c=0; c< matrizProduto[l].length;c++){
                    matrizProduto[l][c]=getCalculoAuxiliarProdutoMatriz(matriz1,matriz2,l,c);
                }
            }
            return matrizProduto;
        }
        else {
            return null;
        }
    }

    public static int getCalculoAuxiliarProdutoMatriz(int[][]matriz1,int[][]matriz2, int linhaMatrizResultado, int colunaMatrizResultado){
        int produtoLinhaColuna =0;
        for(int k=0; k<matriz2.length ; k++){
            produtoLinhaColuna += matriz1[linhaMatrizResultado][k]*matriz2[k][colunaMatrizResultado];
        }
        return produtoLinhaColuna;
    }

    public static boolean getSePossivelMultiplocarMatrizes(int[][]matriz1, int[][]matriz2){
        boolean produtoMatrizesValido = false;
        boolean matriz1Uniforme = getSeMatrizUniforme(matriz1);
        boolean matriz2Uniforme = getSeMatrizUniforme(matriz2);
        if(matriz1!=null && matriz2!=null && matriz1Uniforme && matriz2Uniforme){
            if(matriz1[0].length == matriz2.length){
                produtoMatrizesValido = true;
            }
        }
        return produtoMatrizesValido;
    }

    /**
     * Verifica se uma dada matriz tem todas as linhas com o mesmo numero de colunas
     *
     * @param matriz
     * @return valida true se for uma matriz uniforme
     */
    public static boolean getSeMatrizUniforme(int[][] matriz) {
        if (matriz == null || matriz.length == 0) {
            return false;
        } else {
            boolean matrizUniforme = true;
            int nLinhas = matriz.length;
            int nColunas;

            for (int i = 0; i < matriz.length && matrizUniforme == true; i++) {
                if (matriz[i].length != matriz[0].length) {
                    //return false;
                    matrizUniforme = false;
                }
            }
            //return true;
            return matrizUniforme;
        }
    }

    /**
     * Verifica se duas matrizes apresentam o mesmo tamanho (mesmo nr de colunas em cada linha)
     *
     * @param matriz1
     * @param matriz2
     * @return true se matrizes tiverem o mesmo tamanho em cada linha
     */
    public static boolean getSeDuasMatrizComTamanhoIguais(int[][] matriz1, int[][] matriz2) {
        boolean matrizesIguais;
        if (matriz1!=null && matriz1.length == matriz2.length){
            matrizesIguais = true;
            for (int i = 0; i < matriz1.length && matrizesIguais; i++) {
                if (matriz1[i].length != matriz2[i].length) {
                    //return false;
                    matrizesIguais = false;
                }
            }
            //return true;
            return matrizesIguais;
        }
       else {
           return false;
        }
    }

}
