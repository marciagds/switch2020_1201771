import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    /**
     * Testes Exercicio16
     */
    @org.junit.jupiter.api.Test
    void criarMatrizAuxiliarLaPlace_teste1(){
        //arrange
        int[][]matriz = {{1,2,3,4},{1,2,3,5},{1,2,3,6},{1,2,3,7}};
        int i = 0;
        int[][]expected = {{2, 3, 5},{2,3,6},{2,3,7}};
        //act
        int[][] result = Main.criarMatrizAuxiliarLaPlace(matriz,i);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void criarMatrizAuxiliarLaPlace_teste2(){
        //arrange
        int[][]matriz = {{1,2,3,4},{1,2,3,5},{1,2,3,6},{1,2,3,7}};
        int i = 1;
        int[][]expected = {{2,3,4},{2,3,6},{2,3,7}};
        //act
        int[][] result = Main.criarMatrizAuxiliarLaPlace(matriz,i);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void criarMatrizAuxiliarLaPlace_teste3(){
        //arrange
        int[][]matriz = {{1,2,3,4},{1,2,4,5},{1,2,3,6},{1,2,3,7}};
        int i = 3;
        int[][]expected = {{2,3,4},{2,4,5},{2,3,6}};
        //act
        int[][] result = Main.criarMatrizAuxiliarLaPlace(matriz,i);
        //assert
        assertArrayEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void multiplicarTermoLinhaPelaMatriz_teste1(){
        //arrange
        int[][]matriz = {{1,2,3},{1,2,4},{1,2,3}};
        int termo = 3;
        int[][]expected = {{3,2,3},{3,2,4},{3,2,3}};
        //act
        int[][] result = Main.multiplicarTermoLinhaPelaMatriz(matriz,termo);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void multiplicarTermoLinhaPelaMatriz_teste2(){
        //arrange
        int[][]matriz = {{1,2,3},{3,2,4},{4,2,3}};
        int termo = 2;
        int[][]expected = {{2,2,3},{6,2,4},{8,2,3}};
        //act
        int[][] result = Main.multiplicarTermoLinhaPelaMatriz(matriz,termo);
        //assert
        assertArrayEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void teste(){
        //arrange
        double [][]matriz = {{1,8,3,5,0},
                {0,-1,7,9,1},
                {0,0,3,2,4},
                {0,0,0,-6,-1},
                {0,0,0,0,2}};
        int n = 5;
       double expected = 6;
        //act
        double result = parte2.detLaplace(n, matriz);
        //assert
        assertEquals(expected, result);
    }



    /**
     * Testes Exercicio17.a
     */
    @org.junit.jupiter.api.Test
    void getMultiplicarMatrizPorConstante_MatrizQuadrada(){
        //arrange
        int [][] matriz = {{1,2,3},{2,4,3},{1,3,4}};
        int number = 2;
        int[][] expected = {{2,4,6},{4,8,6},{2,6,8}};
        //act
        int [][]result = Main.getMultiplicarMatrizPorConstante(matriz,number);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMultiplicarMatrizPorConstante_MatrizRetangular(){
        //arrange
        int [][] matriz = {{1,2,3},{2,4,3}};
        int number = 2;
        int[][] expected = {{2,4,6},{4,8,6}};
        //act
        int [][]result = Main.getMultiplicarMatrizPorConstante(matriz,number);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMultiplicarMatrizPorConstante_MatrizNaoUniforme(){
        //arrange
        int [][] matriz = {{1,2,3},{2,4,3,2}};
        int number = 2;
        int[][] expected = {{2,4,6},{4,8,6,4}};
        //act
        int [][]result = Main.getMultiplicarMatrizPorConstante(matriz,number);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMultiplicarMatrizPorConstante_MatrizVazia(){
        //arrange
        int [][] matriz = {};
        int number = 2;
        int[][] expected = {};
        //act
        int [][]result = Main.getMultiplicarMatrizPorConstante(matriz,number);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getMultiplicarMatrizPorConstante_MatrizNull(){
        //arrange
        int [][] matriz = null;
        int number = 2;
        int[][] expected = null;
        //act
        int [][]result = Main.getMultiplicarMatrizPorConstante(matriz,number);
        //assert
        assertArrayEquals(expected, result);
    }


    /**
     * Testes Exercicio17.b
     */
    @org.junit.jupiter.api.Test
    void getSeDuasMatrizComTamanhoIguaisMatrizesQuadradas(){
        //arrange
        int [][] matriz1 = {{1,2,3},{2,4,3},{1,3,4}};
        int [][] matriz2 = {{2,2,5},{2,3,1},{1,2,4}};
        //act
        boolean result = Main.getSeDuasMatrizComTamanhoIguais(matriz1,matriz2);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeDuasMatrizComTamanhoIguaisMatrizesRetangulas(){
        //arrange
        int [][] matriz1 = {{1,2,3},{2,4,3}};
        int [][] matriz2 = {{2,2,5},{2,3,1}};
        //act
        boolean result = Main.getSeDuasMatrizComTamanhoIguais(matriz1,matriz2);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeDuasMatrizComTamanhoIguaisMatrizesNaoUniformesIguias(){
        //arrange
        int [][] matriz1 = {{1,2,3,2},{2,4,3}};
        int [][] matriz2 = {{2,2,5,3},{2,3,1}};
        //act
        boolean result = Main.getSeDuasMatrizComTamanhoIguais(matriz1,matriz2);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeDuasMatrizComTamanhoIguaisMatrizesDiferentes(){
        //arrange
        int [][] matriz1 = {{1,2,3,2,3},{2,4,3}};
        int [][] matriz2 = {{2,2,5,3},{2,3,1}};
        //act
        boolean result = Main.getSeDuasMatrizComTamanhoIguais(matriz1,matriz2);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeDuasMatrizComTamanhoIguaisMatrizesNulls(){
        //arrange
        int [][] matriz1 = null;
        int [][] matriz2 ={{2,2,5,3},{2,3,1}};
        //act
        boolean result = Main.getSeDuasMatrizComTamanhoIguais(matriz1,matriz2);
        //assert
        assertFalse(result);
    }

    @org.junit.jupiter.api.Test
    void getSomaDeDuasMatrizes_MatrizQuadrada(){
        //arrange
        int [][] matriz1 = {{1,2,3},{2,4,3},{1,3,4}};
        int [][] matriz2 = {{2,2,5},{2,3,1},{1,2,4}};
        int[][] expected = {{3,4,8},{4,7,4},{2,5,8}};
        //act
        int [][]result = Main.getSomaDeDuasMatrizes(matriz1,matriz2);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getSomaDeDuasMatrizes_MatrizRetangular(){
        //arrange
        int [][] matriz1 = {{1,2,3},{1,4,3}};
        int [][] matriz2 = {{3,3,3},{2,-4,3}};
        int[][] expected = {{4,5,6},{3,0,6}};
        //act
        int [][]result = Main.getSomaDeDuasMatrizes(matriz1,matriz2);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getSomaDeDuasMatrizes_MatrizesNaoUniformeIguais(){
        //arrange
        int [][] matriz1 = {{1,2,3,4,5},{2,4,3,2}};
        int [][] matriz2 = {{1,2,3,3,1},{2,4,3,2}};
        int[][] expected = {{2,4,6,7,6},{4,8,6,4}};
        //act
        int [][]result = Main.getSomaDeDuasMatrizes(matriz1,matriz2);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getSomaDeDuasMatrizes_MatrizesNaoUniformeDiferentes(){
        //arrange
        int [][] matriz1 = {{1,2,3},{2,4,3,2}};
        int [][] matriz2 = {{1,2,3,4},{2,4,3,2}};
        int[][] expected = null;
        //act
        int [][]result = Main.getSomaDeDuasMatrizes(matriz1,matriz2);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getSomaDeDuasMatrize_MatrizVazia(){
        //arrange
        int [][] matriz1 = {};
        int [][] matriz2 = {};
        int[][] expected = {};
        //act
        int [][]result = Main.getSomaDeDuasMatrizes(matriz1,matriz2);
        //assert
        assertArrayEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void  getSomaDeDuasMatriz_MatrizNull(){
        //arrange
        int [][] matriz1 = null;
        int [][] matriz2 = null;
        int[][] expected = null;
        //act
        int [][]result = Main.getSomaDeDuasMatrizes(matriz1,matriz2);
        //assert
        assertArrayEquals(expected, result);
    }

    /**
     * Testes Exercicio17.c
     */
    @org.junit.jupiter.api.Test
    void getSeMatrizUniforme_MatrizUniforme(){
        //arrange
        int[][]matriz = {{1,2,3},{1,2,5}};
        //act
        boolean result = Main.getSeMatrizUniforme(matriz);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizUniforme_MatrizNaoUniforme(){
        //arrange
        int[][]matriz = {{1,2,3,2},{1,2,5}};
        //act
        boolean result = Main.getSeMatrizUniforme(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizUniforme_MatrizNull(){
        //arrange
        int[][]matriz =null;
        //act
        boolean result = Main.getSeMatrizUniforme(matriz);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSeMatrizUniforme_MatrizVazia(){
        //arrange
        int[][]matriz = {};
        //act
        boolean result = Main.getSeMatrizUniforme(matriz);
        //assert
        assertFalse(result);
    }

    @org.junit.jupiter.api.Test
    void getSePossivelMultiplocarMatrizes_MatrizesPossiveisteste1(){
        //arrange
        int[][]matriz1 = {{1,2,3},{1,2,5}};
        int[][]matriz2 = {{1,2},{1,2},{2,3}};
        //act
        boolean result = Main.getSePossivelMultiplocarMatrizes(matriz1,matriz2);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSePossivelMultiplocarMatrizes_MatrizesPossiveisteste2(){
        //arrange
        int[][]matriz1 = {{1,2,3},{1,2,5},{2,4,3}};
        int[][]matriz2 = {{1,2,8},{1,2,3},{1,4,5}};
        //act
        boolean result = Main.getSePossivelMultiplocarMatrizes(matriz1,matriz2);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSePossivelMultiplocarMatrizes_MatrizesPossiveisteste3(){
        //arrange
        int[][]matriz1 ={{1,5},{2,4},{3,1}};
        int[][]matriz2 ={{1,2,3,4},{5,6,7,8}};
        //act
        boolean result = Main.getSePossivelMultiplocarMatrizes(matriz1,matriz2);
        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void getSePossivelMultiplocarMatrizes_MatrizesNaoUniformes(){
        //arrange
        int[][]matriz1 = {{1,2,3,3},{1,2,5},{2,4,3}};
        int[][]matriz2 = {{1,2,8,3},{1,2,3},{1,4,5}};
        //act
        boolean result = Main.getSePossivelMultiplocarMatrizes(matriz1,matriz2);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSePossivelMultiplocarMatrizes_MatrizesNull(){
        //arrange
        int[][]matriz1 = null;
        int[][]matriz2 =null;
        //act
        boolean result = Main.getSePossivelMultiplocarMatrizes(matriz1,matriz2);
        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void getSePossivelMultiplocarMatrizes_MatrizesVazias(){
        //arrange
        int[][]matriz1 = {};
        int[][]matriz2 ={};
        //act
        boolean result = Main.getSePossivelMultiplocarMatrizes(matriz1,matriz2);
        //assert
        assertFalse(result);
    }

    @org.junit.jupiter.api.Test
    void getCalculoAuxiliarProdutoMatriz_teste1(){
        //arrange
        int[][]matriz1={{1,5},{2,4},{3,1}};
        int[][]matriz2= {{1,2,3,4},{5,6,7,8}};
        int linhaMatrizResultado=2;
        int colunaMatrizResultado=3;
        int expected = 20;
        //act
        int result =Main.getCalculoAuxiliarProdutoMatriz(matriz1,matriz2,linhaMatrizResultado,colunaMatrizResultado);
        //assert
        assertEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getCalculoAuxiliarProdutoMatriz_teste2(){
        //arrange
        int[][]matriz1={{1,5},{2,4},{3,1}};
        int[][]matriz2= {{1,2,3,4},{5,6,7,8}};
        int linhaMatrizResultado=2;
        int colunaMatrizResultado=2;
        int expected = 16;
        //act
        int result =Main.getCalculoAuxiliarProdutoMatriz(matriz1,matriz2,linhaMatrizResultado,colunaMatrizResultado);
        //assert
        assertEquals(expected,result);
    }

    @org.junit.jupiter.api.Test
    void getProdutoMatrizesInteiras_teste1(){
        //arrange
        int[][]matriz1={{1,5},{2,4},{3,1}};
        int[][]matriz2= {{1,2,3,4},{5,6,7,8}};
        int[][] expected ={{26,32,38,44},{22,28,34,40},{8,12,16,20}};
        //act
        int[][]  result =Main.getProdutoMatrizesInteiras(matriz1,matriz2);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getProdutoMatrizesInteiras_teste2(){
        //arrange
        int[][]matriz1={{1,5,2},{2,4,2},{3,1,2}};
        int[][]matriz2= {{1,2,3,4,1},{5,6,7,8,2},{2,1,1,4,3}};
        int[][] expected ={{30,34,40,52,17},{26,30,36,48,16},{12,14,18,28,11}};
        //act
        int[][]  result =Main.getProdutoMatrizesInteiras(matriz1,matriz2);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getProdutoMatrizesInteiras_MatrizNull(){
        //arrange
        int[][]matriz1=null;
        int[][]matriz2= null;
        int[][] expected =null;
        //act
        int[][]  result =Main.getProdutoMatrizesInteiras(matriz1,matriz2);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getProdutoMatrizesInteiras_ProdutoImpossivel(){
        //arrange
        int[][]matriz1={{1,5},{2,4},{3,1}};
        int[][]matriz2= {{1,5,2},{2,4,2},{3,1,2}};
        int[][] expected =null;
        //act
        int[][]  result =Main.getProdutoMatrizesInteiras(matriz1,matriz2);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getProdutoMatrizesInteiras_MatrizElementosNegativos(){
        //arrange
        int[][]matriz1={{1,-5},{-2,4}};
        int[][]matriz2= {{1,2},{5,6}};
        int[][] expected ={{-24,-28},{18,20}};
        //act
        int[][]  result =Main.getProdutoMatrizesInteiras(matriz1,matriz2);
        //assert
        assertArrayEquals(expected,result);
    }
    @org.junit.jupiter.api.Test
    void getProdutoMatrizesInteiras_MatrizesVazias(){
        //arrange
        int[][]matriz1={};
        int[][]matriz2= {};
        int[][] expected =null;
        //act
        int[][]  result =Main.getProdutoMatrizesInteiras(matriz1,matriz2);
        //assert
        assertArrayEquals(expected,result);
    }
}
