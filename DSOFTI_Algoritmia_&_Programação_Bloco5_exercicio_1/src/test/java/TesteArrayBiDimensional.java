import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TesteArrayBiDimensional {

    //alinea.a
    @Test
    void ArrayUnidimensional_EmptyConstructor() {
        //arrange
        int[][] expected = {};
        //act
        ArrayBiDimensional matrix = new ArrayBiDimensional();
        int[][] result = matrix.toArray();
        //assert
        assertArrayEquals(expected, result);
    }

    //alinea.b
    @Test
    void ArrayUnidimensional_ConstructorFromUniformArray() {
        //arrange
        int[][] expected = {{2, 3, 4}, {1, 2, 3}, {1, 2, 3}};
        //act
        int[][] initialMatrix = {{2, 3, 4}, {1, 2, 3}, {1, 2, 3}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][] result = matrix.toArray();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void ArrayUnidimensional_ConstructorFromNotUniformArray() {
        //arrange
        int[][] expected = {{2, 3, 4}, {1, 2}, {1, 2, 3, 3}};
        //act
        int[][] initialMatrix = {{2, 3, 4}, {1, 2}, {1, 2, 3, 3}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][] result = matrix.toArray();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void ArrayUnidimensional_ConstructorFromNullArray() {
        //arrange
        int[][] expected = {};
        //act
        int[][] initialMatrix = null;
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][] result = matrix.toArray();
        //assert
    }

    //alinea.c
    @Test
    void addValueOnMatrix_addValueToExistentLineMatrix() {
        //arrange
        int lineIndex = 2;
        int newNumber = 24;
        int[][] expected = {{1, 4, 5}, {5, 6, 8, 6}, {4, 5, 24}};
        //act
        int[][] intialArray = {
                {1, 4, 5},
                {5, 6, 8, 6},
                {4, 5}
        };
        ArrayBiDimensional matrix = new ArrayBiDimensional(intialArray);
        matrix.addValueOnMatrix(lineIndex, newNumber);
        int[][] result = matrix.toArray();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void addValueOnMatrix_addValueToNextNotExistentLineMatrix() {
        //arrange
        int lineIndex = 3;
        int newNumber = 24;
        int[][] expected = {{1, 4, 5}, {5, 6, 8, 6}, {4, 5}, {24}};
        //act
        int[][] intialArray = {
                {1, 4, 5},
                {5, 6, 8, 6},
                {4, 5}
        };
        ArrayBiDimensional matrix = new ArrayBiDimensional(intialArray);
        matrix.addValueOnMatrix(lineIndex, newNumber);
        int[][] result = matrix.toArray();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void addValueOnMatrix_addValueToEmptyMatrix() {
        //arrange
        int lineIndex = 0;
        int newNumber = 24;
        int[][] expected = {{24}};
        //act
        int[][] intialArray = null;
        ArrayBiDimensional matrix = new ArrayBiDimensional(intialArray);
        matrix.addValueOnMatrix(lineIndex, newNumber);
        int[][] result = matrix.toArray();
        //assert
        assertArrayEquals(expected, result);
    }

    //alinea.d
    @Test
    void removeEmptyLine_index2() {
        //arrange
        int[][] initialMatrix = {{12, 3}, {1, 4, 5}, {}, {1, 5}};
        int[][] expected = {{12, 3}, {1, 4, 5}, {1, 5}};
        //act
        int[][] result = Utilities.removeFirstEmptyLine(initialMatrix);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void removeEmptyLine_index0() {
        //arrange
        int[][] initialMatrix = {{}, {1, 4, 5}, {1, 2}, {1, 5}};
        int[][] expected = {{1, 4, 5}, {1, 2}, {1, 5}};
        //act
        int[][] result = Utilities.removeFirstEmptyLine(initialMatrix);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void removeEmptyLine_index3() {
        //arrange
        int[][] initialMatrix = {{1, 4, 5}, {1, 2}, {}};
        int[][] expected = {{1, 4, 5}, {1, 2}};
        //act
        int[][] result = Utilities.removeFirstEmptyLine(initialMatrix);
        //assert
        assertArrayEquals(expected, result);
    }

    @Test
    void removeEmptyLine_noIndex() {
        //arrange
        int[][] initialMatrix = {{1, 4, 5}, {1, 2}, {1, 3}};
        int[][] expected = {{1, 4, 5}, {1, 2}, {1, 3}};
        //act
        int[][] result = Utilities.removeFirstEmptyLine(initialMatrix);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void removeEmptyLine_onlyIndex() {
        //arrange
        int[][] initialMatrix = {{}};
        int[][] expected = {};
        //act
        int[][] result = Utilities.removeFirstEmptyLine(initialMatrix);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void valueExistsInMatrix_True() {
        //arrange
        int[][] intialMattrix = {{1, 2, 3}, {2, 3, 6}, {5, 6, 4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(intialMattrix);
        int removeValue = 6;
        int[][] expected = {{1, 2, 3}, {2, 3}, {5, 6, 4}};
        //act
        matrix.removeValueOnMatrix(removeValue);
        int[][] result = matrix.toArray();
        //assertTrue
        assertArrayEquals(expected, result);
    }
    @Test
    void valueExistsInMatrix_TrueAndRemoveLine() {
        //arrange
        int[][] intialMattrix = {{1, 2, 3}, {6}, {5, 6, 4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(intialMattrix);
        int removeValue = 6;
        int[][] expected = {{1, 2, 3}, {5, 6, 4}};
        //act
        matrix.removeValueOnMatrix(removeValue);
        int[][] result = matrix.toArray();
        //assertTrue
        assertArrayEquals(expected, result);
    }
    @Test
    void valueExistsInMatrix_False() {
        //arrange
        int[][] intialMattrix = {{1, 2, 3}, {6}, {5, 6, 4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(intialMattrix);
        int removeValue = 7;
        int[][] expected = {{1, 2, 3}, {6}, {5, 6, 4}};
        //act
        matrix.removeValueOnMatrix(removeValue);
        int[][] result = matrix.toArray();
        //assertTrue
        assertArrayEquals(expected, result);
    }

    //alinea.e
    @Test
    void matrixEmpty_true() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.matrixEmpty();
        //assert
        assertTrue(result);
    }
    @Test
    void matrixEmpty_false1() {
        //arrange
        int[][] initialMatrix = {{}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.matrixEmpty();
        //assert
        assertFalse(result);
    }
    @Test
    void matrixEmpty_false2() {
        //arrange
        int[][] initialMatrix = {{1, 2, 3}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.matrixEmpty();
        //assert
        assertFalse(result);
    }

    //alinea.f
    @Test
    void highestElement() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {-3, 5, 8}, {12, 4, 6}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 12;
        //act
        int result = matrix.highestElement();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void highestElement_EmptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act

        //assert
        assertThrows(IllegalArgumentException.class, () -> matrix.highestElement());
    }
    @Test
    void highestElement_OneElement() {
        //arrange
        int[][] initialMatrix = {{6}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 6;
        //act
        int result = matrix.highestElement();
        //assert
        assertEquals(expected, result);
    }

    //alinea.g
    @Test
    void lowestElement() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {-3, 5, 8}, {12, 4, 6}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = -3;
        //act
        int result = matrix.lowestElement();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void lowestElement_EmptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act

        //assert
        assertThrows(IllegalArgumentException.class, () -> matrix.lowestElement());
    }
    @Test
    void lowestElement_OneElement() {
        //arrange
        int[][] initialMatrix = {{6}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 6;
        //act
        int result = matrix.lowestElement();
        //assert
        assertEquals(expected, result);
    }

    //alinea.h
    @Test
    void elementsAverage_positiveElements() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {0, 5, 8}, {2, 4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        double expected = 3.375;
        //act
        double result = matrix.elementsAverage();
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void elementsAverage_negativeElemens() {
        //arrange
        int[][] initialMatrix = {{-1, 2, 5}, {-2, 5, 8}, {2, 4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        double expected = 2.875;
        //act
        double result = matrix.elementsAverage();
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void elementsAverage_emptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act

        //assert
        assertThrows(IllegalArgumentException.class, () -> matrix.elementsAverage());
    }

    //alinea.i
    @Test
    void sumLineIndexElements_positiveElements() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {0, 5, 8}, {2, 4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[] expected = {8, 13, 6};
        //act
        int[] result = matrix.sumLineIndexElements();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void sumLineIndexElements_negativeElemens() {
        //arrange
        int[][] initialMatrix = {{-7, 2, 5}, {-2, 5, 8}, {2, 4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[] expected = {0, 11, 6};
        //act
        int[] result = matrix.sumLineIndexElements();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void sumLineIndexElements_emptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[] expected = {};
        //act
        int[] result = matrix.sumLineIndexElements();
        //assert
        assertArrayEquals(expected, result);
    }

    //alinea.j
    @Test
    void sumOfEachColumn_positiveElementsUniformMatrix() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4, 1}, {0, 5, 8}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[] expected = {3, 11, 14};
        //act
        int[] result = matrix.sumOfEachColumn();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void sumOfEachColumn_positiveElementsNotUniformMatrix() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4}, {0, 5, 8, 6}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[] expected = {3, 11, 13, 6};
        //act
        int[] result = matrix.sumOfEachColumn();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void sumOfEachColumn_negativeElements() {
        //arrange
        int[][] initialMatrix = {{-7, 2, 5}, {-2, 5, 8}, {2, 4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[] expected = {-7, 11, 13};
        //act
        int[] result = matrix.sumOfEachColumn();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void sumOfEachColumn_emptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[] expected = {0};
        //act
        int[] result = matrix.sumOfEachColumn();
        //assert
        assertArrayEquals(expected, result);
    }

    //alinea.k
    @Test
    void indexLineOfHighestSumLine_positiveElementsUniformMatrix() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4, 1}, {0, 5, 8}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 2;
        //act
        int result = matrix.indexLineOfHighestSumLine();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void indexLineOfHighestSumLine_positiveElementsNotUniformMatrix() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4}, {0, 5, 8, 6}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 2;
        //act
        int result = matrix.indexLineOfHighestSumLine();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void indexLineOfHighestSumLine_positiveElementsUniformMatrixDuplicateMax() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4, 5}, {2, 4, 5}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 1;           //vai dar a primeira linha que é máx.
        //act
        int result = matrix.indexLineOfHighestSumLine();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void indexLineOfHighestSumLine_negativeElements() {
        //arrange
        int[][] initialMatrix = {{-7, 2, 5}, {-2, 5, 8}, {2, 4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 1;
        //act
        int result = matrix.indexLineOfHighestSumLine();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void indexLineOfHighestSumLine_emptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = -1;
        //act
        int result = matrix.indexLineOfHighestSumLine();
        //assert
        assertEquals(expected, result);
    }

    //alinea.l
    @Test
    void squareMatrixUniformMatrixNotSquare() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4, 1}, {0, 5, 8}, {1, 2, 3}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.squareMatrix();
        //assert
        assertFalse(result);
    }
    @Test
    void squareMatrixUniformMatrixSquare() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4, 1}, {0, 5, 8}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.squareMatrix();
        //assert
        assertTrue(result);
    }
    @Test
    void squareMatrixEmptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.squareMatrix();
        //assert
        assertFalse(result);
    }

    //alinea.m
    @Test
    void symmetricMatrixTrue() {
        //arrange
        int[][] initialMatrix = {{2, -1, 0}, {-1, 4, 7}, {0, 7, 8}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.symmetricMatrix();
        //assert
        assertTrue(result);
    }                                                           //Duvida!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    @Test
    void symmetricMatrixFalseNotSquareMatrix() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4, 1}, {0, 5, 8}, {1, 2}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.symmetricMatrix();
        //assert
        assertFalse(result);
    }
    @Test
    void symmetricMatrixFalse() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4, 1}, {0, 5, 8}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.symmetricMatrix();
        //assert
        assertFalse(result);
    }
    @Test
    void symmetricMatrixEmptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.symmetricMatrix();
        //assert
        assertFalse(result);
    }

    //alinea.n
    @Test
    void notNullElementsInMatrix_SquareMatrixDiagonalSomeZeros() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4, 1}, {0, 3, 0}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 2;
        //act
        int result = matrix.countNotNullElementsInMatrixDiagonal();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void notNullElementsInMatrix_SquareMatrixDiagonalNoneZeros() {
        //arrange
        int[][] initialMatrix = {{0, 2, 5}, {2, 0, 1}, {0, 5, 0}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 0;
        //act
        int result = matrix.countNotNullElementsInMatrixDiagonal();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void notNullElementsInMatrix_NotSquareMatrix() {
        //arrange
        int[][] initialMatrix = {{1, 2, 5}, {2, 4, 1}, {0, 5, 8}, {1, 5}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = -1;
        //act
        int result = matrix.countNotNullElementsInMatrixDiagonal();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void notNullElementsInMatrixEmptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = -1;
        //act
        int result = matrix.countNotNullElementsInMatrixDiagonal();
        //assert
        assertEquals(expected, result);
    }

    //alinea.o
    @Test
    void equalMainAndSecondaryDiagonalMatrixTrueSquareMAtrix() {
        //arrange
        int[][] initialMatrix = {{2, 3, 2}, {5, 4, 9}, {3, 6, 3}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.equalMainAndSecondaryDiagonalMatrix();
        //assert
        assertTrue(result);
    }
    @Test
    void equalMainAndSecondaryDiagonalMatrixFalseSquareMatrix() {
        //arrange
        int[][] initialMatrix = {{2, 3, 2}, {5, 4, 9}, {1, 6, 3}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.equalMainAndSecondaryDiagonalMatrix();
        //assert
        assertFalse(result);
    }
    @Test
    void equalMainAndSecondaryDiagonalMatrixTrueRectangularMatrix() {
        //arrange
        int[][] initialMatrix = {{1, 2, 3, 1}, {4, 5, 5, 2}, {3, 3, 3, 2}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.equalMainAndSecondaryDiagonalMatrix();
        //assert
        assertTrue(result);
    }
    @Test
    void equalMainAndSecondaryDiagonalMatrixFalseRectangulareMatrix() {
        //arrange
        int[][] initialMatrix = {{1, 2, 3, 1}, {4, 5, 5, 2}, {3, 3, 1, 1}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.equalMainAndSecondaryDiagonalMatrix();
        //assert
        assertFalse(result);
    }
    @Test
    void equalMainAndSecondaryDiagonalMatrixFalseNotUniformMatrix() {
        //arrange
        int[][] initialMatrix = {{2, 3, 2}, {5, 4, 9, 2}, {1, 6, 3}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.equalMainAndSecondaryDiagonalMatrix();
        //assert
        assertFalse(result);
    }
    @Test
    void equalMainAndSecondaryDiagonalMatrixEmptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        //act
        boolean result = matrix.equalMainAndSecondaryDiagonalMatrix();
        //assert
        assertFalse(result);
    }

    //alinea.p
    @Test
    void averageElementsDigitsInMatrix() {
        //arrange
        int[][] initialMatrix = {{2, 3, 22}, {122, 4, 32}, {5}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 1;
        //act
        int result = matrix.averageElementsDigitsInMatrix();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void averageElementsDigitsInMatrixEmpty() {
        //arrange
        int[][] initialMatrix = null;
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int expected = 0;
        //act
        int result = matrix.averageElementsDigitsInMatrix();
        //assert
        assertEquals(expected, result);
    }

    @Test
    void addToMatrix_IndexWithinMatrix() {
        //arrange
        int[][] initialMatrix = {{1, 2, 3}, {2, 4, 5, 1}, {1, 2}};
        int index = 2;
        int newNumber = 4;
        int[][] expected = {{1, 2, 3}, {2, 4, 5, 1}, {1, 2, 4}};
        //act
        int[][] result = Utilities.addToMatrix(initialMatrix, index, newNumber);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void addToMatrix_IndexOutMatrix() {
        //arrange
        int[][] initialMatrix = {{1, 2, 3}, {2, 4, 5, 1}, {1, 2}};
        int index =5;
        int newNumber = 4;
        int[][] expected = {{1, 2, 3}, {2, 4, 5, 1}, {1, 2},{4}};
        //act
        int[][] result = Utilities.addToMatrix(initialMatrix, index, newNumber);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void addToMatrix_EmptyMatrix() {
        //arrange
        int[][] initialMatrix = {};
        int index =5;
        int newNumber = 4;
        int[][] expected = {{4}};
        //act
        int[][] result = Utilities.addToMatrix(initialMatrix, index, newNumber);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void addToMatrix_EmptyLineMatrix() {
        //arrange
        int[][] initialMatrix = {{}};
        int index =5;
        int newNumber = 4;
        int[][] expected = {{},{4}};
        //act
        int[][] result = Utilities.addToMatrix(initialMatrix, index, newNumber);
        //assert
        assertArrayEquals(expected, result);
    }

    @Test
    void matrixElementsWithNumberDigitSuperiorMediaOneValue(){
        //arrange
        int[][] initialMatrix = {{22, 33, 22}, {122, 4, 32}, {55}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][] expected = {{122}};
        //act
        int[][] result = matrix.matrixElementsWithNumberDigitSuperiorMedia();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void matrixElementsWithNumberDigitSuperiorMediaTwoValues(){
        //arrange
        int[][] initialMatrix = {{22, 33, 221}, {122, 4, 32}, {55}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][] expected = {{221},{122}};
        //act
        int[][] result = matrix.matrixElementsWithNumberDigitSuperiorMedia();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void matrixElementsWithNumberDigitSuperiorMediaTwoValuesSameLine(){
        //arrange
        int[][] initialMatrix = {{22, 333, 221}, {122, 4, 32}, {55}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][] expected = {{333,221},{122}};
        //act
        int[][] result = matrix.matrixElementsWithNumberDigitSuperiorMedia();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void matrixElementsWithNumberDigitSuperiorMediaEmpty(){
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][] expected = {};
        //act
        int[][] result = matrix.matrixElementsWithNumberDigitSuperiorMedia();
        //assert
        assertArrayEquals(expected, result);
    }

    //alinea.q
    @Test
    void evenDigitsPercentageAverageInMatrixElements(){
        //arrange
        int[][] initialMatrix={{2,32,4,5},{3,22,54,2009}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        double expected = 59.375;
        //act
        double result = matrix.evenDigitsPercentageAverageInMatrixElements();
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void evenDigitsPercentageAverageInMatrixElementsEmptyMatrix(){
        //arrange
        int[][] initialMatrix= null;
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        double expected = 0;
        //act
        double result = matrix.evenDigitsPercentageAverageInMatrixElements();
        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void elementsWithHigherPercentageEvenNumbersSomeValues(){
        //arrange
        int[][] initialMatrix={{2,32,4,5},{3,22,54,2009}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][] expected = {{2,4},{22,2009}};
        //act
        int[][] result = matrix.elementsWithHigherPercentageEvenNumbers();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void elementsWithHigherPercentageEvenNumbersAnyValues(){
        //arrange
        int[][] initialMatrix={{1,3315,1,5},{3,11,5351,1}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][] expected = {};
        //act
        int[][] result = matrix.elementsWithHigherPercentageEvenNumbers();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void elementsWithHigherPercentageEvenNumbersEmptyMatrix(){
        //arrange
        int[][] initialMatrix=null;
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][] expected = {};
        //act
        int[][] result = matrix.elementsWithHigherPercentageEvenNumbers();
        //assert
        assertArrayEquals(expected, result);
    }

    //alinea.r
    @Test
    void invertLineElementsNotUniformMatrix1(){
        //arrange
        int[][]initialMatrix = {{1,2,4},{4,6},{3,4,4,9}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{4,2,1},{6,4},{9,4,4,3}};
        //act
        matrix.invertLineElements();
        int[][]result=matrix.toArray();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void invertLineElementsNotUniformMatrix2(){
        //arrange
        int[][]initialMatrix = {{1,2,4},{4},{3,4,4,9}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{4,2,1},{4},{9,4,4,3}};
        //act
        matrix.invertLineElements();
        int[][]result=matrix.toArray();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void invertLineElementsEmptyMatrix(){
        //arrange
        int[][]initialMatrix = null;
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {};
        //act
        matrix.invertLineElements();
        int[][]result=matrix.toArray();
        //assert
        assertArrayEquals(result,expected);
    }

    //alinea.s
    @Test
    void invertColumnsNotUniformMatrix1(){
        //arrange
        int[][]initialMatrix = {{1,2,4},{4,6},{3,4,4,9}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{3,4,4,9},{4,6},{1,2,4}};
        //act
        matrix.invertMatrixLines();
        int[][]result=matrix.toArray();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void invertMatrixLinesNotUniformMatrix2(){
        //arrange
        int[][]initialMatrix = {{1,2,4},{4},{3,4,4,9}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{3,4,4,9},{4},{1,2,4}};
        //act
        matrix.invertMatrixLines();
        int[][] result = matrix.toArray();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void invertMatrixLinesEmptyMatrix(){
        //arrange
        int[][]initialMatrix = null;
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {};
        //act
        matrix.invertMatrixLines();
        int[][]result=matrix.toArray();
        //assert
        assertArrayEquals(result,expected);
    }

    //alinea.t
    @Test
    void rotatePositive90DegSquareMatrix() {
        int[][]initialMatrix = {{1, 2, 3},{4,5,6},{7,8,9}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{3,6,9},{2,5,8},{1,4,7}};
        //act
        matrix.rotatePositive90Deg();
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void rotatePositive90DegRectangularMatrix() {
        int[][]initialMatrix = {{1, 2, 3},{4,5,6},{7,8,9},{5,6,4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{3,6,9,4},{2,5,8,6},{1,4,7,5}};
        //act
        matrix.rotatePositive90Deg();
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void rotatePositive90DegNotUniformMatrix() {
        int[][]initialMatrix = {{1,2,3},{4,5,6},{7,8,9,3}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{1,2,3},{4,5,6},{7,8,9,3}};
        //act
        matrix.rotatePositive90Deg();                       //it won't rotate
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void rotatePositive90DegEmptyMatrix() {
        int[][]initialMatrix = null;
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {};
        //act
        matrix.rotatePositive90Deg();                       //it won't rotate
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }

    //alinea.u
    @Test
    void rotatePositive180DegSquareMatrix() {
        int[][]initialMatrix = {{1, 2, 3},{4,5,6},{7,8,9}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{9,8,7},{6,5,4},{3,2,1}};
        //act
        matrix.rotatePositive180Deg();
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void rotatePositive180DegRectangularMatrix() {
        int[][]initialMatrix = {{1, 2, 3},{4,5,6},{7,8,9},{5,6,4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{4,6,5},{9,8,7},{6,5,4},{3,2,1}};
        //act
        matrix.rotatePositive180Deg();
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void rotatePositive180DegNotUniformMatrix() {
        int[][]initialMatrix = {{1, 2, 3},{4,5,6},{7,8,9,3}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{1, 2, 3},{4,5,6},{7,8,9,3}};
        //act
        matrix.rotatePositive180Deg();                       //it won't rotate
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void rotatePositive180DegEmptyMatrix() {
        int[][]initialMatrix = null;
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {};
        //act
        matrix.rotatePositive180Deg();                       //it won't rotate
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }

    //alinea.v
    @Test
    void rotateNegative90DegSquareMatrix() {
        int[][]initialMatrix = {{1,2,3},{4,5,6},{7,8,9}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{7,4,1},{8,5,2},{9,6,3}};
        //act
        matrix.rotateNegative90Deg();
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void rotateNegative90DegRectangularMatrix() {
        int[][]initialMatrix = {{1, 2, 3},{4,5,6},{7,8,9},{5,6,4}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{5,7,4,1},{6,8,5,2},{4,9,6,3}};
        //act
        matrix.rotateNegative90Deg();
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void rotateNegative90DegNotUniformMatrix() {
        int[][]initialMatrix = {{1, 2, 3},{4,5,6},{7,8,9,3}};
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {{1, 2, 3},{4,5,6},{7,8,9,3}};
        //act
        matrix.rotateNegative90Deg();                       //it won't rotate
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void rotateNegative90DegEmptyMatrix() {
        int[][]initialMatrix = null;
        ArrayBiDimensional matrix = new ArrayBiDimensional(initialMatrix);
        int[][]expected = {};
        //act
        matrix.rotateNegative90Deg();                       //it won't rotate
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected,result);
    }



}
