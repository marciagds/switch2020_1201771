import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class TesteArrayBiDimensional_v2 {
    //alinea a & b
    //Equal Method ans Constructor Tests
    @Test
    void equalsMethod(){
        //arrange
        int[][] initialMatrix = {{1,2,3},{4,5,6}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int [][] initialExpected = {{1,2,3},{4,5,6}};
        ArrayBiDimensional_v2 expected =new ArrayBiDimensional_v2(initialExpected);
        //act

        //assert
        assertEquals(matrix,expected);
    }
    @Test
    void equalsMethod_EmptyMatrix(){
        //arrange
        int[][] initialMatrix = {};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int [][] initialExpected = {};
        ArrayBiDimensional_v2 expected =new ArrayBiDimensional_v2(initialExpected);
        //act

        //assert
        assertEquals(matrix,expected);
    }
    @Test
    void equalsMethod_NullMatrix(){
        //arrange
        int[][] initialMatrix = null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int [][] initialExpected = {};
        ArrayBiDimensional_v2 expected =new ArrayBiDimensional_v2(initialExpected);
        //act

        //assert
        assertEquals(matrix,expected);
    }

    @Test
    void toArray() {
        //arrange
        int[][]initialmatrix = {{1,2,3},{2,4},{5,6,7,8}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialmatrix);
        int[][]expected = {{1,2,3},{2,4},{5,6,7,8}};
        //act
        int[][]result = matrix.toArray();
        //assert
        assertArrayEquals(expected, result);
    }

    //alinea.c
    @Test
    void addToMatrix_IndexWithinMatrixLength(){
        //arrange
        int indexLine =2;
        int newNumber = 4;
        int [][] initialExpected = {{1,2,3},{4,5},{1,2,4},{2,3,4}};
        ArrayBiDimensional_v2 expected = new ArrayBiDimensional_v2(initialExpected);
        int[][] intialMatrix={{1,2,3},{4,5},{1,2},{2,3,4}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);

        //act
        matrix.addToMatrix(indexLine,newNumber);
        ArrayBiDimensional_v2 result = matrix;

       //assert
        assertEquals(expected,result);
    }
    @Test
    void addToMatrix_IndexOutMatrixLength(){
        //arrange
        int indexLine =5;
        int newNumber = 4;
        int [][] initialExpected = {{1,2,3},{4,5},{1,2},{2,3,4},{4}};
        ArrayBiDimensional_v2 expected = new ArrayBiDimensional_v2(initialExpected);
        int[][] intialMatrix={{1,2,3},{4,5},{1,2},{2,3,4}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);

        //act
        matrix.addToMatrix(indexLine,newNumber);
        ArrayBiDimensional_v2 result = matrix;

        //assert
        assertEquals(expected,result);
    }
    @Test
    void addToMatrix_IndexMatrixEmpty(){
        //arrange
        int indexLine =0;
        int newNumber = 4;
        int [][] initialExpected = {{4}};
        ArrayBiDimensional_v2 expected = new ArrayBiDimensional_v2(initialExpected);
        int[][] intialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);

        //act
        matrix.addToMatrix(indexLine,newNumber);
        ArrayBiDimensional_v2 result = matrix;

        //assert
        assertEquals(expected,result);
    }
    @Test
    void addToMatrix_NegativeIndex(){
        //arrange
        int indexLine =-2;
        int newNumber = 4;
        int[][] intialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);

        //act

        //assert
        assertThrows(IndexOutOfBoundsException.class,()-> matrix.addToMatrix(indexLine,newNumber));
    }

    //alinea.d
    @Test
    void removeValueExistsInMatrixOneValue(){
        //arrange
        int value = 5;
        int [][] initialExpected ={{1,2,3},{4},{1,2},{2,3,4}};
        ArrayBiDimensional_v2 expected = new ArrayBiDimensional_v2(initialExpected);
        int[][] intialMatrix={{1,2,3},{4,5},{1,2},{2,3,4}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);

        //act
        matrix.removeValue(value);
        ArrayBiDimensional_v2 result = matrix;

        //assert
        assertEquals(expected,result);
    }
    @Test
    void removeValueExistsInMatrixSeveralValue(){
        //arrange
        int value = 4;
        int [][] initialExpected ={{1,2,3},{5},{1,2},{2,3,4}};
        ArrayBiDimensional_v2 expected = new ArrayBiDimensional_v2(initialExpected);
        int[][] intialMatrix={{1,2,3},{4,5},{1,2},{2,3,4}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);

        //act
        matrix.removeValue(value);
        ArrayBiDimensional_v2 result = matrix;

        //assert
        assertEquals(expected,result);
    }
    @Test
    void removeValueNotExistsInMatrix(){
        //arrange
        int value = 9;
        int [][] initialExpected ={{1,2,3},{4,5},{1,2},{2,3,4}};
        ArrayBiDimensional_v2 expected = new ArrayBiDimensional_v2(initialExpected);
        int[][] intialMatrix={{1,2,3},{4,5},{1,2},{2,3,4}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);

        //act
        matrix.removeValue(value);
        ArrayBiDimensional_v2 result = matrix;

        //assert
        assertEquals(expected,result);
    }
    @Test
    void removeValueIndexMatrixEmpty(){
        //arrange
        int indexLine =0;
        int newNumber = 4;
        int [][] initialExpected = {{4}};
        ArrayBiDimensional_v2 expected = new ArrayBiDimensional_v2(initialExpected);
        int[][] intialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);

        //act
        matrix.addToMatrix(indexLine,newNumber);
        ArrayBiDimensional_v2 result = matrix;

        //assert
        assertEquals(expected,result);
    }

    //alinea.e
    @Test
    void emptyMatrixFalse() {
        //arrange
        int[][]intialMatrix = {{1,2,3},{2,35,5,6}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);
        //act
        boolean result = matrix.emptyMatrix();
        //assert
        assertFalse(result);
    }
    @Test
    void emptyMatrixFalseNullInitialMatrix() {
        //arrange
        int[][]intialMatrix = null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);
        //act
        boolean result = matrix.emptyMatrix();
        //assert
        assertTrue(result);
    }
    @Test
    void emptyMatrixFalseEmptyInitialMatrix() {
        //arrange
        int[][]intialMatrix = {{}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(intialMatrix);
        //act
        boolean result = matrix.emptyMatrix();
        //assert
        assertFalse(result);
    }

    //alinea.f
    @Test
    void highestElementOneLineMatrix() {
        //arrange
        int[][]initialMatrix={{1,5,3}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 5;
        //act
        int result = matrix.highestElementMatrix();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void highestElementNotUniformMatrix() {
        //arrange
        int[][]initialMatrix={{1,2,3},{8,5,4},{1,2}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 8;
        //act
        int result = matrix.highestElementMatrix();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void highestElementEmptyMatrix() {
        //arrange
        int[][]initialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 0;
        //act
        //int result = matrix.highestElement();
        //assert
        assertThrows(IllegalStateException.class,()-> matrix.highestElementMatrix());
    }

    //alinea.g
    @Test
    void lowestElementOneLineMatrix() {
        //arrange
        int[][]initialMatrix={{1,5,3}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 1;
        //act
        int result = matrix.lowestElement();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void lowestElementNotUniformMatrix() {
        //arrange
        int[][]initialMatrix={{1,2,3},{8,5,4},{0,2}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 0;
        //act
        int result = matrix.lowestElement();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void lowestElementEmptyMatrix() {
        //arrange
        int[][]initialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 0;
        //act
        //int result = matrix.highestElement();
        //assert
        assertThrows(IllegalStateException.class,()-> matrix.highestElementMatrix());
    }

    //alinea.h
    @Test
    void averageElementsOneLineMatrix() {
        //arrange
        int[][]initialMatrix={{1,5,3}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        double expected = 3;
        //act
        double result = matrix.averageElements();
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void averageElementsNotUniformMatrix() {
        //arrange
        int[][]initialMatrix={{1,2,3},{8,5,4},{0,2}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        double expected = 3.125;
        //act
        double result = matrix.averageElements();
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void averageElementsNotUniformMatrixEmptyLine() {
        //arrange
        int[][]initialMatrix={{1,2,3},{},{0,2}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        double expected = 1.6;
        //act
        double result = matrix.averageElements();
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void averageElementsEmptyMatrix() {
        //arrange
        int[][]initialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        double expected = 0;
        //act
        double result = matrix.averageElements();
        //assert
        assertEquals(expected,result,0.01);
    }

    //alinea.i
    @Test
    void sumLineIndexElementsOneLineMatrix() {
        //arrange
        int[][]initialMatrix={{1,5,3}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int[] initialexpected = {9};
        UnidimensionalArray expected = new UnidimensionalArray(initialexpected);
        //act
        UnidimensionalArray result = matrix.sumLineIndexElements();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void sumLineIndexElementsNotUniformMatrix() {
        //arrange
        int[][]initialMatrix={{1,2,3},{8,5,4},{0,2}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int[] initialexpected = {6,17,2};
        UnidimensionalArray expected = new UnidimensionalArray(initialexpected);
        //act
        UnidimensionalArray result = matrix.sumLineIndexElements();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void sumLineIndexElementsNotUniformMatrixEmptyLine() {
        //arrange
        int[][]initialMatrix={{1,2,3},{},{0,2}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int[] initialexpected = {6,0,2};
        UnidimensionalArray expected = new UnidimensionalArray(initialexpected);
        //act
        UnidimensionalArray result = matrix.sumLineIndexElements();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void sumLineIndexElementsEmptyMatrix() {
        //arrange
        int[][]initialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int[] initialexpected = {};
        UnidimensionalArray expected = new UnidimensionalArray(initialexpected);
        //act
        UnidimensionalArray result = matrix.sumLineIndexElements();
        //assert
        assertEquals(expected,result);
    }

    //alinea.j
    @Test
    void sumColumnIndexElementsOneLineMatrix() {
        //arrange
        int[][]initialMatrix={{1,5,3}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int[] initialexpected = {1,5,3};
        UnidimensionalArray expected = new UnidimensionalArray(initialexpected);
        //act
        UnidimensionalArray result = matrix.sumColumnIndexElements();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void sumColumnIndexElementsNotUniformMatrix() {
        //arrange
        int[][]initialMatrix={{1,2,3},{8,5,4},{0,2}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int[] initialexpected = {9,9,7};
        UnidimensionalArray expected = new UnidimensionalArray(initialexpected);
        //act
        UnidimensionalArray result = matrix.sumColumnIndexElements();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void sumColumnIndexElementsNotUniformMatrixEmptyLine() {
        //arrange
        int[][]initialMatrix={{1,2,3},{},{0,2}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int[] initialexpected = {1,4,3};
        UnidimensionalArray expected = new UnidimensionalArray(initialexpected);
        //act
        UnidimensionalArray result = matrix.sumColumnIndexElements();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void sumColumnIndexElementsEmptyMatrix() {
        //arrange
        int[][]initialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int[] initialexpected = {};
        UnidimensionalArray expected = new UnidimensionalArray(initialexpected);
        //act
        UnidimensionalArray result = matrix.sumColumnIndexElements();
        //assert
        assertEquals(expected,result);
    }

    //alinea.k
    @Test
    void indexLineOfHighestSumLineOneLineMatrix() {
        //arrange
        int[][]initialMatrix={{1,5,3}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 0;
        //act
        int result = matrix.indexLineOfHighestSumLine();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void indexLineOfHighestSumLineNotUniformMatrix() {
        //arrange
        int[][]initialMatrix={{1,2,3},{8,5,4},{0,2}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 1;
        //act
        int result = matrix.indexLineOfHighestSumLine();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void indexLineOfHighestSumLineNotUniformMatrixEmptyLine() {
        //arrange
        int[][]initialMatrix={{1,2,3},{},{0,2}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 0;
        //act
        int result = matrix.indexLineOfHighestSumLine();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void indexLineOfHighestSumLineEmptyMatrix() {
        //arrange
        int[][]initialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = -1;
        //act
        int result = matrix.indexLineOfHighestSumLine();
        //assert
        assertEquals(expected,result);
    }

    //alinea.l
    @Test
    void squareMatrixTrue() {
        //arrange
        int[][] initialmatrix = {{1,2,3},{4,5,6},{55,6,7}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialmatrix);
        //act
        boolean result = matrix.squareMatrix();
        //assert
        assertTrue(result);
    }
    @Test
    void squareMatrixFalseUniformMatrix() {
        //arrange
        int[][] initialmatrix = {{1,2,3},{4,5,6},{55,6,7},{1,2,3}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialmatrix);
        //act
        boolean result = matrix.squareMatrix();
        //assert
        assertFalse(result);
    }
    @Test
    void squareMatrixFalseNotUniformMatrix() {
        //arrange
        int[][] initialmatrix = {{1, 2, 3}, {4, 5}, {55, 6, 7}, {1, 2, 3}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialmatrix);
        //act
        boolean result = matrix.squareMatrix();
        //assert
        assertFalse(result);
    }
    @Test
    void squareMatrixFalseEmptyMatrix() {
        //arrange
        int[][]initialMatrix= null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        //act
        boolean result = matrix.squareMatrix();
        //assert
        assertFalse(result);
    }

    //alinea.m                                                                              //FALTA!!!!!!!!!!!!!!!!!!!!!!!!


    //alinea.n
    @Test
    void diagonalElementsNotNulls_squareMatrix3Elements(){
        //arrange
        int[][]initialMatrix={{1,2,3},{2,5,0},{1,7,6}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 3;
        //act
        int result = matrix.diagonalElementsNotZero();
        //assert
        assertEquals(result, expected);
    }
    @Test
    void diagonalElementsNotNulls_squareMatrix1Element(){
        //arrange
        int[][]initialMatrix={{0,2,3},{2,5,0},{1,7,0}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 1;
        //act
        int result = matrix.diagonalElementsNotZero();
        //assert
        assertEquals(result, expected);
    }
    @Test
    void diagonalElementsNotNulls_squareMatrix0Element(){
        //arrange
        int[][]initialMatrix={{0,2,3},{2,0,0},{1,7,0}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = 0;
        //act
        int result = matrix.diagonalElementsNotZero();
        //assert
        assertEquals(result, expected);
    }
    @Test
    void diagonalElementsNotNulls_NotSquareMatrix(){
        //arrange
        int[][]initialMatrix={{0,2,3},{2,5,0},{1,7,0},{2,3,4}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = -1;
        //act
        int result = matrix.diagonalElementsNotZero();
        //assert
        assertEquals(result, expected);
    }
    @Test
    void diagonalElementsNotNullsEmptyMatrix(){
        //arrange
        int[][]initialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        int expected = -1;
        //act
        int result = matrix.diagonalElementsNotZero();
        //assert
        assertEquals(result, expected);
    }

    //alinea.o
    @Test
    void mainAndSecondaryDiagonalEqualsSquareMatrixTrue(){
        //arrange
        int[][]initialMatrix={{1,2,1},{2,5,0},{3,4,3}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        //act
        boolean result = matrix.mainAndSecondaryDiagonalEquals();
        //assert
        assertTrue(result);
    }
    @Test
    void mainAndSecondaryDiagonalEqualsSquareMatrixFalse(){
        //arrange
        int[][]initialMatrix={{2,2,1},{2,5,0},{3,4,3}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        //act
        boolean result = matrix.mainAndSecondaryDiagonalEquals();
        //assert
        assertFalse(result);
    }
    @Test
    void mainAndSecondaryDiagonalEqualsNotSquareMatrix(){
        //arrange
        int[][]initialMatrix={{0,2,3},{2,0,0},{1,7,0},{1}};
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        //act
        boolean result = matrix.mainAndSecondaryDiagonalEquals();
        //assert
        assertFalse(result);
    }
    @Test
    void mainAndSecondaryDiagonalEqualsEmptyMatrix(){
        //arrange
        int[][]initialMatrix=null;
        ArrayBiDimensional_v2 matrix = new ArrayBiDimensional_v2(initialMatrix);
        //act
        boolean result = matrix.mainAndSecondaryDiagonalEquals();
        //assert
        assertFalse(result);
    }

    //alinea.p


}

