import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TesteArrayUnidimensional {


    //alinea.a
    @Test
    void ArrayUnidimensional_testeConstrutorVazio() {
        //arrange
        int[] expected = {};
        //act
        UnidimensionalArray vetor = new UnidimensionalArray();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }

    //alinea.b
    @Test
    void ArrayUnidimensional_teste1ConstrutorComDeterminadosValores() {
        //arrange
        int[] expected = {1, 2, 3};
        //act
        UnidimensionalArray vetor = new UnidimensionalArray(1, 2, 3);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void ArrayUnidimensional_teste2ConstrutorComDeterminadosValores() {
        //arrange
        int[] expected = {2, 2, 4};
        //act
        UnidimensionalArray vetor = new UnidimensionalArray(2, 2, 4);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void  ArrayUnidimensional_testeCriarVetorNull() {
        //arrange
        int[] expected = {1, 2, 4, 4, 3};
        int numeroRemover = 3;
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        //assert
        assertThrows(IllegalStateException.class,()-> vetor.removerPrimeiroElementoDoArrayComUmDadoValor(numeroRemover));
        //na classe vetor não permite criar vetor null. Sempre que for o caso avisa erro.
    }

    //alinea.c
    @Test
    void ArrayUnidimensional_teste1ConstrutorComVetor() {
        //arrange
        int[] expected = {1, 2, 3, 4};
        //act
        int[] vetorInicial = {1, 2, 3, 4};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void ArrayUnidimensional_teste2ConstrutorComVetor() {
        //arrange
        int[] expected = {1, 2, 3, 4, 7, 6};
        //act
        int[] vetorInicial = {1, 2, 3, 4, 7, 6};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void ArrayUnidimensional_teste2ConstrutorComVetorVazio() {
        //arrange
        int[] expected = {};
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void adicionarNoVetor_teste1() {
        //arrange
        int[] expected = {1, 2, 3, 4};
        //act
        UnidimensionalArray vetor = new UnidimensionalArray(1, 2, 3);
        vetor.adicionarNoVetor(4);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void adicionarNoVetor_teste2() {
        //arrange
        int[] expected = {1, 2, 3, 4, 5};
        //act
        UnidimensionalArray vetor = new UnidimensionalArray(1, 2, 3);
        vetor.adicionarNoVetor(4);
        vetor.adicionarNoVetor(5);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }

    //alinea.d
    @Test
    void removerPrimeiroElementoDoArrayComUmDadoValor_teste1() {
        //arrange
        int[] expected = {1, 2, 4, 4, 3};
        int numeroRemover = 3;
        //act
        int[] vetorInicial = {1, 2, 3, 4};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.adicionarNoVetor(4);
        vetor.adicionarNoVetor(3);
        vetor.removerPrimeiroElementoDoArrayComUmDadoValor(numeroRemover);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void removerPrimeiroElementoDoArrayComUmDadoValor_teste2() {
        //arrange
        int[] expected = {1, 2, 3, 4, 3};
        int numeroRemover = 4;
        //act
        int[] vetorInicial = {1, 2, 3, 4};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.adicionarNoVetor(4);
        vetor.adicionarNoVetor(3);
        vetor.removerPrimeiroElementoDoArrayComUmDadoValor(numeroRemover);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void removerPrimeiroElementoDoArrayComUmDadoValor_teste3() {
        //arrange
        int[] expected = {1, 3, 4, 3, 2, 1, 2, 4};
        int numeroRemover = 2;
        //act
        int[] vetorInicial = {1, 2, 3, 4, 3, 2, 1, 2, 4};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.removerPrimeiroElementoDoArrayComUmDadoValor(numeroRemover);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void removerPrimeiroElementoDoArrayComUmDadoValor_teste4() {
        //arrange
        int[] expected = {3, 5, 4, 3, 1, 2};
        int numeroRemover = 2;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.removerPrimeiroElementoDoArrayComUmDadoValor(numeroRemover);
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected, result);
    }

    //alinea e.
    @Test
    void getValorDoElementoNumDadoIndice_teste1_IndexValido() {
        //arrange
        int expected = 4;
        int index = 3;
        //act
        int[] vetorInicial = {1, 2, 3, 4};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.valueOnIndex(index);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getValorDoElementoNumDadoIndice_teste2_IndexNaoValido(){
        //arrange
        int index = 5;
        int expected =0;
        //act
        int []vetorInicial = {1,2,3,4};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.valueOnIndex(index);
        //assert
        assertEquals(expected,result);
    }
    @Test
    void getValorDoElementoNumDadoIndice_teste3_EmptyVector(){
        //arrange
        int index = 5;
        int expected =0;
        //act
        int []vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.valueOnIndex(index);
        //assert
        assertEquals(expected,result);
    }

    //alinea f.
    @Test
    void numeroElementosVetor_teste1(){
        //arrange
        int expected = 7;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.countElements();
        //assert
        assertEquals(result, expected);
    }
    @Test
    void numeroElementosVetor_teste2(){
        //arrange
        int expected = 5;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.countElements();
        //assert
        assertEquals(result, expected);
    }
    @Test
    void numeroElementosVetor_testeMatrizVazia(){
        //arrange
        int expected =0;
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.countElements();
        //assert
        assertEquals(result, expected);
    }
    @Test
    void numeroElementosVetor_testeMatrizNull(){
        //arrange
        int expected =0;
        //act
        int[] vetorInicial = null;
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.countElements();
        //assert
        assertEquals(result, expected);
    }

    //alinea g.
    @Test
    void maiorElementoDoVetor_teste1(){
        //arrange
        int expected = 5;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.highestElement();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void maiorElementoDoVetor_teste2(){
        //arrange
        int expected = -1;
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -3, -1, -2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.highestElement();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void maiorElementoDoVetor_testeVetorVazio(){
        //arrange

        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);

        //assert
        assertThrows(IllegalStateException.class,()-> vetor.highestElement());
    }

    //alinea h.
    @Test
    void menorElementoDoVetor_teste1(){
        //arrange
        int expected = 1;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.lowestElement();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void menorElementoDoVetor_teste2(){
        //arrange
        int expected = -5;
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -3, -1, -2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.lowestElement();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void menorElementoDoVetor_testeVetorVazio(){
        //arrange

        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);

        //assert
        assertThrows(IllegalStateException.class,()-> vetor.lowestElement());
    }

    //alinea i.
    @Test
    void calcularSomaElementos_teste1(){
        //arrange
        int expected = 20;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementos();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void calcularSomaElementos_teste2(){
        //arrange
        int expected = -20;
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -3, -1, -2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementos();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void calcularSomaElementos_testeVetorVazio(){
        //arrange
        int expected = 0;
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementos();
        //assert
        assertEquals(expected, result);
    }

    @Test
    void calcularMediaElementosDoVetor_teste1(){
        //arrange
        double expected = 2.8571;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.averageElements();
        //assert
        assertEquals(expected, result, 0.001);
    }
    @Test
    void calcularMediaElementosDoVetor_teste2(){
        //arrange
        double expected = - 2.8571;
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -3, -1, -2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.averageElements();
        //assert
        assertEquals(expected, result, 0.001);
    }
    @Test
    void calcularMediaElementosDoVetor_testeVetorVazio(){
        //arrange
        double expected = 0;
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.averageElements();
        //assert
       assertEquals(expected, result);
    }

    //alinea j.
    @Test
    void contarElementosPares_teste1(){
        //arrange
        int expected = 3;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterNumeroElementosPares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void contarElementosPares_teste2(){
        //arrange
        int expected = 3;
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -3, -1, -2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterNumeroElementosPares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void contarElementosPares_testeVetorVazio(){
        //arrange
        int expected =0;
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterNumeroElementosPares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void contarElementosPares_testeSemElementosPares(){
        //arrange
        int expected =0;
        //act
        int[] vetorInicial = { -3, -5, 3, -1};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterNumeroElementosPares();
        //assert
        assertEquals(expected, result);
    }

    @Test
    void calculaSomaElementosPares_teste1(){
        //arrange
        int expected = 8;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementosPares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void calculaSomaElementosPares_teste2(){
        //arrange
        int expected = -8;
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -3, -1, -2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementosPares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void calculaSomaElementosPares_testeVetorVazio(){
        //arrange
        int expected =0;
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementosPares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void calculaSomaElementosPares_testeSemElementosPares(){
        //arrange
        int expected =0;
        //act
        int[] vetorInicial = { -3, -5, 3, -1};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementosPares();
        //assert
        assertEquals(expected, result);
    }

    @Test
    void mediaElementosPares_teste1(){
        //arrange
        double expected = 2.666;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.mediaElementosPares();
        //assert
        assertEquals(expected, result, 0.001);
    }
    @Test
    void mediaElementosPares_teste2(){
        //arrange
        double expected = - 2.666;
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -3, -1, -2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.mediaElementosPares();
        //assert
        assertEquals(expected, result, 0.001);
    }
    @Test
    void mediaElementosPares_testeVetorVazio(){
        //arrange

        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);

        //assert
        assertThrows(IllegalStateException.class,()-> vetor.mediaElementosPares());
    }
    @Test
    void mediaElementosPares_testeSemElementosPares(){
        //arrange
        //Integer expected =null;
        //act
        int [] vetorInicial = { -3, -5, 3, -1};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        Double result = vetor.mediaElementosPares();
        //assert
        assertNull(result);
    }


    //alinea k.
    @Test
    void contarElementosImpares_teste1(){
        //arrange
        int expected = 4;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterNumeroElementosImpares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void contarElementosImpares_teste2(){
        //arrange
        int expected = 4;
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -3, -1, -2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterNumeroElementosImpares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void contarElementosImpares_testeVetorVazio(){
        //arrange
        int expected =0;
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterNumeroElementosImpares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void contarElementosPares_testeSemElementosImpares(){
        //arrange
        int expected =0;
        //act
        int[] vetorInicial = { -2, -2, 4, -8};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterNumeroElementosImpares();
        //assert
        assertEquals(expected, result);
    }


    @Test
    void calculaSomaElementosImpares_teste1(){
        //arrange
        int expected = 12;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementosImpares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void calculaSomaElementosImpares_teste2(){
        //arrange
        int expected = -12;
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -3, -1, -2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementosImpares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void calculaSomaElementosImpares_testeVetorVazio(){
        //arrange
        int expected =0;
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementosImpares();
        //assert
        assertEquals(expected, result);
    }
    @Test
    void calculaSomaElementosPares_testeSemElementosImpares(){
        //arrange
        int expected =0;
        //act
        int[] vetorInicial = { -4, -6, 2, -0};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterSomaElementosImpares();
        //assert
        assertEquals(expected, result);
    }

    @Test
    void mediaElementosImpares_teste1(){
        //arrange
        double expected = 3;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.mediaElementosImpares();
        //assert
        assertEquals(expected, result, 0.001);
    }
    @Test
    void mediaElementosImpares_teste2(){
        //arrange
        double expected = -3;
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -3, -1, -2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.mediaElementosImpares();
        //assert
        assertEquals(expected, result, 0.001);
    }
    @Test
    void mediaElementosImpares_testeVetorVazio(){
        //arrange

        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);

        //assert
        assertThrows(IllegalStateException.class,()-> vetor.mediaElementosImpares());
    }
    @Test
    void mediaElementosImpares_testeSemElementosImpares(){
        //arrange

        //act
        int[] vetorInicial = { -2, -4, 2, -6};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        Double result = vetor.mediaElementosImpares();
        //assert
        assertNull(result);
    }

    //alinea l.
    @Test
    void verificarSeNumeroMultiplo_testeTrue(){
        //arrange
        int valor = 4;
        int divisor = 2;
        //act
        boolean result = Utilities.verificarSeElementoMultiploDeUmDadoDivisor(valor, divisor);
        //assert
        assertTrue(result);
    }
    @Test
    void verificarSeNumeroMultiplo_testeFalse(){
        //arrange
        int valor = 3;
        int divisor = 2;
        //act
        boolean result = Utilities.verificarSeElementoMultiploDeUmDadoDivisor(valor, divisor);
        //assert
        assertFalse(result);
    }
    @Test
    void verificarSeNumeroMultiplo_testeTrueParaZero(){
        //arrange
        int valor = 0;
        int divisor = 2;
        //act
        boolean result = Utilities.verificarSeElementoMultiploDeUmDadoDivisor(valor, divisor);
        //assert
        assertTrue(result);
    }
    @Test
    void verificarSeNumeroMultiplo_testeTrueParaDivisorZero(){
        //arrange
        int valor = 5;
        int divisor = 0;
        //act
        boolean result = Utilities.verificarSeElementoMultiploDeUmDadoDivisor(valor, divisor);
        //assert
        assertTrue(result);
    }
    @Test
    void verificarSeNumeroMultiplo_testeTrueParaDivisorZeroValorZero(){
        //arrange
        int valor = 0;
        int divisor = 0;
        //act
        boolean result = Utilities.verificarSeElementoMultiploDeUmDadoDivisor(valor, divisor);
        //assert
        assertTrue(result);
    }

    @Test
    void mediaElementosMultiplos_teste1(){
        //arrange
        double expected = 2.6667;
        int divisor = 2;
        //act
        int[] vetorInicial = {2, 3, 5, 4, 3, 1, 2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.mediaElementosMultiplos(divisor);
        //assert
        assertEquals(expected, result, 0.001);
    }
    @Test
    void mediaElementosMultiplos_testeVetorVazio(){
        //arrange
        int divisor = 3;
        double expected = 0;
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.mediaElementosMultiplos(divisor);
        //assert
        assertEquals(expected, result, 0.001);
    }
    @Test
    void mediaElementosMultiplos_testeSemElementosMultiplos(){
        //arrange
        int divisor = 3;
        double expected = 0;
        //act
        int[] vetorInicial = { -2, -4, 2, -1};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.mediaElementosMultiplos(divisor);
        //assert
        assertEquals(expected, result, 0.001);
    }

    //alinea m.
    @Test
    void ordenarOrdemAscendenteVetor_ElementosPositivosDistintos(){
        //arrange
        int[] expected = {1,2,3,4,5,6,7};
        //act
        int[] vetorInicial = {2, 3, 5, 4, 6, 1, 7};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemAscendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void ordenarOrdemAscendenteVetor_ElementosNegativosDistintos(){
        //arrange
        int[] expected = {-7,-6,-5,-4,-3,-2,-1};
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -6, -1, -7};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemAscendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void ordenarOrdemAscendenteVetor_ElementosPositivos_NegativosDistintos(){
        //arrange
        int[] expected = {-7,-5,-2,-1,0,4,6};
        //act
        int[] vetorInicial = {-2, -5, 4, 6, -1, -7,0};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemAscendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void ordenarOrdemAscendenteVetor_ElementosPositivos_NegativosRepetidos(){
        //arrange
        int[] expected = {-7,-5,-2,-1,0,4,4,6};
        //act
        int[] vetorInicial = {-2, -5, 4,4, 6, -1, -7,0};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemAscendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void ordenarOrdemAscendenteVetor_VetorVazio(){
        //arrange
        int[] expected = {};
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemAscendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void ordenarOrdemAscendenteVetor_VetorNull(){
        //arrange
        int[] expected = {};
        //act
        int[] vetorInicial = null;
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemAscendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }

    @Test
    void ordenarOrdemDescendenteVetor_ElementosPositivosDistintos(){
        //arrange
        int[] expected = {7,6,5,4,3,2,1};
        //act
        int[] vetorInicial = {2, 3, 5, 4, 6, 1, 7};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemDescendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void ordenarOrdemDescendenteVetor_ElementosNegativosDistintos(){
        //arrange
        int[] expected = {-1,-2,-3,-4,-5,-6,-7};
        //act
        int[] vetorInicial = {-2, -3, -5, -4, -6, -1, -7};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemDescendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void ordenarOrdemDescendenteVetor_ElementosPositivos_NegativosDistintos(){
        //arrange
        int[] expected = {6,4,0,-1,-2,-5,-7};
        //act
        int[] vetorInicial = {-2, -5, 4, 6, -1, -7,0};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemDescendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void ordenarOrdemDescendenteVetor_ElementosPositivos_NegativosRepetidos(){
        //arrange
        int [] vetorExpected = {6,4,4,0,-1,-2,-5,-7};
        UnidimensionalArray expected = new UnidimensionalArray(vetorExpected);
        //act
        int[] vetorInicial = {-2, -5, 4, 4, 6, -1, -7,0};
        UnidimensionalArray result = new UnidimensionalArray(vetorInicial);
        result.ordenarOrdemDescendenteVetor();
        //assert
        assertEquals(expected,result);
    }                                                                                                                               //Teste com Metodo Equals da Classe!!!!!!!!!!!!
    @Test
    void ordenarOrdemDescendenteVetor_VetorVazio(){
        //arrange
        int[] expected = {};
        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemDescendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void ordenarOrdemDescendenteVetor_VetorNull(){
        //arrange
        int[] expected = {};
        //act
        int[] vetorInicial = null;
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        vetor.ordenarOrdemDescendenteVetor();
        int[] result = vetor.representarObjetoEmVetor();
        //assert
        assertArrayEquals(expected,result);
    }

    //alinea 0.
    @Test
    void verificarSeVetorTemApenasUmElemento_testeTrue(){
        //arrange

        //act
        int[] vetorInicial = {0};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificarSeVetorTemApenasUmElemento();
        //assert
        assertTrue(result);
    }
    @Test
    void verificarSeVetorTemApenasUmElemento_testeFalse(){
        //arrange

        //act
        int[] vetorInicial = {0,3};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificarSeVetorTemApenasUmElemento();
        //assert
        assertFalse(result);
    }
    @Test
    void verificarSeVetorTemApenasUmElemento_VetorVazio(){
        //arrange

        //act
        int[] vetorInicial = null;
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificarSeVetorTemApenasUmElemento();
        //assert
        assertFalse(result);
    }

    //alinea_p.
    @Test
    void verificarSeVetorPar_testeTrue(){
        //arrange

        //act
        int[] vetorInicial = {2,6,4,8};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificarSeVetorPar();
        //assert
        assertTrue(result);
    }
    @Test
    void verificarSeVetorPar_testeFalse_ElementosImpares(){
        //arrange

        //act
        int[] vetorInicial = {1,3};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificarSeVetorPar();
        //assert
        assertFalse(result);
    }
    @Test
    void verificarSeVetorPar_testeFalse_ElementosImpares_Pares(){
        //arrange

        //act
        int[] vetorInicial = {1,3,4,2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificarSeVetorPar();
        //assert
        assertFalse(result);
    }
    @Test
    void verificarSeVetorPar_testeVetorVazio(){
        //arrange

        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);

        //assert
        assertThrows(IllegalStateException.class,()-> vetor.verificarSeVetorPar());
    }


    //alinea_q.
    @Test
    void verificarSeVetorImpar_testeFalse_ElementosPares(){
        //arrange

        //act
        int[] vetorInicial = {2,6,4,8};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificarSeVetorImpar();
        //assert
        assertFalse(result);
    }
    @Test
    void verificarSeVetorImpar_testeTrue(){
        //arrange

        //act
        int[] vetorInicial = {1,3};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificarSeVetorImpar();
        //assert
        assertTrue(result);
    }
    @Test
    void verificarSeVetorImpar_testeFalse_ElementosImpares_Pares(){
        //arrange

        //act
        int[] vetorInicial = {1,3,4,2};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificarSeVetorImpar();
        //assert
        assertFalse(result);
    }
    @Test
    void verificarSeVetorImpar_testeVetorVazio(){
        //arrange

        //act
        int[] vetorInicial = {};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);

        //assert
        assertThrows(IllegalStateException.class,()-> vetor.verificarSeVetorImpar());
    }

    //alinea_r.
    @Test
    void verificaSeVetorTemElementosDuplicados_testeFalse(){
        //arrange

        //act
        int[] vetorInicial = {2,6,4,8};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificaSeVetorTemElementosDuplicados();
        //assert
        assertFalse(result);
    }
    @Test
    void verificaSeVetorTemElementosDuplicados_testeTrue(){
        //arrange

        //act
        int[] vetorInicial = {1,3,4,3};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificaSeVetorTemElementosDuplicados();
        //assert
        assertTrue(result);
    }
    @Test
    void verificaSeVetorTemElementosDuplicados_testeVetorVazio(){
        //arrange

        //act
        int[] vetorInicial = null;
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        boolean result = vetor.verificaSeVetorTemElementosDuplicados();
        //assert
        assertFalse(result);
    }

    //alinea_s.
    @Test
    void contaDigitosDeUmElemento_3digitosPositivo(){
        //arrange
        int numero=223;
        int expected = 3;
        //act
        int result = Utilities.contaDigitosDeUmElemento(numero);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void contaDigitosDeUmElemento_3digitosNegativo(){
        //arrange
        int numero=-223;
        int expected = 3;
        //act
        int result = Utilities.contaDigitosDeUmElemento(numero);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void contaDigitosDeUmElemento_4digitos(){
        //arrange
        int numero=2343;
        int expected =4;
        //act
        int result = Utilities.contaDigitosDeUmElemento(numero);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void contaDigitosDeUmElemento_1digito(){
        //arrange
        int numero=1;
        int expected =1;
        //act
        int result = Utilities.contaDigitosDeUmElemento(numero);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void contaDigitosDeUmElemento_zero(){
        //arrange
        int numero=0;
        int expected =1;
        //act
        int result = Utilities.contaDigitosDeUmElemento(numero);
        //assert
        assertEquals(expected, result);
    }

    @Test
    void mediaNumeroDigitosElementosVetor_teste1(){
        //arrange
        int expected = 2;
        //act
        int[] vetorInicial = {2,66,433,82};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterMediaNumeroDigitosElementos();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void mediaNumeroDigitosElementosVetor_teste2(){
        //arrange
        int expected = 1;
        //act
        int[] vetorInicial = {2,66,4,81};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterMediaNumeroDigitosElementos();
        //assert
        assertEquals(expected,result);
    }
    @Test
    void mediaNumeroDigitosElementosVetor_teste3(){
        //arrange
        int expected = 2;
        //act
        int[] vetorInicial = {2,665,4,812,5637};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int result = vetor.obterMediaNumeroDigitosElementos();
        //assert
        assertEquals(expected,result);
    }

    @Test
    void obterVetorComElementosComNumeroDigitosSuperiorMedia_teste1(){
        //arrange
        int[]expected={234,456};
        //act
        int[]vetorInicial = {2,33,234,1,33,456,32};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int[]result = vetor.obterVetorComElementosComNumeroDigitosSuperiorMedia();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void obterVetorComElementosComNumeroDigitosSuperiorMedia_teste2(){
        //arrange
        int[]expected={33,234,33,456,32,78};
        //act
        int[]vetorInicial = {2,33,234,1,33,456,32,78,1,6};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int[]result = vetor.obterVetorComElementosComNumeroDigitosSuperiorMedia();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void obterVetorComElementosComNumeroDigitosSuperiorMedia_testeVetorVazio(){
        //arrange

        //act
        int[]vetorInicial = null;
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        //assert
        assertThrows(IllegalStateException.class,()-> vetor.obterVetorComElementosComNumeroDigitosSuperiorMedia());

    }

    //alinea_t.
    @Test
    void verificarSeAlgarismoPar_testeAlgarismoPar(){
        //arrange
        int algarismo = 6;
        //act
        boolean result = Utilities.verificarSeAlgarismoPar(algarismo);
        //assert
        assertTrue(result);
    }
    @Test
    void verificarSeAlgarismoPar_testeAlgarismoImpar(){
        //arrange
        int algarismo = 3;
        //act
        boolean result = Utilities.verificarSeAlgarismoPar(algarismo);
        //assert
        assertFalse(result);
    }
    @Test
    void verificarSeAlgarismoPar_testeAlgarismoZero(){
        //arrange
        int algarismo = 0;
        //act
        boolean result = Utilities.verificarSeAlgarismoPar(algarismo);
        //assert
        assertTrue(result);
    }
    @Test
    void verificarSeAlgarismoPar_testeNaoAlgarismo(){
        //arrange
        int numero = 44;
        //act

        //assert
        assertThrows(IllegalArgumentException.class,()-> Utilities.verificarSeAlgarismoPar(numero));
    }

    @Test
    void obterPercentagemAlgarismosParNumDadoNumero_teste1(){
        //arrange
        int numero = 2341;
        double expected = 50;
        //act
        double result = Utilities.obterPercentagemAlgarismosParNumDadoNumero(numero);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void obterPercentagemAlgarismosParNumDadoNumero_teste2(){
        //arrange
        int numero = 2342;
        double expected = 75;
        //act
        double result = Utilities.obterPercentagemAlgarismosParNumDadoNumero(numero);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void obterPercentagemAlgarismosParNumDadoNumero_teste3(){
        //arrange
        int numero = 3537;
        double expected = 0;
        //act
        double result = Utilities.obterPercentagemAlgarismosParNumDadoNumero(numero);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void obterPercentagemAlgarismosParNumDadoNumero_teste4(){
        //arrange
        int numero = 2;
        double expected = 100;
        //act
        double result = Utilities.obterPercentagemAlgarismosParNumDadoNumero(numero);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void obterPercentagemAlgarismosParNumDadoNumero_teste5(){
        //arrange
        int numero = 0;
        double expected = 100;
        //act
        double result = Utilities.obterPercentagemAlgarismosParNumDadoNumero(numero);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void obterPercentagemAlgarismosParNumDadoNumero_teste6(){
        //arrange
        int numero = 123;
        double expected = 33.333;
        //act
        double result = Utilities.obterPercentagemAlgarismosParNumDadoNumero(numero);
        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void mediaPercentagemAlgarismosParesElementosVetor_testemMediaInteiro(){
        //arrange
        double expected = 55;
        //act
        int[]vetorInicial = {22,23,333,2345,2243};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.obterMediaPercentagemAlgarismosParesElementosVetor();
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void mediaPercentagemAlgarismosParesElementosVetor_testeMediaDecimal(){
        //arrange
        double expected = 51.388;
        //act
        int[]vetorInicial = {22,23,333,2345,2243,123};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.obterMediaPercentagemAlgarismosParesElementosVetor();
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void mediaPercentagemAlgarismosParesElementosVetor_testeMediaZero(){
        //arrange
        double expected = 0;
        //act
        int[]vetorInicial = {33,333,1357,3333,153};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.obterMediaPercentagemAlgarismosParesElementosVetor();
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void mediaPercentagemAlgarismosParesElementosVetor_testeMedia100(){
        //arrange
        double expected = 100;
        //act
        int[]vetorInicial = {24,264,888,246};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double result = vetor.obterMediaPercentagemAlgarismosParesElementosVetor();
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void mediaPercentagemAlgarismosParesElementosVetor_testeVetorVazio(){
        //arrange

        //act
        int[]vetorInicial =null;
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        //assert
        assertThrows(IllegalStateException.class,()-> vetor.obterVetorComElementosComNumeroDigitosSuperiorMedia());
    }

    @Test
    void vetorElementosTemPercentagemDigitosParesSuperiorMediaPercentagemAlgarismosParesElementosVetor_teste1(){
        //arrange
        int [] expected = {22,2243};
        int[] vetorInicial =  {22,23,333,2345,2243};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double averageEvenDigitsTotalElements = vetor.obterMediaPercentagemAlgarismosParesElementosVetor();
        //act
        int[] result = vetor.elementsWithHigherPercentageEvenNumbersThanAverage(averageEvenDigitsTotalElements);
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void vetorElementosTemPercentagemDigitosParesSuperiorMediaPercentagemAlgarismosParesElementosVetor_teste2(){
        //arrange
        int [] expected = {-40};
        int[] vetorInicial =  {1101,-2431,10,-40,23};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double averageEvenDigitsTotalElements = vetor.obterMediaPercentagemAlgarismosParesElementosVetor();
        //act
        int[] result = vetor.elementsWithHigherPercentageEvenNumbersThanAverage(averageEvenDigitsTotalElements);
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void vetorElementosTemPercentagemDigitosParesSuperiorMediaPercentagemAlgarismosParesElementosVetor_teste3(){
        //arrange
        int [] expected = {40};
        int[] vetorInicial =  {1101,2431,10,40,23};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double averageEvenDigitsTotalElements = vetor.obterMediaPercentagemAlgarismosParesElementosVetor();
        //act
        int[] result = vetor.elementsWithHigherPercentageEvenNumbersThanAverage(averageEvenDigitsTotalElements);
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void vetorElementosTemPercentagemDigitosParesSuperiorMediaPercentagemAlgarismosParesElementosVetor_VetorVazio(){
        //arrange
        int[] vetorInicial =  null;
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        double averageEvenDigitsTotalElements = vetor.obterMediaPercentagemAlgarismosParesElementosVetor();
        int[]expected = {};
        //act
        int[] result = vetor.elementsWithHigherPercentageEvenNumbersThanAverage(averageEvenDigitsTotalElements);
        //assert
        assertArrayEquals(expected,result);
    }

    //alinea_u.
    @Test
    void verificarSeElementoApenasCompostoPorDigitosPares_trueElementoPositivo(){
        //arrange
        int numero = 42;
        //act
        boolean result = Utilities.verificarSeElementoApenasCompostoPorDigitosPares(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void verificarSeElementoApenasCompostoPorDigitosPares_true_ElementoNegativo(){
        //arrange
        int numero = -442;
        //act
        boolean result = Utilities.verificarSeElementoApenasCompostoPorDigitosPares(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void verificarSeElementoApenasCompostoPorDigitosPares_false_ElementoNegativo(){
        //arrange
        int numero = -434;
        //act
        boolean result = Utilities.verificarSeElementoApenasCompostoPorDigitosPares(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void verificarSeElementoApenasCompostoPorDigitosPares_false_ElementoPositivo(){
        //arrange
        int numero = 4324;
        //act
        boolean result = Utilities.verificarSeElementoApenasCompostoPorDigitosPares(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void verificarSeElementoApenasCompostoPorDigitosPares_false_ElementoZero(){
        //arrange
        int numero = 0;
        //act
        boolean result = Utilities.verificarSeElementoApenasCompostoPorDigitosPares(numero);
        //assert
        assertTrue(result);
    }


    @Test
    void vetorElementosComApenasDigitosParesTesteElementosPositivos(){
        //arrange
        int [] expected = {22};
        //act
        int[] vetorInicial =  {22,23,333,2345,2243};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosComApenasDigitosPares();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void vetorElementosComApenasDigitosParesTesteElementosNegativos(){
        //arrange
        int [] expected = {22,-24,-64};
        //act
        int[] vetorInicial =  {22,-24,333,2345,2243,-64};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosComApenasDigitosPares();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void vetorElementosComApenasDigitosParesTesteSemElementos(){
        //arrange
        int [] expected = {};
        //act
        int[] vetorInicial =  {221,23,333,2345,2243};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosComApenasDigitosPares();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void vetorElementosComApenasDigitosParesTesteTodosElementos(){
        //arrange
        int [] expected ={22,2,222,8,2220};
        //act
        int[] vetorInicial =  {22,2,222,8,2220};
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosComApenasDigitosPares();
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void vetorElementosComApenasDigitosParesTesteVezorVazio(){
        //arrange
        int [] expected ={};
        //act
        int[] vetorInicial =null;
        UnidimensionalArray vetor = new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosComApenasDigitosPares();
        //assert
        assertArrayEquals(expected,result);
    }



    //alinea_v.
    @Test
    void numeroSequenciaCrescente_TesteTrue123() {
        //arrange
        int numero = 123;
        //act
        boolean result = Utilities.numeroSequenciaCrescente(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void numeroSequenciaCrescente_TesteFalse143() {
        //arrange
        int numero = 123;
        //act
        boolean result = Utilities.numeroSequenciaCrescente(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void numeroSequenciaCrescente_TesteFalse111() {
        //arrange
        int numero = 111;
        //act
        boolean result = Utilities.numeroSequenciaCrescente(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void numeroSequenciaCrescente_TesteFalseNeg111() {
        //arrange
        int numero = -111;
        //act
        boolean result = Utilities.numeroSequenciaCrescente(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void numeroSequenciaCrescente_TesteTrueNeg431() {
        //arrange
        int numero = -431;
        //act
        boolean result = Utilities.numeroSequenciaCrescente(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void numeroSequenciaCrescente_TesteTrueNeg134() {
        //arrange
        int numero = -134;
        //act
        boolean result = Utilities.numeroSequenciaCrescente(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void numeroSequenciaCrescente_TesteTrueAlgarismo2() {
        //arrange
        int numero = 2;
        //act
        boolean result = Utilities.numeroSequenciaCrescente(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void numeroSequenciaCrescente_TesteTrueAlgarismo0() {
        //arrange
        int numero = 0;
        //act
        boolean result = Utilities.numeroSequenciaCrescente(numero);
        //assert
        assertTrue(result);
    }

    @Test
    void vetorElementosSequenciaCrescenteDigitos_testeComElementosPossiveis(){
        //arrange
        int[] expected= {123,467,-432,89};
        //act
        int[]vetorInicial= {123,467,523,-462,-432,537,89};
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosSequenciaCrescenteDigitos();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void vetorElementosSequenciaCrescenteDigitos_testeSemElementosPossiveis(){
        //arrange
        int[] expected= {};
        //act
        int[]vetorInicial= {-123,-467,523,462,432,537,989};
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosSequenciaCrescenteDigitos();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void vetorElementosSequenciaCrescenteDigitos_testeVetorVazio(){
        //arrange
        int[] expected= {};
        //act
        int[]vetorInicial= null;
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosSequenciaCrescenteDigitos();
        //assert
        assertArrayEquals(result,expected);
    }


    //alinea_w.
    @Test
    void getInverseNumber_testeValorPositivo(){
        //arrange
        int numero = 1234;
        int expected=4321;
        //act
        int result = Utilities.getInverseNumber(numero);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getInverseNumber_testeValorNegativo(){
        //arrange
        int numero = -1234;
        int expected=-4321;
        //act
        int result = Utilities.getInverseNumber(numero);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getInverseNumber_testeAlgarismoPositivo(){
        //arrange
        int numero = 3;
        int expected=3;
        //act
        int result = Utilities.getInverseNumber(numero);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getInverseNumber_testeAlgarismoNegativo(){
        //arrange
        int numero = -3;
        int expected=-3;
        //act
        int result = Utilities.getInverseNumber(numero);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getInverseNumber_testeZero(){
        //arrange
        int numero = 0;
        int expected=0;
        //act
        int result = Utilities.getInverseNumber(numero);
        //assert
        assertEquals(expected, result);
    }

    @Test
    void checkIfNumberisPalindromicNumber_testeValorPositivoFalse(){
        //arrange
        int numero = 1243;
        //act
        boolean result = Utilities.checkIfNumberisPalindromicNumber(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void checkIfNumberisPalindromicNumber_testeValorPositivoTrue(){
        //arrange
        int numero = 12521;
        //act
        boolean result = Utilities.checkIfNumberisPalindromicNumber(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void checkIfNumberisPalindromicNumber_testeValorNegativoFalse(){
        //arrange
        int numero = -12521;
        //act
        boolean result = Utilities.checkIfNumberisPalindromicNumber(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void checkIfNumberisPalindromicNumber_testeAlgarismoPositivoTrue(){
        //arrange
        int numero = 3;
        //act
        boolean result = Utilities.checkIfNumberisPalindromicNumber(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void checkIfNumberisPalindromicNumber_testeAlgarismoNegativoFalse(){
        //arrange
        int numero = -3;
        //act
        boolean result = Utilities.checkIfNumberisPalindromicNumber(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void checkIfNumberisPalindromicNumber_testeNumeroTerminadoEmZeroFalse(){
        //arrange
        int numero = 110;
        //act
        boolean result = Utilities.checkIfNumberisPalindromicNumber(numero);
        //assert
        assertFalse(result);
    }

    @Test
    void vetorElementosCapicuas_testeComElementosPossiveis(){
        //arrange
        int[] expected= {13531,898};
        //act
        int[]vetorInicial= {1234,13531,-13531,898,2390};
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosCapicuas();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void vetorElementosCapicuas_testeSemElementosPossiveis(){
        //arrange
        int[] expected= {};
        //act
        int[]vetorInicial= {-123,-467,523,462,432,537,389};
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosCapicuas();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void vetorElementosCapicuas_testeVetorVazio(){
        //arrange
        int[] expected= {};
        //act
        int[]vetorInicial= null;
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosCapicuas();
        //assert
        assertArrayEquals(result,expected);
    }


    //alinea_x.
    @Test
    void numeroCompostoPorUmMesmoDigito_testeValorPositivoTrue(){
        //arrange
        int numero = 222;
        //act
        boolean result = Utilities.numeroCompostoPorUmMesmoDigito(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void numeroCompostoPorUmMesmoDigito_testeValorPositivoFalse(){
        //arrange
        int numero = 232;
        //act
        boolean result = Utilities.numeroCompostoPorUmMesmoDigito(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void numeroCompostoPorUmMesmoDigito_testeValorNegativoTrue(){
        //arrange
        int numero = -111;
        //act
        boolean result = Utilities.numeroCompostoPorUmMesmoDigito(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void numeroCompostoPorUmMesmoDigito_testeValorNegativoFalse(){
        //arrange
        int numero = -292;
        //act
        boolean result = Utilities.numeroCompostoPorUmMesmoDigito(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void numeroCompostoPorUmMesmoDigito_testeAlgarismoPositivo(){
        //arrange
        int numero = 3;
        //act
        boolean result = Utilities.numeroCompostoPorUmMesmoDigito(numero);
        //assert
       assertTrue(result);
    }
    @Test
    void numeroCompostoPorUmMesmoDigito_testeAlgarismoNegativo(){
        //arrange
        int numero = -3;
        //act
        boolean result = Utilities.numeroCompostoPorUmMesmoDigito(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void numeroCompostoPorUmMesmoDigito_testeZero(){
        //arrange
        int numero = 0;
        //act
        boolean result = Utilities.numeroCompostoPorUmMesmoDigito(numero);
        //assert
        assertTrue(result);
    }

    @Test
    void vetorElementosCompostosPorUmMesmoDigito_testeComElementosPossiveis(){
        //arrange
        int[] expected= {222,-55,111,3};
        //act
        int[]vetorInicial= {1234,222,-55,898,111,10,3};
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosCompostosPorUmMesmoDigito();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void vetorElementosCompostosPorUmMesmoDigito_testeSemElementosPossiveis(){
        //arrange
        int[] expected= {};
        //act
        int[]vetorInicial= {-123,-467,523,462,432,537,389};
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosCompostosPorUmMesmoDigito();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void vetorElementosCompostosPorUmMesmoDigito_testeVetorVazio(){
        //arrange
        int[] expected= {};
        //act
        int[]vetorInicial= null;
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosCompostosPorUmMesmoDigito();
        //assert
        assertArrayEquals(result,expected);
    }

    //alinea_y.
    @Test
    void getCheckIfArmstrongNumber_testeTrue(){
        //arrange
        int numero = 370;
        //act
        boolean result = Utilities.getCheckIfArmstrongNumber(numero);
        //assert
        assertTrue(result);
    }
    @Test
    void getCheckIfArmstrongNumber_testeFalse1(){
        //arrange
        int numero = -371;
        //act
        boolean result = Utilities.getCheckIfArmstrongNumber(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void getCheckIfArmstrongNumber_testeFalse2(){
        //arrange
        int numero = 372;
        //act
        boolean result = Utilities.getCheckIfArmstrongNumber(numero);
        //assert
        assertFalse(result);
    }
    @Test
    void getCheckIfArmstrongNumber_testeTrue2(){
        //arrange
        int numero = 0;
        //act
        boolean result = Utilities.getCheckIfArmstrongNumber(numero);
        //assert
        assertTrue(result);
    }

    @Test
    void vetorElementosNotArmstrong_testeSemElementosPossiveis(){
        //arrange
        int[] expected= {};
        //act
        int[]vetorInicial= {3,153,3,407};
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosNotArmstrong();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void vetorElementosNotArmstrong_testeComElementosPossiveis(){
        //arrange
        int[] expected= {-123,-467,523,462,432,537,389};
        //act
        int[]vetorInicial= {-123,-467,523,462,432,537,389};
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosNotArmstrong();
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void vetorElementosNotArmstrong_testeVetorVazio(){
        //arrange
        int[] expected= {};
        //act
        int[]vetorInicial= null;
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorElementosNotArmstrong();
        //assert
        assertArrayEquals(result,expected);
    }

    //alinea_z.
    @Test
    void vetorLEmentosSequenciaCrescenteDeNDigitos_testeComElementosPossiveis(){
        //arrange
        int[] expected= {1234,-431};
        int numeroDigitos = 3;
        //act
        int[]vetorInicial= {1234,222,-431,898,111,17,3};
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorLEmentosSequenciaCrescenteDeNDigitos(numeroDigitos);
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void vetorLEmentosSequenciaCrescenteDeNDigitos_testeSemElementosPossiveis(){
        //arrange
        int[] expected= {};
        int numeroDigitos =4;
        //act
        int[]vetorInicial= {-123,-467,523,462,432,537,389};
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorLEmentosSequenciaCrescenteDeNDigitos(numeroDigitos);
        //assert
        assertArrayEquals(result,expected);
    }
    @Test
    void vetorLEmentosSequenciaCrescenteDeNDigitos_testeVetorVazio(){
        //arrange
        int[] expected= {};
        int numeroDigitos =2;
        //act
        int[]vetorInicial= null;
        UnidimensionalArray vetor= new UnidimensionalArray(vetorInicial);
        int[] result = vetor.vetorLEmentosSequenciaCrescenteDeNDigitos(numeroDigitos);
        //assert
        assertArrayEquals(result,expected);
    }

    //alinea_aa.
    @Test                                                                                                                    // DUVIDA!!!!!!!!!!!!!!!!!!!!!!
    void metodoEqualsDaClasseArrayUnidimensional_true(){
        //arrange
        int [] vetorExpected = {6,4,4,0,-1,-2,-5,-7};
        UnidimensionalArray expected = new UnidimensionalArray(vetorExpected);
        //act
        int[] vetorInicial = {-2, -5, 4, 4, 6, -1, -7,0};
        UnidimensionalArray result = new UnidimensionalArray(vetorInicial);
        result.ordenarOrdemDescendenteVetor();
        //assert
        assertEquals(expected,result);
    }


    //Exercicio3 Metodos
    @Test                                                                                                                    // DUVIDA!!!!!!!!!!!!!!!!!!!!!!
    void transposeVector() {
        //arrange
        int[]initialVector={1,2,3};
        UnidimensionalArray vector = new UnidimensionalArray(initialVector);
        int[][]expected = {{1},{2},{3}};
        //act
        int[][] result = vector.transposeVector();
        //assert
        assertArrayEquals(expected, result);
    }
    @Test                                                                                                                    // DUVIDA!!!!!!!!!!!!!!!!!!!!!!
    void transposeVectorEmptyVector() {
        //arrange
        int[]initialVector=null;
        UnidimensionalArray vector = new UnidimensionalArray(initialVector);
        int[][]expected = {};
        //act
        int[][] result = vector.transposeVector();
        //assert
        assertArrayEquals(expected, result);
    }






}