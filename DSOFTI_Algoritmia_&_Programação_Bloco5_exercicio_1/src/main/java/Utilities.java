public class Utilities {

    //Exercise 1

    /**
     * Verifica se um dado Elemento é primo de um dado divisor
     *
     * @param valor
     * @param divisor
     * @return
     */
    public static boolean verificarSeElementoMultiploDeUmDadoDivisor(int valor, int divisor) {
        boolean elementoMultiplo = false;
        if (valor >= 0) {
            if (divisor == 0) {
                elementoMultiplo = true;
            } else if (valor % divisor == 0) {
                elementoMultiplo = true;
            }
        }
        return elementoMultiplo;
    }

    /**
     * Calcula o numero de digitos em cada elemento
     *
     * @param numero
     * @return
     */
    public static int contaDigitosDeUmElemento(int numero) {
        int copynumero = Math.abs(numero);      //valor em absoluto para contar os algarismos
        int numeroDigitos = 0;
        do {
            numeroDigitos += 1;
            copynumero = copynumero / 10;

        } while (copynumero > 0);
        return numeroDigitos;
    }

    /**
     * Adicionar um elemento ao vetor.
     *
     * @param v
     * @param number
     * @return
     */
    public static int[] adicionarNoVetor(int[] v, int number) {
        int size = 1;
        if (v != null) {
            size += v.length;
        }
        int[] vetorResultante = new int[size];
        for (int position = 0; position < size - 1; position++) {   //garante que o vetor resultante guarda os elementos do vetor inicial
            vetorResultante[position] = v[position];
        }
        vetorResultante[size - 1] = number;     //insere o valor do elemento adicionado ao vetor resultante.
        return vetorResultante;
    }

    /**
     * Verifica se um dado algarismo é par.
     *
     * @param numero
     * @return
     */
    public static boolean verificarSeAlgarismoPar(int numero) {
        int algarismo = Math.abs(numero);
        if (algarismo >= 0 && algarismo < 10) {
            boolean algarismoPar = false;
            if (algarismo % 2 == 0) {
                algarismoPar = true;
            }
            return algarismoPar;
        } else {
            throw new IllegalArgumentException("Metodo exclusivo para algarismos");
        }

    }

    /**
     * Calcular a percentagem de digitos pares num dado numero.
     *
     * @return
     */
    public static double obterPercentagemAlgarismosParNumDadoNumero(int numero) {
        int copynumero = Math.abs(numero);
        int somaAlgarismosPares = 0;
        do {
            int unidade = copynumero % 10;
            boolean algarismoPar = verificarSeAlgarismoPar(unidade);
            if (algarismoPar) {
                somaAlgarismosPares = somaAlgarismosPares + 1;
            }
            copynumero = copynumero / 10;
        } while (copynumero > 0);
        int numeroDigitosNumero = contaDigitosDeUmElemento(numero);
        double percentagemDigitosPares = somaAlgarismosPares * 100 * 1.0 / numeroDigitosNumero;
        return percentagemDigitosPares;
    }

    /**
     * Verifica se um dado Elemento é apenas composto por algarismos pares
     *
     * @param numero
     * @return
     */
    public static boolean verificarSeElementoApenasCompostoPorDigitosPares(int numero) {
        int copyNumero = Math.abs(numero);
        do {
            int unidade = copyNumero % 10;
            boolean unidadePar = verificarSeAlgarismoPar(unidade);
            if (!unidadePar) {
                return false;
            }
            copyNumero = copyNumero / 10;
        } while (copyNumero > 0);
        return true;
    }

    /**
     * Valida se um dado numero é uma sequência crescente (digitos por ordem crescente)
     *
     * @param numero
     * @return
     */
    public static boolean numeroSequenciaCrescente(int numero) {
        int unidade = (numero % 10);
        if (numero > 0) {
            int copyNumero = numero / 10;
            while (copyNumero > 0) {
                int valorReferência = unidade;
                unidade = copyNumero % 10;
                if (unidade >= valorReferência) {
                    return false;
                }
                copyNumero = copyNumero / 10;
            }
        } else {
            int copyNumero = Math.abs(numero);
            while (copyNumero > 0) {
                int valorReferência = unidade;
                unidade = copyNumero % 10;
                if (unidade <= valorReferência) {
                    return false;
                }
                copyNumero = copyNumero / 10;
            }
        }
        return true;
    }

    /**
     * Get the inverse number
     *
     * @param number
     * @return the inverse number
     */
    public static int getInverseNumber(int number) {
        int unitNumber;
        int inverseNumber;
        inverseNumber = 0;
        while (number != 0) {
            unitNumber = number % 10;
            inverseNumber = inverseNumber * 10 + unitNumber;
            number = number / 10;
        }
        return inverseNumber;
    }

    /**
     * Get if a number is a Palindromic Number. Palindromic Number are only valid for positive integers.
     *
     * @param number
     * @return true or false
     */
    public static boolean checkIfNumberisPalindromicNumber(int number) {
        boolean palindromicNumber = false;
        int inverseNumber = getInverseNumber(number);
        if (number > 0) {
            palindromicNumber = number == inverseNumber;
        }
        return palindromicNumber;
    }

    /**
     * Verifica se um dado numero é composto pela repetição de apenas um único digito.
     *
     * @return
     */
    public static boolean numeroCompostoPorUmMesmoDigito(int numero) {
        boolean numeroCompostoPeloMesmoDigito = true;
        int copyNumero = numero;
        int numeroReferencia = numero % 10;
        do {
            int unidade = copyNumero % 10;
            if (unidade != numeroReferencia) {
                numeroCompostoPeloMesmoDigito = false;
            }
            copyNumero = copyNumero / 10;
        } while (copyNumero != 0 && numeroCompostoPeloMesmoDigito);
        return numeroCompostoPeloMesmoDigito;
    }

    /**
     * Get if a Number is a Armstrong Number
     *
     * @param number
     * @return true or false
     */
    public static boolean getCheckIfArmstrongNumber(int number) {
        boolean armstrongNumber = false;
        int countArmstrongNumbers = contaDigitosDeUmElemento(number);      //indica-me a potência n.
        int unitDigit;
        double result = 0;
        int num = number;
        while (num > 0) {                  //decompozição do numero, removendo o algarismo das unidades.
            unitDigit = num % 10;
            result = result + Math.pow(unitDigit, countArmstrongNumbers);
            num = num / 10;
        }
        armstrongNumber = result == number;
        return armstrongNumber;
    }

    //Exercise2

    /**
     * @param initialMatrix
     * @return
     */
    public static int[][] removeFirstEmptyLine(int[][] initialMatrix) {
        if (emptyLine(initialMatrix)) {
            int[][] resultMatrix = new int[initialMatrix.length - 1][];
            boolean lineRemoved = false;
            for (int line = 0; line < resultMatrix.length; line++) {
                if (initialMatrix[line].length == 0) {
                    lineRemoved = true;
                }
                if (lineRemoved) {
                    resultMatrix[line] = new int[initialMatrix[line + 1].length];
                    for (int column = 0; column < resultMatrix[line].length; column++) {
                        resultMatrix[line][column] = initialMatrix[line + 1][column];
                    }
                } else {
                    resultMatrix[line] = new int[initialMatrix[line].length];
                    for (int column = 0; column < resultMatrix[line].length; column++) {
                        resultMatrix[line][column] = initialMatrix[line][column];
                    }
                }
            }
            return resultMatrix;
        } else {
            return initialMatrix;
        }

    }

    /**
     * Verifies if matrix has an empty line
     *
     * @param intialMatrix
     * @return
     */
    public static boolean emptyLine(int[][] intialMatrix) {
        for (int line = 0; line < intialMatrix.length; line++) {
            if (intialMatrix[line].length == 0) {
                return true;
            }
        }
        return false;
    }

    public static int[][] addToMatrix(int[][] initialMatrix, int lineIndex, int newNumber) {
        int[][] resultMatrix;
        if (initialMatrix.length == 0) {
            resultMatrix = new int[1][1];                                                       //array cannot be null, but it can be empty
            resultMatrix[0][0] = newNumber;
        } else {
            if (lineIndex < initialMatrix.length && lineIndex >= 0) {                                          //Add a new element to the encapsulated array on an existing line
               resultMatrix = new int[initialMatrix.length][];
                for (int line = 0; line < initialMatrix.length; line++) {
                    if (lineIndex == line) {
                        resultMatrix[line] = new int[initialMatrix[line].length + 1];
                    } else {
                        resultMatrix[line] = new int[initialMatrix[line].length];
                    }
                    for (int column = 0; column < initialMatrix[line].length; column++) {                   //It ensures that the resulting matrix holds the elements of the initial matrix
                        resultMatrix[line][column] = initialMatrix[line][column];
                    }
                }
                resultMatrix[lineIndex][resultMatrix[lineIndex].length - 1] = newNumber;
            } else {                                                                                     //create new line in matrix
                resultMatrix = new int[initialMatrix.length + 1][];
                for (int line = 0; line < initialMatrix.length; line++) {
                    resultMatrix[line] = new int[initialMatrix[line].length];
                    for (int column = 0; column < initialMatrix[line].length; column++) {               //It ensures that the resulting matrix holds the elements of the initial matrix
                        resultMatrix[line][column] = initialMatrix[line][column];
                    }
                }
                resultMatrix[resultMatrix.length-1] = new int[1];
                resultMatrix[resultMatrix.length-1][0] = newNumber;
            }
        }
        return resultMatrix;
    }
}
