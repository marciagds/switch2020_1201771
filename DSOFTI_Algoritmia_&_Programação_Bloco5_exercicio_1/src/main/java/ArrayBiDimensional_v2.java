import java.util.Arrays;

public class ArrayBiDimensional_v2 {
    //atributos
    private UnidimensionalArray[] matrix;           //só acrescento uma dimensão (a outra dimensão foi criada na classe vetor)

    //Métodos Construtores

    /**
     * Construção da matriz a partir de um objeto vazio
     */
    public ArrayBiDimensional_v2() {
        this.matrix = new UnidimensionalArray[0];      //construir matriz vazia (não existe - não tem dimensão)
    }

    public ArrayBiDimensional_v2(UnidimensionalArray[] initialMatrix) {
        this.matrix = createCopyOfUnidimensionalArray(initialMatrix);

        /*
        this.matrix = new ArrayUnidimensional[matrizInicial.length];        //quantidade de linhas na matriz
        for (int i = 0; i < matrizInicial.length; i++) {
            this.matrix[i] = matrizInicial[i];              //estou a copiar cada vetor da matriz (linha a linha)           //criar metodo para a copia arrayunidimensiona[] -> arrayunidimensiona[]
        }
         */
    }

    /**
     * Construtctor Method that inicializes the matrix with imput int matrix
     *
     * @param initialMatrix
     */
    public ArrayBiDimensional_v2(int[][] initialMatrix) {
        if (initialMatrix == null) {
            this.matrix = new UnidimensionalArray[0];
        } else {
            this.matrix = createCopyOfIntMatrix(initialMatrix);
        }
    }


    //Equal Method
    @Override
    public boolean equals(Object o) {
        if (this == o) {            //Serem da mesma classe
            return true;
        }
        if (!(o instanceof ArrayBiDimensional_v2)) {          //se são do mesmo tipo de variável
            return false;
        }

        ArrayBiDimensional_v2 other = (ArrayBiDimensional_v2) o;        //converter o objeto(o) no tipo de classe criada

        if (this.matrix.length == (other.matrix.length)) {
            for (int i = 0; i < matrix.length; i++) {                //verifica se o conteúdo dentro do objeto é igual ao conteúdo do this.matrix
                if (!this.matrix[i].equals(other.matrix[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }


    //Other Methods
    public UnidimensionalArray[] toArrayUnidimensional() {
        UnidimensionalArray[] resultMatrix = this.matrix;
        return resultMatrix;
    }

    /**
     * @param
     */
    public int[][] toArray() {
        int[][] resultMatrix = new int[0][];
        if (this.matrix.length == 0) {
            resultMatrix[0] = new int[0];
        } else {
            resultMatrix = createCopy(this.matrix);
        }
        return resultMatrix;
    }

    //UnidimensionalArray[] initialMatrix

    /**
     * Method that allows to create a copy of an input array.
     *
     * @param initialMatrix - imput array
     * @return copy initialMatrix
     */
    private int[][] createCopy(UnidimensionalArray[] initialMatrix) {
        int[][] copy;
        if (initialMatrix == null) {            //Not allow null arrays in the class
            copy = new int[0][0];
        } else {
            copy = new int[initialMatrix.length][];
            for (int i = 0; i < initialMatrix.length; i++) {
                copy[i] = matrix[i].toArray();
            }
        }
        return copy;
    }


    //Business Methods

    /**
     * Criar cópia de uma matriz inteiros para o atributo da classe (ArrayUnidimensional[])
     *
     * @param initialMatrix
     * @return
     */
    private UnidimensionalArray[] createCopyOfIntMatrix(int[][] initialMatrix) {
        UnidimensionalArray[] copy;
        if (initialMatrix == null) {
            copy = new UnidimensionalArray[0];          //para evitar throw - nunca é null;
        } else {
            copy = new UnidimensionalArray[initialMatrix.length];
            for (int i = 0; i < initialMatrix.length; i++) {
                copy[i] = new UnidimensionalArray(initialMatrix[i]);            //primeiro convertes matrizInical para ArrayUnidimensional. E depois é que igualas ao copy
            }
        }
        return copy;
    }

    /**
     * Criar cópia de uma matriz ArrayUnidimensional[] para o atributo da classe (ArrayUnidimensional[])
     *
     * @param initialMatrix
     * @return
     */
    private UnidimensionalArray[] createCopyOfUnidimensionalArray(UnidimensionalArray[] initialMatrix) {
        UnidimensionalArray[] copy;
        if (initialMatrix == null) {
            copy = new UnidimensionalArray[0];
        } else {
            copy = new UnidimensionalArray[initialMatrix.length];
            for (int i = 0; i < initialMatrix.length; i++) {
                copy[i] = initialMatrix[i];                         //posso igualar porque são dois objetos iguais (do mesmo tipo de variável)
            }
        }
        return copy;
    }

    /**
     * Add value to matrix
     *
     * @param indexLine
     * @param newNumber
     */
    public void addToMatrix(int indexLine, int newNumber) {
        if (matrix.length == 0 && indexLine >= 0) {
            UnidimensionalArray[] matrixAux = new UnidimensionalArray[1];
            matrixAux[0] = new UnidimensionalArray(newNumber);
            this.matrix = matrixAux;
        } else {
            if (indexLine >= 0) {
                if (indexLine < matrix.length) {
                    UnidimensionalArray vector = matrix[indexLine];
                    vector.adicionarNoVetor(newNumber);
                } else {
                    UnidimensionalArray vector = new UnidimensionalArray(newNumber);
                    UnidimensionalArray[] matrixAux = new UnidimensionalArray[matrix.length + 1];
                    for (int line = 0; line < matrix.length; line++) {
                        matrixAux[line] = matrix[line];
                    }
                    matrixAux[matrixAux.length - 1] = vector;
                    this.matrix = matrixAux;
                }
            } else {
                throw new IndexOutOfBoundsException("Invalid Index");
            }
        }
    }

    /**
     * Remove one value from matrix
     *
     * @param value
     */
    public void removeValue(int value) {
        boolean valueRemoved = false;
        for (int line = 0; line < matrix.length && !valueRemoved; line++) {
            UnidimensionalArray vectorAux = matrix[line].createClone();
            vectorAux.removerPrimeiroElementoDoArrayComUmDadoValor(value);
            if (!vectorAux.equals(matrix[line])) {
                valueRemoved = true;
            }
            this.matrix[line] = vectorAux;
        }
    }


    /**
     * It checks if matrix is empty
     *
     * @return
     */
    public boolean emptyMatrix() {
        return matrix.length == 0;
    }

    /**
     * It returns the highest element on the matrix
     *
     * @return
     */
    public int highestElementMatrix() {
        int highestElement;
        int[] auxVector = new int[matrix.length];
        for (int line = 0; line < matrix.length; line++) {
            UnidimensionalArray vector = matrix[line];
            auxVector[line] = vector.highestElement();
        }
        UnidimensionalArray auxResultVector = new UnidimensionalArray(auxVector);
        highestElement = auxResultVector.highestElement();
        return highestElement;
    }

    /**
     * It returns the lowest element on the matrix
     *
     * @return
     */
    public int lowestElement() {
        int lowestElement;
        int[] auxVector = new int[matrix.length];
        for (int line = 0; line < matrix.length; line++) {
            UnidimensionalArray vector = matrix[line];
            auxVector[line] = vector.lowestElement();
        }
        UnidimensionalArray auxResultVector = new UnidimensionalArray(auxVector);
        lowestElement = auxResultVector.lowestElement();
        return lowestElement;
    }

    /**
     * It returns the average of elements on the matrix
     *
     * @return
     */
    public double averageElements() {
        if (matrix.length == 0) {
            return 0;
        } else {
            double averageElements;
            double sumElemnts = 0;
            int countElements = 0;
            for (int line = 0; line < matrix.length; line++) {
                UnidimensionalArray vector = matrix[line];
                sumElemnts += vector.sumElements();
                countElements += vector.countElements();
            }
            averageElements = sumElemnts / countElements;
            return averageElements;
        }
    }

    /**
     * It returns an UnidimensionalArray with the sum of the Elements in each line index
     *
     * @return
     */
    public UnidimensionalArray sumLineIndexElements() {
        int[] auxVector = new int[matrix.length];
        for (int line = 0; line < matrix.length; line++) {
            UnidimensionalArray vector = matrix[line];
            auxVector[line] = vector.sumElements();
        }
        UnidimensionalArray resultVector = new UnidimensionalArray(auxVector);
        return resultVector;
    }

    /**
     * It returns an UnidimensionalArray with the sum of the Elements in each line index
     *
     * @return
     */
    public UnidimensionalArray sumColumnIndexElements() {
        int[] auxResultVector = new int[highestColumn()];
        for (int column = 0; column < highestColumn(); column++) {
            int sumColumnElements = 0;
            for (int line = 0; line < matrix.length; line++) {
                sumColumnElements += matrix[line].valueOnIndex(column);
            }
            auxResultVector[column] = sumColumnElements;
        }
        UnidimensionalArray resultVector = new UnidimensionalArray(auxResultVector);
        return resultVector;
    }

    /**
     * Returns the highest number of columns in the matrix.
     *
     * @return
     */
    private int highestColumn() {
        if (matrix.length != 0) {
            int highest = matrix[0].countElements();
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i].countElements() > highest) {
                    highest = matrix[i].countElements();
                }
            }
            return highest;
        }
        return 0;
    }

    /**
     * Index line of the highest
     *
     * @return
     */
    public int indexLineOfHighestSumLine() {
        UnidimensionalArray vectorSumElementsLineMatrix = sumLineIndexElements();
        int index = vectorSumElementsLineMatrix.indexOfHighestElement();
        return index;
    }

    /**
     * Check if square matrix
     *
     * @return
     */
    public boolean squareMatrix() {
        if (matrix.length == 0) {
            return false;
        }
        boolean squareMatrix = true;
        for (int line = 0; line < matrix.length && squareMatrix; line++) {
            UnidimensionalArray vector = matrix[line];
            if (vector.countElements() != matrix.length) {
                squareMatrix = false;
            }
        }
        return squareMatrix;
    }

    /**
     * It returns the transpose matrix
     * @return
     */
    public int[][] transposedMatrix() {
        if (squareMatrix()) {
            ArrayBiDimensional_v2 transposedResultMatrix = new ArrayBiDimensional_v2();
            int[][]transposedMatrix=new int[matrix.length][matrix.length];          //square matrix
            for (int i = 0; i < matrix.length; i++) {
                int[][] auxVextor = matrix[i].transposeVector();
                for (int indexauxVextor = 0; indexauxVextor < auxVextor.length; indexauxVextor++) {
                    transposedResultMatrix.addToMatrix(indexauxVextor,auxVextor[indexauxVextor][0]);
                }
            }
            return transposedMatrix;
        }
        return null;

    }

    /**
     * It verifies if matrix is symmetric
     *
     * @return
     */
    /////ACERTAR/////////////////////
    public boolean symmetricMatrix() {
        if (squareMatrix()) {
            int[][] transposedResultMatrix = transposedMatrix();
            for (int line = 0; line < matrix.length; line++) {
                if (!Arrays.equals(matrix[line], transposedResultMatrix[line])) {            ///Aplica-se o método to.equals para arrays primitivos.
                    return false;
                }
            }/*
            for (int line = 0; line < matrix.length; line++) {
                for (int column = 0; column < matrix.length; column++) {
                    if (matrix[line][column] != transposedMatrix[line][column]) {

                    }
                }
            }
             */
            return true;
        }
        return false;
    }


    /**
     *Return the number of non-null elements on the main diagonal of the matrix (if square) or -1 (if not square).
     * @return
     */
    public int diagonalElementsNotZero() {
        if (squareMatrix()) {
            int countNotNullElements=0;
            for (int line = 0; line < matrix.length; line++) {
                if(matrix[line].valueOnIndex(line) !=0){
                    countNotNullElements +=1;
                }
            }
            return countNotNullElements;
        }else{
            return  -1;
        }
    }

    /**
     *Return the number of non-null elements on the main diagonal of the matrix (if square) or -1 (if not square).
     * @return
     */
    public boolean mainAndSecondaryDiagonalEquals() {
        if (squareMatrix()) {
            for (int line = 0; line < matrix.length; line++) {
                int secondDiagIndex = (matrix[line].countElements() - 1) - line;
                if (matrix[line].valueOnIndex(line) != matrix[line].valueOnIndex(secondDiagIndex)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

/*
    public int[][] elementsWithHigherPercentageEvenNumbers() {
        int[][] elementsMatrix = new int[0][0];
        double averageEvenDigits=;
        for (int line = 0; line < matrix.length; line++) {
            elementsMatrix[line] += matrix[line].elementsWithHigherPercentageEvenNumbersThanAverage(averageEvenDigits);
           // int element = matrix[line].;
                double evenDigitsAverageElement = Utilities.obterPercentagemAlgarismosParNumDadoNumero(matrix[i][k]);
                if (evenDigitsAverageElement > evenDigitsPercentageAverageInMatrixElements()) {
                    elementsMatrix = Utilities.addToMatrix(elementsMatrix, i, matrix[i][k]);
                }
            }
        }
        return elementsMatrix;
    }

 */




}