import java.util.Arrays;

public class UnidimensionalArray {
    //Atributos
    private int[] vector;


    //Métodos Construtores

    /**
     * Constructor method that initializes an empty vector.
     */
    public UnidimensionalArray() {          //guarda a morada do vetor
        this.vector = new int[0];            //definir vetor
    }

    /**
     *Constructor method that initializes the vector with 3 elements.
     * @param number1
     * @param number2
     * @param number3
     */
    public UnidimensionalArray(int number1, int number2, int number3) {
        this.vector = new int[3];
        this.vector[0] = number1;
        this.vector[1] = number2;
        this.vector[2] = number3;
    }

    /**
     * Constructor method that initializes the vector with 1 element.
     * @param number

     */
    public UnidimensionalArray(int number) {
        this.vector = new int[1];
        this.vector[0] = number;
    }

    /**
     * Constructor method that initializes the vector from another integer vector
     * @param initialVector
     */
    public UnidimensionalArray(int[] initialVector) {
        this.vector = createCopyIntegerVector(initialVector);
    }

    /**
     * Constructor method that initializes the vector with 1 element.
     * @param number

     */
   /* public UnidimensionalArray(int size) {
        this.vector = new int[size];
    }

    */




    //Outros Métodos
    /**
     * Método para criar copia de um vetor (Evita duplicação de linhas de código)
     *
     * @param v
     * @return
     */
    private UnidimensionalArray[] createCopyIntegerVector(UnidimensionalArray[] v) {
        UnidimensionalArray[] copy;
        if (v == null) {
            copy = new UnidimensionalArray[0];
        } else {
            copy = new UnidimensionalArray[v.length];
            for (int i = 0; i < v.length; i++) {
                copy[i] = v[i];
            }
        }
        return copy;
    }

    /**
     * Print the class matrix in Array
     *
     * @return int[][] matrix
     */
    public int[] toArray() {
        int[] intitialArray = createCopyIntegerVector(this.vector);
        return intitialArray;
    }

    /**
     * It creats a clone of the vector
     * @return
     */
    public UnidimensionalArray createClone() {      //impede referencias partilhadas!!!!
        UnidimensionalArray copy = new UnidimensionalArray(vector);
        for (int i = 0; i < this.vector.length; i++) {
            copy.vector[i] = this.vector[i];                        // aceder ao valor no indice
        }
        return copy;
    }

    /**
     * Method for creating copies of a Integer vector ( It avoids duplication of lines of code)
     * @param initialVector
     * @return
     */
    private int[] createCopyIntegerVector(int[] initialVector) {
        int[] copy;
        if (initialVector == null) {
            copy = new int[0];
        } else {
            copy = new int[initialVector.length];
            for (int i = 0; i < initialVector.length; i++) {
                copy[i] = initialVector[i];
            }
        }
        return copy;
    }

    /**
     * Permite imprimir o vetor da classe
     *
     * @return
     */
    public String toString() {      //Metodo de interface. Permite imprimir a classe noutra classe.
        return Arrays.toString(vector);
    }

    /**
     * Método que garante que se o vetor for null dá erro.
     */
    private void emptyVector() {
        if (this.vector.length == 0) {
            throw new IllegalStateException("O vetor é vazio");
        }
    }


    //Métodos de negócio
    /**
     * Verifica se o vetor apresenta apenas um único elemento
     *
     * @return
     */
    public boolean verificarSeVetorTemApenasUmElemento() {
        boolean vetorComElementoUnico = false;
        if (vector.length == 1) {
            vetorComElementoUnico = true;
        }
        return vetorComElementoUnico;
    }

    /**
     * Adiciona um novo valor ao vetor da classe
     *
     * @param newNumber
     */
    public void adicionarNoVetor(int newNumber) {
        emptyVector();
        int size = 1;
        if (this.vector != null) {
            size += this.vector.length;
        }
        int[] vetorResultante = new int[size];
        for (int position = 0; position < size - 1; position++) {   //garante que o vetor resultante guarda os elementos do vetor inicial
            vetorResultante[position] = this.vector[position];
        }
        vetorResultante[size - 1] = newNumber;     //insere o valor do elemento adicionado ao vetor resultante.
        this.vector = vetorResultante;
    }

    /**
     * Remove o primeiro elemento que apresente um determinado valor
     *
     * @param numeroRemover
     */
    public void removerPrimeiroElementoDoArrayComUmDadoValor(int numeroRemover) {
        emptyVector();
        boolean existeNumeroRemoverNoVetor = verificarSeDadoNumeroExisteNoVetor(numeroRemover);
        if (existeNumeroRemoverNoVetor) {
            int[] vetorResultante = new int[vector.length - 1];
            boolean numeroRemoverEncontrado = false;
            for (int i = 0; i < vetorResultante.length; i++) {
                if (vector[i] == numeroRemover) {
                    numeroRemoverEncontrado = true;
                }
                if (numeroRemoverEncontrado == false) {
                    vetorResultante[i] = vector[i];
                } else {
                    vetorResultante[i] = vector[i + 1];
                }
            }
            this.vector = vetorResultante;
        }
    }

    /**
     * Verifica se um dado valor está presente no vetor
     *
     * @param number
     * @return
     */
    private boolean verificarSeDadoNumeroExisteNoVetor(int number) {
        emptyVector();
        boolean numeroExisteNoVetor = false;
        for (int i = 0; i < vector.length && numeroExisteNoVetor == false; i++) {
            if (vector[i] == number) {
                numeroExisteNoVetor = true;
            }
        }
        return numeroExisteNoVetor;
    }

    /**
     * Retorna o valor do Elemento do Array num dado index
     *
     * @param index
     * @return
     */
    public int valueOnIndex(int index) {
        if(vector.length==0 || index<0){
            return 0;
        }
        else {
            if (vector.length != 0 && index < vector.length) {
                int valueOnIndex = vector[index];
                return valueOnIndex;
            }
            return 0;
        }
    }

    /**
     * Returns the highest element of the vector
     * @return
     */
    public int highestElement() {
        emptyVector();
        int highestElement = this.vector[0];                        //reference value
        for (int i = 0; i < this.vector.length; i++) {
            if (vector[i] > highestElement) {
                highestElement = vector[i];
            }
        }
        return highestElement;
    }

    /**
     *  Returns the lowest element of the vector
     * @return
     */
    public int lowestElement() {
        emptyVector();
        int lowestElement = this.vector[0];                     //reference value
        for (int i = 0; i < this.vector.length; i++) {
            if (vector[i] < lowestElement) {
                lowestElement = vector[i];
            }
        }
        return lowestElement;
    }

    /**
     * Average vector's elements
     * @return
     */
    public double averageElements() {
        int sumElements = sumElements();
        int countElements = countElements();
        if (countElements == 0) {
            return 0;
        }
        else {
            double averageElements = (sumElements * 1.0) / countElements;
            return averageElements;
        }
    }

    /**
     * Sum of the vector elements
     * @return
     */
    public int sumElements() {
        int sum = 0;
        for (int i = 0; i < this.vector.length; i++) {
            sum += vector[i];
        }
        return sum;
    }

    /**
     * Contar numero de Elementos Pares
     *
     * @return
     */
    private int contarElementosPares() {
        int numeroElementosPares = 0;
        for (int i = 0; i < vector.length; i++) {
            boolean elementoPar = verificarSeElementoPar(vector[i]);
            if (elementoPar) {
                numeroElementosPares += 1;
            }
        }
        return numeroElementosPares;
    }

    /**
     * Calcula a soma dos Elementos Pares
     *
     * @return
     */
    private int calculaSomaElementosPares() {
        int somaElementosPares = 0;
        for (int i = 0; i < vector.length; i++) {
            boolean elementoPar = verificarSeElementoPar(vector[i]);
            if (elementoPar) {
                somaElementosPares += vector[i];
            }
        }
        return somaElementosPares;
    }

    /**
     * Contar numero de Elementos Impares
     *
     * @return
     */
    private int contarElementosImpares() {
        int numeroElementosImpares = 0;
        for (int i = 0; i < vector.length; i++) {
            boolean elementoPar = verificarSeElementoPar(vector[i]);
            if (!elementoPar) {
                numeroElementosImpares += 1;
            }
        }
        return numeroElementosImpares;
    }

    /**
     * Calcula a soma dos Elementos Impares
     *
     * @return
     */
    private int calculaSomaElementosImpares() {
        int somaElementosImpares = 0;
        for (int i = 0; i < vector.length; i++) {
            boolean elementoPar = verificarSeElementoPar(vector[i]);
            if (elementoPar == false) {
                somaElementosImpares += vector[i];
            }
        }
        return somaElementosImpares;
    }

    /**
     * Calcula a média dos Elementos Pares do vetor.
     *
     * @return
     */
    public Double mediaElementosPares() {
        emptyVector();
        int somaElementosPares = calculaSomaElementosPares();
        int numeroElementosPares = contarElementosPares();
        if (numeroElementosPares != 0) {
            double mediaElementosPares = (somaElementosPares * 1.0) / numeroElementosPares;
            return mediaElementosPares;
        } else {
            return null;
        }


    }

    /**
     * Verifica se um dado elemento do vetor é par
     *
     * @param elemento
     * @return
     */
    private boolean verificarSeElementoPar(int elemento) {
        boolean elementoPar = false;
        if (elemento % 2 == 0) {
            elementoPar = true;
        }
        return elementoPar;
    }

    /**
     * Calcula a média dos Elementos Pares do vetor.
     *
     * @return
     */
    public Double mediaElementosImpares() {
        emptyVector();
        int somaElementosImpares = calculaSomaElementosImpares();
        int numeroElementosImpares = contarElementosImpares();
        if (numeroElementosImpares != 0) {
            double mediaElementosImpares = (somaElementosImpares * 1.0) / numeroElementosImpares;
            return mediaElementosImpares;
        } else {
            return null;
        }
    }

    /**
     * Calcula a média dos Elementos Multiplos de uma dado divisor
     *
     * @param divisor
     * @return
     */
    public double mediaElementosMultiplos(int divisor) {
        int somaMultiplos = 0;
        int contaElementoPrimos = 0;
        for (int i = 0; i < vector.length; i++) {
            boolean valorMultiplo = Utilities.verificarSeElementoMultiploDeUmDadoDivisor(vector[i], divisor);
            if (valorMultiplo) {
                somaMultiplos += vector[i];
                contaElementoPrimos += 1;
            }
        }
        if (contaElementoPrimos != 0) {
            double mediaElementosPrimos = (somaMultiplos * 1.0) / contaElementoPrimos;
            return mediaElementosPrimos;
        } else {
            return 0;
        }
    }

    /**
     * Ordena os elementos do vetor por ordem crescente
     */
    public void ordenarOrdemAscendenteVetor() {
        for (int i = 0; i < vector.length; i++) {
            int index = i;
            int valorReferencia = vector[i];
            for (int j = (i + 1); j < (vector.length); j++) {
                if (vector[j] < valorReferencia) {
                    valorReferencia = vector[j];
                    index = j;
                }
            }
            //trocar elementos do sitio
            trocarElementosNoVetor(i, index);
        }
    }

    /**
     * Troca os elementos de dois indices no vetor
     *
     * @param i
     * @param j
     */
    private void trocarElementosNoVetor(int i, int j) {
        if (i != j) {
            int temp = vector[i];
            vector[i] = vector[j];
            vector[j] = temp;
        }

    }

    /**
     * Ordena os elementos do vetor por ordem decrescente
     */
    public void ordenarOrdemDescendenteVetor() {
        for (int i = 0; i < vector.length; i++) {
            int index = i;
            int valorReferencia = vector[i];
            for (int j = (i + 1); j < (vector.length); j++) {
                if (vector[j] > valorReferencia) {
                    valorReferencia = vector[j];
                    index = j;
                }
            }
            //trocar elementos do sitio
            trocarElementosNoVetor(i, index);
        }
    }

    /**
     * Verifica se todos os elementos de um dado vetor são pares.
     *
     * @return
     */
    public boolean verificarSeVetorPar() {
        emptyVector();
        boolean vetorPar = true;
        boolean elementoPar = true;
        for (int i = 0; i < vector.length && elementoPar; i++) {
            elementoPar = verificarSeElementoPar(vector[i]);
            if (!elementoPar) {
                vetorPar = false;
            }
        }
        return vetorPar;
    }

    /**
     * Verifica se todos os elementos de um dado vetor são impares.
     *
     * @return
     */
    public boolean verificarSeVetorImpar() {
        emptyVector();
        boolean vetorImpar = true;
        boolean elementoPar = true;
        for (int i = 0; i < vector.length && vetorImpar; i++) {
            elementoPar = verificarSeElementoPar(vector[i]);
            if (elementoPar) {
                vetorImpar = false;
            }
        }
        return vetorImpar;
    }

    /**
     * Verifica se o vetor tem elementos duplicados.
     *
     * @return
     */
    public boolean verificaSeVetorTemElementosDuplicados() {
        boolean elementosDuplicados = false;
        for (int i = 0; i < vector.length && !elementosDuplicados; i++) {
            for (int j = i + 1; j < vector.length; j++) {
                if (vector[i] == vector[j]) {
                    elementosDuplicados = true;
                }
            }
        }
        return elementosDuplicados;
    }

    /**
     * Retorne os elementos do vetor cujo número de algarismos é superior ao número médio de algarismos de todos os elementos do vetor.
     */
    public int[] obterVetorComElementosComNumeroDigitosSuperiorMedia() {
        int[] vetorResultante = new int[0];
        int mediaNumeroDigitosVetor = mediaNumeroDigitosElementosVetor();
        for (int i = 0; i < vector.length; i++) {
            int numeroDigitosElemento = Utilities.contaDigitosDeUmElemento(vector[i]);
            if (numeroDigitosElemento > mediaNumeroDigitosVetor) {
                vetorResultante = Utilities.adicionarNoVetor(vetorResultante, vector[i]);
            }
        }
        return vetorResultante;
    }

    /**
     * Calculo da média do numero de Digitos dos ELementos do vetor
     *
     * @return
     */
    private int mediaNumeroDigitosElementosVetor() {
        emptyVector();
        int somaNumeroDigitosElementos = 0;
        int numeroDigitos;
        for (int i = 0; i < vector.length; i++) {
            numeroDigitos = Utilities.contaDigitosDeUmElemento(vector[i]);
            somaNumeroDigitosElementos += numeroDigitos;
        }
        int mediaNumeroDigitos = somaNumeroDigitosElementos / vector.length;
        return mediaNumeroDigitos;
    }

    /**
     * média da percentagem de algarismos pares de todos os elementos do vetor.
     *
     * @return
     */
    private double mediaPercentagemAlgarismosParesElementosVetor() {
        if(vector.length==0){
            return 0;
        }
        else{
        double somaPercentagemDigitosParesElementos = 0;
        for (int i = 0; i < vector.length; i++) {
            double percentagemDigitosParesElemento = Utilities.obterPercentagemAlgarismosParNumDadoNumero(vector[i]);
            somaPercentagemDigitosParesElementos += percentagemDigitosParesElemento;
        }
            double mediaPercentagemAlgarismosParesElementosVetor = somaPercentagemDigitosParesElementos / vector.length;
            return mediaPercentagemAlgarismosParesElementosVetor;
        }
    }

    /**
     * Obter vetor cujos elementos apresentam  percentagem de algarismos pares é superior à média da percentagem de algarismos pares de todos os elementos do vetor
     *
     * @return
     */
    public int[] elementsWithHigherPercentageEvenNumbersThanAverage(double averageEvenDigitsTotalElements) {
        int[] vetorElementos = new int[0];
        for (int i = 0; i < vector.length; i++) {
            double mediaAlgarismosParesElemento = Utilities.obterPercentagemAlgarismosParNumDadoNumero(vector[i]);
            if (mediaAlgarismosParesElemento > averageEvenDigitsTotalElements) {
                vetorElementos = Utilities.adicionarNoVetor(vetorElementos, vector[i]);
            }
        }
        return vetorElementos;
    }

    /**
     * Retorna os elementos do vetor compostos exclusivamente por algarismos pares.
     *
     * @return
     */
    public int[] vetorElementosComApenasDigitosPares() {
        int[] vetorElementosComApenasDigitosPares = new int[0];
        for (int i = 0; i < vector.length; i++) {
            boolean elementoComApenasDigitosPares = Utilities.verificarSeElementoApenasCompostoPorDigitosPares(vector[i]);
            if (elementoComApenasDigitosPares) {
                vetorElementosComApenasDigitosPares = Utilities.adicionarNoVetor(vetorElementosComApenasDigitosPares, vector[i]);
            }
        }
        return vetorElementosComApenasDigitosPares;
    }

    /**
     * Retorna o vetos cujos elementos são sequencias crescentes.
     *
     * @return
     */
    public int[] vetorElementosSequenciaCrescenteDigitos() {
        int[] vetorElementos = new int[0];
        for (int i = 0; i < vector.length; i++) {
            boolean digitosSequenciaCrescente = Utilities.numeroSequenciaCrescente(vector[i]);
            if (digitosSequenciaCrescente) {
                vetorElementos = Utilities.adicionarNoVetor(vetorElementos, vector[i]);
            }
        }
        return vetorElementos;
    }

    /**
     * Retorna um vetor composto apenas pelos Elementos que são capicuas.
     *
     * @return
     */
    public int[] vetorElementosCapicuas() {
        int[] vetorElementosCapicuas = new int[0];
        for (int i = 0; i < vector.length; i++) {
            boolean elementoCapicua = Utilities.checkIfNumberisPalindromicNumber(vector[i]);
            if (elementoCapicua) {
                vetorElementosCapicuas = Utilities.adicionarNoVetor(vetorElementosCapicuas, vector[i]);
            }
        }
        return vetorElementosCapicuas;
    }

    /**
     * Retorna um vetor composto apenas pelos Elementos que são capicuas.
     *
     * @return
     */
    public int[] vetorElementosCompostosPorUmMesmoDigito() {
        int[] vetorElementosResultante = new int[0];
        for (int i = 0; i < vector.length; i++) {
            boolean numeroCompostoPeloMesmoDigito = Utilities.numeroCompostoPorUmMesmoDigito(vector[i]);
            if (numeroCompostoPeloMesmoDigito) {
                vetorElementosResultante = Utilities.adicionarNoVetor(vetorElementosResultante, vector[i]);
            }
        }
        return vetorElementosResultante;
    }

    /**
     * Retorna vetor cujos Elementos não são Números de Armstrong
     * @return
     */
    public int[] vetorElementosNotArmstrong() {
        int[] vetorElementosNotArmstrong = new int[0];
        for (int i = 0; i < vector.length; i++) {
            boolean elementoArmstrong = Utilities.getCheckIfArmstrongNumber(vector[i]);
            if (!elementoArmstrong) {
                vetorElementosNotArmstrong = Utilities.adicionarNoVetor(vetorElementosNotArmstrong, vector[i]);
            }
        }
        return vetorElementosNotArmstrong;
    }

    /**
     * Reetorna um vetor cujos elementos são Sequencia Crescente de n digitos
     * @return
     */
    public int[] vetorLEmentosSequenciaCrescenteDeNDigitos(int numeroDigitos){
        int[] vetorElementos = new int[0];
        for (int i = 0; i < vector.length; i++) {
            boolean elementoSequenciaCrescente = Utilities.numeroSequenciaCrescente(vector[i]);
            int digitosElemento = Utilities.contaDigitosDeUmElemento(vector[i]);
            if (elementoSequenciaCrescente && digitosElemento >= numeroDigitos) {
                vetorElementos = Utilities.adicionarNoVetor(vetorElementos, vector[i]);
            }
        }
        return vetorElementos;
    }

    //Metodos Getters and Setters


    /**
     * Permite aceder ao vetor
     *
     * @return
     */
    public int[] representarObjetoEmVetor() {
        int[] vetorResultante = createCopyIntegerVector(this.vector);
        return vetorResultante;
    }

    /**
     * Retorna o tamanho do vetor
     *
     * @return
     */
    public int countElements() {
        return this.vector.length;
    }

    /**
     * Obter o valor da Soma dos Elementos do Vetor.
     *
     * @return
     */
    public int obterSomaElementos() {
        int somaElementos = sumElements();
        return somaElementos;
    }

    /**
     * Retorna o número de Elementos Pares de um vetor.
     *
     * @return
     */
    public int obterNumeroElementosPares() {
        int numeroElementosPares = contarElementosPares();
        return numeroElementosPares;
    }

    /**
     * Retorna a soma dos Elementos Pares do vetor
     *
     * @return
     */
    public int obterSomaElementosPares() {
        int somaElementosPares = calculaSomaElementosPares();
        return somaElementosPares;
    }

    /**
     * Retorna o número de Elementos Impares de um vetor.
     *
     * @return
     */
    public int obterNumeroElementosImpares() {
        int numeroElementosImpares = contarElementosImpares();
        return numeroElementosImpares;
    }

    /**
     * Retorna a soma dos Elementos Impares do vetor
     *
     * @return
     */
    public int obterSomaElementosImpares() {
        int somaElementosImpares = calculaSomaElementosImpares();
        return somaElementosImpares;
    }

    /**
     * obter valor da média do numero de Digitos dos Elementos do vetor
     *
     * @return
     */
    public int obterMediaNumeroDigitosElementos() {
        int mediaNumeroDigitosElementos = mediaNumeroDigitosElementosVetor();
        return mediaNumeroDigitosElementos;
    }

    /**
     * obter valor da média da percentagem de algarismos pares de todos os elementos do vetor.
     *
     * @return
     */
    public double obterMediaPercentagemAlgarismosParesElementosVetor() {
        double mediaPercentagemAlgarismosParesElementosVetor = mediaPercentagemAlgarismosParesElementosVetor();
        return mediaPercentagemAlgarismosParesElementosVetor;
    }


    //Exercicio3_extras
    //Business Methods

    /**
     * Index of the highest element
     * @return
     */
    public int indexOfHighestElement() {
        int index=0;
        if(vector.length ==0){
            index = -1;
        }
        else{
            int referenceValue = vector[0];
            for(int i=0; i<vector.length; i++) {
                if(vector[i]>referenceValue){
                    referenceValue = vector[i];
                    index = i;
                }
            }
        }
        return index;
    }

    /**
     * Returns a matrix that corresponds to the transposed vector
     * @return
     */
    public int[][] transposeVector() {
        int[][] transposeVector = new int[vector.length][1];
        for (int line = 0; line < transposeVector.length; line++) {
            transposeVector[line][0]=vector[line];
        }
        return transposeVector;
    }





    //Método de Equals()
    @Override
    public boolean equals(Object o) {
        if (this == o) {            //Serem da mesma classe
            return true;
        }
        if (!(o instanceof UnidimensionalArray)) {          //se são do mesmo tipo de variável
            return false;
        }

        UnidimensionalArray other = (UnidimensionalArray) o;        //converter o objeto(o) no tipo de classe criada

        if (this.vector.length == (other.vector.length)) {
            for (int i = 0; i < vector.length; i++) {                //verifica se o conteúdo dentro do objeto é igual ao conteúdo do this.vector
                if (this.vector[i] != other.vector[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}

