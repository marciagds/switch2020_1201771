import java.util.Arrays;

public class ArrayBiDimensional {
    //Attributes
    private int[][] matrix;

    //Constructor Methods

    /**
     * Constructor method that initializes the an empty array.
     */
    public ArrayBiDimensional() {          //guarda a morada da matriz
        this.matrix = new int[0][0];            //define matrix
    }

    /**
     * Constructor method that initializes the matrix from another input matrix
     *
     * @param initialMatrix - input matrix
     */
    public ArrayBiDimensional(int[][] initialMatrix) {
        this.matrix = createCopy(initialMatrix);
    }

    //Equals Method
    @Override
    public boolean equals(Object o) {
        if (this == o) {            //Serem da mesma classe
            return true;
        }
        if (!(o instanceof ArrayBiDimensional)) {          //se são do mesmo tipo de variável
            return false;
        }

        ArrayBiDimensional other = (ArrayBiDimensional) o;        //converter o objeto(o) no tipo de classe criada

        if (this.matrix.length == (other.matrix.length)) {
            for (int i = 0; i < matrix.length; i++) {                //verifica se o conteúdo dentro do objeto é igual ao conteúdo do this.matrix
                if (!this.matrix[i].equals(other.matrix[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }


    //Other Methods

    /**
     * Print the class matrix in String
     *
     * @return String matrix
     */
    public String toString() {      //Metodo de interface. Permite imprimir a classe noutra classe.
        return Arrays.toString(matrix);
    }

    /**
     * Print the class matrix in Array
     *
     * @return int[][] matrix
     */
    public int[][] toArray() {
        int[][] intitialArray = createCopy(this.matrix);
        return intitialArray;
    }

    /**
     * Method that allows to create a copy of an input array.
     *
     * @param initialMatrix - imput array
     * @return copy initialMatrix
     */
    private int[][] createCopy(int[][] initialMatrix) {
        int[][] copy;
        if (initialMatrix == null) {            //Not allow null arrays in the class
            copy = new int[0][0];
        } else {
            copy = new int[initialMatrix.length][];
            for (int i = 0; i < initialMatrix.length; i++) {
                copy[i] = new int[initialMatrix[i].length];               //para matrizes não uniformes
                for (int j = 0; j < initialMatrix[i].length; j++) {
                    copy[i][j] = initialMatrix[i][j];
                }
            }
        }
        return copy;
    }


    //Business Methods

    /**
     * Add new value to the matrix at a given index
     *
     * @param lineIndex - index where the value will be added.
     */
    public void addValueOnMatrix(int lineIndex, int newNumber) {
        //if array empty
        if (matrix.length == 0) {
            int[][] resultMatrix = new int[1][1];                                                       //array cannot be null, but it can be empty
            resultMatrix[0][0] = newNumber;
            this.matrix = resultMatrix;
        } else {
            if (lineIndex < matrix.length && lineIndex >= 0) {                                          //Add a new element to the encapsulated array on an existing line
                int[][] resultMatrix = new int[matrix.length][];
                for (int line = 0; line < matrix.length; line++) {
                    if (lineIndex == line) {
                        resultMatrix[line] = new int[matrix[line].length + 1];
                    } else {
                        resultMatrix[line] = new int[matrix[line].length];
                    }
                    for (int column = 0; column < matrix[line].length; column++) {                   //It ensures that the resulting matrix holds the elements of the initial matrix
                        resultMatrix[line][column] = this.matrix[line][column];
                    }
                }
                resultMatrix[lineIndex][resultMatrix[lineIndex].length - 1] = newNumber;
                this.matrix = resultMatrix;
            } else if (lineIndex == (matrix.length)) {                                              //create new line in matrix
                int[][] resultMatrix = new int[matrix.length + 1][];
                for (int line = 0; line < matrix.length; line++) {
                    resultMatrix[line] = new int[matrix[line].length];
                    for (int column = 0; column < matrix[line].length; column++) {               //It ensures that the resulting matrix holds the elements of the initial matrix
                        resultMatrix[line][column] = this.matrix[line][column];
                    }
                }
                resultMatrix[lineIndex] = new int[1];
                resultMatrix[lineIndex][0] = newNumber;
                this.matrix = resultMatrix;
            } else {
                throw new IndexOutOfBoundsException("Invalid Index");
            }
        }
    }                           //DUVIDA (código extenso)!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    /**
     * Remove the first value (x) of the matrix
     *
     * @param removeValue - fist value that it would be remove
     */
    public void removeValueOnMatrix(int removeValue) {
        boolean valueExists = valueExistsOnMatrix(removeValue);
        if (valueExists) {
            boolean valueRemoved = false;
            int indexLineOfValue = indexOfvalueExistsOnMatrix(removeValue);
            int[][] resultMatrix = new int[matrix.length][];
            for (int line = 0; line < matrix.length; line++) {
                if (line == indexLineOfValue) {
                    resultMatrix[line] = new int[matrix[line].length - 1];
                    for (int column = 0; column < resultMatrix[line].length; column++) {
                        if (matrix[line][column] == removeValue) {
                            valueRemoved = true;
                        }
                        if (!valueRemoved) {
                            resultMatrix[line][column] = matrix[line][column];
                        } else {
                            resultMatrix[line][column] = matrix[line][column + 1];
                        }
                    }
                } else {
                    resultMatrix[line] = new int[matrix[line].length];
                    for (int column = 0; column < matrix[line].length; column++) {
                        resultMatrix[line][column] = matrix[line][column];
                    }
                }
            }
            resultMatrix = Utilities.removeFirstEmptyLine(resultMatrix);
            this.matrix = resultMatrix;
        }
    }

    /**
     * Verify if a value exists on the matrix
     *
     * @param value
     * @return
     */
    public boolean valueExistsOnMatrix(int value) {
        for (int line = 0; line < matrix.length; line++) {
            for (int column = 0; column < matrix[line].length; column++) {
                if (matrix[line][column] == value) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns the index of the matrix where a value exists
     *
     * @param value value that is on the matrix
     * @return
     */
    public int indexOfvalueExistsOnMatrix(int value) {
        for (int line = 0; line < matrix.length; line++) {
            for (int column = 0; column < matrix[line].length; column++) {
                if (matrix[line][column] == value) {
                    return line;
                }
            }
        }
        return -1;
    }

    /**
     * Check if empty matrix
     *
     * @return
     */
    public boolean matrixEmpty() {
        return matrix.length == 0;
    }

    /**
     * Retunrs the highest value of the matrix
     *
     * @return
     */
    public int highestElement() {
        if (matrix.length == 0) {
            throw new IllegalArgumentException("Empty Matrix");
        } else {
            int highestElement = matrix[0][0];
            for (int line = 0; line < matrix.length; line++) {
                for (int column = 0; column < matrix[line].length; column++)
                    if (matrix[line][column] > highestElement) {
                        highestElement = matrix[line][column];
                    }
            }
            return highestElement;
        }
    }

    /**
     * Retunrs the lowest value of the matrix
     *
     * @return
     */
    public int lowestElement() {
        if (matrix.length == 0) {
            throw new IllegalArgumentException("Empty Matrix");
        } else {
            int lowestElement = matrix[0][0];
            for (int line = 0; line < matrix.length; line++) {
                for (int column = 0; column < matrix[line].length; column++)
                    if (matrix[line][column] < lowestElement) {
                        lowestElement = matrix[line][column];
                    }
            }
            return lowestElement;
        }
    }

    /**
     * Average of Elements in the matrix
     *
     * @return
     */
    public double elementsAverage() {
        if (matrix.length == 0) {
            throw new IllegalArgumentException("Empty Matrix");
        } else {
            int sum = 0;
            int countElements = 0;
            for (int line = 0; line < matrix.length; line++) {
                for (int column = 0; column < matrix[line].length; column++) {
                    sum += matrix[line][column];
                    countElements += 1;
                }
            }
            double elementsAverage = (sum * 1.0) / countElements;
            return elementsAverage;
        }
    }

    /**
     * Return a vector where each position corresponds to the sum of the elements of the homologous line of the encapsulated array.
     *
     * @return
     */
    public int[] sumLineIndexElements() {
        int[] sum = new int[matrix.length];
        for (int line = 0; line < matrix.length; line++) {
            int sumElementsindexLine = 0;
            for (int column = 0; column < matrix[line].length; column++) {
                sumElementsindexLine += matrix[line][column];
            }
            sum[line] = sumElementsindexLine;
        }
        return sum;
    }

    /**
     * Return a vector where each position corresponds to the sum of the elements of the homologous column of the encapsulated array.
     *
     * @return
     */
    public int[] sumOfEachColumn() {
        int[] sumColumnElements = new int[0];
        int k = 0;
        int sumElementsIndexColumn;
        do {
            sumElementsIndexColumn = 0;
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i].length - 1 >= k) {
                    sumElementsIndexColumn = sumElementsIndexColumn + matrix[i][k];
                }
            }
            sumColumnElements = Utilities.adicionarNoVetor(sumColumnElements, sumElementsIndexColumn);
            k++;
        } while (k < highestColumn());
        return sumColumnElements;
    }

    /**
     * Returns the highest number of columns in the matrix.
     *
     * @return
     */
    private int highestColumn() {
        if (matrix.length != 0) {
            int highest = matrix[0].length;
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i].length > highest) {
                    highest = matrix[i].length;
                }
            }
            return highest;
        }
        return 0;
    }

    /**
     * Return the index of the array row with the largest sum of the respective elements
     *
     * @return
     */
    public int indexLineOfHighestSumLine() {
        int[] vectorSumElementsLineMatrix = sumLineIndexElements();
        int index = -1;
        if (vectorSumElementsLineMatrix.length > 0) {
            int referenceValue = vectorSumElementsLineMatrix[0];
            for (int line = 0; line < vectorSumElementsLineMatrix.length; line++) {
                if (vectorSumElementsLineMatrix[line] > referenceValue) {
                    referenceValue = vectorSumElementsLineMatrix[line];
                    index = line;
                }
            }
        }
        return index;
    }

    /**
     * Checks if the matrix is square
     *
     * @return
     */
    public boolean squareMatrix() {
        if (matrix.length == 0) {
            return false;
        } else {
            boolean squareMatrix = true;
            int indexLine = matrix.length;
            int indexColumn;

            for (int i = 0; i < matrix.length && squareMatrix == true; i++) {
                indexColumn = matrix[i].length;
                if (indexColumn != indexLine) {
                    //return false;
                    squareMatrix = false;
                }
            }
            //return true;
            return squareMatrix;
        }
    }

    /**
     * It verifies if matrix is symmetric
     *
     * @return
     */
    public boolean symmetricMatrix() {
        if (squareMatrix()) {
            int[][] transposedResultMatrix = transposedMatrix();
            for (int line = 0; line < matrix.length; line++) {
                if (!Arrays.equals(matrix[line], transposedResultMatrix[line])) {            ///Aplica-se o método to.equals para arrays primitivos.
                    return false;
                }
            }/*
            for (int line = 0; line < matrix.length; line++) {
                for (int column = 0; column < matrix.length; column++) {
                    if (matrix[line][column] != transposedMatrix[line][column]) {

                    }
                }
            }
            */
            return true;
        }
        return false;
    }

    /**
     * It verifies if matrix is rectangular
     *
     * @return
     */
    public boolean rectangularMatrix() {
        if (matrix == null || matrix.length == 0) {
            return false;
        } else {
            boolean rectangularMatrix = true;
            int nColunasReferencia = matrix[0].length;
            int indexLine = matrix.length;
            int indexColumn;
            for (int i = 0; i < matrix.length && rectangularMatrix; i++) {
                indexColumn = matrix[i].length;
                if (indexColumn != nColunasReferencia) {  //a matriz tem de ter as mesmas colunas em todas as linhas
                    //return false;
                    rectangularMatrix = false;
                }
                if (indexColumn == indexLine) {
                    rectangularMatrix = false;           //a matriz é quadrada e não retangular
                }
            }
            //return true;
            return rectangularMatrix;
        }
    }

    /**
     * It returns the transpose matrix
     *
     * @return
     */
    public int[][] transposedMatrix() {
        int[][] transposedMatrix;
        if (squareMatrix() || rectangularMatrix()) {
            transposedMatrix = new int[matrix[0].length][matrix.length];
            for (int i = 0; i < transposedMatrix.length; i++) {
                for (int k = 0; k < transposedMatrix[i].length; k++) {
                    transposedMatrix[i][k] = matrix[k][i];
                }
            }
        } else {
            transposedMatrix = null;
        }
        return transposedMatrix;
    }

    /**
     * Return the number of non-null elements on the main diagonal of the matrix (if square).
     *
     * @return count elements or -1 (if not square)
     */
    public int countNotNullElementsInMatrixDiagonal() {
        if (squareMatrix()) {
            int countNotNullELements = 0;
            for (int index = 0; index < matrix.length; index++) {
                if (matrix[index][index] != 0) {
                    countNotNullELements += 1;
                }
            }
            return countNotNullELements;
        } else {
            return -1;
        }
    }

    /**
     * It verifies if main and secondary matrix diagonals are equal
     *
     * @return
     */
    public boolean equalMainAndSecondaryDiagonalMatrix() {
        if (squareMatrix() || rectangularMatrix()) {
            int lowestIndex = matrix.length;
            if (matrix[0].length < matrix.length) {
                lowestIndex = matrix[0].length;
            }
            for (int index = 0; index < lowestIndex; index++) {
                if (matrix[index][index] != matrix[index][(matrix[index].length - 1) - index]) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * It return the elements of the vector whose number of digits is greater than the average number of digits of all elements of the matrix
     *
     * @return
     */
    public int[][] matrixElementsWithNumberDigitSuperiorMedia() {
        int[][] resultMatrix = new int[0][0];
        int averageElementsDigitsMatrix = averageElementsDigitsInMatrix();
        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                int elementCountDigit = Utilities.contaDigitosDeUmElemento(matrix[i][k]);
                if (elementCountDigit > averageElementsDigitsMatrix) {
                    resultMatrix = Utilities.addToMatrix(resultMatrix, i, matrix[i][k]);
                }
            }
        }
        return resultMatrix;
    }

    /**
     * Calculation of the average of the number of digital elements of the matrix elements.
     *
     * @return
     */
    public int averageElementsDigitsInMatrix() {
        if (!matrixEmpty()) {
            int sumElementDigits = 0;
            int countElements = 0;
            int elementAmountDigits;
            for (int i = 0; i < matrix.length; i++) {
                for (int k = 0; k < matrix[i].length; k++) {
                    elementAmountDigits = Utilities.contaDigitosDeUmElemento(matrix[i][k]);
                    sumElementDigits += elementAmountDigits;
                    countElements += 1;
                }
            }
            int averageDigits = sumElementDigits / countElements;
            return averageDigits;
        } else {
            return 0;
        }
    }

    /**
     * Average of the percentage of even numbers of all elements of the matrix.
     *
     * @return
     */
    public double evenDigitsPercentageAverageInMatrixElements() {
        double sumEvenDigitsPercentageInElement = 0;
        int countElements = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                double evenDigitPercentageInElement = Utilities.obterPercentagemAlgarismosParNumDadoNumero(matrix[i][k]);
                sumEvenDigitsPercentageInElement += evenDigitPercentageInElement;
                countElements += 1;
            }
        }
        if (countElements > 0) {
            double mediaPercentagemAlgarismosParesElementosVetor = sumEvenDigitsPercentageInElement / countElements;
            return mediaPercentagemAlgarismosParesElementosVetor;
        } else {
            return 0;
        }
    }

    /**
     * It allows obtaining elements that have a percentage of even numbers with higher average than
     * the percentage of even numbers of all the elements of the matrix.
     *
     * @return
     */
    public int[][] elementsWithHigherPercentageEvenNumbers() {
        int[][] elementsMatrix = new int[0][0];
        for (int i = 0; i < matrix.length; i++) {
            for (int k = 0; k < matrix[i].length; k++) {
                double evenDigitsAverageElement = Utilities.obterPercentagemAlgarismosParNumDadoNumero(matrix[i][k]);
                if (evenDigitsAverageElement > evenDigitsPercentageAverageInMatrixElements()) {
                    elementsMatrix = Utilities.addToMatrix(elementsMatrix, i, matrix[i][k]);
                }
            }
        }
        return elementsMatrix;
    }

    /**
     * Invert Elements within indexLine
     */
    public void invertLineElements() {
        int[][] resultMatrix = new int[matrix.length][];
        for (int line = 0; line < matrix.length; line++) {
            resultMatrix[line] = new int[matrix[line].length];
            for (int column = 0; column < matrix[line].length; column++) {
                resultMatrix[line][column] = matrix[line][(matrix[line].length - 1) - column];
            }
        }
        this.matrix = resultMatrix;
    }

    /**
     * Invert Elements within indexColumn
     */
    public void invertMatrixLines() {
        int[][] resultMatrix = new int[matrix.length][];
        for (int line = 0; line < matrix.length; line++) {
            resultMatrix[line] = new int[matrix[(matrix.length - 1) - line].length];
            resultMatrix[line] = matrix[(matrix.length - 1) - line];
        }
        this.matrix = resultMatrix;
    }

    /**
     * Verifies if uniform Matrix
     *
     * @return
     */
    public boolean uniformMatrix() {
        return squareMatrix() || rectangularMatrix();
    }

    /**
     * Rotate the matrix 90 degrees anticlockwise.
     */
    public void rotatePositive90Deg() {
        if (uniformMatrix()) {
            invertLineElements();
            this.matrix = transposedMatrix();
        }
    }

    /**
     * Rotate the matrix 90 degrees clockwise.
     */
    public void rotateNegative90Deg() {
        if (uniformMatrix()) {
            invertMatrixLines();
            this.matrix = transposedMatrix();
        }
    }

    /**
     * Rotate the matrix 180 degrees anticlockwise.
     */
    public void rotatePositive180Deg() {
        if (uniformMatrix()) {
            invertLineElements();
            invertMatrixLines();
        }
    }


}


