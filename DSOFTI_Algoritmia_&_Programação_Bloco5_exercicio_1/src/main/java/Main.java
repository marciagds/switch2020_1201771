import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        //Exercicio1
/*
        //alinea_a.
        ArrayUnidimensional vetor_a = new ArrayUnidimensional();
        System.out.println((vetor_a));          //Imprime valores do array

        //alinea_b.
        ArrayUnidimensional vetor_b = new ArrayUnidimensional(1,2,3);
        System.out.println((vetor_b));

        //alinea_c.
        ArrayUnidimensional vetor_c = new ArrayUnidimensional(1,4,5);
        vetor_c.adicionarNoVetor(3);        //adiciona o nº3 no vetor;
        int[] vetorResultante = vetor_c.obterVetor();
        System.out.println(Arrays.toString(vetorResultante));

        //alinea_d.
        int[]vetorInicial_d={1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_d = new ArrayUnidimensional(vetorInicial_d);
        vetor_d.removerPrimeiroElementoDoArrayComUmDadoValor(3); //remove o 1º numero 3, caso exista
        int [] vetorResultante_d = vetor_d.obterVetor();
        System.out.println(Arrays.toString(vetorResultante_d));

        //alinea_e.
        int[]vetorInicial_e= {1,3,4,5,2,3,5};
        int index = 3;
        ArrayUnidimensional vetor_e = new ArrayUnidimensional(vetorInicial_e);
        int valorElemento = vetor_e.obterValorDoElementoNumDadoIndice(index);
        System.out.println("Valor do vetor no index " + index + " : " + valorElemento);

        //alinea_f.
        int[]vetorInicial_f= {1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_f = new ArrayUnidimensional(vetorInicial_f);
        int numeroElementosVetor = vetor_f.obterNumeroElementosVetor();
        System.out.println(numeroElementosVetor);

        //alinea_g.
        int[]vetorInicial_g= {1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_g = new ArrayUnidimensional(vetorInicial_g);
        int elementoMaiorValor = vetor_g.maiorElementoDoVetor();
        System.out.println(elementoMaiorValor);

        //alinea_h.
        int[]vetorInicial_h= {1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_h = new ArrayUnidimensional(vetorInicial_h);
        int elementoMenorValor = vetor_h.menorElementoDoVetor();
        System.out.println(elementoMenorValor);

        //alinea_i.
        int[]vetorInicial_i= {1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_i = new ArrayUnidimensional(vetorInicial_i);
        double mediaElementosPares = vetor_i.mediaElementosPares();
        System.out.println(mediaElementosPares);

        //alinea_k.
        int[]vetorInicial_k= {1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_k = new ArrayUnidimensional(vetorInicial_k);
        double mediaElementosImpares = vetor_k.mediaElementosImpares();
        System.out.println(mediaElementosImpares);

        //alinea_l.
        int[]vetorInicial_l= {1,3,6,5,0,3,12};
        int divisor = 3;
        ArrayUnidimensional vetor_l = new ArrayUnidimensional(vetorInicial_l);
        double mediaElementosMultiplos = vetor_l.mediaElementosMultiplos(divisor);
        System.out.println(mediaElementosMultiplos);

         //alinea_m.
        int[] vetorInicial_m = {1, 3, 6, 5, 0, 3, 12};
        ArrayUnidimensional vetor_m = new ArrayUnidimensional(vetorInicial_m);
        vetor_m.ordenarOrdemAscendenteVetor();
        int[] vetorResultante_m = vetor_m.obterVetor();
        System.out.println(Arrays.toString(vetorResultante_m));

        //alinea_n - já estava feita numa alinea anterior

        int[]vetorInicial_g= {1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_g = new ArrayUnidimensional(vetorInicial_g);
        int elementoMaiorValor = vetor_g.maiorElementoDoVetor();
        System.out.println(elementoMaiorValor);

        //alinea_o.
        int[]vetorInicial_o= {1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_o = new ArrayUnidimensional(vetorInicial_o);
        boolean vetorComElementoUnico = vetor_o.verificarSeVetorTemApenasUmElemento();
        System.out.println(vetorComElementoUnico);

        //alinea_p.
        int[]vetorInicial_p= {1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_p = new ArrayUnidimensional(vetorInicial_p);
        boolean vetorPar = vetor_p.verificarSeVetorPar();
        System.out.println(vetorPar);

        //alinea_q.
        int[]vetorInicial_q= {1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_q = new ArrayUnidimensional(vetorInicial_q);
        boolean vetorImpar = vetor_q.verificarSeVetorImpar();
        System.out.println(vetorImpar);

         //alinea_r.
        int[]vetorInicial_r= {1,3,4,5,2,3,5};
        ArrayUnidimensional vetor_r = new ArrayUnidimensional(vetorInicial_r);
        boolean elementosDuplicados = vetor_r.verificaSeVetorTemElementosDuplicados();
        System.out.println(elementosDuplicados);

        //alinea_s.
        int[]vetorInicial_s= {1,33,4444,533,222,33333,544};
        ArrayUnidimensional vetor_s = new ArrayUnidimensional(vetorInicial_s);
        int[] vetorResultante_s = vetor_s.obterVetorComElementosComNumeroDigitosSuperiorMedia();
        System.out.println(Arrays.toString(vetorResultante_s));

        //alinea_t.
        int[]vetorInicial_t= {1,33,4444,533,222,33333,544};
        ArrayUnidimensional vetor_t = new ArrayUnidimensional(vetorInicial_t);
        int[] vetorResultante_t = vetor_t.vetorElementosTemPercentagemDigitosParesSuperiorMediaPercentagemAlgarismosParesElementosVetor();
        System.out.println(Arrays.toString(vetorResultante_t));

        //alinea_u.
        int[]vetorInicial_u= {1,33,4444,533,222,33333,544};
        ArrayUnidimensional vetor_u = new ArrayUnidimensional(vetorInicial_u);
        int[] vetorResultante_u = vetor_u.vetorElementosComApenasDigitosPares();
        System.out.println(Arrays.toString(vetorResultante_u));

        //alinea_v.
        int[]vetorInicial_v= {1,33,4444,533,222,33333,544};
        ArrayUnidimensional vetor_v = new ArrayUnidimensional(vetorInicial_v);
        int[] vetorResultante_v = vetor_v.vetorElementosSequenciaCrescenteDigitos();
        System.out.println(Arrays.toString(vetorResultante_v));

        //alinea_w.
        int[]vetorInicial_w= {1,33,4114,533,222,33323,544};
        ArrayUnidimensional vetor_w = new ArrayUnidimensional(vetorInicial_w);
        int[] vetorResultante_w = vetor_w.vetorElementosCapicuas();
        System.out.println(Arrays.toString(vetorResultante_w));

        //alinea_x.
        int[]vetorInicial_x= {1,33,4114,533,222,33323,544};
        ArrayUnidimensional vetor_x = new ArrayUnidimensional(vetorInicial_x);
        int[] vetorResultante_x = vetor_x.vetorElementosCapicuas();
        System.out.println(Arrays.toString(vetorResultante_x));
   */

        //alinea_y.
        int[]vetorInicial_y= {1,33,4114,533,222,33323,544};
        UnidimensionalArray vetor_y = new UnidimensionalArray(vetorInicial_y);
        int[] vetorResultante_y = vetor_y.vetorElementosCapicuas();
        System.out.println(Arrays.toString(vetorResultante_y));

        //alinea_z.
        int[]vetorInicial_z= {1,33,4114,533,222,33323,544};
        UnidimensionalArray vetor_z = new UnidimensionalArray(vetorInicial_z);
        int[] vetorResultante_z = vetor_z.vetorElementosCapicuas();
        System.out.println(Arrays.toString(vetorResultante_z));
    }
}




