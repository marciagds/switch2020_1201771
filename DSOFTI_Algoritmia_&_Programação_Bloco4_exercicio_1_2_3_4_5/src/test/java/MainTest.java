import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    //Testes exercicio1
    @org.junit.jupiter.api.Test
    void getContagemDigitos_teste1() {
        //arrange
        int numero = 23456;
        int expected = 5;
        //act
        int result = Main.getNumeroAlgarismosdeumNumero(numero);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getContagemDigitos_teste2() {
        //arrange
        int numero = -23456;
        int expected = 5;
        //act
        int result = Main.getNumeroAlgarismosdeumNumero(numero);
        //assert
        assertEquals(expected, result);
    }

    //Testes exercicio2
    @Test
    void getTransformarNumeroEmVetor_testeValorPositivo1() {
        //arrange
        int numero_lido = 12345;
        int [] expected = {1,2,3,4,5};
        //act
        int [] result = Main.getTransformarNumeroEmVetor(numero_lido);
        //assert
        assertArrayEquals(expected, result);

    }
    @Test
    void getTransformarNumeroEmVetor_testeValorPositivo2() {
        //arrange
        int numero_lido = 12;
        int [] expected = {1,2};
        //act
        int [] result = Main.getTransformarNumeroEmVetor(numero_lido);
        //assert
        assertArrayEquals(expected, result);

    }
    @Test
    void getTransformarNumeroEmVetor_teste_valorNegativo() {
        //arrange
        int numero_lido = -12345;
        int [] expected = {-1,-2,-3,-4,-5};
        //act
        int [] result = Main.getTransformarNumeroEmVetor(numero_lido);
        //assert
        assertArrayEquals(expected, result);

    }
    @Test
    void getTransformarNumeroEmVetor_teste_valornulo1() {
        //arrange
        int numero_lido = 10045;
        int [] expected = {1,0,0,4,5};
        //act
        int [] result = Main.getTransformarNumeroEmVetor(numero_lido);
        //assert
        assertArrayEquals(expected, result);

    }

    //Testes exercicio3
    @Test
    void getSomaDosElementosDeUmVetor_teste_valorPositivo1(){
        //arrange
        int [] vetor = {1,4,6,7,4};
        int expected = 22;
        //act
        int result = Main.getSomaDosElementosDeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaDosElementosDeUmVetor_teste_valorPositivo2(){
        //arrange
        int [] vetor = {6,7,44,3,12};
        int expected = 72;
        //act
        int result = Main.getSomaDosElementosDeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaDosElementosDeUmVetor_teste_valorNegativo(){
        //arrange
        int [] vetor = {6,-7,44,-3,12};
        int expected = 52;
        //act
        int result = Main.getSomaDosElementosDeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaDosElementosDeUmVetor_teste_valorNulo(){
        //arrange
        int [] vetor = {6,0,44,0,12};
        int expected = 62;
        //act
        int result = Main.getSomaDosElementosDeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void ggetSomaDosElementosDeUmVetor_vetorVazio(){
        //arrange
        int [] vetor = {};
        int expected = 0;
        //act
        int result = Main.getSomaDosElementosDeUmVetor(vetor);
        //assert
        assertEquals(expected,result);
    }

    //Testes exercicio4
    @Test
    void getVetorApenasDeNumerosPares_valoresPositivos1(){
        //arrange
        int [] vetor = {1,4,6,2,4};
        int []expected = {4,6,2,4};
        //act
        int [] result = Main.getVetorApenasDeNumerosPares(vetor);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void getVetorApenasDeNumerosPares_valoresPositivos2(){
        //arrange
        int [] vetor = {1,4,5,22,8};
        int []expected = {4,22,8};
        //act
        int [] result = Main.getVetorApenasDeNumerosPares(vetor);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void getVetorApenasDeNumerosPares_valoresNegativos(){
        //arrange
        int [] vetor = {1,-4,-5,22,8};
        int []expected = {-4,22,8};
        //act
        int [] result = Main.getVetorApenasDeNumerosPares(vetor);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void getVetorApenasDeNumerosPares_valoresNulos(){
        //arrange
        int [] vetor = {1,0,-5,-22,8};
        int []expected = {0,-22,8};
        //act
        int [] result = Main.getVetorApenasDeNumerosPares(vetor);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void getVetorApenasDeNumerosPares_vetorVazio(){
        //arrange
        int [] vetor = {};
        int []expected = {};
        //act
        int [] result = Main.getVetorApenasDeNumerosPares(vetor);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void getVetorApenasDeNumerosImpares_valoresPositivos1(){
        //arrange
        int [] vetor = {1,4,6,2,4};
        int []expected = {1};
        //act
        int [] result = Main.getVetorApenasDeNumerosImpares(vetor);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void getVetorApenasDeNumerosImpares_valoresPositivos2(){
        //arrange
        int [] vetor = {1,4,55,22,8};
        int []expected = {1,55};
        //act
        int [] result = Main.getVetorApenasDeNumerosImpares(vetor);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void getVetorApenasDeNumerosImpares_valoresNegativos(){
        //arrange
        int [] vetor = {1,-4,-5,22,8};
        int []expected = {1,-5};
        //act
        int [] result = Main.getVetorApenasDeNumerosImpares(vetor);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void getVetorApenasDeNumerosImpares_valoresNulos(){
        //arrange
        int [] vetor = {1,0,-5,-22,8};
        int []expected = {1,-5};
        //act
        int [] result = Main.getVetorApenasDeNumerosImpares(vetor);
        //assert
        assertArrayEquals(expected, result);
    }
    @Test
    void getVetorApenasDeNumerosImpares_vetorVazio(){
        //arrange
        int [] vetor = {};
        int []expected = {};
        //act
        int [] result = Main.getVetorApenasDeNumerosImpares(vetor);
        //assert
        assertArrayEquals(expected, result);
    }

    //Testes exercicio5
    @Test
    void getSomaElementosParesdeumVetor_valoresPositivos(){
        //arrange
        int [] vetor = {3,22,5,12,33};
        int expected = 34;

        //act
        int result = Main.getSomaElementosParesdeumVetor(vetor);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaElementosParesdeumVetor_valoresNegativos(){
        //arrange
        int [] vetor = {3,22,5,-12,33};
        int expected = 10;

        //act
        int result = Main.getSomaElementosParesdeumVetor(vetor);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaElementosParesdeumVetor_valoresNulos(){
        //arrange
        int [] vetor = {1,22,5,12,0};
        int expected = 34;

        //act
        int result = Main.getSomaElementosParesdeumVetor(vetor);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaElementosImparesdeumVetor_valoresPositivos(){
        //arrange
        int [] vetor = {3,22,5,12,33};
        int expected = 41;

        //act
        int result = Main.getSomaElementosImparesdeumVetor(vetor);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaElementosImparesdeumVetor_valoresNegativos(){
        //arrange
        int [] vetor = {3,22,-3,-12,-33};
        int expected = -33;

        //act
        int result = Main.getSomaElementosImparesdeumVetor(vetor);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaElementosImparesdeumVetor_valoresNulos(){
        //arrange
        int [] vetor = {1,0,5,12,0};
        int expected = 6;

        //act
        int result = Main.getSomaElementosImparesdeumVetor(vetor);

        //assert
        assertEquals(expected, result);
    }
}