import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // write your code here
        //exercicio1();
        //exercicio2();
        //exercicio3();
        //exercicio4();
        exercicio5();
    }

    public static void exercicio1(){
        //Variáveis envolvidas
        int numero;
        int contagemDigitos;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Insira numero:");
        numero = ler.nextInt();

        //Processamento
        contagemDigitos = getNumeroAlgarismosdeumNumero(numero);

        //Saida dos dados
        System.out.println("Numero de digitos: " + contagemDigitos);

    }

    public static int getNumeroAlgarismosdeumNumero(int numero_longo) {
        String numero = Integer.toString(numero_longo);
        int contaDigitos = 0;
        char algarismos;
        for (int i = 0; i < numero.length(); i++) {
            algarismos = numero.charAt(i);
            if (Character.isDigit(algarismos)) {
                contaDigitos += 1;
            }
        }
        return contaDigitos;
    }

    public static void exercicio2(){
        //Variáveis envolvidas
        int numero_lido;
        int[] vetor;                //declaração da variavel array
        int contaDigitos;
        char algarismo;
        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Insira numero:");
        numero_lido = ler.nextInt();

        //Processamento
        vetor = getTransformarNumeroEmVetor(numero_lido);

        //Saida dos dados
        System.out.println(Arrays.toString(vetor));   //Imprime valores do array
    }

    public static int[] getTransformarNumeroEmVetor(int numero){
        int[] vetor;                //declaração da variavel array
        int contaDigitos;
        contaDigitos = getNumeroAlgarismosdeumNumero(numero);
        vetor = new int[contaDigitos];                  //construção do array
        for (int i=(contaDigitos -1); i>=0;i--){
           vetor [i] = numero % 10;
           numero = numero /10;
        }
        return vetor;
    }

    public static void exercicio3(){
        //Variáveis envolvidas
        int tamanho, somaVetor;
        int[]vetor;
       //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura do array
        System.out.println("Qual o tamanho do vetor?");
        tamanho = ler.nextInt();
        vetor = new int[tamanho];
        System.out.println("Inserir valor dos elementos: ");
        for(int indice = 0; indice < tamanho; indice++) {
            vetor[indice] = ler.nextInt();
        }

        //Processamento
        somaVetor = getSomaDosElementosDeUmVetor(vetor);

        //Saida dos dados
        System.out.println("A soma dos elementos do vetor é " + somaVetor);
    }

    public static int getSomaDosElementosDeUmVetor(int[]vetor){
        int somaVetor =0;
        for (int i=0; i<vetor.length;i++){
            somaVetor += vetor[i];
        }
        return somaVetor;
    }

    public static void exercicio4(){
        //Variáveis envolvidas
        int tamanho;
        int[]vetor;
        int []vetorPares, vetorImpares;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura do array
        System.out.println("Qual o tamanho do vetor?");
        tamanho = ler.nextInt();
        vetor = new int[tamanho];
        System.out.println("Inserir valor dos elementos: ");
        for(int indice = 0; indice < tamanho; indice++) {
            vetor[indice] = ler.nextInt();
        }

        //Processamento
        vetorPares = getVetorApenasDeNumerosPares(vetor);
        vetorImpares = getVetorApenasDeNumerosImpares(vetor);

        //Saida dos dados
        System.out.println("Vetor com os elementos pares: " + Arrays.toString(vetorPares));   //Imprime valores do array
        System.out.println("Vetor com os elementos impares: " + Arrays.toString(vetorImpares));
    }

    public static int[] getVetorApenasDeNumerosPares(int[]vetor){
        int contaElementosPares = 0;
        int []vetorPares;

        for (int i=0; i<vetor.length;i++){          //contagem de elementos pares. Tamanho do vetor solução
            double resto = vetor[i] % 2;
                if(resto== 0){
                    contaElementosPares += 1;
                }
        }
        vetorPares = new int[contaElementosPares];      //construção do vetor
        int indicePares =0;
            for(int i =0; i<vetor.length;i++){
                double resto = vetor[i] % 2;
                if (resto == 0 ){
                    vetorPares[indicePares]= vetor[i];
                    indicePares++;
                }
            }
        return vetorPares;
    }

    public static int[] getVetorApenasDeNumerosImpares(int[]vetor){
        int contaElementosImpares = 0;
        int []vetorImpares;

        for (int i=0; i<vetor.length;i++){          //contagem de elementos pares. Tamanho do vetor solução
            double resto = vetor[i] % 2;
            if(resto!=0){
                contaElementosImpares += 1;
            }
        }
        vetorImpares = new int[contaElementosImpares];      //construção do vetor
        int indiceImpares =0;
        for(int i =0; i<vetor.length;i++){
            double resto = vetor[i] % 2;
            if (resto !=0 ){
                vetorImpares[indiceImpares]= vetor[i];
                indiceImpares++;
            }
        }
        return vetorImpares;
    }

    public static void exercicio5(){
        //Variáveis envolvidas
        int tamanho, somaElementosPares, somaElementosImpares;
        int[]vetor;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura do array
        System.out.println("Qual o tamanho do vetor?");
        tamanho = ler.nextInt();
        vetor = new int[tamanho];
        System.out.println("Inserir valor dos elementos: ");
        for(int indice = 0; indice < tamanho; indice++) {
            vetor[indice] = ler.nextInt();
        }

        //Processamento
        somaElementosImpares = getSomaElementosImparesdeumVetor(vetor);
        somaElementosPares =getSomaElementosParesdeumVetor(vetor);

        //Saida dos dados
        System.out.println("Soma dos elementos pares: " + somaElementosPares);   //Imprime valores do array
        System.out.println("Soma dos elementos impares: " + somaElementosImpares);
    }

    public static int getSomaElementosParesdeumVetor(int [] vetor){

        int [] vetorPares = getVetorApenasDeNumerosPares(vetor);    //obter o vetor só com os elementos pares

        int somaElementosPares = getSomaDosElementosDeUmVetor(vetorPares);      //somar os elementos do vetor dos elementos pares

        return  somaElementosPares;
    }

    public static int getSomaElementosImparesdeumVetor(int [] vetor){

        int []vetorImpares = getVetorApenasDeNumerosImpares(vetor);      //obter o vetor só com os elementos impares

        int somaElementosImpares = getSomaDosElementosDeUmVetor(vetorImpares);      //somar os elementos do vetor dos elementos pares

        return  somaElementosImpares;

    }

}
