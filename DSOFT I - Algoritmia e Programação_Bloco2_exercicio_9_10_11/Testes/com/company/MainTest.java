package com.company;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    //Teste Exercicio9
    @org.junit.jupiter.api.Test
    void getIfSequenciaCrescente_teste1() {
        //arrange
        int number = 134;
        String expected ="A sequência é crescente";

        //act
        String result = Main.getIfSequenciaCrescente(number);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getIfSequenciaCrescente_teste2() {
        //arrange
        int number = 1202;
        String expected ="O número não tem 3 algarismos";;

        //act
        String result = Main.getIfSequenciaCrescente(number);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getIfSequenciaCrescente_teste3() {
        //arrange
        int number = -543;
        String expected ="A sequência é crescente";;

        //act
        String result = Main.getIfSequenciaCrescente(number);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getIfSequenciaCrescente_teste4() {
        //arrange
        int number = 476;
        String expected ="A sequência não é crescente.";

        //act
        String result = Main.getIfSequenciaCrescente(number);

        //assert
        assertEquals(expected, result);
    }

    //Teste Exercicio 10
    @org.junit.jupiter.api.Test
    void get$custo_final_teste_p32() {
        //arrange
        double custo = 32;
        double expected =25.6;

        //act
        double result = Main.get$custo_final(custo);

        //assert
        assertEquals(expected, result,0.01);
    }

    @org.junit.jupiter.api.Test
    void get$custo_final_teste_p65() {
        //arrange
        double custo = 65;
        double expected =45.5;

        //act
        double result = Main.get$custo_final(custo);

        //assert
        assertEquals(expected, result,0.01);
    }

    @org.junit.jupiter.api.Test
    void get$custo_final_teste_p102() {
        //arrange
        double custo = 102;
        double expected = 61.2;

        //act
        double result = Main.get$custo_final(custo);

        //assert
        assertEquals(expected, result,0.01);
    }

    @org.junit.jupiter.api.Test
    void get$custo_final_teste_p354() {
        //arrange
        double custo = 354;
        double expected = 141.6;

        //act
        double result = Main.get$custo_final(custo);

        //assert
        assertEquals(expected, result,0.01);
    }

    @org.junit.jupiter.api.Test
    void get$custo_final_teste_p0() {
        //arrange
        double custo = 0;
        double expected = -1;

        //act
        double result = Main.get$custo_final(custo);

        //assert
        assertEquals(expected, result,0.01);
    }

    @org.junit.jupiter.api.Test
    void get$custo_final_teste_p_negativo() {
        //arrange
        double custo = -13;
        double expected = -1;

        //act
        double result = Main.get$custo_final(custo);

        //assert
        assertEquals(expected, result,0.01);
    }

    @org.junit.jupiter.api.Test
    void get$custo_final_teste_p_50() {
        //arrange
        double custo = 50 ;
        double expected = 40;

        //act
        double result = Main.get$custo_final(custo);

        //assert
        assertEquals(expected, result,0.01);
    }

    @org.junit.jupiter.api.Test
    void get$custo_final_teste_p_100() {
        //arrange
        double custo = 100 ;
        double expected = 70;

        //act
        double result = Main.get$custo_final(custo);

        //assert
        assertEquals(expected, result,0.01);
    }

    @org.junit.jupiter.api.Test
    void get$custo_final_teste_p_200() {
        //arrange
        double custo = 200 ;
        double expected = 120;

        //act
        double result = Main.get$custo_final(custo);

        //assert
        assertEquals(expected, result,0.01);
    }

    //Teste Exercicio 11
    //1ª parte
    @org.junit.jupiter.api.Test
    void getClassClassification_valor_negativo() {
        //arrange
        double aprovados = -0.2 ;
        String expected ="Valor Inválido.";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_0() {
        //arrange
        double aprovados = 0;
        String expected = "Turma má.";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_01() {
        //arrange
        double aprovados = 0.1;
        String expected = "Turma má.";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_02() {
        //arrange
        double aprovados = 0.2;
        String expected = "Turma fraca.";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_05() {
        //arrange
        double aprovados = 0.5;
        String expected = "Turma Razoável.";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_06() {
        //arrange
        double aprovados = 0.6;
        String expected = "Turma Razoável.";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_07() {
        //arrange
        double aprovados = 0.7;
        String expected = "Turma Boa.";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_08() {
        //arrange
        double aprovados = 0.8;
        String expected = "Turma Boa.";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_09() {
        //arrange
        double aprovados = 0.9;
        String expected = "Turma Excelente!";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_095() {
        //arrange
        double aprovados = 0.95;
        String expected = "Turma Excelente!";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_1() {
        //arrange
        double aprovados = 1;
        String expected = "Turma Excelente!";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassification_valor_12() {
        //arrange
        double aprovados = 1.2;
        String expected = "Valor Inválido.";

        //act
        String result = Main.get_ClassClassification(aprovados);

        //assert
        assertEquals(expected, result);
    }

    //2ª parte
    @org.junit.jupiter.api.Test
    void getClassClassificationv2_valor_12() {
        //arrange
        double aprovados = 1.2;
        double limite_mau = 0.1;
        double limite_fraco = 0.3;
        double limite_razoavel = 0.6;
        double limite_bom = 0.85;
        String expected = "Valor Inválido.";

        //act
        String result = Main.get_ClassClassification_v2(aprovados,limite_mau,limite_fraco,limite_razoavel,limite_bom);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassificationv2_valor_negativo() {
        //arrange
        double aprovados = -0.2 ;
        double limite_mau = 0.1;
        double limite_fraco = 0.3;
        double limite_razoavel = 0.6;
        double limite_bom = 0.85;
        String expected ="Valor Inválido.";

        //act
        String result = Main.get_ClassClassification_v2(aprovados,limite_mau,limite_fraco,limite_razoavel,limite_bom);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassificationv2_valor_0() {
        //arrange
        double aprovados = 0;
        double limite_mau = 0.1;
        double limite_fraco = 0.3;
        double limite_razoavel = 0.6;
        double limite_bom = 0.85;
        String expected = "Turma má.";

        //act
        String result = Main.get_ClassClassification_v2(aprovados,limite_mau,limite_fraco,limite_razoavel,limite_bom);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassificationv2_valor_005() {
        //arrange
        double aprovados = 0.05;
        double limite_mau = 0.1;
        double limite_fraco = 0.3;
        double limite_razoavel = 0.6;
        double limite_bom = 0.85;
        String expected = "Turma má.";

        //act
        String result = Main.get_ClassClassification_v2(aprovados,limite_mau,limite_fraco,limite_razoavel,limite_bom);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getClassClassificationv2_valor_01() {
        //arrange
        double aprovados = 0.1;
        double limite_mau = 0.1;
        double limite_fraco = 0.3;
        double limite_razoavel = 0.6;
        double limite_bom = 0.85;
        String expected = "Turma fraca.";

        //act
        String result = Main.get_ClassClassification_v2(aprovados,limite_mau,limite_fraco,limite_razoavel,limite_bom);

        //assert
        assertEquals(expected, result);
    }
    }