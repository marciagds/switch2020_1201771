package com.company;

import org.jcp.xml.dsig.internal.dom.ApacheNodeSetData;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        //exercicio9();
        //exercicio10();
        //exercicio11();
        exercicio11_e();
    }

    public static void exercicio9() {
        // Saber se a sequencia dos 3 algarismos que compõe um número é crescente

        // Variáveis envolvidas
        int number;
        String sequencia;

        //Inicialização da leitura;
        Scanner ler = new Scanner(System.in);

        // Entrada de variáveis
        System.out.println(" Escolha um número de 3 algarismos.");
        number = ler.nextInt();

        //Processamento
        sequencia = Main.getIfSequenciaCrescente(number);

        // Saída de dados
        System.out.println(sequencia);
    }

    public static String getIfSequenciaCrescente (int number) {
        String sequencia;

        if (number >= 100 && number <= 999 || number<=-100 && number>=-999) {
            int centena = number / 100;
            int dezena = (number - centena * 100) / 10;
            int unidade = (number - centena * 100 - dezena * 10);

            if (centena < dezena) {
                if (dezena < unidade) {
                    sequencia = "A sequência é crescente";
                    return sequencia;
                } else {
                    sequencia = "A sequência não é crescente.";
                    return sequencia;
                }
            }
            else {
                sequencia = "A sequência não é crescente.";
                return sequencia;
            }
        }
    else {
        sequencia = "O número não tem 3 algarismos";
        return sequencia;
    }
    }

    public static void exercicio10(){
    //Calcular preço do artigo, com desconto

      // Variáveis envolvidas
        int desconto;
        double custo;

      //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

     // Leitura dos dados
        System.out.println("Qual é o preço do artigo ?");
        custo = ler.nextDouble();

    // Processamento
        custo = get$custo_final(custo);

     // Saída dos dados
        System.out.println("O custo do artigo é " + String.format ("%.2f", custo));
        }

    public static double get$custo_final(double custo) {
        double desconto;
        if (custo <= 0) {
            custo = -1;
            return custo;
        }
        else {
            if (custo <= 50) {
                desconto = 0.20;
            } else {
                if (custo <= 100) {
                    desconto = 0.30;
                } else {
                    if (custo <= 200) {
                        desconto = 0.40;
                    } else {
                        desconto = 0.60;
                    }
                }
            }
            custo = custo * (1 - desconto);
            return custo;
        }
    }

    public static void exercicio11(){
        //Classificação da turma de acordo com a percentagem de aprovados

        //Variáveis envolvidas
        double aprovados;
        String classification;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura de dados
        System.out.println("Qual a percentagem de aprovados ?");
        aprovados = ler. nextDouble();

        //Processamento
        classification = Main.get_ClassClassification(aprovados);

        //Saída dos dados
        System.out.println(classification);
    }

    public static void exercicio11_e(){
        //Classificação da turma de acordo com a percentagem de aprovados

        //Variáveis envolvidas
        double aprovados;
        String classification;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura de dados
        System.out.println("Qual a percentagem de aprovados ?");
        aprovados = ler. nextDouble();
        System.out.println("Qual o limite para Classificação Mau ?");
        double limite_mau = ler. nextDouble();
        System.out.println("Qual o limite para Classificação Fraco ?");
        double limite_fraco = ler. nextDouble();
        System.out.println("Qual o limite para Classificação Razoável ?");
        double limite_razoavel = ler. nextDouble();
        System.out.println("Qual o limite para Classificação Bom ?");
        double limite_bom= ler. nextDouble();

        //Processamento
        classification = Main.get_ClassClassification_v2(aprovados,limite_mau,limite_fraco, limite_razoavel, limite_bom);

        //Saída dos dados
        System.out.println(classification);
    }

    public static String get_ClassClassification (double aprovados){
       String classification;

        if(aprovados <0 || aprovados >1){
            classification = "Valor Inválido.";
            return classification;
        }
        else {
            if (aprovados <0.2){
                classification = "Turma má.";
                return classification;
            }
            else {
                if (aprovados <0.5){
                    classification = "Turma fraca.";
                    return classification;
                }
                else {
                    if (aprovados <0.7){
                        classification = "Turma Razoável.";
                        return classification;
                    }
                    else {
                        if(aprovados <0.9){
                            classification = "Turma Boa.";
                            return classification;
                        }
                        else {
                            classification = "Turma Excelente!";
                            return classification;
                        }
                    }
                }
            }
        }
    }

    public static String get_ClassClassification_v2 (double aprovados, double limite_mau, double limite_fraco, double limite_razoavel, double limite_bom){
        String classification;

        if(aprovados <0 || aprovados >1){
            classification = "Valor Inválido.";
            return classification;
        }
        else {
            if (aprovados <limite_mau){
                classification = "Turma má.";
                return classification;
            }
            else {
                if (aprovados <limite_fraco){
                    classification = "Turma fraca.";
                    return classification;
                }
                else {
                    if (aprovados <limite_razoavel){
                        classification = "Turma Razoável.";
                        return classification;
                    }
                    else {
                        if(aprovados <limite_bom){
                            classification = "Turma Boa.";
                            return classification;
                        }
                        else {
                            classification = "Turma Excelente!";
                            return classification;
                        }
                    }
                }
            }
        }
    }

}
