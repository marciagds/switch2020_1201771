package com.company;

import java.util.StringTokenizer;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    //Testes exercicio 14
    @org.junit.jupiter.api.Test
    void getValorCambio_testeDolar() {
        //arrange
        double euros = 2.50;
        char option = 'd';
        double expected = 3.835;
        //act
        double result = Main.getValorCambio(euros, option);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getValorCambio_testeLibra() {
        //arrange
        double euros = 2.50;
        char option = 'L';
        double expected = 1.935;
        //act
        double result = Main.getValorCambio(euros, option);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getValorCambio_testIene() {
        //arrange
        double euros = 2.50;
        char option = 'i';
        double expected = 403.7;
        //act
        double result = Main.getValorCambio(euros, option);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getValorCambio_testCoroaSueca() {
        //arrange
        double euros = 2.50;
        char option = 'c';
        double expected = 23.98;
        //act
        double result = Main.getValorCambio(euros, option);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getValorCambio_testeFranco() {
        //arrange
        double euros = 2.50;
        char option = 'f';
        double expected = 4.00;
        //act
        double result = Main.getValorCambio(euros, option);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getValorCambio_testevalornulo() {
        //arrange
        double euros = 0;
        char option = 'f';
        double expected = 0;
        //act
        double result = Main.getValorCambio(euros, option);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getValorCambio_teste_moedaErrada() {
        //arrange
        double euros = 23;
        char option = 't';
        double expected = -1;
        //act
        double result = Main.getValorCambio(euros, option);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getValorCambio_teste_eurosNegativos() {
        //arrange
        double euros = -43;
        char option = 'f';
        double expected = -2;
        //act
        double result = Main.getValorCambio(euros, option);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getValorCambio_teste_eurosNegativos_MoedaErrada() {
        //arrange
        double euros = -43;
        char option = 't';
        double expected = -2;
        //act
        double result = Main.getValorCambio(euros, option);
        //assert
        assertEquals(expected, result, 0.01);
    }

    //Testes exercicio 15
    @org.junit.jupiter.api.Test
    void getNotaQualitativa_valor4() {
        //arrange
        int nota = 4;
        String expected ="Mau";
        //act
        String result = Main.getNotaQualitativa(nota);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getNotaQualitativa_valor8() {
        //arrange
        int nota = 8;
        String expected ="Medíocre";
        //act
        String result = Main.getNotaQualitativa(nota);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getNotaQualitativa_valor9() {
        //arrange
        int nota = 9;
        String expected ="Medíocre";
        //act
        String result = Main.getNotaQualitativa(nota);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getNotaQualitativa_valor20() {
        //arrange
        int nota = 20;
        String expected ="Muito Bom";
        //act
        String result = Main.getNotaQualitativa(nota);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getNotaQualitativa_valorSuperior20() {
        //arrange
        int nota = 33;
        String expected = "Erro. Nota entre 0 - 20.";
        //act
        String result = Main.getNotaQualitativa(nota);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getNotaQualitativa_valornegativo() {
        //arrange
        int nota = -5;
        String expected = "Erro. Nota entre 0 - 20.";
        //act
        String result = Main.getNotaQualitativa(nota);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getNotaQualitativa_valornulo() {
        //arrange
        int nota =0;
        String expected ="Mau";
        //act
        String result = Main.getNotaQualitativa(nota);
        //assert
        assertEquals(expected, result);
    }

    //Teste Exercicio16
    @org.junit.jupiter.api.Test
    void getSalarioLiquido_valor350(){
        //arrange
        double salario_bruto = 350;
        double expected = 315;
        //act
        double result = Main.getSalarioLiquido(salario_bruto);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getSalarioLiquido_valor500(){
        //arrange
        double salario_bruto = 500;
        double expected = 450;
        //act
        double result = Main.getSalarioLiquido(salario_bruto);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getSalarioLiquido_valor740(){
        //arrange
        double salario_bruto = 740;
        double expected = 629;
        //act
        double result = Main.getSalarioLiquido(salario_bruto);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getSalarioLiquido_valor1000(){
        //arrange
        double salario_bruto = 1000;
        double expected = 850;
        //act
        double result = Main.getSalarioLiquido(salario_bruto);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getSalarioLiquido_valor2000(){
        //arrange
        double salario_bruto = 2000;
        double expected = 1600;
        //act
        double result = Main.getSalarioLiquido(salario_bruto);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getSalarioLiquido_valor0(){
        //arrange
        double salario_bruto = 0;
        double expected = -1;
        //act
        double result = Main.getSalarioLiquido(salario_bruto);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @org.junit.jupiter.api.Test
    void getSalarioLiquido_valorNegativo(){
        //arrange
        double salario_bruto = -350;
        double expected = -1;
        //act
        double result = Main.getSalarioLiquido(salario_bruto);
        //assert
        assertEquals(expected, result, 0.01);
    }

    //Teste Exercicio17
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso8True(){
        //arrange
        double animal_weight = 8;
        double animal_food = 100;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso8False(){
        //arrange
        double animal_weight = 8;
        double animal_food = 230;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso10True(){
        //arrange
        double animal_weight = 10;
        double animal_food = 100;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso10False(){
        //arrange
        double animal_weight = 10;
        double animal_food = 230;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso16True(){
        //arrange
        double animal_weight = 16;
        double animal_food = 250;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso16False(){
        //arrange
        double animal_weight = 16;
        double animal_food = 230;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso25True(){
        //arrange
        double animal_weight = 25;
        double animal_food = 250;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso25False(){
        //arrange
        double animal_weight = 25;
        double animal_food = 230;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso30True(){
        //arrange
        double animal_weight = 30;
        double animal_food = 300;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso30False(){
        //arrange
        double animal_weight = 30;
        double animal_food = 230;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso45True(){
        //arrange
        double animal_weight = 45;
        double animal_food = 300;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso45False(){
        //arrange
        double animal_weight = 35;
        double animal_food = 100;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso60True(){
        //arrange
        double animal_weight = 60;
        double animal_food = 500;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertTrue(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPeso60False(){
        //arrange
        double animal_weight = 60;
        double animal_food =240;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPesoNegativo(){
        //arrange
        double animal_weight = -12;
        double animal_food = 0;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertFalse(result);
    }
    @org.junit.jupiter.api.Test
    void  getIfAnimalFoodAdquated_valorPesoNulo(){
        //arrange
        double animal_weight = 0;
        double animal_food = 0;

        //act
        boolean result = Main.getIfAnimalFoodAdquated(animal_weight,animal_food);

        //assert
        assertFalse(result);
    }
}