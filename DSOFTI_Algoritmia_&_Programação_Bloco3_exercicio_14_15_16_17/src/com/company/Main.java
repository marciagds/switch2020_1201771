package com.company;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //exercicio14();
        //exercicio15();
        //exercicio16();
        exercicio17();
    }
    public static void exercicio14(){
        //Variáveis envolvidas
        double euros, cambio;
        char option;
        //Inicialização da leitura
        Scanner ler =  new Scanner(System.in);

        //Leitura dos dados
        do {
            System.out.println("Qual o valor de cambio, em euros?");
            euros = ler.nextDouble();

            if(euros>=0) {
                System.out.println("Qual a opção de cambio?");    //Possibilidades: D, L, I, C, F
                option = ler.next().charAt(0);                    //ler a opção considerando a 1ª letra inserida(posição:0)
                cambio = getValorCambio(euros, option);
                System.out.println("O valor de cambio correspondente é de " + String.format("%.2f", cambio));
            }

        }while (euros>=0);
    }

    public static double getValorCambio(double euros, char option){
        double dolar, libra, iene, coroaSueca, franco;
        if(euros>=0){
            if(option == 'D'|| option=='d'){
            dolar = euros * 1.534;
            return dolar;
            } else if(option == 'L'|| option=='l'){
            libra = euros * 0.774;
            return libra;
            } else if(option == 'I'|| option=='i'){
            iene = euros * 161.480;
            return iene;
            }else if(option == 'C'|| option=='c'){
            coroaSueca = euros * 9.593;
            return coroaSueca;
            }else if(option == 'F'|| option=='f'){
            franco = euros * 1.601;
            return franco;
            }else { return -1; }
        }
        else{
            return -2;
        }
    }

    public static void exercicio15(){
        //Variáveis envolvidas
        int nota=0;
        String qualitativeClassification;
        //Inicialização da leitura
        Scanner ler =  new Scanner(System.in);

        //Leitura dos dados
        while (nota>=0) {
            System.out.println("Qual o valor da nota ?");
            nota = ler.nextInt();

            qualitativeClassification = getNotaQualitativa(nota);
            System.out.println(qualitativeClassification);
    }
    }

    public static String getNotaQualitativa(int nota) {
        String notaQualitativa;
        if (nota >= 0 && nota <= 20) {
            if (nota <= 4) {
                notaQualitativa = "Mau";
                return notaQualitativa;
            } else if (nota <= 9) {
                notaQualitativa = "Medíocre";
                return notaQualitativa;
            } else if (nota <= 13) {
                notaQualitativa = "Suficiente";
                return notaQualitativa;
            } else if (nota <= 17) {
                notaQualitativa = "Bom";
                return notaQualitativa;
            } else {
                notaQualitativa = "Muito Bom";
                return notaQualitativa;
            }
        }
        else {
            notaQualitativa = "Erro. Nota entre 0 - 20.";
            return notaQualitativa;
        }
    }
        
    public static void exercicio16(){
        //Ler salário liquido do trabalhador
        //Variáveis envolvidas
        double salario_bruto =1;
        double salario_liquido, imposto;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura de dados & Processamento
        while (salario_bruto >0) {
            System.out.println("Salario bruto do trabalhador?");
            salario_bruto = ler.nextDouble();

            if (salario_bruto >= 0) {
                salario_liquido = getSalarioLiquido(salario_bruto);
                System.out.println("O salario liquido do trabalhador é " + String.format("%.2f", salario_liquido) + " euros.");
            }
            else {
                System.out.println("Erro. Programa interrompido.");
            }
        }
    }

    public static double getSalarioLiquido(double salario_bruto){
        double imposto, salarioLiquido;
        if(salario_bruto <=0 ){
            return -1;
        }else if(salario_bruto>0 && salario_bruto<=500){
            imposto = 0.10;
        }else if(salario_bruto<=1000){
            imposto = 0.15;
        }
        else{
            imposto = 0.20;
        }
        salarioLiquido = salario_bruto * (1-imposto);
        return salarioLiquido;
    }

    public static void exercicio17(){
        //Variáveis envolvidas
        double animal_food;
        double  animal_weight = 1;
        boolean adequatedAnimal_food;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados & Processamento
        while (animal_weight >=0) {
            System.out.println("Qual o peso do animal, em kg?");
            animal_weight = ler.nextDouble();
            if (animal_weight > 0) {
                System.out.println("Qual a quantida de ração que lhe tem alimentado, em g?");
                animal_food = ler.nextDouble();
                adequatedAnimal_food = getIfAnimalFoodAdquated(animal_weight,animal_food);
                if (adequatedAnimal_food == true) {
                    System.out.println("A quantidade de ração é adequada.");
                } else {
                    System.out.println("A quantidade de ração não é adequada.");
                }
            }
            else {
                System.out.println("Erro. Programa interrompido");
            }
        }

    }

    public static boolean getIfAnimalFoodAdquated (double animal_weight,double animal_food) {
        boolean adequatedAnimal_food=false;
        if (animal_weight <= 0) {
        }else if (animal_weight <= 10) {
            if (animal_food == 100) {
                adequatedAnimal_food = true;
            }
        } else if (animal_weight <= 25) {
            if (animal_food == 250) {
                adequatedAnimal_food = true;
            }
        } else if (animal_weight <= 45) {
            if (animal_food == 300) {
                adequatedAnimal_food = true;
            }
        }else {
            if (animal_food == 500) {
                adequatedAnimal_food = true;
            }
        }
        return adequatedAnimal_food;
    }
}