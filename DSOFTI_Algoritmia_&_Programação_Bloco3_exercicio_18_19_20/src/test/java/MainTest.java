import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    //Testes Exercicio18
    @org.junit.jupiter.api.Test
    void getVerificarSeCartaoCidadaoValido_testeTrue_numerosuplementarDiferentezeroTrue() {
        //arrange
        int n_CC = 150909985;
        //act
        boolean result = Main.getVerificarSeCartaoCidadaoValido(n_CC);
        //assert
        assertTrue(result);
    }

    @org.junit.jupiter.api.Test
    void getVerificarSeCartaoCidadaoValido_testeTrue_numerosuplementarDiferentezeroFalse() {
        //arrange
        int n_CC = 119409986;
        //act
        boolean result = Main.getVerificarSeCartaoCidadaoValido(n_CC);
        //assert
        assertFalse(result);
    }

    @org.junit.jupiter.api.Test
    void getVerificarSeCartaoCidadaoValido_testeTrue_numerosuplementarIgualzeroTrue() {
        //arrange
        int n_CC = 62350080;
        //act
        boolean result = Main.getVerificarSeCartaoCidadaoValido(n_CC);
        //assert
        assertTrue(result);
    }

    @org.junit.jupiter.api.Test
    void getVerificarSeCartaoCidadaoValido_testeTrue_numerosuplementarigualzeroFalse() {
        //arrange
        int n_CC = 62350082;
        //act
        boolean result = Main.getVerificarSeCartaoCidadaoValido(n_CC);
        //assert
        assertFalse(result);
    }

   /* @org.junit.jupiter.api.Test
    void getVerificarSeCartaoCidadaoValido_testeTrue_numerosuplementarIgualDezTrue() {
        //arrange
        int n_CC = ;
        //act
        boolean result = Main.getVerificarSeCartaoCidadaoValido(n_CC);
        //assert
        assertTrue(result);
    }

    */ // teste em falta

   @org.junit.jupiter.api.Test
   void getVerificarSeCartaoCidadaoValido_testeTrue_numeroszeroTrue() {
       //arrange
       int n_CC = 954535;   //no teste nºIdentificação: 00954535. Mas como declaro int não posso colocar os 2 primeiros zeros.
                            //No exercicio main isto não é um problema.
       //act
       boolean result = Main.getVerificarSeCartaoCidadaoValido(n_CC);
       //assert
       assertTrue(result);
   }


    //Teste Exercicio19
    @org.junit.jupiter.api.Test
    void getSequenciaNumerosReorganizada_teste1() {
        //arrange
        int sequencia = 23543;
        String expected = "35324";
        //act
        String result = Main.getSequenciaNumerosReorganizada(sequencia);
        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void getSequenciaNumerosReorganizada_teste2() {
        //arrange
        int sequencia = 169873;
        String expected = "197368";
        //act
        String result = Main.getSequenciaNumerosReorganizada(sequencia);
        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void getSequenciaNumerosReorganizada_teste3() {
        //arrange
        int sequencia = 2097564;
        String expected = "9752064";
        //act
        String result = Main.getSequenciaNumerosReorganizada(sequencia);
        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void getSequenciaNumerosReorganizada_teste4() {
        //arrange
        int sequencia = 1974650;
        String expected = "1975460";
        //act
        String result = Main.getSequenciaNumerosReorganizada(sequencia);
        //assert
        assertEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void getSequenciaNumerosReorganizada_testeValorNegativo() {
        //arrange
        int sequencia = -1974650;
        String expected = "-1";
        //act
        String result = Main.getSequenciaNumerosReorganizada(sequencia);
        //assert
        assertEquals(expected, result);

    }

    //Testes Exercicio20
    @org.junit.jupiter.api.Test
    void getSeNumero_Perfeito_Abundante_ou_Reduzido_testePerfeito() {
        //arrange
        int numero = 6;
        String expected = "Numero Perfeito";
        //act
        String result = Main.getSeNumero_Perfeito_Abundante_ou_Reduzido(numero);
        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getSeNumero_Perfeito_Abundante_ou_Reduzido_testeAbundante() {
        //arrange
        int numero = 12;
        String expected = "Numero Abundante";
        //act
        String result = Main.getSeNumero_Perfeito_Abundante_ou_Reduzido(numero);
        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getSeNumero_Perfeito_Abundante_ou_Reduzido_testeReduzido() {
        //arrange
        int numero = 9;
        String expected = "Numero Reduzido";
        //act
        String result = Main.getSeNumero_Perfeito_Abundante_ou_Reduzido(numero);
        //assert
        assertEquals(expected, result);
    }
    @org.junit.jupiter.api.Test
    void getSeNumero_Perfeito_Abundante_ou_Reduzido_testeNumeroNuegativo() {
        //arrange
        int numero = -12;
        String expected = "Erro. Numero deve ser superior 0.";
        //act
        String result = Main.getSeNumero_Perfeito_Abundante_ou_Reduzido(numero);
        //assert
        assertEquals(expected, result);
    }
}