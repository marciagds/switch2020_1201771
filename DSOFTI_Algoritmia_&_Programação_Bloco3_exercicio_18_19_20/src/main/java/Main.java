import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        exercicio18();
        //exercicio19();
        //exercicio20();
    }

    public static void exercicio18() {
        //Misterio do bilhete de identidade;

        //Variáveis envolvidas
        int n_CC;

        boolean n_CC_verificado;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos daddos
        System.out.println("Insira o seu numero de BI ou CC com o algarismo suplementar:");
        n_CC = ler.nextInt();

        //Processamento
        n_CC_verificado = getVerificarSeCartaoCidadaoValido(n_CC);

        //Saída dos dados
        if (n_CC_verificado == true) {
            System.out.println("Numero de Identificação válido");
        } else {
            System.out.println("Numero de Identificação não válido");
        }
    }

    public static int getNumeroAlgarismosDeUmaSequencia(int numero_CC) {
        String numero = Integer.toString(numero_CC);
        int contaDigitos = 0;
        char algarismos;
        for (int i = 0; i < numero.length(); i++) {
            algarismos = numero.charAt(i);
            if (Character.isDigit(algarismos)) {
                contaDigitos += 1;
            }
        }
        return contaDigitos;
    }

    public static boolean getVerificarSeCartaoCidadaoValido(int n_CC) {
        int numero = n_CC;
        int algarismo;
        int qtdNumetos_CC = getNumeroAlgarismosDeUmaSequencia(n_CC);
        int somaPonderada = 0;
        boolean n_CC_verificado = false;
        int algarismo1 = n_CC % 10;
        double resto;

        if (algarismo1 != 0) {     //algarismo suplementar é diferente de zero.
            for (int i = 1; i <= qtdNumetos_CC; i++) {
                algarismo = numero % 10;
                somaPonderada += (algarismo * i);
                numero = numero / 10;
            }
            resto = somaPonderada % 11;
            if (resto == 0) {
                n_CC_verificado = true;
            }
            return n_CC_verificado;
        } else {                     //quando o algarismo suplementar é zero. Para dar o algoritmo certo, o algarismo suplementar pode ser o ou 10.
            for (int i = 1; i <= qtdNumetos_CC; i++) {
                algarismo = numero % 10;
                somaPonderada += (algarismo * i);
                numero = numero / 10;
            }
            resto = somaPonderada % 11;
            if (resto == 0) {                     //O numero suplememntar vale zero.
                n_CC_verificado = true;
                return n_CC_verificado;
            } else {                              // O numero suplementar vale 10.
                somaPonderada = somaPonderada + 10;
                resto = somaPonderada % 11;
                numero = numero / 10;
                if (resto == 0) {
                    n_CC_verificado = true;
                    return n_CC_verificado;
                } else {
                    n_CC_verificado = false;
                    return n_CC_verificado;
                }
            }
        }
    }

    public static void exercicio19() {
        // reorganizar sequencia de numeros.(pares à direita e os ímpares à esquerda).
        //Variáveis envolvidas
        int sequencia = 1;
        String resultado;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados & Processamento
        while (sequencia >= 0) {          //O algoritmo para quando sequencia toma valor negativo
            System.out.println("Inserir sequencia de numeros:");
            sequencia = ler.nextInt();
            if (sequencia >= 0) {
                resultado = getSequenciaNumerosReorganizada(sequencia);
            } else {
                resultado = "Erro. Programa interrompido";
            }
            System.out.println(resultado);
        }
    }

    public static String getSequenciaNumerosReorganizada(int sequencia) {
        // Variáveis envolvidas
        int numero, contagemDigitos, algarismo;
        int i;
        double resto;
        int sequenciaPares = 0;
        int sequenciaImpares = 0;
        String sequenciaImparesPares;
        //Processamento
        numero = sequencia;
        contagemDigitos = getNumeroAlgarismosDeUmaSequencia(sequencia);
        if (sequencia > 0) {
            for (i = contagemDigitos; i > 0; i--) {
                algarismo = (int) (numero / Math.pow(10, i - 1));
                resto = algarismo % 2;
                if (resto == 0) {
                    sequenciaPares = sequenciaPares * 10 + algarismo;
                } else {
                    sequenciaImpares = sequenciaImpares * 10 + algarismo;
                }
                numero = (int) (numero - (algarismo * Math.pow(10, i - 1)));
            }
            sequenciaImparesPares = sequenciaImpares + "" + sequenciaPares;
            return sequenciaImparesPares;
        } else {
            return "-1";
        }

    }

    public static void exercicio20() {
        //Variaveis envolvidas
        int numero =1;
        String classification;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        while (numero>0){               //Paragem do programa para numero <=0
            System.out.println("Insira um numero:");
            numero = ler.nextInt();
                classification = getSeNumero_Perfeito_Abundante_ou_Reduzido(numero);
                System.out.println(classification);
        }
    }

    public static String getSeNumero_Perfeito_Abundante_ou_Reduzido(int numero) {
        double resto;
        int somaMultiplos = 0;
        String classification;
        if (numero > 0) {
            for (int i = 1; i < numero; i++) {
                resto = numero % i;
                if (resto == 0) {
                    somaMultiplos += i;
                }
            }
            if (somaMultiplos == numero) {
                classification = "Numero Perfeito";
                return classification;
            } else if (somaMultiplos < numero) {
                classification = "Numero Reduzido";
                return classification;
            } else {
                classification = "Numero Abundante";
                return classification;
            }
        } else {
            classification = "Erro. Numero deve ser superior 0.";
            return classification;
        }

    }
}
