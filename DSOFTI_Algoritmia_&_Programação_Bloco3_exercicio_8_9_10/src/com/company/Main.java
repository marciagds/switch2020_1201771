package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        exercicio8();
        //exercicio9();
        //exercicio10();
    }

    public static void exercicio8(){
        //Variáveis envolvidas
        int valorMaximoSoma, numero;
        int somaNumeros=0;
        int valor_minimo=-1;        //assegura o registo do primeiro valor como referência

        //Inicialização da Leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados;
        System.out.print("Qual o valor máximo da soma?");
        valorMaximoSoma = ler.nextInt();

        //Processamento
        do{
            do {                        //validação do numero lido. Este tem de ser positivo
                System.out.print("Número:");
                numero = ler.nextInt();
            }while(numero<=0);

        somaNumeros += numero;      //Soma dos numeros;
            valor_minimo = getValorMinimoInseridoNoConjuntodeDados(numero, valor_minimo);

        }while(somaNumeros<valorMaximoSoma);

        //Saída dos dados
        System.out.print("O valor minimo registado foi: " + valor_minimo);
    }

    public static int getValorMinimoInseridoNoConjuntodeDados(int numero, int valor_minimo){

        if(valor_minimo==-1){   //condição aplicável na primeira leitura, pois não tenho ainda referência para valor minimo.
            valor_minimo = numero;
        }

        if(numero<valor_minimo){
            valor_minimo = numero;
        }
        return valor_minimo;
    }

    public static void exercicio9(){
/*
//Ler o número de horas extraordinárias e o salário base de cada funcionário.
Validação: Salário devem ser superior a zero, e as horas >=0;
-Calcular e mostrar o salário mensal que cada empregado irá receber (salário base + valor
referente às horas extraordinárias); O preço de cada hora extraordinária é de 2% do salário base.
-Calcular e mostrar a média dos salários mensais pagos pela empresa, no mês corrente.
-Termine a leitura dos dados introduzindo o valor -1 nas horas extraordinárias. Todos os valores
introduzidos pelo utilizador devem ser validados
 */
        //Variáveis envolvidas
        double salario_base, salario_mensal;
        int horas_extraordinarias;
        double somaSalario=0;
        int contaEmpregado=0;
        double mediaSalarioMensal;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados + Processamento

        do {
            do {
                System.out.print("Quantidade de horas extradordinárias:");
                horas_extraordinarias = ler.nextInt();
            } while (horas_extraordinarias <0 && horas_extraordinarias!= -1);

            if(horas_extraordinarias >=0) {
                do {
                    System.out.print("Qual o salário do trabalhador:");
                    salario_base = ler.nextDouble();
                } while (salario_base <= 0);
                salario_mensal = salario_base + horas_extraordinarias * 0.02 * salario_base;
                System.out.print("Salário mensal: " + salario_mensal + " \n");

                if (horas_extraordinarias >= 0) {
                    somaSalario += salario_mensal;
                    contaEmpregado += 1;
                }
            }
        }while(horas_extraordinarias>=0);

        if (contaEmpregado >0) {
            mediaSalarioMensal = somaSalario / contaEmpregado;
        }
        else {
            mediaSalarioMensal =-1;
        }
        //saída dos resultados
        System.out.print("Média do salário mensal: " + mediaSalarioMensal);

/* Este exercicio não pode ser modelizado, pois o processamento acontece em simultâneo
 com a entrada dos dados. */
    }

    public static void exercicio10(){
        //Variáveis envolvidas
        int valorMaximoProduto, numero;
        int valor_maximo=-1;        //assegura o registo do primeiro valor como referência
        int produtoNumeros=1;

        //Inicialização da Leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados;
        System.out.print("Qual o valor máximo do produto?");
        valorMaximoProduto = ler.nextInt();

        //Processamento
        do{
            do {                        //validação do numero lido. Este tem de ser positivo
                System.out.print("Número:");
                numero = ler.nextInt();
            }while(numero<=0);
            if(valor_maximo==-1){   //condição aplicável na primeira leitura, pois não tenho ainda referência para valor minimo.
                valor_maximo= numero;
            }
            produtoNumeros =produtoNumeros* numero;      //Soma dos numeros;
            if(numero>valor_maximo){
                valor_maximo= numero;
            }

        }while(produtoNumeros<valorMaximoProduto);

        //Saída dos dados
        System.out.print("O valor minimo registado foi: " + valor_maximo);

 /* Este exercicio não pode ser modelizado, pois o processamento acontece em simultâneo
 com a entrada dos dados. */
    }

}
