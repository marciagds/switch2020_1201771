package com.company;

import org.omg.PortableServer._ServantActivatorStub;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        //exercicio12();
        //exercicio13();
        exercicio14();
    }
    public static void exercicio12(){
        //Emissão de notificação face aos niveis de poluição

        //Variáveis envolvidas
        double indice;
        String  notification;

        //Inicialização da Leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual o índice de poluição ?");
        indice= ler.nextDouble();

        //Processamento
        notification = getNotification(indice);

        //Saída dos dados
        System.out.println(notification);
    }

    public static String getNotification(double indice){
        String notification;

        if(indice <0 ){
            notification = "Índice de Poluição Incorreto.";
            return notification;
        }
        else {
            if (indice <0.3){
                notification = "Índice de Poluição aceitável";
                return notification;
            }
            else  {
                if (indice <0.4){
                    notification = "O 1º grupo tem de suspender as atividades.";
                    return notification;
                }
                else {
                    if (indice <=0.5){
                        notification = "O 1º e 2º grupo têm de suspenser as suas atividades.";
                        return notification;
                    }
                    else {
                        notification = "Todos os grupos têm de suspender as suas atividades.";
                        return notification;
                    }
                }
            }
        }

    }

    public static void exercicio13(){
        // Custo de uma empresa de jardinagem.

        //Variáveis envolvidas
        double area_grama;
        int n_arvores, n_arbustos;

        //Inicialização da Leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual a area de grama ?");
        area_grama = ler.nextDouble();
        System.out.println("Qual o número de árvores ?");
        n_arvores = ler.nextInt();
        System.out.println("Qual o número de arbustos ?");
        n_arbustos = ler.nextInt();

        //Processamento

        double $material = getCustoMaterialJardinagem(area_grama,n_arbustos,n_arvores);
        double $tempo = getCustoTempoJardinagem(area_grama,n_arbustos,n_arvores);
        double $total = getCustoTotalJardinagem(area_grama,n_arbustos,n_arvores);

        //Saída dos dados
        System.out.println("O custo de material é "+String.format("%.2f",$material));
        System.out.println("O custo de mão de obra total  é "+String.format("%.2f",$tempo));
        System.out.println("O custo total estimado é "+String.format("%.2f",$total));

    }

    public static double getCustoMaterialJardinagem(double area_grama, int n_arbustos, int n_arvores) {
        double $grama, $arvore, $arbusto;
        $grama = 10;
        $arvore= 20;
        $arbusto = 15;

        if(area_grama <0 ){
            area_grama =0;
        }
        else {}

        if(n_arbustos<0){
            n_arbustos = 0;
        }
        else {}

        if(n_arvores <0){
            n_arvores = 0;
        }
        else {}
        double $material = $grama * area_grama + $arbusto * n_arbustos + $arvore * n_arvores;
        return $material;
    }

    public static double getCustoTempoJardinagem(double area_grama, int n_arbustos, int n_arvores) {
        int tempo_arbustos, tempo_arvores, tempo_grama, $mao_de_obra;
        tempo_arbustos = 400;
        tempo_arvores = 600;
        tempo_grama = 300;
        $mao_de_obra = 10;

        if(area_grama <0 ){
            area_grama =0;
        }
        else {}

        if(n_arbustos<0){
            n_arbustos = 0;
            }
        else {}

        if(n_arvores <0){
        n_arvores = 0;
        }
        else {}


        double $tempo = (area_grama * tempo_grama + n_arbustos * tempo_arbustos + n_arvores * tempo_arvores)*$mao_de_obra/60;
        return $tempo;
    }

    public static double getCustoTotalJardinagem(double area_grama, int n_arbustos, int n_arvores){
        double $material = getCustoMaterialJardinagem(area_grama, n_arbustos, n_arvores);
        double $tempo = getCustoTempoJardinagem(area_grama, n_arbustos, n_arvores);

        double $total = $material + $tempo;
        return $total;
    }

    public static void exercicio14() {
        //Cálculo da distância média de um estafeta durante uma semana

        //Variáveis envolvidas
        double d_segunda, d_terca, d_quarta, d_quinta, d_sexta, d_medio;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("QUal a distância percorrida na segunda-feira, em milhas ?");
        d_segunda = ler.nextDouble();
        System.out.println("QUal a distância percorrida na terça-feira, em milhas ?");
        d_terca = ler.nextDouble();
        System.out.println("QUal a distância percorrida na quarta-feira, em milhas ?");
        d_quarta = ler.nextDouble();
        System.out.println("QUal a distância percorrida na quinta-feira, em milhas ?");
        d_quinta = ler.nextDouble();
        System.out.println("QUal a distância percorrida na sexta-feira, em milhas ?");
        d_sexta = ler.nextDouble();

        //Processamento
        d_medio = getDistanciaMedia_km(d_segunda, d_terca, d_quarta, d_quinta, d_sexta);

        //Saída dos dados
        if (d_medio >= 0) {
            System.out.println("A distância média diária é " + String.format("%.2f", d_medio) + " km");
        }
        else {
            System.out.println("Erro. Não existem distâncias negativas.");
        }
    }

    public static double getDistanciaMedia_km(double d_segunda, double d_terca, double d_quarta, double d_quinta, double d_sexta) {
        double d_medio;
        if (d_segunda < 0 || d_terca < 0 || d_quarta < 0 || d_quinta < 0 || d_sexta < 0) {
            d_medio = -1;
            return d_medio;
        } else {
            d_medio = (d_segunda + d_terca + d_quarta + d_quinta + d_sexta) / 5;
            d_medio = (d_medio * 1609) / 1000;
            return d_medio;
        }
    }
}
