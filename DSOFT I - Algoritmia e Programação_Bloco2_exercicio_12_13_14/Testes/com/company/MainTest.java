package com.company;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    //Testes Exercicio12
    @org.junit.jupiter.api.Test
    void getNotification_valor_negativo() {
        //arrange
        double indice = -1;
        String expected = "Índice de Poluição Incorreto.";

        //act
        String result =  Main.getNotification(indice);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getNotification_valor_nulo() {
        //arrange
        double indice = 0;
        String expected = "Índice de Poluição aceitável";

        //act
        String result =  Main.getNotification(indice);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getNotification_valor_02() {
        //arrange
        double indice = 0.2;
        String expected = "Índice de Poluição aceitável";

        //act
        String result =  Main.getNotification(indice);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getNotification_valor_03() {
        //arrange
        double indice = 0.3;
        String expected =  "O 1º grupo tem de suspender as atividades.";

        //act
        String result =  Main.getNotification(indice);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getNotification_valor_033() {
        //arrange
        double indice = 0.33;
        String expected =  "O 1º grupo tem de suspender as atividades.";

        //act
        String result =  Main.getNotification(indice);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getNotification_valor_04() {
        //arrange
        double indice = 0.4;
        String expected = "O 1º e 2º grupo têm de suspenser as suas atividades.";

        //act
        String result =  Main.getNotification(indice);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getNotification_valor_041() {
        //arrange
        double indice = 0.41;
        String expected = "O 1º e 2º grupo têm de suspenser as suas atividades.";

        //act
        String result =  Main.getNotification(indice);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getNotification_valor_05() {
        //arrange
        double indice = 0.5;
        String expected = "O 1º e 2º grupo têm de suspenser as suas atividades.";

        //act
        String result =  Main.getNotification(indice);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getNotification_valor_6() {
        //arrange
        double indice = 6;
        String expected = "Todos os grupos têm de suspender as suas atividades.";

        //act
        String result =  Main.getNotification(indice);

        //assert
        assertEquals(expected, result);
    }

    //Testes Exercicio13
    @org.junit.jupiter.api.Test
    void getCustoMaterialJardinagem_teste1() {
        //arrange
        double area_grama = 12;
        int n_arvores = 3;
        int n_arbustos = 5;
        double expected = 255;

        //act
        double result = Main.getCustoMaterialJardinagem(area_grama,n_arbustos,n_arvores);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getCustoMaterialJardinagem_teste2() {
        //arrange
        double area_grama = 12;
        int n_arvores = -3;
        int n_arbustos = 5;
        double expected = 195;

        //act
        double result = Main.getCustoMaterialJardinagem(area_grama,n_arbustos,n_arvores);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getCustoTempoJardinagem_teste1() {
        //arrange
        double area_grama = 12;
        int n_arvores = 3;
        int n_arbustos = 5;
        double expected = 1233.33;

        //act
        double result = Main.getCustoTempoJardinagem(area_grama,n_arbustos,n_arvores);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getCustoTempoJardinagem_teste2() {
        //arrange
        double area_grama = 12;
        int n_arvores = -3;
        int n_arbustos = 5;
        double expected = 933.33;

        //act
        double result = Main.getCustoTempoJardinagem(area_grama,n_arbustos,n_arvores);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getCustoTotalJardinagem_teste1() {
        //arrange
        double area_grama = 12;
        int n_arvores = 3;
        int n_arbustos = 5;
        double expected = 1488.33;

        //act
        double result = Main.getCustoTotalJardinagem(area_grama,n_arbustos,n_arvores);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getCustoTotalJardinagem_teste2() {
        //arrange
        double area_grama = 0;
        int n_arvores = 6;
        int n_arbustos = 4;
        double expected = 1046.66;

        //act
        double result = Main.getCustoTotalJardinagem(area_grama,n_arbustos,n_arvores);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getCustoTotalJardinagem_teste3() {
        //arrange
        double area_grama = 7;
        int n_arvores = -2;
        int n_arbustos = 5;
        double expected = 828.33;

        //act
        double result = Main.getCustoTotalJardinagem(area_grama,n_arbustos,n_arvores);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getCustoTotalJardinagem_teste4() {
        //arrange
        double area_grama = -2;
        int n_arvores = -2;
        int n_arbustos = -5;
        double expected = 0;

        //act
        double result = Main.getCustoTotalJardinagem(area_grama,n_arbustos,n_arvores);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getCustoTotalJardinagem_teste7() {
        //arrange
        double area_grama = 0;
        int n_arvores = 0;
        int n_arbustos = 0;
        double expected = 0;

        //act
        double result = Main.getCustoTotalJardinagem(area_grama,n_arbustos,n_arvores);

        //assert
        assertEquals(expected, result, 0.01);
    }

    //Testes Exercicio14
    @org.junit.jupiter.api.Test
    void getDistanciaMedia_km_teste1() {
        //arrange
        double d_segunda = 1.2;
        double d_terca = 0.4;
        double d_quarta = 1;
        double d_quinta = 3;
        double d_sexta = 4.3;
        double expected = 3.185;

        //act
        double result = Main.getDistanciaMedia_km(d_segunda,d_terca,d_quarta,d_quinta,d_sexta);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getDistanciaMedia_km_teste2() {
        //arrange
        double d_segunda = 0;
        double d_terca = 0;
        double d_quarta = 0;
        double d_quinta = 0;
        double d_sexta = 0;
        double expected = 0;

        //act
        double result = Main.getDistanciaMedia_km(d_segunda,d_terca,d_quarta,d_quinta,d_sexta);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getDistanciaMedia_km_teste3() {
        //arrange
        double d_segunda = -1.2;
        double d_terca = 0.4;
        double d_quarta = 1;
        double d_quinta = 3;
        double d_sexta = 4.3;
        double expected = -1;

        //act
        double result = Main.getDistanciaMedia_km(d_segunda,d_terca,d_quarta,d_quinta,d_sexta);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @org.junit.jupiter.api.Test
    void getDistanciaMedia_km_teste4() {
        //arrange
        double d_segunda = 1.2;
        double d_terca = 0.4;
        double d_quarta = -1;
        double d_quinta = 3;
        double d_sexta = 4.3;
        double expected = -1;

        //act
        double result = Main.getDistanciaMedia_km(d_segunda,d_terca,d_quarta,d_quinta,d_sexta);

        //assert
        assertEquals(expected, result, 0.01);
    }
}
