package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void exercicio2_teste1() {
        //arrange
        int nrosa = 2;
        int ntulipa = 1;
        double $rosa = 10;
        double $tulipa = 5;
        double expected = 25;

        //act
        double result = Main.exercicio2(nrosa, ntulipa, $rosa, $tulipa);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio2_teste2() {
        //arrange
        int nrosa = 5;
        int ntulipa = 4;
        double $rosa = 2;
        double $tulipa = 12;
        double expected = 58;

        //act
        double result = Main.exercicio2(nrosa, ntulipa, $rosa, $tulipa);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio3_teste1() {
        //arrange
        double raio = 2.5;
        double altura = 5;
        double expected = 98174.77;

        //act
        double result = Main.exercicio3(raio, altura);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @org.junit.jupiter.api.Test
    void exercicio3_teste2() {
        //arrange
        double raio = 5;
        double altura = 5;
        double expected = 392699.08;

        //act
        double result = Main.exercicio3(raio, altura);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio4_teste1() {
        //arrange
        int tempo = 10;
        double expected = 3.4;

        //act
        double result = Main.exercicio4(tempo);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio4_teste2() {
        //arrange
        int tempo = 50;
        double expected = 17;

        //act
        double result = Main.exercicio4(tempo);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio5_teste1() {
        //arrange
        double tempo = 2;
        double expected = 19.6;

        //act
        double result = Main.exercicio5(tempo);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio5_teste2() {
        //arrange
        double tempo = 10;
        double expected = 490;

        //act
        double result = Main.exercicio5(tempo);

        //assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio6_teste1() {
        //arrange
        double sombra_pessoa = 4;
        double sombra_edificio = 40;
        double altura_pessoa = 2;
        double expected = 20;

        //act
        double result = Main.exercicio6(sombra_pessoa, sombra_edificio, altura_pessoa);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio6_teste2() {
        //arrange
        double sombra_pessoa = 6;
        double sombra_edificio = 30;
        double altura_pessoa = 1.8;
        double expected = 9;

        //act
        double result = Main.exercicio6(sombra_pessoa, sombra_edificio, altura_pessoa);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio7_teste1() {
        //arrange
        double tempo_manel = (4 * 3600) + (2 * 60) + 10;
        double tempo_ze = (1 * 3600) + (5 * 60);
        double distancia_manel = 42195;
        double expected = 11325.567;

        //act
        double result = Main.exercicio7(tempo_ze, tempo_manel, distancia_manel);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio7_teste2() {
        //arrange
        double tempo_manel = (5 * 3600) + (7 * 60) + 15;
        double tempo_ze = (3 * 3600) + (5 * 60) + 12;
        double distancia_manel = 42195;
        double expected = 25433.731;

        //act
        double result = Main.exercicio7(tempo_ze, tempo_manel, distancia_manel);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio8_teste1() {
        //arrange
        double operario_a = 40;
        double operario_b = 60;
        double angulo_ab = 60;
        double expected = 52.915;

        //act
        double result = Main.exercicio8(operario_a, operario_b, angulo_ab);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio8_teste2() {
        //arrange
        double operario_a = 30;
        double operario_b = 45;
        double angulo_ab = 40;
        double expected = 29.269;

        //act
        double result = Main.exercicio8(operario_a, operario_b, angulo_ab);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio9_teste1() {
        //arrange
        double comprimento = 3;
        double largura = 4;
        double expected = 14;

        //act
        double result = Main.exercicio9(comprimento, largura);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio9_teste2() {
        //arrange
        double comprimento = 6;
        double largura = 2;
        double expected = 16;

        //act
        double result = Main.exercicio9(comprimento, largura);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio10_teste1() {
        //arrange
        double cateto_a = 3;
        double cateto_b = 4;
        double expected = 5;

        //act
        double result = Main.exercicio10(cateto_a, cateto_b);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio10_teste2() {
        //arrange
        double cateto_a = 5;
        double cateto_b = 7;
        double expected = 8.602;

        //act
        double result = Main.exercicio10(cateto_a, cateto_b);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio11_teste1() {
        //arrange
        double x = 3;
        double expected = 1;

        //act
        double result = Main.exercicio11(x);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio11_teste2() {
        //arrange
        double x = -3;
        double expected = 19;

        //act
        double result = Main.exercicio11(x);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio12_teste1() {
        //arrange
        double temperature_C = 40;
        double expected = 104;

        //act
        double result = Main.exercicio12(temperature_C);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio12_teste2() {
        //arrange
        double temperature_C = -12;
        double expected = 10.40;

        //act
        double result = Main.exercicio12(temperature_C);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio13_teste1() {
        //arrange
        int H = 12;
        int M = 40;
        int expected = 760;

        //act
        int result = (int) Main.exercicio13(H,M);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio13_teste2() {
        //arrange
        int H = 10;
        int M = 13;
        int expected = 613;

        //act
        int result = (int) Main.exercicio13(H,M);

        //assert
        assertEquals(expected, result, 0.01);
    }
}

