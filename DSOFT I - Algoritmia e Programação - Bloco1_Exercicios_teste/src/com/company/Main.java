package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here
        System.out.println(exercicio2(1,1,1,1));
        System.out.println(exercicio3(2.5, 5));
    }
    public static double exercicio2(double nrosas, double ntulipas, double $rosa, double $tulipa) {
        // Calculo do preço final do ramo de flores

        // Processamento dos dados
        double $final = ($rosa * nrosas) + ($tulipa * ntulipas);
        return $final;

    }
    public static double exercicio3(double raio, double altura) {
        // Calculo do volume do cilindro

        // Processamento dos dados
        double volume = getCilinderVolume(raio, altura) * 1000;
        return volume;
    }
    private static double getCilinderVolume(double raio, double altura) {
        return Math.PI * raio * raio * altura;
    }

    public static double exercicio4(double tempo){
        //Qual a distância a que se encontra uma trovoada?

        // Processamento
        int velocidadesom = 1224;   // km/h
        double distancia = velocidadesom * (tempo/3600);
        return distancia;

    }
    public static double exercicio5(double tempo ){
        // Calcular a altura de um edifício usando a gravidade

        // Processamento
        double v_inicial= 0;
        double gravidade = 9.8;
        double altura_predio = (v_inicial * tempo)+((gravidade * Math.pow(tempo,2))/2);
        return altura_predio;

    }
    public static double exercicio6(double sombra_pessoa, double sombra_edificio, double altura_pessoa){
        // Calcular a altura de um edifício usando o teorema de Tales

        // Processamento
        double altura_edificio = (sombra_edificio * altura_pessoa) / sombra_pessoa;
        return altura_edificio;
    }

    public static double exercicio7(double time_ze, double time_manel, double distancia_manel){
        // Calcular a distância percorrida pelo Zé.

        // Processamento
        double veloc_manel = getVeloc_manel(time_manel, distancia_manel);
        double distancia_ze = (veloc_manel * time_ze);
        return distancia_ze;

    }

    private static double getVeloc_manel(double time, double distancia) {
        return distancia / time;
    }


    public static double exercicio8(double operario_a, double operario_b, double angulo_ab){
        //Calcular a distância entre os dois operários.

        // Processamento
        double angulo_ab_rad = angulo_ab * (Math.PI / 180);
        double distancia = Math.sqrt(Math.pow(operario_a,2)+Math.pow(operario_b,2)-2*operario_a*operario_b*Math.cos(angulo_ab_rad));
        return distancia;

    }

    public static double exercicio9(double comprimento, double largura){
        //Calcular perímetro do retângulo

        //Processamento
        double perimetro_retangulo= getrectangle_perimeter(comprimento, largura);
        return perimetro_retangulo;
    }
    private static double getrectangle_perimeter(double length, double largura) {
        return (2 * length) + (2 * largura);
    }

    public static double exercicio10(double cateto_a, double cateto_b){
        // Calcular hipotenusa do triângulo

        //Processamento
        double hipotenusa = getTeoremaDePitagoras(cateto_a, cateto_b);
        return hipotenusa;
    }

    private static double getTeoremaDePitagoras(double cateto_a, double cateto_b) {
        return Math.sqrt(Math.pow(cateto_a, 2) + Math.pow(cateto_b, 2));
    }

    public static double exercicio11(double x){
        // Executar o cálculo da função

        //Processamento
        double solution = Math.pow(x,2) - 3 * x + 1;
        return solution;

    }

    public static double exercicio12(double temperature_C){
        // Conversor ºC em ºF

        //Processamento
        double temperature_F= 32 + 1.8 * temperature_C;
        return temperature_F;

    }
    public static double exercicio13(int H, int M){
        // Calculo dos minutos totais passados, desde as 0H

        //Processamento
        double minutos_totais = H * 60 + M;
        return minutos_totais;

    }
}
