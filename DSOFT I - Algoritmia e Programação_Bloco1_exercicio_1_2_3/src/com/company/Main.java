package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        //exercicio1();
        exercicio2();
        //exercicio3();
    }

    public static void exercicio1() {
        // Calcular a % de rapazes e raparigas na aula.

        // Variáveis envolvidas
        int rapazes, raparigas;
        double total, percentagemrapazes, percentagemraparigas;

        // Iniciaização da leitura dos dados requeridos
        Scanner ler = new Scanner(System.in);

        // Leitura dos dados
        System.out.println("Introduza o nº de rapazes");
        rapazes = ler.nextInt();
        System.out.println("Introduza o nº de raparigas");
        raparigas = ler.nextInt();

        // Processamento dos dados
        total = raparigas + rapazes;
        percentagemraparigas = getPercentage(raparigas, total);
        percentagemrapazes = getPercentage(rapazes, total);

        // Saída dos dados
        System.out.println("Percentagem de Raparigas: " + percentagemraparigas + "%");
        System.out.println("Percentagem de rapazes: " + percentagemrapazes + "%");
    }

    private static double getPercentage(int raparigas, double total) {
        return (raparigas / total) * 100;
    }

    public static void exercicio2() {
        // Calculo do preço final do ramo de flores

        // Variáveis envolvidas
        double nrosas, ntulipas;
        double $rosa, $tulipa, $final;

        // Iniciaização da leitura dos dados requeridos
        Scanner ler = new Scanner(System.in);

        // Leitura dos dados
        System.out.println("Introduza o preço unitário das rosas");
        $rosa = ler.nextDouble();
        System.out.println("Introduza o preço unitário das tulipas");
        $tulipa = ler.nextDouble();
        System.out.println("Introduza a quantidade de rosas");
        nrosas = ler.nextInt();
        System.out.println("Introduza a quantidade de tulipas");
        ntulipas = ler.nextInt();

        // Processamento dos dados
        $final = ($rosa * nrosas) + ($tulipa * ntulipas);

        //  Saída dos dados
        System.out.println("O preço do ramo é " + String.format("%.2f", $final) + " euros.");


    }

    public static void exercicio3() {

        // Calculo do volume do cilindro

        // Variáveis envolvidas
        double raio, altura, volume;

        // Iniciaização da leitura dos dados requeridos
        Scanner ler = new Scanner(System.in);

        // Leitura dos dados
        System.out.println("Qual é o raio do cilindro em metros?");
        raio = ler.nextDouble();
        System.out.println("Qual é a altura do cilindro em metros?");
        altura = ler.nextDouble();

        // Processamento dos dados
        volume = getCilinderVolume(raio, altura) * 1000;

        // Saida de dados
        System.out.println("O volume do cilindro é " + String.format("%.2f", volume) + "L");
    }

    private static double getCilinderVolume(double raio, double altura) {
        return Math.PI * raio * raio * altura;
    }

}
