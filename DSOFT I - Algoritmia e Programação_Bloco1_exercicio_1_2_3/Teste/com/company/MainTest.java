package com.company;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MainTest {
    @Test
    public void teste_ex2() {
        double expected = 25;
        double result = Main. exercicio2();
        assertEquals(expected, result, 0.01);

    }

}