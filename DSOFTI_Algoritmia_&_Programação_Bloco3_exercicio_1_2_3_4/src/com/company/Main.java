package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        //exercicio1();
        //exercicio2();
        //exercicio3();
        exercicio4();
    }

    public static void exercicio1() {
        //Cálculo do valor de um numero fatoria. Exemplo: 4!=4x3x2x1

        //Variáveis envolvidas
        int resultado, number;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura da variável
        System.out.println("Escolha um número inteiro");
        number = ler.nextInt();

        //Processamento
        resultado = getFatorialDoNumero(number);

        //Saída dos dados
        System.out.println("O resultado é: " + resultado);
    }

    public static int getFatorialDoNumero(int number) {
        int resultado;
        if (number < 0) {
            return -1;
        } else {
            if (number == 0) {
                resultado = 1;
                return resultado;
            } else {
                resultado = 1;
                for (int i = number; i >= 1; i = i - 1) {
                    int x = i;
                    resultado = resultado * x;
                }
                return resultado;
            }
        }
    }

    public static void exercicio2() {
        //Calcular % de notas positivas e média das notas negativas

        //Variáveis envolvidas
        int n_alunos, contaPositivo;
        double nota, somaNegativo;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Ler nº alunos. Obrigar a que seja maior do que zero.
        do {
            System.out.println("Insira o nº de alunos:");
            n_alunos = ler.nextInt();
        } while (n_alunos <= 0);

        //Processamento
        contaPositivo = 0;
        somaNegativo = 0;
        for (int i = 0; i < n_alunos; i++) {
            nota = ler.nextDouble();
            if (nota >= 10) {
                contaPositivo += 1;
            } else {
                somaNegativo += nota;
            }
        }
        double percentagem_positivo = contaPositivo / n_alunos;
        double media_negativos = somaNegativo / (n_alunos - contaPositivo);

        //Saída de dados
        System.out.print("% de notas positivas: " + String.format("%.2f", percentagem_positivo));
        System.out.print("média das notas negativas: " + String.format("%.2f", media_negativos));
    }
/* NOTA: Este problema não deve ser modelizado, pois a leitura das variáveis e o processamento
da informação não podem ser separados na sua totalidade. Isto acontece porque o valor das notas
não é armazenado em lugar nenhum, pelo que não pode ser chamado para a comparação em testes.
 */

    public static void exercicio3() {
        //Calcular percentagem de numeros pares e média de números impares, de uma dada sequencia.

        //Variáveis envolvidas
        int i = 0;
        int number, resto;
        double contaPar = 0;
        double contaImpar = 0;
        double somaImpar = 0;
        double percentagem_Par, media_Impar;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura da variável + Processamento
        do {
            System.out.print("Inserir numero: ");
            number = ler.nextInt();
            if (number >= 0) {
                resto = number % 2;

                if (resto == 0) {
                    contaPar += 1;
                } else {
                    somaImpar += number;
                    contaImpar += 1;
                }
            } else {
            }

        } while (number >= 0);

        percentagem_Par = contaPar / (contaImpar + contaImpar) * 100.0;
        media_Impar = somaImpar / contaImpar;

        //Saída dos dados
        System.out.print("percentagem números pares: " + String.format("%.2f", percentagem_Par) + " ");
        System.out.print("média números impares: " + String.format("%.2f", media_Impar));
    }

    /* ATENÇÃO: Tal como o exercicio anterior, este problema não pode ser modelizado. Não é possivel
    separar a leitura e entrada dos dados com o processamento.
     */
    public static void exercicio4() {
        //Variáveis envolvidas
        int numero1, lim_min, lim_max, resultado_alinea_a, resultado_alinea_b, numero2, resultado_alinea_c, resultado_alinea_d;
        double resultado_alinea_e;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura das variáveis
        System.out.print("Limite mínimo do intervalo:");
        lim_min = ler.nextInt();
        System.out.print("Limite máximo do intervalo:");
        lim_max = ler.nextInt();
        System.out.print("Multiplo de:");
        numero1 = ler.nextInt();
        System.out.print("Multiplo de:");
        numero2 = ler.nextInt();


        //Processamento
        resultado_alinea_a = getNumeroMultiplosdeUmNumero(lim_min, lim_max, numero1);
        resultado_alinea_b = getNumeroMultiplosdeUmNumero(lim_min, lim_max,  12);
        resultado_alinea_c = getNumeroMultiplosde2Numeros(lim_min, lim_max,  numero1, numero2);
        resultado_alinea_d = getNumeroMultiplosde2Numeros(lim_min, lim_max,  12, 8);
        resultado_alinea_e = getSomaMultiplosde2Numeros(lim_min, lim_max,numero1, numero2);

                //Saída de dados
        System.out.print("alinea a: "+ resultado_alinea_a +" ");
        System.out.print("alinea b: "+ resultado_alinea_b +" ");
        System.out.print("alinea c: "+ resultado_alinea_c +" ");
        System.out.print("alinea d: "+ resultado_alinea_d +" ");
        System.out.print("alinea e: "+ resultado_alinea_e +" ");
    }

    public static int getNumeroMultiplosdeUmNumero(int lim_min, int lim_max, int numero) {   //Em vez de utilizar este código poderia utilizar o valor 1 num dos numeros inseridos
        double resto;
        int contaMultiplo = 0;
        if (lim_min>=0 && numero >0 ){
             for (int i = lim_min; i <= lim_max; i++) {
            resto =getRestodaDivisão(i, numero);
                if (resto == 0) {
                contaMultiplo += 1;
                }
                else { }
            }
        }
        else {
            contaMultiplo = -1;
        }
        return contaMultiplo;
    }

    public static int getNumeroMultiplosde2Numeros(int lim_min, int lim_max, int numero1, int numero2) {
        double resto1, resto2;
        int contaMultiplo = 0;
        if (lim_min>=0 && numero1 >0 && numero2>0){
            for (int i = lim_min; i <= lim_max; i++) {
                resto1 = getRestodaDivisão(i, numero1);
                resto2 = getRestodaDivisão(i, numero2);
                if (resto1 == 0 && resto2==0) {
                    contaMultiplo += 1;
                }
                else { }
            }
        }
        else {
            contaMultiplo = -1;
        }
        return contaMultiplo;
    }

    public static double getSomaMultiplosde2Numeros (int lim_min, int lim_max, int numero1, int numero2){
        double resto1, resto2;
        double somaMultiplos = 0;
        if (lim_min>=0 && numero1 >0 && numero2>0){
            for (int i = lim_min; i <= lim_max; i++) {
                resto1 = getRestodaDivisão(i, numero1);
                resto2 = getRestodaDivisão(i, numero2);
                if (resto1 == 0 && resto2 ==0) {
                    somaMultiplos += i;
                }
                else { }
            }
        }
        else {
            somaMultiplos = -1;
        }
        return somaMultiplos;

    }

    public static double getRestodaDivisão (int numero, int divisor){
    double resto;
    resto = numero % divisor;
    return resto;
    }
}
