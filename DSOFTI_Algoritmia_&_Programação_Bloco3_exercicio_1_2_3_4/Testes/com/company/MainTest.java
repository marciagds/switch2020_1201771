package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    //Testes Exercicio1
    @org.junit.jupiter.api.Test
    void getFatorialDoNumero_teste1() {
        //arrange
        int number =4;
        int expected = 24;
        //act
        int result = Main.getFatorialDoNumero(number);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getFatorialDoNumero_teste2() {
        //arrange
        int number =0;
        int expected = 1;
        //act
        int result = Main.getFatorialDoNumero(number);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getFatorialDoNumero_teste3() {
        //arrange
        int number =-3;
        int expected = -1;
        //act
        int result = Main.getFatorialDoNumero(number);

        //assert
        assertEquals(expected, result);
    }

    //Testes Exercicio4
    @org.junit.jupiter.api.Test
    void getNumeroMultiplosde1Algarismo_teste1() {
        //arrange
        int lim_min = 0;
        int lim_max = 21;
        int numero = 2;
        int expected =11;

        //act
        int resultado = Main.getNumeroMultiplosdeUmNumero(lim_min,lim_max,numero);

        //assert
        assertEquals(expected, resultado);
    }

    @org.junit.jupiter.api.Test
    void getNumeroMultiplosde1Algarismo_teste2() {
        //arrange
        int lim_min = 0;
        int lim_max = 12;
        int numero = -2;
        int expected =-1;

        //act
        int resultado = Main.getNumeroMultiplosdeUmNumero(lim_min,lim_max,numero);

        //assert
        assertEquals(expected, resultado);
    }

    @org.junit.jupiter.api.Test
    void getNumeroMultiplosde1Algarismo_teste3() {
        //arrange
        int lim_min = -2;
        int lim_max = 12;
        int numero = 3;
        int expected =-1;

        //act
        int resultado = Main.getNumeroMultiplosdeUmNumero(lim_min,lim_max,numero);

        //assert
        assertEquals(expected, resultado);
    }

    @org.junit.jupiter.api.Test
    void getNumeroMultiplosde1Algarismo_teste4() {
        //arrange
        int lim_min = 12;
        int lim_max = 10;
        int numero = 3;
        int expected =0;        //Errp no sistema, pois lim_max<lim_min.

        //act
        int resultado = Main.getNumeroMultiplosdeUmNumero(lim_min,lim_max,numero);

        //assert
        assertEquals(expected, resultado);
    }

    @org.junit.jupiter.api.Test
    void getNumeroMultiplosde1Algarismo_teste5() {
        //arrange
        int lim_min = 20;
        int lim_max = 100;
        int numero = 12;
        int expected =7;        //Erro no sistema, pois lim_max<lim_min.

        //act
        int resultado = Main.getNumeroMultiplosdeUmNumero(lim_min,lim_max,numero);

        //assert
        assertEquals(expected, resultado);
    }

    @Test
    void getNumeroMultiplosde2Numeros_teste1() {
        //arrange
        int lim_min = 10;
        int lim_max = 60;
        int numero1 = 12;
        int numero2 = 8;
        int expected =2;

        //act
        int resultado = Main.getNumeroMultiplosde2Numeros(lim_min, lim_max, numero1, numero2);

        // assert
        assertEquals(expected, resultado);
    }

    @Test
    void getNumeroMultiplosde2Numeros_teste2() {
        //arrange
        int lim_min = 10;
        int lim_max = 60;
        int numero1 = 12;
        int numero2 = 13;
        int expected =0;

        //act
        int resultado = Main.getNumeroMultiplosde2Numeros(lim_min, lim_max, numero1, numero2);

        // assert
        assertEquals(expected, resultado);
    }

    @Test
    void getNumeroMultiplosde2Numeros_teste3() {
        //arrange
        int lim_min = 10;
        int lim_max = 60;
        int numero1 = -12;
        int numero2 = 13;
        int expected =-1;

        //act
        int resultado = Main.getNumeroMultiplosde2Numeros(lim_min, lim_max, numero1, numero2);

        // assert
        assertEquals(expected, resultado);
    }

    @Test
    void getSomaMultiplosde2Numeros_teste1() {
        //arrange
        int lim_min = 10;
        int lim_max = 70;
        int numero1 = 12;
        int numero2 = 5;
        double expected = 60;

        //act
        double resultado = Main.getSomaMultiplosde2Numeros(lim_min, lim_max, numero1, numero2);

        // assert
        assertEquals(expected, resultado);
    }

    @Test
    void getSomaMultiplosde2Numeros_teste2() {
        //arrange
        int lim_min = 10;
        int lim_max = 70;
        int numero1 = 12;
        int numero2 = 5;
        double expected = 60;

        //act
        double resultado = Main.getSomaMultiplosde2Numeros(lim_min, lim_max, numero1, numero2);

        // assert
        assertEquals(expected, resultado);
    }

    @Test
    void getSomaMultiplosde2Numeros_teste3() {
        //arrange
        int lim_min = 10;
        int lim_max = 70;
        int numero1 = -12;
        int numero2 = 5;
        double expected = -1;

        //act
        double resultado = Main.getSomaMultiplosde2Numeros(lim_min, lim_max, numero1, numero2);

        // assert
        assertEquals(expected, resultado);
    }

    @Test
    void getSomaMultiplosde2Numeros_teste4() {
        //arrange
        int lim_min = 10;
        int lim_max = 3;
        int numero1 = 12;
        int numero2 = 5;
        double expected = 0;

        //act
        double resultado = Main.getSomaMultiplosde2Numeros(lim_min, lim_max, numero1, numero2);

        // assert
        assertEquals(expected, resultado);
    }

}