package com.company;

import com.sun.xml.internal.fastinfoset.util.CharArrayArray;
import sun.plugin.net.protocol.jar.CachedJarURLConnection;

import java.awt.*;

public class Main {

    public static void main(String[] args) {
        // write your code here
        //exercicio5();
        //exercicio6();
        exercicio7();
    }

    public static void exercicio5() {
        System.out.print("alinea a: " + getSomaNumerosPares(2, 12) + "\n");
        System.out.print("alinea b: " + getQtdNumerosPares(2, 12) + "\n");
        System.out.print("alinea c: " + getSomaNumerosImpares(2, 12) + "\n");
        System.out.print("alinea d: " + getQtdNumerosImpares(2, 12) + "\n");
        System.out.print("alinea e: " + getSomaMultiplosdeumNumero_independentementedosLimites(9, 2, 2) + "\n");
        System.out.print("alinea f: " + getProdutoNumerosMultiplosdeUmNumero(3, 20, 9) + "\n");
        System.out.print("alinea g: " + getMediaMultiplosdeumNUmero(2, 12, 2) + "\n");
        System.out.print("alinea h: " + getMediaNumerosMultiplosde1ou2Numeros(2, 14, 2, 5) + "\n");
    }

    public static double getSomaNumerosPares(int lim_min, int lim_max) {
        double resto;
        double somaPares = 0;
        for (int i = lim_min; i <= lim_max; i++) {
            resto = getRestoDivisao(i, 2);
            if (resto == 0) {
                somaPares += i;
            } else {
            }
        }
        return somaPares;
    }

    public static int getQtdNumerosPares(int lim_min, int lim_max) {
        double resto;
        int contaPares = 0;
        for (int i = lim_min; i <= lim_max; i++) {
            resto = getRestoDivisao(i, 2);
            if (resto == 0) {
                contaPares += 1;
            } else {
            }
        }
        return contaPares;
    }

    public static double getSomaNumerosImpares(int lim_min, int lim_max) {
        double resto;
        double somaImpares = 0;
        for (int i = lim_min; i <= lim_max; i++) {
            resto = getRestoDivisao(i, 2);
            if (resto == 0) {
            } else {
                somaImpares += i;
            }
        }
        return somaImpares;
    }

    public static int getQtdNumerosImpares(int lim_min, int lim_max) {
        double resto;
        int contaImpares = 0;
        for (int i = lim_min; i <= lim_max; i++) {
            resto = getRestoDivisao(i, 2);
            if (resto == 0) {
            } else {
                contaImpares += 1;
            }
        }
        return contaImpares;
    }

    public static double getSomaNumerosMultiplosdeUmNumero(int lim_min, int lim_max, int multiplo) {
        double resto;
        double somaMultiplos = 0;
        if (lim_min >= 0) {
            for (int i = lim_min; i <= lim_max; i++) {
                resto = getRestoDivisao(i, multiplo);
                if (resto == 0) {
                    somaMultiplos += i;
                } else {
                }
            }
            return somaMultiplos;
        } else {
            double resultado = -1;
            return resultado;
        }
    }

    public static double getSomaMultiplosdeumNumero_independentementedosLimites(int number1, int number2, int multiplo) {
        double resultado;
        int lim_min, lim_max;
        if (number1 < number2) {
            resultado = getSomaNumerosMultiplosdeUmNumero(number1, number2, multiplo);
        } else {
            resultado = getSomaNumerosMultiplosdeUmNumero(number2, number1, multiplo);
        }
        return resultado;
    }

    public static double getProdutoNumerosMultiplosdeUmNumero(int lim_min, int lim_max, int multiplo) {
        double resto;
        double productMultiplos = 1;
        if (lim_min >= 0) {
            for (int i = lim_min; i <= lim_max; i++) {
                resto = getRestoDivisao(i, multiplo);
                if (resto == 0) {
                    productMultiplos =productMultiplos * i;
                } else {
                }
            }
            return productMultiplos;
        } else {
            double resultado = -1;
            return resultado;
        }
    }

    public static int getQtdumerosMultiplosdeUmNumero(int lim_min, int lim_max, int multiplo) {
        double resto;
        int contaMultiplos = 0;
        if (lim_min >= 0) {
            for (int i = lim_min; i <= lim_max; i++) {
                resto = getRestoDivisao(i, multiplo);
                if (resto == 0) {
                    contaMultiplos += 1;
                } else {
                }
            }
            return contaMultiplos;
        } else {
            int resultado = -1;
            return resultado;
        }
    }

    public static double getMediaMultiplosdeumNUmero(int lim_min, int lim_max, int multiplo) {
        double somaMultiplos, mediaMultiplos;
        int contaMultiplos;
        if (lim_min >= 0 && lim_min < lim_max) {      //Acrescentei a condição lim_min<lim_max para não ter 0/0 (indeterminação)
            somaMultiplos = getSomaNumerosMultiplosdeUmNumero(lim_min, lim_max, multiplo);
            contaMultiplos = getQtdumerosMultiplosdeUmNumero(lim_min, lim_max, multiplo);
            mediaMultiplos = somaMultiplos / contaMultiplos;
            return mediaMultiplos;
        } else {
            double resultado = -1;
            return resultado;
        }

    }

    public static double getMediaNumerosMultiplosde1ou2Numeros(int lim_min, int lim_max, int multiplo1, int multiplo2) {
        double resto1, resto2, mediaMultiplos;
        double somaMultiplos = 0;
        double somaMultiplosDuplicados = 0;
        int contaMultiplos = 0;
        int contaMultiplosDuplicados = 0;

        if (lim_min > 0 && lim_min < lim_max) {
            for (int i = lim_min; i <= lim_max; i++) {
                resto1 = getRestoDivisao(i, multiplo1);
                resto2 = getRestoDivisao(i, multiplo2);
                if (resto1 == 0 || resto2 == 0) {
                    somaMultiplos += i;
                    contaMultiplos += 1;
                } else {
                    if (resto1 == 0 && resto2 == 0) {
                        somaMultiplosDuplicados += i;
                        contaMultiplos += 1;
                    } else {
                    }
                }
            }
            mediaMultiplos = (somaMultiplos - somaMultiplosDuplicados) / (contaMultiplos - contaMultiplosDuplicados);
            return mediaMultiplos;
        } else {
            double resultado = -1;
            return resultado;
        }
    }

    public static double getRestoDivisao(int numero, int divisor) {
        double resto;
        resto = numero % divisor;
        return resto;
    }


    public static void exercicio6() {
        System.out.print("alinea a: " + getNumeroAlgarismosdeumNumero(3362538) + "\n");
        System.out.print("alinea b: " + getQtdAlgarismosParesdeumNumero(33625383) + "\n");
        System.out.print("alinea c: " + getQtdAlgarismosImparesdeumNumero(134675834) + "\n");
        System.out.print("alinea d: " + getSomaDosAlgarismosdeUmNumero(2345823) + "\n");
        System.out.print("alinea e: " + getSomaDosAlgarismosParesdeUmNumero(7563956) + "\n");
        System.out.print("alinea f: " + getSomaDosAlgarismosImparesdeUmNumero(3487659) + "\n");
        System.out.print("alinea g: " + getMediaDosAlgarismosdeUmNumero(36582996) + "\n");
        System.out.print("alinea h: " + getMediaDosAlgarismosParesdeUmNumero(5639674) + "\n");
        System.out.print("alinea i: " + getMediaDosAlgarismosImparesdeUmNumero(5639674) + "\n");
        System.out.print("alinea j: " + getNumeroEspelhado(5639674) + "\n");
    }

    public static int getNumeroAlgarismosdeumNumero(int numero_longo) {
        String numero = Integer.toString(numero_longo);
        int contaDigitos = 0;
        char algarismos;
        for (int i = 0; i < numero.length(); i++) {
            algarismos = numero.charAt(i);
            if (Character.isDigit(algarismos)) {
                contaDigitos += 1;
            }
        }
        return contaDigitos;
    }

    public static int getQtdAlgarismosParesdeumNumero(int numero_longo) {
        int contaDigitosPares = 0;
        String numero = Integer.toString(numero_longo);
        double resto;
        char algarismo;

        for (int i = 0; i < numero.length(); i++) {
            algarismo = numero.charAt(i);
            if (Character.isDigit(algarismo)) {
                resto = getRestoDivisao(algarismo, 2);
                if (resto == 0) {
                    contaDigitosPares += 1;
                }
            } else {
            }
        }
        return contaDigitosPares;
    }

    public static int getQtdAlgarismosImparesdeumNumero(int numero_longo) {
        int contaDigitosImpares = 0;
        String numero = Integer.toString(numero_longo);
        char algarismo;
        double resto;

        for (int i = 0; i < numero.length(); i++) {
            algarismo = numero.charAt(i);
            if (Character.isDigit(algarismo)) {
                resto = getRestoDivisao(algarismo, 2);
                if (resto == 0) {
                } else {
                    contaDigitosImpares += 1;
                }
            }
        }
        return contaDigitosImpares;
    }

    public static double getSomaDosAlgarismosdeUmNumero(int numero_longo) {
        double somaDigitos = 0;

        while (numero_longo > 0) {
            somaDigitos = somaDigitos + (numero_longo % 10);        //vai buscar o último algarismo
            numero_longo = numero_longo / 10;                       //vai tirar o ultimo valor das unidades
        }
        return somaDigitos;
    }

    public static double getSomaDosAlgarismosParesdeUmNumero(int numero_longo) {
        double somaDigitos = 0;
        double resto;

        while (numero_longo > 0) {
            resto = getRestoDivisao(numero_longo, 2);
            if (resto == 0) {
                somaDigitos = somaDigitos + (numero_longo % 10);        //vai buscar o último algarismo
            }
            numero_longo = numero_longo / 10;       //vai tirar o ultimo valor das unidades
        }
        return somaDigitos;
    }

    public static double getSomaDosAlgarismosImparesdeUmNumero(int numero_longo) {
        double somaDigitos = 0;
        double resto;

        while (numero_longo > 0) {
            resto = getRestoDivisao(numero_longo, 2);
            if (resto == 0) {
            } else {
                somaDigitos = somaDigitos + (numero_longo % 10);        //vai buscar o último algarismo
            }
            numero_longo = numero_longo / 10;                           //vai tirar o ultimo valor das unidades
        }
        return somaDigitos;
    }

    public static double getMediaDosAlgarismosdeUmNumero(int numero_longo) {
        double mediaDigitos;
        double somaDigitos;
        int qtdDigitos;

        somaDigitos = getSomaDosAlgarismosdeUmNumero(numero_longo);
        qtdDigitos = getNumeroAlgarismosdeumNumero(numero_longo);

        mediaDigitos = somaDigitos / qtdDigitos;
        return mediaDigitos;
    }

    public static double getMediaDosAlgarismosParesdeUmNumero(int numero_longo) {
        double mediaDigitosPares;
        double somaDigitosPares;
        int qtdDigitosPares;

        somaDigitosPares = getSomaDosAlgarismosParesdeUmNumero(numero_longo);
        qtdDigitosPares = getQtdAlgarismosParesdeumNumero(numero_longo);

        if (somaDigitosPares == 0 || qtdDigitosPares == 0) {
            return -1;
        } else {
            mediaDigitosPares = somaDigitosPares / qtdDigitosPares;
            return mediaDigitosPares;
        }
    }

    public static double getMediaDosAlgarismosImparesdeUmNumero(int numero_longo) {
        double mediaDigitosImpares;
        double somaDigitosImpares;
        int qtdDigitosImpares;

        somaDigitosImpares = getSomaDosAlgarismosImparesdeUmNumero(numero_longo);
        qtdDigitosImpares = getQtdAlgarismosImparesdeumNumero(numero_longo);

        if (somaDigitosImpares == 0 || qtdDigitosImpares == 0) {
            return -1;
        } else {
            mediaDigitosImpares = somaDigitosImpares / qtdDigitosImpares;
            return mediaDigitosImpares;
        }
    }

    public static double getNumeroEspelhado(int numero) {
        int unidades;
        double resultado;
        resultado = 0;
        while (numero > 0) {
            unidades = numero % 10;
            resultado = resultado * 10 + unidades;
            numero = numero / 10;
        }
        return resultado;
    }


    public static void exercicio7() {
        //alinea a:
        boolean capicua = getVerificarSeNumeroCapicua(29592);
        if (capicua = true) {
            System.out.print("alinea a: O Número é Capicua! " + "\n");
        } else {
            System.out.print("alinea a: O Número não é Capicua! " + "\n");
        }

        //alinea b.
        boolean numeroArmstrong = getVerificarseNumeroAmstrong(1634);
        if (numeroArmstrong = true) {
            System.out.print("alinea a: O Número é um número de Armstrong! " + "\n");
        } else {
            System.out.print("alinea a: O Número não é um número de Armstrong! " + "\n");
        }

        System.out.print("alinea c: " + getPrimeiraCapicuaNumDadoIntervalo(0, 30) + "\n");
        System.out.print("alinea d: " + getMaiorCapicuaNumDadoIntervalo(12, 400) + "\n");
        System.out.print("alinea e: " + getContagemCapicuasNumdadoIntervalo(12,40) + "\n");
        System.out.print("alinea f: " + getPrimeiroNumeroAmstrongNumDadoIntervalo(13, 200)+ "\n");
        System.out.print("alinea g: " + getContagemNumerosAmstrongNumdadoIntervalo(100, 500)+ "\n");

    }

    public static boolean getVerificarSeNumeroCapicua(int numero) {
        boolean capicua = false;
        int numero_inverso = (int) getNumeroEspelhado(numero);
        if (numero == numero_inverso) {
            capicua = true;
        } else {
            capicua = false;
        }
        return capicua;
    }

    public static int getPrimeiraCapicuaNumDadoIntervalo(int lim_min, int lim_max) {
        int numero_inverso;
        int resultado;
        if (lim_min > 10) {
            int i = lim_min;
            do {
                numero_inverso = (int) getNumeroEspelhado(i);
                if (i == numero_inverso) {
                    resultado = i;
                } else {
                    resultado = 0;
                }
                i++;
            } while (resultado <= 0 && i <= lim_max);
            return resultado;
        } else {
            resultado = -2;
        }
        return resultado;
    }

    public static int getMaiorCapicuaNumDadoIntervalo(int lim_min, int lim_max) {
        int numero_inverso;
        int resultado = 0;
        if (lim_min > 10) {
            int i = lim_min;
            while (i <= lim_max) {
                numero_inverso = (int) getNumeroEspelhado(i);
                if (i == numero_inverso) {
                    resultado = i;
                } else {
                }
                i++;
            }
            return resultado;
        } else {
            resultado = -2;
        }
        return resultado;
    }

    public static boolean getVerificarseNumeroAmstrong(int numero_inicial) {
 /* Definição: O conceito da Álgebra para um número de Armstrong diz que:
é um número de n dígitos que é igual a soma de cada um dos seus dígitos elevado a n-ésima potência .
Por exemplo, 153 (n = três dígitos) é igual a 1^3 + 5^3 + 3^3 = 1 + 125 + 27 = 153      */

        boolean numeroArmstrong = false;
        int qtdALgarismos = getNumeroAlgarismosdeumNumero(numero_inicial);      //indica-me a potência n.
        int unidades;
        double resultado = 0;
        int numero = numero_inicial;
        while (numero > 0) {                  //decompozição do numero, removendo o algarismo das unidades.
            unidades = numero % 10;
            resultado = resultado + Math.pow(unidades, qtdALgarismos);
            numero = numero / 10;
        }
        if (resultado == numero_inicial) {
            numeroArmstrong = true;
        } else {
            numeroArmstrong = false;
        }
        return numeroArmstrong;
    }

    public static int getContagemCapicuasNumdadoIntervalo(int lim_min, int lim_max) {
        boolean capicua;
        int contaCapicuas = 0;

        for (int i = lim_min; i <= lim_max; i++) {
            capicua = getVerificarSeNumeroCapicua(i);
            if (capicua == true) {
                contaCapicuas += 1;
            }
            else {}
        }
        return contaCapicuas;
    }

    public static int getPrimeiroNumeroAmstrongNumDadoIntervalo(int lim_min, int lim_max) {
        boolean numeroAmstrong;
        numeroAmstrong = false;
        int i = lim_min;
        int resultado=0;
        if (lim_min >= 0) {
            do {
                numeroAmstrong = getVerificarseNumeroAmstrong(i);
                if(numeroAmstrong==true){
                    resultado = i;
                }
/*o else aqui é indiferente colocar porque lá em cima diz resultado=0. Se nada
acontecer vai sempre retornar valor nulo. (Se não colocar o valor resultado=0 lá
em cima vai dar ciclo infinito, pois não existe nenhum valor para retornar. */
                i++;
            } while (resultado<=0 && i <= lim_max);
        }
        else {
            resultado = -1;
        }
        return resultado;
    }

    public static int getContagemNumerosAmstrongNumdadoIntervalo(int lim_min, int lim_max) {
        boolean numeroAmostrong;
        int contaArmstrongNumbers = 0;
        for (int i = lim_min; i <= lim_max; i++) {
            numeroAmostrong= getVerificarseNumeroAmstrong(i);
            if (numeroAmostrong == true) {
                contaArmstrongNumbers += 1;
            }
            else {}
        }
        return contaArmstrongNumbers;
    }

}