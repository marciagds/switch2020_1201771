package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    //Testes exercicio5
    @Test
    void getSomaNumerosPares_valores_positivos() {
        //arrange
        int lim_min = 12;
        int lim_max = 23;
        double expected = 102;
        //act
        double result = Main.getSomaNumerosPares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaNumerosPares_teste_limites_errados() {
        //arrange
        int lim_min = 23;
        int lim_max = 12;
        double expected = 0;
        //act
        double result = Main.getSomaNumerosPares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaNumerosPares_teste_valores_negativos() {
        //arrange
        int lim_min = -2;
        int lim_max = 10;
        double expected = 28;
        //act
        double result = Main.getSomaNumerosPares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getQtdNumerosPares_teste_valores_negativos() {
        //arrange
        int lim_min = -4;
        int lim_max = 10;
        double expected = 8;
        //act
        double result = Main.getQtdNumerosPares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getQtdNumerosPares_teste_valores_positivos() {
        //arrange
        int lim_min = 12;
        int lim_max = 33;
        double expected = 11;
        //act
        double result = Main.getQtdNumerosPares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getQtdNumerosPares_teste_valores_limites_errados() {
        //arrange
        int lim_min = 12;
        int lim_max = -3;
        double expected = 0;
        //act
        double result = Main.getQtdNumerosPares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getSomaNumerosImpares_valores_positivos() {
        //arrange
        int lim_min = 12;
        int lim_max = 23;
        double expected = 108;
        //act
        double result = Main.getSomaNumerosImpares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaNumerosImpares_teste_limites_errados() {
        //arrange
        int lim_min = 23;
        int lim_max = 12;
        double expected = 0;
        //act
        double result = Main.getSomaNumerosImpares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaNumerosImpares_teste_valores_negativos() {
        //arrange
        int lim_min = -2;
        int lim_max = 10;
        double expected = 24;
        //act
        double result = Main.getSomaNumerosImpares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getQtdNumerosImpares_teste_valores_negativos() {
        //arrange
        int lim_min = -4;
        int lim_max = 10;
        double expected = 7;
        //act
        double result = Main.getQtdNumerosImpares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getQtdNumerosImpares_teste_valores_positivos() {
        //arrange
        int lim_min = 12;
        int lim_max = 33;
        double expected = 11;
        //act
        double result = Main.getQtdNumerosImpares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getQtdNumerosImpares_teste_valores_limites_errados() {
        //arrange
        int lim_min = 12;
        int lim_max = -3;
        double expected = 0;
        //act
        double result = Main.getQtdNumerosPares(lim_min, lim_max);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getSomaNumerosMultiplosdeUmNumero_teste_valores_negativos() {
        //arrange
        int lim_min = -4;
        int lim_max = 10;
        int multiplo = 3;
        double expected = -1;
        //act
        double result = Main.getSomaNumerosMultiplosdeUmNumero(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaNumerosMultiplosdeUmNumero_teste_valores_positivos() {
        //arrange
        int lim_min = 0;
        int lim_max = 33;
        int multiplo = 3;
        double expected = 198;
        //act
        double result = Main.getSomaNumerosMultiplosdeUmNumero(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaNumerosMultiplosdeUmNumero_teste_valores_limites_errados() {
        //arrange
        int lim_min = 12;
        int lim_max = 0;
        int multiplo = 3;
        double expected = 0;
        //act
        double result = Main.getSomaNumerosMultiplosdeUmNumero(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getSomaMultiplosdeumNumero_independentementedosLimites_teste_valores_negativos() {
        //arrange
        int lim_min = -4;
        int lim_max = 10;
        int multiplo = 3;
        double expected = -1;
        //act
        double result = Main.getSomaMultiplosdeumNumero_independentementedosLimites(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaMultiplosdeumNumero_independentementedosLimites_teste_valores_positivos() {
        //arrange
        int lim_min = 0;
        int lim_max = 33;
        int multiplo = 3;
        double expected = 198;
        //act
        double result = Main.getSomaMultiplosdeumNumero_independentementedosLimites(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaMultiplosdeumNumero_independentementedosLimites_teste_valores_limites_trocados() {
        //arrange
        int lim_min = 12;
        int lim_max = 0;
        int multiplo = 3;
        double expected = 30;
        //act
        double result = Main.getSomaMultiplosdeumNumero_independentementedosLimites(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaMultiplosdeumNumero_independentementedosLimites_teste_valores_limites_iguais() {
        //arrange
        int lim_min = 0;
        int lim_max = 0;
        int multiplo = 3;
        double expected = 0;
        //act
        double result = Main.getSomaMultiplosdeumNumero_independentementedosLimites(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getProdutoNumerosMultiplosdeUmNumero_teste_valores_negativos() {
        //arrange
        int lim_min = -4;
        int lim_max = 10;
        int multiplo = 3;
        double expected = -1;
        //act
        double result = Main.getProdutoNumerosMultiplosdeUmNumero(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getProdutoNumerosMultiplosdeUmNumero_teste_valores_positivos() {
        //arrange
        int lim_min = 2;
        int lim_max = 12;
        int multiplo = 3;
        double expected = 1944;
        //act
        double result = Main.getProdutoNumerosMultiplosdeUmNumero(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getProdutoumerosMultiplosdeUmNumero_teste_valores_limites_errados() {
        //arrange
        int lim_min = 12;
        int lim_max = 0;
        int multiplo = 3;
        double expected = 1.0;
        //act
        double result = Main.getProdutoNumerosMultiplosdeUmNumero(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getMediaMultiplosdeumNumero_teste_valores_negativos() {
        //arrange
        int lim_min = -4;
        int lim_max = 10;
        int multiplo = 3;
        double expected = -1;
        //act
        double result = Main.getMediaMultiplosdeumNUmero(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getMediaMultiplosdeumNumero_teste_valores_positivos() {
        //arrange
        int lim_min = 2;
        int lim_max = 12;
        int multiplo = 3;
        double expected = 7.5;
        //act
        double result = Main.getMediaMultiplosdeumNUmero(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getMediaMultiplosdeumNumero_teste_valores_limites_errados() {
        //arrange
        int lim_min = 12;
        int lim_max = 0;
        int multiplo = 3;
        double expected = -1;
        //act
        double result = Main.getMediaMultiplosdeumNUmero(lim_min, lim_max, multiplo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getMediaNumerosMultiplosde1ou2Numeros_teste_valores_negativos() {
        //arrange
        int lim_min = -4;
        int lim_max = 10;
        int multiplo1 = 3;
        int multiplo2 =5;
        double expected = -1;
        //act
        double result = Main.getMediaNumerosMultiplosde1ou2Numeros(lim_min, lim_max, multiplo1, multiplo2);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getMediaNumerosMultiplosde1ou2Numeros_teste_valores_positivos() {
        //arrange
        int lim_min = 1;
        int lim_max = 10;
        int multiplo1 = 2;
        int multiplo2 = 5;
        double expected =5.83 ;
        //act
        double result = Main.getMediaNumerosMultiplosde1ou2Numeros(lim_min, lim_max, multiplo1, multiplo2);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getMediaNumerosMultiplosde1ou2Numeros_teste_valores_limites_errados() {
        //arrange
        int lim_min = 12;
        int lim_max = 0;
        int multiplo1 = 3;
        int multiplo2 = 5;
        double expected = -1;
        //act
        double result = Main.getMediaNumerosMultiplosde1ou2Numeros(lim_min, lim_max, multiplo1, multiplo2);

        //assert
        assertEquals(expected, result);
    }


    //Testes exercicio6
    @Test
    void getNumeroAlgarismosdeumNumero_teste_valores_numéricos() {
        //arrange
        int numero_longo = 1342864;
        int expected = 7 ;
        //act
        int result = Main.getNumeroAlgarismosdeumNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getNumeroAlgarismosdeumNumero_teste_valores_numéricos_nulo_1() {
        //arrange
        int numero_longo = 13400264;
        int expected = 8 ;
        //act
        int result = Main.getNumeroAlgarismosdeumNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getNumeroAlgarismosdeumNumero_teste_valores_numéricos_negativo() {
        //arrange
        int numero_longo = -13400264;
        int expected = 8 ;
        //act
        int result = Main.getNumeroAlgarismosdeumNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getNumeroAlgarismosdeumNumero_teste_valores_numéricos_nulo_2() {
        //arrange
        int numero_longo = 3400260;
        int expected = 7 ;
        //act
        int result = Main.getNumeroAlgarismosdeumNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getQtdAlgarismosParesdeumNumero_teste_valores_numéricos() {
        //arrange
        int numero_longo = 1342864;
        int expected = 5 ;
        //act
        int result = Main.getQtdAlgarismosParesdeumNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getQtdAlgarismosParesdeumNumero_teste_valores_numéricos_nulo_1() {
        //arrange
        int numero_longo = 13400264;
        int expected = 6 ;
        //act
        int result = Main.getQtdAlgarismosParesdeumNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getQtdAlgarismosParesdeumNumero_teste_valores_numéricos_negativo() {
        //arrange
        int numero_longo = -13400264;
        int expected = 6 ;
        //act
        int result = Main.getQtdAlgarismosParesdeumNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getQtdAlgarismosImparesdeumNumero_teste_valores_numéricos() {
        //arrange
        int numero_longo = 1342864;
        int expected = 2 ;
        //act
        int result = Main.getQtdAlgarismosImparesdeumNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getQtdAlgarismosImparesdeumNumero_teste_valores_numéricos_nulo_1() {
        //arrange
        int numero_longo = 13400264;
        int expected = 2 ;
        //act
        int result = Main.getQtdAlgarismosImparesdeumNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getQtdAlgarismosImparesdeumNumero_teste_valores_numéricos_negativo() {
        //arrange
        int numero_longo = -13400264;
        int expected = 2 ;
        //act
        int result = Main.getQtdAlgarismosImparesdeumNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getSomaDosAlgarismosdeUmNumero_teste_valores_numéricos() {
        //arrange
        int numero_longo = 345;
        double expected = 12 ;
        //act
        double result = Main.getSomaDosAlgarismosdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaDosAlgarismosdeUmNumero_teste_valores_numéricos_nulo_1() {
        //arrange
        int numero_longo = 13400264;
        double expected = 20 ;
        //act
        double result = Main.getSomaDosAlgarismosdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaDosAlgarismosdeUmNumero_teste_valores_numéricos_negativo() {
        //arrange
        int numero_longo = -13400264;
        double expected = 0 ;
        //act
        double result = Main.getSomaDosAlgarismosdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getSomaDosAlgarismosParesdeUmNumero_teste_valores_numéricos() {
        //arrange
        int numero_longo = 3456;
        double expected = 10 ;
        //act
        double result = Main.getSomaDosAlgarismosParesdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaDosAlgarismosParesdeUmNumero_teste_valores_numéricos_nulo_1() {
        //arrange
        int numero_longo = 13400264;
        double expected = 16 ;
        //act
        double result = Main.getSomaDosAlgarismosParesdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaDosAlgarismosParesdeUmNumero_teste_valores_numéricos_nulo_2() {
        //arrange
        int numero_longo = -13400264;
        double expected = 0 ;
        //act
        double result = Main.getSomaDosAlgarismosdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getSomaDosAlgarismosImparesdeUmNumero_teste_valores_numéricos() {
        //arrange
        int numero_longo = 3456;
        double expected = 8 ;
        //act
        double result = Main.getSomaDosAlgarismosImparesdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaDosAlgarismosImparesdeUmNumero_teste_valores_numéricos_nulo_1() {
        //arrange
        int numero_longo = 22400264;
        double expected = 0 ;
        //act
        double result = Main.getSomaDosAlgarismosImparesdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getSomaDosAlgarismosImparesdeUmNumero_teste_valores_numéricos_nulo_2() {
        //arrange
        int numero_longo = -13400264;
        double expected = 0 ;
        //act
        double result = Main.getSomaDosAlgarismosdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getMediaDosAlgarismosdeUmNumero_teste_valores_numéricos() {
        //arrange
        int numero_longo = 3456;
        double expected = 4.5 ;
        //act
        double result = Main.getMediaDosAlgarismosdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMediaDosAlgarismosdeUmNumero_teste_valores_numéricos_nulo_1() {
        //arrange
        int numero_longo = 22400264;
        double expected = 2.5;
        //act
        double result = Main.getMediaDosAlgarismosdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMediaDosAlgarismosdeUmNumero_teste_valores_numéricos_nulo_2() {
        //arrange
        int numero_longo = -13400264;
        double expected = 0 ;
        //act
        double result = Main.getSomaDosAlgarismosdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getMediaDosAlgarismosParesdeUmNumero_teste_valores_numéricos() {
        //arrange
        int numero_longo = 3456;
        double expected = 5 ;
        //act
        double result = Main.getMediaDosAlgarismosParesdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMediaDosAlgarismosParesdeUmNumero_teste_valores_numéricos_nulo_1() {
        //arrange
        int numero_longo = 224002641;
        double expected = 2.5;
        //act
        double result = Main.getMediaDosAlgarismosParesdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMediaDosAlgarismosParesdeUmNumero_teste_valores_numéricos_nulo_2() {
        //arrange
        int numero_longo = -13400264;
        double expected = -1 ;
        //act
        double result = Main.getMediaDosAlgarismosParesdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getMediaDosAlgarismosImparesdeUmNumero_teste_valores_numéricos() {
        //arrange
        int numero_longo = 3456;
        double expected = 4 ;
        //act
        double result = Main.getMediaDosAlgarismosImparesdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMediaDosAlgarismosImparesdeUmNumero_teste_valores_numéricos_nulo_1() {
        //arrange
        int numero_longo = 22400264;
        double expected =-1;
        //act
        double result = Main.getMediaDosAlgarismosImparesdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMediaDosAlgarismosImparesdeUmNumero_teste_valores_numéricos_nulo_2() {
        //arrange
        int numero_longo = -13400264;
        double expected = -1 ;
        //act
        double result = Main.getMediaDosAlgarismosImparesdeUmNumero(numero_longo);

        //assert
        assertEquals(expected, result);
    }

    @Test
    void getNumeroEspelhado_teste_valores_numéricos() {
        //arrange
        int numero = 3456;
        double expected = 6543 ;
        //act
        double result = Main.getNumeroEspelhado(numero);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getNumeroEspelhado_teste_valores_numéricos_nulo_1() {
        //arrange
        int numero = 22400264;
        double expected =46200422;
        //act
        double result = Main.getNumeroEspelhado(numero);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getNumeroEspelhado_teste_valores_numéricos_nulo_2() {
        //arrange
        int numero_longo = -13400264;
        double expected = 0 ;
        //act
        double result = Main.getNumeroEspelhado(numero_longo);

        //assert
        assertEquals(expected, result);
    }

    //Testes Exercicio7
    //alinea a.
    @Test
    void getVerificarSeNumeroCapicua_teste_valor_Capicua_1() {
        //arrange
        int numero = 34543;
        boolean expected = true ;
        //act
        boolean result = Main.getVerificarSeNumeroCapicua(numero);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getVerificarSeNumeroCapicua_teste_valor_Capicua_2() {
        //arrange
        int numero = 22400264;
        boolean expected = false;
        //act
        boolean result = Main.getVerificarSeNumeroCapicua(numero);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getVerificarSeNumeroCapicua_teste_valor_não_Capicua() {
        //arrange
        int numero_longo = -13400264;
        double expected = 0 ;
        //act
        double result = Main.getNumeroEspelhado(numero_longo);

        //assert
        assertEquals(expected, result);
    }
    //alinea b.
    @Test
    void getVerificarseNumeroAmstrong_teste_Numero_Amstrong1() {
        //arrange
        int numero_inicial = 153;
        boolean expected = true;
        //act
        boolean result = Main.getVerificarseNumeroAmstrong(numero_inicial);

        //assert
        assertTrue(result);                     //Quando testamos funções booleanas é mais intuitivo fazer este assert,
        //assertEquals(expected, result);       //para quem está a visualizar.
    }
    @Test
    void getVerificarseNumeroAmstrong_teste_Numero_Amstrong2() {
        //arrange
        int numero_inicial = 1634;
        boolean expected = true;
        //act
        boolean result = Main.getVerificarseNumeroAmstrong(numero_inicial);

        //assert
        assertTrue(result);
        //assertEquals(expected, result);
    }
    @Test
    void getVerificarseNumeroAmstrong_teste_Numero_Amstrong3() {
        //arrange
        int numero_inicial = 8;
        boolean expected = true;
        //act
        boolean result = Main.getVerificarseNumeroAmstrong(numero_inicial);

        //assert
        assertTrue(result);
        //assertEquals(expected, result);
    }
    @Test
    void getVerificarseNumeroAmstrong_teste_Não_Numero_Amstrong() {
        //arrange
        int numero_inicial = 1632;
        boolean expected = false;
        //act
        boolean result = Main.getVerificarseNumeroAmstrong(numero_inicial);

        //assert
        assertFalse(result);
        //assertEquals(expected, result);
    }
    //alinea c.
    @Test
    void getPrimeiraCapicuaNumDadoIntervalo_teste_valor_Capicua() {
        //arrange
        int lim_min = 20;
        int lim_max = 40;
        int expected = 22;
        //act
        int result = Main.getPrimeiraCapicuaNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getPrimeiraCapicuaNumDadoIntervalo_teste_valor_Não_Capicua() {
        //arrange
        //arrange
        int lim_min = 12;
        int lim_max = 21;
        int expected = 0;
        //act
        int result = Main.getPrimeiraCapicuaNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getPrimeiraCapicuaNumDadoIntervalo_teste_limites_errados() {
        //arrange
        int lim_min = 122;
        int lim_max = 13;
        int expected = 0;
        //act
        double result = Main.getPrimeiraCapicuaNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    //alinea d.
    @Test
    void getMaiorCapicuaNumDadoIntervalo_teste_valor_Capicua1() {
        //arrange
        int lim_min = 20;
        int lim_max = 40;
        int expected = 33;
        //act
        int result = Main.getMaiorCapicuaNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMaiorCapicuaNumDadoIntervalo_teste_valor_Capicua2() {
        //arrange
        int lim_min = 20;
        int lim_max = 400;
        int expected = 393;
        //act
        int result = Main.getMaiorCapicuaNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMaiorCapicuaNumDadoIntervalo_teste_valor_Não_Capicua() {
        //arrange
        int lim_min = 12;
        int lim_max = 21;
        int expected = 0;
        //act
        int result = Main.getMaiorCapicuaNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMaiorCapicuaNumDadoIntervalo_teste_limites_errados() {
        //arrange
        int lim_min = 122;
        int lim_max = 13;
        int expected = 0;

        //act
        double result = Main.getMaiorCapicuaNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    //alinea e.
    @Test
    void getContagemCapicuasNumdadoIntervalo_teste_valor_Capicua1() {
        //arrange
        int lim_min = 20;
        int lim_max = 40;
        int expected = 2;
        //act
        int result = Main.getContagemCapicuasNumdadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getContagemCapicuasNumdadoIntervalo_teste_valor_Capicua2() {
        //arrange
        int lim_min = 20;
        int lim_max = 150;
        int expected = 13;
        //act
        int result = Main.getContagemCapicuasNumdadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getContagemCapicuasNumdadoIntervalo_teste_valor_Não_Capicua() {
        //arrange
        int lim_min = 12;
        int lim_max = 21;
        int expected = 0;
        //act
        int result = Main.getContagemCapicuasNumdadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getContagemCapicuasNumdadoIntervalo_teste_limites_errados() {
        //arrange
        int lim_min = 122;
        int lim_max = 13;
        int expected = 0;

        //act
        double result = Main.getContagemCapicuasNumdadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    //alinea f.
    @Test
    void getPrimeiroNumeroAmstrongNumDadoIntervalo_teste_1valorDentrodoIntervalo() {
        //arrange
        int lim_min = 150;
        int lim_max = 400;
        int expected = 153;
        //act
        int result = Main.getPrimeiroNumeroAmstrongNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getPrimeiroNumeroAmstrongNumDadoIntervalo_teste_Variosvalores_entrodoIntervalo() {
        //arrange
        int lim_min = 20;
        int lim_max = 400;
        int expected = 153;
        //act
        int result = Main.getPrimeiroNumeroAmstrongNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getPrimeiroNumeroAmstrongNumDadoIntervalo_teste_valor_Não_Capicua() {
        //arrange
        int lim_min = 12;
        int lim_max = 21;
        int expected = 0;
        //act
        int result = Main.getPrimeiroNumeroAmstrongNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getPrimeiroNumeroAmstrongNumDadoIntervalo_teste_limites_errados() {
        //arrange
        int lim_min = 122;
        int lim_max = 13;
        int expected = 0;

        //act
        double result = Main.getPrimeiroNumeroAmstrongNumDadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    //alinea g.
    @Test
    void getContagemNumerosAmstrongNumdadoIntervalo_teste_1valorDentrodoIntervalo() {
        //arrange
        int lim_min = 20;
        int lim_max = 200;
        int expected = 1;
        //act
        int result = Main.getContagemNumerosAmstrongNumdadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getContagemNumerosAmstrongNumdadoIntervalo_teste_3valorDentrodoIntervalo() {
        //arrange
        int lim_min = 20;
        int lim_max = 400;
        int expected = 3;
        //act
        int result = Main.getContagemNumerosAmstrongNumdadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getContagemNumerosAmstrongNumdadoIntervalo_teste_0valorDentrodoIntervalo() {
        //arrange
        int lim_min = 12;
        int lim_max = 21;
        int expected = 0;
        //act
        int result = Main.getContagemNumerosAmstrongNumdadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getContagemNumerosAmstrongNumdadoIntervalo_teste_limites_errados() {
        //arrange
        int lim_min = 122;
        int lim_max = 13;
        int expected = 0;

        //act
        double result = Main.getContagemNumerosAmstrongNumdadoIntervalo(lim_min,lim_max);

        //assert
        assertEquals(expected, result);
    }

}