package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //xercicio7();
        exercicio8();
    }

    public static void exercicio7(){
        // Calcular a distância percorrida pelo Zé.

        //Variáveis envolvidas
        double tempo_manel, tempo_ze,distancia_manel, distancia_ze, veloc_manel, veloc_ze;

        //Inicialização para leitura de dados
        Scanner ler= new Scanner(System.in);

        // Processamento
        distancia_manel = 42195;
        tempo_manel = (4*3600) + (2*60) + 10;
        tempo_ze = (1*3600) + (5*60);
        veloc_manel = getVeloc_manel(tempo_manel, distancia_manel);
        veloc_ze = veloc_manel;
        distancia_ze = (veloc_ze * tempo_ze);

        //return distancia_ze;

        //Saída dos dados
        System.out.println("O Zé percorreu "+ String.format("%.2f",distancia_ze) + "km");
    }

    private static double getVeloc_manel(double time, double distancia) {
        return distancia / time;
    }

    public static void exercicio8(){
        //Calcular a distância entre os dois operários.

        //Variáveis envolvidas
        double operario_a, operario_b, angulo_ab, distancia;

        //Inicialização para leitura de dados
        Scanner ler= new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual a distância do operário a?");
        operario_a=ler.nextDouble();
        System.out.println("Qual a distância do operário b?");
        operario_b = ler.nextDouble();
        System.out.println("Qual o ângulo entre os dois operários?");
        angulo_ab= ler.nextDouble();

        // Processamento
        double angulo_ab_rad = angulo_ab * (Math.PI / 180);
        distancia = Math.sqrt(Math.pow(operario_a,2)+Math.pow(operario_b,2)-2*operario_a*operario_b*Math.cos(angulo_ab_rad));

        //Saída dos dados
        System.out.println("A distância entre os dois operários é de "+ String.format("%.2f",distancia)+" metros.");

    }
}
