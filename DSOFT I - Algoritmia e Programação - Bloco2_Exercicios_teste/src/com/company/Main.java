package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }

    public static double exercicio1(double nota1, double peso1, double nota2, double peso2, double nota3, double peso3){
        //Cálculo da média das notas

        //Processamento
            double media_pesada = (nota1*peso1+nota2*peso2+nota3*peso3)/(peso1+peso2+peso3);
            return media_pesada;

       /*
       //Saída dos resultados
       System.out.println(String.format("%.2f",media_pesada));
       if (media_pesada>=8){
         System.out.println("O aluno apresenta nota mínima exigida");}

      else {
          System.out.println("O aluno não apresenta nota mínima exigida");}
       */
    }

    public static void exercicio2(int numero, int digito1, int digito2, ) {
        //Separação dos algaristmos que constituem um número

        // Variáveis envolvidas
        int numero, digito1, digito2, digito3, par_ou_impar;
        String tipo_numero;

        //Inicialização da leituda
        Scanner ler = new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Escolha um número de 3 algarismos ?");
        numero = ler.nextInt();

        //Processamento
        par_ou_impar = numero % 2;
        if (numero < 100 || numero > 999) {
            System.out.println("O número não tem 3 digitos");
        } else {
            digito3 = numero % 10;
            digito2 = (numero / 10) % 10;
            digito1 = (numero / 100) % 10;

            if(par_ou_impar==0){
                tipo_numero = "PAR";
            }
            else {
                tipo_numero = "IMPAR";
            }

            System.out.println(digito1 + " " + digito2 + " " + digito3 + " ");
            System.out.println("O número que escolheu é "+tipo_numero);
        }
    }
}
