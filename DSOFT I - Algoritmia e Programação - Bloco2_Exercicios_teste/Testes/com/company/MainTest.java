package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void exercicio1_teste1() {
        //arrange
        double nota1 = 11;
        double nota2 = 11;
        double nota3 = 6;
        double peso1 = 3;
        double peso2 = 5;
        double peso3 = 6;
        double expected = 8.857;

        //act
        double result = Main.exercicio1(nota1, peso1, nota2, peso2, nota3, peso3);

        //assert
        assertEquals(expected, result, 0.01);
    }

    @Test
    void exercicio1_teste2() {
        //arrange
        double nota1 = 4;
        double nota2 = 10;
        double nota3 = 17;
        double peso1 = 2;
        double peso2 = 3;
        double peso3 = 9;
        double expected = 13.642;

        //act
        double result = Main.exercicio1(nota1, peso1, nota2, peso2, nota3, peso3);

        //assert
        assertEquals(expected, result, 0.01);
    }
}