package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //exercicio9();
        //exercicio10();
        //exercicio11();
        exercicio12();
        //exercicio13();
    }
    public static void exercicio9(){
        //Calcular perímetro do retângulo

        //Variáveis envolvidas
        double comprimento, largura, perimetro_retangulo;

        //Inicialização para leitura de dados
        Scanner ler= new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual é o comprimento do retângulo, em metros?");
        comprimento= ler.nextDouble();
        System.out.println("Qual é a largura do retângulo, em metros?");
        largura = ler.nextDouble();

        //Processamento
        perimetro_retangulo= getrectangle_perimeter(comprimento, largura);

        //Saída dos dados
        System.out.println("O perimetro do retângulo é "+String.format("%.2f",perimetro_retangulo)+" metros");

    }

    private static double getrectangle_perimeter(double length, double largura) {
        return (2 * length) + (2 * largura);
    }

    public static void exercicio10(){
        // Calcular hipotenusa do triângulo

        //Variáveis envolvidas
        double cateto_a, cateto_b, hipotenusa;

        //Inicialização para leitura de dados
        Scanner ler= new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual é o valor do cateto A ?");
        cateto_a = ler.nextDouble();
        System.out.println("Qual é o valor do cateto B ?");
        cateto_b = ler.nextDouble();

        //Processamento
        hipotenusa = getTeoremaDePitagoras(cateto_a, cateto_b);

        //Saída dos dados
        System.out.println("O valor da hipotenusa é "+String.format("%.2f",hipotenusa));

    }

    private static double getTeoremaDePitagoras(double cateto_a, double cateto_b) {
        return Math.sqrt(Math.pow(cateto_a, 2) + Math.pow(cateto_b, 2));
    }

    public static void exercicio11(){
        // Executar o cálculo da função

        //Variáveis envolvidas
        double x, solution;

        //Inicialização para leitura de dados
        Scanner ler= new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual é o valor de x ?");
        x = ler.nextDouble();

        //Processamento
        solution = Math.pow(x,2) - 3 * x + 1;

        //Saída dos dados
        System.out.println("A solução é "+String.format("%.2f",solution));

    }
    public static void exercicio12(){
        // Conversor ºC em ºF

        // Variáveis envolvidas
        double temperature_C, temperature_F;

        //Inicialização para leitura de dados
        Scanner ler= new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual é o valor da temperatura em ºC ?");
        temperature_C = ler.nextDouble();

        //Processamento
        temperature_F= 32 + (9/5.0) * temperature_C;

        //Saída dos dados
        System.out.println(String.format("%.2f",temperature_F) + " ºF");

    }

    public static void exercicio13(){
        // Calculo dos minutos totais passados, desde as 0H

        // Variáveis envolvidas
        double H, M, minutos_totais;

        //Inicialização para leitura de dados
        Scanner ler= new Scanner(System.in);

        //Leitura dos dados
        System.out.println("Qual o valor de H (horas)?");
        H = ler.nextInt();
        System.out.println("Qual o valor de M (minutos)?");
        M = ler.nextInt();

        //Processamento
        minutos_totais = H * 60 + M;

        //Saída dos dados
        System.out.println(String.format("%.2f",minutos_totais) + " ºF");

    }
}
