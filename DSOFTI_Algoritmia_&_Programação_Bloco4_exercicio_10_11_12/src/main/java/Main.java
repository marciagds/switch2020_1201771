public class Main {
    public static void main(String[] args) {
        // write your code here
        //exercicio10();
        //exercicio11();
        //exercicio12();
    }

    public static void exercicio10() {
        System.out.println("Exercicio10.a: " + getElementodeMenorValordeUmVetor(new int[]{3, 6, 9, 4, 5, 7, 8}));
        System.out.println("Exercicio10.b: " + getElementodeMenorValordeUmVetor(new int[]{3, 6, 9, 4, 5, 7, 8}));
        System.out.println("Exercicio10.c: " + getMediaValoresdeUmVetor(new int[]{3, 6, 9, 4, 5, 7, 8}));
        System.out.println("Exercicio10.d: " + getProdutodosElementosUmVetor(new int[]{3, 6, 9, 4, 5, 7, 8}));
        System.out.println("Exercicio10.e: " + getVetorElementosdoVetorNãoRepetidos(new int[]{3, 6, 9, 3, 5, 7, 8}));
        System.out.println("Exercicio10.f: " + getVetorElementospelaOrdemInversa(new int[]{3, 6, 9, 3, 5, 7, 8}));
        System.out.println("Exercicio10.g: " + getElementosPrimosDoVetor(new int[]{3, 6, 9, 3, 5, 7, 8}));
    }

    public static Integer getElementodeMenorValordeUmVetor(int[] vetor) {        //utiliza-se Interger para retornar null.
        int menor_valor;
        if (vetor == null) {
            return null;
        } else {
            menor_valor = vetor[0];       //assumo o primeiro elemento do vetor como referência
            for (int index = 0; index < vetor.length; index++) {
                if (vetor[index] < menor_valor) {
                    menor_valor = vetor[index];
                }
            }
            return menor_valor;
        }
    }

    public static Integer getElementodeMaiorValordeUmVetor(int[] vetor) {
        int maior_valor;
        if (vetor == null) {
            return null;
        } else {
            maior_valor = vetor[0];        //assumo o primeiro elemento do vetor como referência
            for (int index = 0; index < vetor.length; index++) {
                if (vetor[index] > maior_valor) {
                    maior_valor = vetor[index];
                }
            }
            return maior_valor;
        }
    }

    public static double getMediaValoresdeUmVetor(int[] vetor) {
        double mediaElementos;
        if (vetor == null) {
            return -1;
        } else {
            double somaElementos = 0;
            for (int index = 0; index < vetor.length; index++) {
                somaElementos += vetor[index];
            }
            mediaElementos = somaElementos / vetor.length;
            return mediaElementos;
        }
    }

    public static double getProdutodosElementosUmVetor(int[] vetor) {
        double produtoElementos = 1;
        if (vetor == null) {
            return -1;
        } else {
            for (int index = 0; index < vetor.length; index++) {
                produtoElementos *= vetor[index];
            }
            return produtoElementos;
        }
    }

    public static int getContagemElementosdoVetorNãoRepetidos(int[] vetor) {
        //contar elementos não repetidos
        int contagemElementosVetorNaoRepetidos = 0;
        for (int i = 0; i < vetor.length; i++) {
            int contagem_auxiliar = 0;
            for (int k = 0; k < vetor.length; k++) {
                if (k != i) {
                    if (vetor[i] == vetor[k]) {
                        contagem_auxiliar += 1;
                    }
                }
            }
            if (contagem_auxiliar == 0) {
                contagemElementosVetorNaoRepetidos += 1;
            }
        }
        return contagemElementosVetorNaoRepetidos;
    }

    public static int[] getVetorElementosdoVetorNãoRepetidos(int[] vetor) {
        //criar vetor dos elementos não repetidos
        int nElementosNaoRepetidos = getContagemElementosdoVetorNãoRepetidos(vetor);
        int[] elementosNaoRepetidos = new int[nElementosNaoRepetidos];
        int j = 0;
        for (int i = 0; i < vetor.length; i++) {
            int contagem_auxiliar = 0;
            for (int k = 0; k < vetor.length; k++) {
                if (k != i) {
                    if (vetor[i] == vetor[k]) {
                        contagem_auxiliar += 1;
                    }
                }
            }
            if (contagem_auxiliar == 0) {                 //colocação dos valores não repetidos
                elementosNaoRepetidos[j] = vetor[i];
                j++;
            }
        }
        return elementosNaoRepetidos;
    }

    public static int[] getVetorElementospelaOrdemInversa(int[] vetor) {
        int[] vetorInverso = new int[vetor.length];
        int k = vetorInverso.length - 1;
        for (int i = 0; i < vetor.length; i++) {
            vetorInverso[i] = vetor[k];
            k--;
        }
        return vetorInverso;
    }

    public static int getContagemElementosPrimosdoVetor(int[] vetor) {
        int contagemElementosPrimos = 0;
        for (int i = 0; i < vetor.length; i++) {
            int contagem_auxiliar = 0;
            if (vetor[i] > 1) {
                //numeros primos só têm 2 divisores. 1 e ele proprio. Apenas valores positivos
                for (int k = 2; k < vetor[i]; k++) {
                    double resto = vetor[i] % k;
                    if (resto == 0) {       //tem um divisor.
                        contagem_auxiliar += 1;
                    }
                }
                if (contagem_auxiliar == 0) {
                    contagemElementosPrimos += 1;
                }
            }
        }
        return contagemElementosPrimos;
    }

    public static int[] getElementosPrimosDoVetor(int[] vetor) {
        int[] vetorElementosPrimos;
        int nElementosPrimos = getContagemElementosPrimosdoVetor(vetor);
        vetorElementosPrimos = new int[nElementosPrimos];
        int j = 0;
        for (int i = 0; i < vetor.length; i++) {
            int contagem_auxiliar = 0;
            if (vetor[i] > 1) {
                for (int k = 2; k < vetor[i]; k++) {
                    double resto = vetor[i] % k;
                    if (resto == 0) {       //tem um divisor.
                        contagem_auxiliar += 1;
                    }
                }
                if (contagem_auxiliar == 0) {
                    vetorElementosPrimos[j] = vetor[i];
                    j++;
                }
            }
        }
        return vetorElementosPrimos;
    }

    public static void exercicio11(){
        System.out.println("Exercicio11: " + getProdutoEscalardeDoisVetores(new double[]{3,4}, new double[] {5,3}));
    }

    public  static Double getProdutoEscalardeDoisVetores(double [] vetor1, double [] vetor2){
        // O produto escalar apenas pode ser calculado para dois vetores do mesmo tamanho.
        double produtoEscalar =0 ;
        double produto;
        if (vetor1.length == vetor2.length ){
                for (int i=0; i<vetor1.length; i++) {
                    for (int k=0; k< vetor2.length; k++){
                        if (k==i){
                            produto = vetor1 [i] * vetor2 [k];
                            produtoEscalar += produto ;
                        }
                    }
                }
            return produtoEscalar;
        }
        else {
            return null;
        }
    }


    public static void exercicio12(){
        int [][] matriz = {{1,2,3},{4,5,6}};
        boolean matrizChecked = getSeMatrizTemLinhasComMesmoNumeroColunasIguais(matriz);
        if (matrizChecked == true){
            System.out.println("A matriz tem todas as linhas com o mesmo número de colunas.");
        }
        else {
            System.out.println("A matriz não tem todas as linhas com o mesmo número de colunas.");
        }
    }

    public static boolean getSeMatrizTemLinhasComMesmoNumeroColunasIguais(int[][]matriz){
        boolean matrizChecked = true;
        int nColunasReferencia = matriz[0].length;
        int nColunas;
        for(int i=1; i<matriz.length && matrizChecked == true; i++){        // o ciclo só vai acontecer enquanto for true. Quando passar a false pára.
                nColunas = matriz[i].length;
                if (nColunas != nColunasReferencia ) {
                    //return false;
                    matrizChecked = false;
                }
        }
        //return true;
        return matrizChecked;
    }
}
