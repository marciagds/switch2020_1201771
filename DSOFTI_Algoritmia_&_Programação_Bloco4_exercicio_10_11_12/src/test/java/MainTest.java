import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    /**
     * Testes exercicio10
     * alinea a.
     */
    @Test
    void getElementodeMenorValordeUmVetor_testeValoresPositivos() {
        //arrange
        int []vetor = {4,6,7,2,5};
        int expected = 2;
        //act
        int result = Main.getElementodeMenorValordeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getElementodeMenorValordeUmVetor_testeValoresNegativos() {
        //arrange
        int []vetor = {4,-6,-7,2,5};
        int expected = -7;
        //act
        int result = Main.getElementodeMenorValordeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getElementodeMenorValordeUmVetor_testeValoresNulos() {
        //arrange
        int []vetor = {4,-6,0,2,5};
        int expected = -6;
        //act
        int result = Main.getElementodeMenorValordeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getElementodeMenorValordeUmVetor_testeVetorVazio() {
        //arrange
        int []vetor = null;
        Integer expected = null;
        //act
        Integer result = Main.getElementodeMenorValordeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getElementodeMenorValordeUmVetor_testeVetorNulo() {
        //arrange
        int []vetor = {0};
        int expected = 0;
        //act
        int result = Main.getElementodeMenorValordeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    /**
     * Testes exercicio10
     * alinea b.
     */
    @Test
    void getElementodeMaiorValordeUmVetor_testeValoresPositivos() {
        //arrange
        int []vetor = {4,6,7,22,5};
        int expected = 22;
        //act
        int result = Main.getElementodeMaiorValordeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getElementodeMaiorValordeUmVetor_testeValoresNegativos() {
        //arrange
        int []vetor = {4,-6,-7,2,5};
        int expected = 5;
        //act
        int result = Main.getElementodeMaiorValordeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getElementodeMaiorValordeUmVetor_testeValoresNulos() {
        //arrange
        int []vetor = {4,-6,0,2,0};
        int expected = 4;
        //act
        int result = Main.getElementodeMaiorValordeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getElementodeMaiorValordeUmVetor_testeVetorVazio() {
        //arrange
        int []vetor = null;
        Integer expected = null;
        //act
        Integer result = Main.getElementodeMaiorValordeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getElementodeMaiorValordeUmVetor_testeVetorNulo() {
        //arrange
        int []vetor = {0};
        int expected = 0;
        //act
        int result = Main.getElementodeMaiorValordeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    /**
     * Testes exercicio10
     * alinea c.
     */
    @Test
    void getMediaValoresdeUmVetor_testeValoresPositivos() {
        //arrange
        int []vetor = {4,6,7,22,5};
        double expected = 8.8;
        //act
        double result = Main.getMediaValoresdeUmVetor(vetor);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getMediaValoresdeUmVetor_testeValoresNegativos() {
        //arrange
        int []vetor = {4,-6,-7,2,5};
        double expected = -0.4;
        //act
        double result = Main.getMediaValoresdeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMediaValoresdeUmVetor_testeValoresNulos() {
        //arrange
        int []vetor = {4,6,0,2,0};
        double expected = 2.4;
        //act
        double result = Main.getMediaValoresdeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMediaValoresdeUmVetor_testeVetorVazio() {
        //arrange
        int []vetor = null;
        double expected = -1;
        //act
        double result = Main.getMediaValoresdeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getMediaValoresdeUmVetor_testeVetorNulo() {
        //arrange
        int []vetor = {0,0};
        int expected = 0;
        //act
        double result = Main.getMediaValoresdeUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    /**
     * Testes exercicio10
     * alinea d.
     */
    @Test
    void getProdutodosElementosUmVetor_testeValoresPositivos() {
        //arrange
        int []vetor = {4,6,7,22,5};
        double expected = 18480;
        //act
        double result = Main.getProdutodosElementosUmVetor(vetor);
        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getProdutodosElementosUmVetor_testeValoresNegativos() {
        //arrange
        int []vetor = {4,-6,-7,2,5};
        double expected = 1680;
        //act
        double result = Main.getProdutodosElementosUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getProdutodosElementosUmVetor_testeValoresNulos() {
        //arrange
        int []vetor = {4,6,0,2,0};
        double expected = 0;
        //act
        double result = Main.getProdutodosElementosUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getProdutodosElementosUmVetor_testeVetorVazio() {
        //arrange
        int []vetor = null;
        double expected = -1;
        //act
        double result = Main.getProdutodosElementosUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void getProdutodosElementosUmVetor_testeVetorNulo() {
        //arrange
        int []vetor = {0,0};
        double expected = 0;
        //act
        double result = Main.getProdutodosElementosUmVetor(vetor);
        //assert
        assertEquals(expected, result);
    }
    /**
     * Testes exercicio10
     * alinea e.
     */
    @Test
    void  getContagemElementosdoVetorNãoRepetidos_valorNulo(){
        //arrange
        int []vetor = new int[]{0,0};
        int expected =0;

        //act
        int result = Main.getContagemElementosdoVetorNãoRepetidos(vetor);

        //assert
        assertEquals(expected,result);
    }
    @Test
    void  getContagemElementosdoVetorNãoRepetidos_ElementosPositivos(){
        //arrange
        int []vetor = new int[]{1,2,5,3,4,3,6,2};
        int expected = 4;

        //act
        int result = Main.getContagemElementosdoVetorNãoRepetidos(vetor);

        //assert
        assertEquals(expected,result);
    }
    @Test
    void  getContagemElementosdoVetorNãoRepetidos_ElementosNegativos(){
        //arrange
        int []vetor = new int[]{1,-2,5,3,4,3,6,-2};
        int expected = 4;

        //act
        int result = Main.getContagemElementosdoVetorNãoRepetidos(vetor);

        //assert
        assertEquals(expected,result);
    }
    @Test
    void  getContagemElementosdoVetorNãoRepetidos_ElementosNulos(){
        //arrange
        int []vetor = new int[]{0,0};
        int expected = 0;

        //act
        int result = Main.getContagemElementosdoVetorNãoRepetidos(vetor);

        //assert
        assertEquals(expected,result);
    }
    @Test
    void  getContagemElementosdoVetorNãoRepetidos_vetorVazio(){
        //arrange
        int []vetor = new int[]{};
        int expected = 0;

        //act
        int result = Main.getContagemElementosdoVetorNãoRepetidos(vetor);

        //assert
        assertEquals(expected,result);
    }

    @Test
    void  getVetorElementosdoVetorNãoRepetidos_ElementosPositivoseNegativos(){
        //arrange
        int []vetor = new int[]{1,-2,5,3,4,3,6,2};
        int[] expected = {1,-2,5,4,6,2};

        //act
        int [] result = Main.getVetorElementosdoVetorNãoRepetidos(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getVetorElementosdoVetorNãoRepetidos_positivos(){
        //arrange
        int []vetor = new int[]{1,2,5,3,4,3,6,2};
        int[] expected = {1,5,4,6};

        //act
        int [] result = Main.getVetorElementosdoVetorNãoRepetidos(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getVetorElementosdoVetorNãoRepetidos_ElementosNulos(){
        //arrange
        int []vetor = new int[]{1,2,0,3,4,3,6,2};
        int[] expected = {1,0,4,6};

        //act
        int [] result = Main.getVetorElementosdoVetorNãoRepetidos(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getVetorElementosdoVetorNãoRepetidos_vetorNulo(){
        //arrange
        int []vetor = new int[]{0};
        int[] expected = {0};

        //act
        int [] result = Main.getVetorElementosdoVetorNãoRepetidos(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getVetorElementosdoVetorNãoRepetidos_vetorVazio(){
        //arrange
        int []vetor = new int[]{};
        int[] expected = {};

        //act
        int [] result = Main.getVetorElementosdoVetorNãoRepetidos(vetor);

        //assert
        assertArrayEquals(expected,result);
    }

    /**
     * Testes exercicio10
     * alinea f.
     */
    @Test
    void  getVetorElementospelaOrdemInversa_vetorVazio (){
        //arrange
        int []vetor = new int[]{};
        int[] expected = {};

        //act
        int [] result = Main.getVetorElementospelaOrdemInversa(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void ggetVetorElementospelaOrdemInversa_ElementosPositivoseNegativos(){
        //arrange
        int []vetor = new int[]{1,-2,5,3,4,33,6,2};
        int[] expected = {2,6,33,4,3,5,-2,1};

        //act
        int [] result = Main.getVetorElementospelaOrdemInversa(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getVetorElementospelaOrdemInversa_Elementospositivos(){
        //arrange
        int []vetor = new int[]{1,22,5,3,4,3,6,2};
        int[] expected = {2,6,3,4,3,5,22,1};

        //act
        int [] result = Main.getVetorElementospelaOrdemInversa(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getVetorElementospelaOrdemInversa_ElementosNulos(){
        //arrange
        int []vetor = new int[]{1,2,0,3};
        int[] expected = {3,0,2,1};

        //act
        int [] result = Main.getVetorElementospelaOrdemInversa(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getVetorElementospelaOrdemInversa_vetorNulo(){
        //arrange
        int []vetor = new int[]{0};
        int[] expected = {0};

        //act
        int [] result = Main.getVetorElementospelaOrdemInversa(vetor);

        //assert
        assertArrayEquals(expected,result);
    }

    /**
     * Testes exercicio10
     * alinea g.
     */
    @Test
    void  getContagemElementosPrimosdoVetor_valorNulo(){
        //arrange
        int []vetor = new int[]{0,0};
        int expected =0;

        //act
        int result = Main.getContagemElementosPrimosdoVetor(vetor);

        //assert
        assertEquals(expected,result);
    }
    @Test
    void  getContagemElementosPrimosdoVetor_ElementosPositivos(){
        //arrange
        int []vetor = new int[]{1,2,5,3,4,3,6,2};
        int expected = 5;

        //act
        int result = Main.getContagemElementosPrimosdoVetor(vetor);

        //assert
        assertEquals(expected,result);
    }
    @Test
    void  getContagemElementosPrimosdoVetor_ElementosNegativos(){
        //arrange
        int []vetor = new int[]{1,-2,5,3,4,3,6,-2};
        int expected = 3;

        //act
        int result = Main.getContagemElementosPrimosdoVetor(vetor);

        //assert
        assertEquals(expected,result);
    }
    @Test
    void  getContagemElementosPrimosdoVetor_ElementosNulos(){
        //arrange
        int []vetor = new int[]{0,0};
        int expected = 0;

        //act
        int result = Main.getContagemElementosPrimosdoVetor(vetor);

        //assert
        assertEquals(expected,result);
    }
    @Test
    void  getContagemElementosPrimosdoVetor_vetorVazio(){
        //arrange
        int []vetor = new int[]{};
        int expected = 0;

        //act
        int result = Main.getContagemElementosPrimosdoVetor(vetor);

        //assert
        assertEquals(expected,result);
    }

    @Test
    void  getElementosPrimosDoVetor_ElementosPositivoseNegativos(){
        //arrange
        int []vetor = new int[]{1,-2,5,3,4,3,6,2};
        int[] expected = {5,3,3,2};

        //act
        int [] result = Main.getElementosPrimosDoVetor(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getElementosPrimosDoVetor_ElementosTodosNegativos(){
        //arrange
        int []vetor = new int[]{-1,-2,-5,-3};
        int[] expected = {};

        //act
        int [] result = Main.getElementosPrimosDoVetor(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getElementosPrimosDoVetor_positivos(){
        //arrange
        int []vetor = new int[]{1,2,5,3,4,3,6,2};
        int[] expected = {2,5,3,3,2};

        //act
        int [] result = Main.getElementosPrimosDoVetor(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getElementosPrimosDoVetor_ElementosNulos(){
        //arrange
        int []vetor = new int[]{11,2,0,3,4,3,6,2};
        int[] expected = {11,2,3,3,2};

        //act
        int [] result = Main.getElementosPrimosDoVetor(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getElementosPrimosDoVetor_vetorNulo(){
        //arrange
        int []vetor = new int[]{0};
        int[] expected = {};

        //act
        int [] result = Main.getElementosPrimosDoVetor(vetor);

        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void  getElementosPrimosDoVetor_vetorVazio(){
        //arrange
        int []vetor = new int[]{};
        int[] expected = {};

        //act
        int [] result = Main.getElementosPrimosDoVetor(vetor);

        //assert
        assertArrayEquals(expected,result);
    }

    /**
     * Testes exercicio11
     */
    @Test
    void getProdutoEscalardeDoisVetores_teste_MesmoTamanho_ElementosPositivosInteiros(){
        //arrange
        double [] vetor1 = {2,3,4};
        double [] vetor2 = {1,4,5};
        double expected = 34;

        //act
        double result = Main.getProdutoEscalardeDoisVetores(vetor1, vetor2);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getProdutoEscalardeDoisVetores_teste_MesmoTamanho_ElementosPositivoseNegativosInteiros1(){
        //arrange
        double [] vetor1 = {2,-3,4};
        double [] vetor2 = {1,4,5};
        double expected = 10;

        //act
        double result = Main.getProdutoEscalardeDoisVetores(vetor1, vetor2);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getProdutoEscalardeDoisVetores_teste_MesmoTamanho_ElementosPositivoseNegativosInteiros2(){
        //arrange
        double [] vetor1 = {2,-3,4};
        double [] vetor2 = {1,-4,5};
        double expected = 34;

        //act
        double result = Main.getProdutoEscalardeDoisVetores(vetor1, vetor2);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getProdutoEscalardeDoisVetores_teste_MesmoTamanho_ElementosPositivosDecimais(){
        //arrange
        double [] vetor1 = {2.5,3,4.2};
        double [] vetor2 = {1.2,4,5};
        double expected = 36;

        //act
        double result = Main.getProdutoEscalardeDoisVetores(vetor1, vetor2);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getProdutoEscalardeDoisVetores_teste_MesmoTamanho_ElementosPositivoseNegativosDecimais1(){
        //arrange
        double [] vetor1 = {2.1,-3,4};
        double [] vetor2 = {1,4,5.3};
        double expected = 11.3;

        //act
        double result = Main.getProdutoEscalardeDoisVetores(vetor1, vetor2);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getProdutoEscalardeDoisVetores_teste_MesmoTamanho_ElementosPositivoseNegativosDecimaiss2(){
        //arrange
        double [] vetor1 = {2.2,-3.3,4};
        double [] vetor2 = {1,-4,5};
        double expected = 35.4;

        //act
        double result = Main.getProdutoEscalardeDoisVetores(vetor1, vetor2);

        //assert
        assertEquals(expected, result, 0.01);
    }
    @Test
    void getProdutoEscalardeDoisVetores_teste_TamanhoDiferente(){
        //arrange
        double [] vetor1 = {2,3,4,3};
        double [] vetor2 = {1,4,5};
        Double expected = null;

        //act
        Double result = Main.getProdutoEscalardeDoisVetores(vetor1, vetor2);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getProdutoEscalardeDoisVetores_teste_VetoresNulos(){
        //arrange
        double [] vetor1 = {0};
        double [] vetor2 = {0};
        double expected = 0;

        //act
        double result = Main.getProdutoEscalardeDoisVetores(vetor1, vetor2);

        //assert
        assertEquals(expected, result);
    }
    @Test
    void getProdutoEscalardeDoisVetores_teste_VetoresVazios(){
        //arrange
        double [] vetor1 = {};
        double [] vetor2 = {};
        double expected = 0;

        //act
        double result = Main.getProdutoEscalardeDoisVetores(vetor1, vetor2);

        //assert
        assertEquals(expected, result);
    }

    /**
     * Testes exercicio12
     */
    @Test
    void getSeMatrizTemLinhasComNumeroColunasIguais_True(){
        //arrange
        int[][]matriz ={{1,2,3},{2,3,4},{1,4,5}};
        //act
        boolean result = Main.getSeMatrizTemLinhasComMesmoNumeroColunasIguais(matriz);
        //asssert
        assertTrue(result);
    }
    @Test
    void getSeMatrizTemLinhasComNumeroColunasIguais_False(){
        //arrange
        int[][]matriz ={{1,2,3},{2,3,4},{145}};
        //act
        boolean result = Main.getSeMatrizTemLinhasComMesmoNumeroColunasIguais(matriz);
        //asssert
        assertFalse(result);
    }
    @Test
    void getSeMatrizTemLinhasComNumeroColunasIguais_False_ElementosNegativos(){
        //arrange
        int[][]matriz ={{1,2,-3},{2,3,4},{-145}};
        //act
        boolean result = Main.getSeMatrizTemLinhasComMesmoNumeroColunasIguais(matriz);
        //asssert
        assertFalse(result);
    }
    @Test
    void getSeMatrizTemLinhasComNumeroColunasIguais_True_ElementosNegativos(){
        //arrange
        int[][]matriz ={{1,2,-3},{2,3,4},{1,4,5}};
        //act
        boolean result = Main.getSeMatrizTemLinhasComMesmoNumeroColunasIguais(matriz);
        //asssert
        assertTrue(result);
    }
    @Test
    void getSeMatrizVaziaTemLinhasComNumeroColunasIguais_True(){
        //arrange
        int[][]matriz ={{},{}};
        //act
        boolean result = Main.getSeMatrizTemLinhasComMesmoNumeroColunasIguais(matriz);
        //asssert
        assertTrue(result);
    }
    @Test
    void getSeMatrizVaziaTemLinhasComNumeroColunasIguais_False(){
        //arrange
        int[][]matriz ={{1,2,3},{},{145}};
        //act
        boolean result = Main.getSeMatrizTemLinhasComMesmoNumeroColunasIguais(matriz);
        //asssert
        assertFalse(result);
    }
    @Test
    void getSeMatrizNulaTemLinhasComNumeroColunasIguais_True(){
        //arrange
        int[][]matriz ={{0},{0}};
        //act
        boolean result = Main.getSeMatrizTemLinhasComMesmoNumeroColunasIguais(matriz);
        //asssert
        assertTrue(result);
    }
    @Test
    void getSeMatrizNulaVaziaTemLinhasComNumeroColunasIguais_False(){
        //arrange
        int[][]matriz ={{0},{0,0},{0}};
        //act
        boolean result = Main.getSeMatrizTemLinhasComMesmoNumeroColunasIguais(matriz);
        //asssert
        assertFalse(result);
    }
}