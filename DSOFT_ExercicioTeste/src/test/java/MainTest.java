import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    /**
     * Teste para obter o vetor com os caracteres todos em minusculas
     */
    @Test
    void obterVetorCaracteresLetrasMinuscuas_testeLetrasMinusculas() {
        //arrange
        String palavra = "sol";
        char[]expected = {'s','o','l'};
        //act
        char[]result = Main.obterVetorCaracteresLetrasMinuscuas(palavra);
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void obterVetorCaracteresLetrasMinuscuas_testeLetrasMaiusculas() {
        //arrange
        String palavra = "SoL";
        char[]expected = {'s','o','l'};
        //act
        char[]result = Main.obterVetorCaracteresLetrasMinuscuas(palavra);
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void obterVetorCaracteresLetrasMinuscuas_testePalavraVazia() {
        //arrange
        String palavra = "";
        char[]expected = {};
        //act
        char[]result = Main.obterVetorCaracteresLetrasMinuscuas(palavra);
        //assert
        assertArrayEquals(expected,result);
    }

    /**
     * Teste para obter o indice do limite inferior
     */
    @Test
    void verificarLimiteInferiordeProcura_testevariacaoNegativa(){
        //arrange
        int index=0;
        int janela=3;
        int expected=0;
        //act
        int result=Main.verificarLimiteInferiordeProcura(index,janela);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void verificarLimiteInferiordeProcura_testevariacaoNula(){
        //arrange
        int index=3;
        int janela=3;
        int expected=0;
        //act
        int result=Main.verificarLimiteInferiordeProcura(index,janela);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void verificarLimiteInferiordeProcura_testevariacaoPositiva(){
        //arrange
        int index=5;
        int janela=3;
        int expected=2;
        //act
        int result=Main.verificarLimiteInferiordeProcura(index,janela);
        //assert
        assertEquals(expected, result);
    }

    /**
     * Teste para obter o indice do limite superior
     */
    @Test
    void verificarLimiteSuperiordeProcura_testevariacaoSuperiorNumeroCaracteres(){
        //arrange
        int index=4;
        int janela=3;
        int numeroCaracteres=5;
        int expected=4;
        //act
        int result=Main.verificarLimiteSuperiordeProcura(index,janela,numeroCaracteres);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void verificarLimiteSuperiordeProcura_teste2variacaoInferiorNumeroCaracteres(){
        //arrange
        int index=1;
        int janela=3;
        int numeroCaracteres=5;
        int expected=4;
        //act
        int result=Main.verificarLimiteSuperiordeProcura(index,janela,numeroCaracteres);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void verificarLimiteSuperiordeProcura_teste1variacaoInferiorNumeroCaracteres() {
        //arrange
        int index = 2;
        int janela = 3;
        int numeroCaracteres = 5;
        int expected = 4;
        //act
        int result = Main.verificarLimiteSuperiordeProcura(index, janela, numeroCaracteres);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void verificarLimiteSuperiordeProcura_teste3variacaoInferiorNumeroCaracteres() {
        //arrange
        int index = 2;
        int janela = 3;
        int numeroCaracteres = 7;
        int expected = 5;
        //act
        int result = Main.verificarLimiteSuperiordeProcura(index, janela, numeroCaracteres);
        //assert
        assertEquals(expected, result);
    }
    @Test
    void verificarLimiteSuperiordeProcura_teste4variacaoInferiorNumeroCaracteres() {
        //arrange
        int index = 1;
        int janela = 1;
        int numeroCaracteres = 3;
        int expected = 2;
        //act
        int result = Main.verificarLimiteSuperiordeProcura(index, janela, numeroCaracteres);
        //assert
        assertEquals(expected, result);
    }

    /**
     * Teste para obter numero de caracteres correspondentes.
     */
    @Test
    void obterNumeroCaracteresCorrespondentes_teste1(){
        //arrange
        char[]palavra1 = {'s','o','l'};
        char[]palavra2 = {'s','u','l'};
        int p2 = 3;
        int janela = 1;
        int expected = 2;
        //act
        int result = Main.obterNumeroCaracteresCorrespondentes(palavra1,palavra2,p2,janela);
        //assert
        assertEquals(expected,result);
    }
    @Test
    void obterNumeroCaracteresCorrespondentes_teste2(){
        //arrange
        char[]palavra1 = {'a','b','a','r','c','a','r'};
        char[]palavra2 = {'a','m','a','r','r','a','r'};
        int p2 = 6;
        int janela = 2;
        int expected = 5;
        //act
        int result = Main.obterNumeroCaracteresCorrespondentes(palavra1,palavra2,p2,janela);
        //assert
        assertEquals(expected,result);
    }
    @Test
    void obterNumeroCaracteresCorrespondentes_teste3(){
        //arrange
        char[]palavra1 = {'t','e','r','r','a'};
        char[]palavra2 = {'a','m','a','r','r','a','r'};
        int p2 = 6;
        int janela = 2;
        int expected = 3;
        //act
        int result = Main.obterNumeroCaracteresCorrespondentes(palavra1,palavra2,p2,janela);
        //assert
        assertEquals(expected,result);
    }

    /**
     * Teste para obter numero de caracteres correspondentes.
     */
    @Test
    void criarVetorComCaracteresCorrespondentesDaPalavra_teste1(){
        //arrange
        int m = 2;
        int janela = 1;
        char[]palavraPretendida = {'s','o','l'};
        char[]palavraAuxiliar = {'s','u','l'};
        int nCaracteresPalavraAuxiliar = 3;
        char[] expected = {'s','l'};
        //act
        char[] result = Main.criarVetorComCaracteresCorrespondentesDaPalavra(m,janela,palavraPretendida,palavraAuxiliar,nCaracteresPalavraAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void criarVetorComCaracteresCorrespondentesDaPalavra_testeAMARRAReTERRA(){
        //arrange
        int m = 3;
        int janela = 2;
        char[]palavraPretendida = {'a','m','a','r','r','a','r'};
        char[]palavraAuxiliar = {'t','e','r','r','a'};
        int nCaracteresPalavraAuxiliar = 5;
        char[] expected = {'a','r','r'};
        //act
        char[] result = Main.criarVetorComCaracteresCorrespondentesDaPalavra(m,janela,palavraPretendida,palavraAuxiliar,nCaracteresPalavraAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void criarVetorComCaracteresCorrespondentesDaPalavra_testeTERRAeAMARRAR(){
        //arrange
        int m = 3;
        int janela = 2;
        char[]palavraAuxiliar= {'a','m','a','r','r','a','r'};
        char[]palavraPretendida= {'t','e','r','r','a'};
        int nCaracteresPalavraAuxiliar = 7;
        char[] expected = {'r','r','a'};
        //act
        char[] result = Main.criarVetorComCaracteresCorrespondentesDaPalavra(m,janela,palavraPretendida,palavraAuxiliar,nCaracteresPalavraAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void criarVetorComCaracteresCorrespondentesDaPalavra_testeAMARRAReABARCAR(){
        //arrange
        int m = 5;
        int janela = 2;
        char[]palavraPretendida = {'a','m','a','r','r','a','r'};
        char[]palavraAuxiliar = {'a','b','a','r','c','a','r'};
        int nCaracteresPalavraAuxiliar = 7;
        char[] expected = {'a','a','r','r','a'};
        //act
        char[] result = Main.criarVetorComCaracteresCorrespondentesDaPalavra(m,janela,palavraPretendida,palavraAuxiliar,nCaracteresPalavraAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }
    @Test
    void criarVetorComCaracteresCorrespondentesDaPalavra_testeABARCAReAMARRAR(){
        //arrange
        int m = 5;
        int janela = 2;
        char[]palavraPretendida={'a','b','a','r','c','a','r'};
        char[]palavraAuxiliar={'a','m','a','r','r','a','r'};
        int nCaracteresPalavraAuxiliar = 7;
        char[] expected = {'a','a','r','a','r'};
        //act
        char[] result = Main.criarVetorComCaracteresCorrespondentesDaPalavra(m,janela,palavraPretendida,palavraAuxiliar,nCaracteresPalavraAuxiliar);
        //assert
        assertArrayEquals(expected,result);
    }

    /**
     * Teste para obter numero de transposições
     */
    @Test
    void obterContagemTrans_testeAMARRAReTERRA(){
        //arrange
        char[]letrasPalavra1= {'a','r','r'};
        char[]letrasPalavra2 = {'r','r','a'};
        int expected = 2;
        //act
        int result = Main.obterContagemTransp(letrasPalavra1,letrasPalavra2);
        //assert
       assertEquals(expected,result);
    }
    @Test
    void obterContagemTrans_testeAMARRAReABARCAR(){
        //arrange
        char[]letrasPalavra1= {'a','a','r','r','a'};
        char[]letrasPalavra2 ={'a','a','r','a','r'};
        int expected = 2;
        //act
        int result = Main.obterContagemTransp(letrasPalavra1,letrasPalavra2);
        //assert
        assertEquals(expected,result);
    }
    @Test
    void obterContagemTrans_testeSOLeSUL(){
        //arrange
        char[]letrasPalavra1= {'s','l'};
        char[]letrasPalavra2 ={'s','l'};
        int expected = 0;
        //act
        int result = Main.obterContagemTransp(letrasPalavra1,letrasPalavra2);
        //assert
        assertEquals(expected,result);
    }

    /**
     * Teste para equação de grau de semelhança
     */
    @Test
    void  calculoGrauSemelhanca(){
        //arrange
        int m=3;
        int t=2;
        int p1=5;
        int p2=6;
        double expected = 0.4777;
        //act
        double result = Main.calculoGrauSemelhanca(m,t,p1,p2);
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void  calculoGrauSemelhanca_mNulo(){
        //arrange
        int m=0;
        int t=2;
        int p1=5;
        int p2=6;
        double expected =0;
        //act
        double result = Main.calculoGrauSemelhanca(m,t,p1,p2);
        //assert
        assertEquals(expected,result,0.01);
    }

    /**
     * Teste metodo final
     */
    @Test
    void obterGrauSemelhancaEntreDuasPalavras_teste1(){
        //arrange
        String palavra1 ="sol";
        String palavra2 = "bar";
        double expected = 0;
        //act
        double result = Main.obterGrauSemelhancaEntreDuasPalavras(palavra1,palavra2);
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void obterGrauSemelhancaEntreDuasPalavras_teste2(){
        //arrange
        String palavra1 ="mar";
        String palavra2 = "bar";
        double expected = 0.777;
        //act
        double result = Main.obterGrauSemelhancaEntreDuasPalavras(palavra1,palavra2);
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void obterGrauSemelhancaEntreDuasPalavras_teste3(){
        //arrange
        String palavra1 ="mar";
        String palavra2 = "maré";
        double expected = 0.9166;
        //act
        double result = Main.obterGrauSemelhancaEntreDuasPalavras(palavra1,palavra2);
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void obterGrauSemelhancaEntreDuasPalavras_teste4(){
        //arrange
        String palavra1 ="maré";
        String palavra2 = "maresia";
        double expected = 0.7261;
        //act
        double result = Main.obterGrauSemelhancaEntreDuasPalavras(palavra1,palavra2);
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void obterGrauSemelhancaEntreDuasPalavras_teste5(){
        //arrange
        String palavra1 ="maresia";
        String palavra2 = "abarcar";
        double expected = 0.619;
        //act
        double result = Main.obterGrauSemelhancaEntreDuasPalavras(palavra1,palavra2);
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void obterGrauSemelhancaEntreDuasPalavras_teste6(){
        //arrange
        String palavra1 ="abarcar";
        String palavra2 = "amarrar";
        double expected =  0.7428;
        //act
        double result = Main.obterGrauSemelhancaEntreDuasPalavras(palavra1,palavra2);
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void obterGrauSemelhancaEntreDuasPalavras_teste7(){
        //arrange
        String palavra1 ="amarrar";
        String palavra2 = "terra";
        double expected = 0.5650;
        //act
        double result = Main.obterGrauSemelhancaEntreDuasPalavras(palavra1,palavra2);
        //assert
        assertEquals(expected,result,0.01);
    }
    @Test
    void obterGrauSemelhancaEntreDuasPalavras_teste8(){
        //arrange
        String palavra1 ="terra";
        String palavra2 = "Terra";
        double expected = 1.0;
        //act
        double result = Main.obterGrauSemelhancaEntreDuasPalavras(palavra1,palavra2);
        //assert
        assertEquals(expected,result,0.01);
    }

}