import com.sun.org.apache.bcel.internal.generic.ARETURN;

public class Main {
    public static void main(String[] args) {

    }

    public static double obterGrauSemelhancaEntreDuasPalavras(String palavra1, String palavra2){
       char[]palavra_1 = obterVetorCaracteresLetrasMinuscuas(palavra1);
       char[]palavra_2 = obterVetorCaracteresLetrasMinuscuas(palavra2);
       int p1 = palavra_1.length;
       int p2 = palavra_2.length;
       int janela = (Math.max(p1,p2)/2)-1;
       int m = obterNumeroCaracteresCorrespondentes(palavra_1,palavra_2,p2,janela);
       char[]letrasCorrespondentesPalavra1 = criarVetorComCaracteresCorrespondentesDaPalavra(m,janela,palavra_1,palavra_2,p2);
       char[]letrasCorrespondentesPalavra2 = criarVetorComCaracteresCorrespondentesDaPalavra(m,janela,palavra_2,palavra_1,p1);
       int t = obterContagemTransp(letrasCorrespondentesPalavra1,letrasCorrespondentesPalavra2);
       double grauSemelhanca = calculoGrauSemelhanca(m,t,p1,p2);

       return  grauSemelhanca;
    }



    public static char[] obterVetorCaracteresLetrasMinuscuas(String palavra){
        String letrasMinusculas = palavra.toLowerCase();
        char[]palavraEmMinusculas = letrasMinusculas.toCharArray();
        return palavraEmMinusculas;
    }

    public static int obterNumeroCaracteresCorrespondentes(char[]palavra1, char[]palavra2, int numeroCaracteresPalavra2, int janela){
        int m=0;
        for(int indexPalavra1=0; indexPalavra1<palavra1.length; indexPalavra1++){
            boolean letraEncontrada=false;
            int indexLimiteInferior=verificarLimiteInferiordeProcura(indexPalavra1, janela);
            int indexLimiteSuperior = verificarLimiteSuperiordeProcura(indexPalavra1,janela, numeroCaracteresPalavra2);
            for(int indexPalavra2=indexLimiteInferior; indexPalavra2<=indexLimiteSuperior && letraEncontrada==false; indexPalavra2++){
                if(palavra1[indexPalavra1]==palavra2[indexPalavra2]){
                m+=1;
                palavra1[indexPalavra1]='-';
                letraEncontrada = true;
                }
            }
        }
        return m;
    }

    public static int verificarLimiteInferiordeProcura(int index,int janela ){
        int indexLimiteInferior;
        int variacao = index - janela;
        if(variacao<=0){
            indexLimiteInferior = 0;
        }
        else{
            indexLimiteInferior = variacao;
        }
        return indexLimiteInferior;
    }

    public static int verificarLimiteSuperiordeProcura(int index,int janela,int numeroCaracteres){
        int indexLimiteSuperior;
        int variacao = index + janela;
        if(variacao>=numeroCaracteres){
             indexLimiteSuperior = numeroCaracteres-1;
        }
        else{
            indexLimiteSuperior = variacao;
        }
        return indexLimiteSuperior;
    }

    public static char[] criarVetorComCaracteresCorrespondentesDaPalavra(int m, int janela,char[]palavraPretendida, char[]palavraAuxiliar, int numeroCaracteresPalavraAuxiliar ){
        char[]vetorCaracteresCorrespondentesDaPalavra = new char[m];
        int indexVetor=0;
        for(int indexPalavraPretendida=0; indexPalavraPretendida<palavraPretendida.length && indexVetor<m; indexPalavraPretendida++){
            boolean letraEncontrada=false;
            int indexLimiteInferior=verificarLimiteInferiordeProcura(indexPalavraPretendida, janela);
            int indexLimiteSuperior = verificarLimiteSuperiordeProcura(indexPalavraPretendida,janela, numeroCaracteresPalavraAuxiliar);
            for(int indexPalavraAuxiliar=indexLimiteInferior; indexPalavraAuxiliar<=indexLimiteSuperior && letraEncontrada==false; indexPalavraAuxiliar++){
                if(palavraPretendida[indexPalavraPretendida]==palavraAuxiliar[indexPalavraAuxiliar]){
                    vetorCaracteresCorrespondentesDaPalavra[indexVetor]= palavraPretendida[indexPalavraPretendida];
                    palavraPretendida[indexPalavraPretendida]='-';
                    letraEncontrada = true;
                    indexVetor++;
                }
            }
        }
        return vetorCaracteresCorrespondentesDaPalavra;


    }

    public static int obterContagemTransp(char[]letrasPalavra1, char[]letrasPalavra2){
        int t=0;
        for(int index =0; index<letrasPalavra1.length; index++){
            if(letrasPalavra1[index] != letrasPalavra2[index]){
                t+=1;
            }
        }
        return t;
    }

    public static double calculoGrauSemelhanca (int m, int t, int p1, int p2){
       double grauSemelhanca;
       double m2 = m;
        if(m2==0){
            grauSemelhanca = 0;
        }
        else{
            grauSemelhanca = ((1/3.0) * ((m2/p1) + (m2/p2) + ((m2-t)/m2)));
        }
        return  grauSemelhanca;


    }
}
