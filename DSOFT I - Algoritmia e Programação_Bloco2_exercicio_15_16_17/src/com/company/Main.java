package com.company;

import javax.sound.midi.SysexMessage;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //exercicio15();
        //exercicio16();
        exercicio17();
    }

    public static void exercicio15(){
        //Classificação de um triângulo (escaleno, isósceles e equilátero), dados os 3 lados.

        //Variáveis envolvidas
        double lado_a, lado_b, lado_c;
        String classification;

        //Inicialização da leitura
        Scanner ler = new Scanner (System.in);

        //Leitura dos dados
        System.out.println("Qual o comprimento do lado A ?");
        lado_a = ler.nextDouble();
        System.out.println("Qual o comprimento do lado B ?");
        lado_b = ler.nextDouble();
        System.out.println("Qual o comprimento do lado C ?");
        lado_c = ler.nextDouble();

        //Processamento
        classification = getTrianguleClassification(lado_a,lado_b,lado_c);

        //Saída dos dados
        System.out.println(classification);
    }

    public static String getTrianguleClassification(double lado_a, double lado_b, double lado_c){
        String classification;

        if(lado_a <=0 || lado_b <=0 || lado_c <=0){
            classification = "O lado do triângulo tem de tomar valor positivo, obrigatoriamente.";
            return classification;
        }
        else {
            if (lado_a + lado_b < lado_c || lado_a + lado_c < lado_b || lado_b + lado_c < lado_a){
                classification = "A combinação dos comprimentos dos lados inseridos não impossiveis.";
                return classification;
            }
            else {
                if(lado_a == lado_b && lado_a==lado_c){
                    classification = "Triângulo escaleno";
                    return classification;
                }
                else {
                    if (lado_a==lado_b || lado_a==lado_c || lado_b==lado_c){
                        classification = "Triângulo isósceles";
                        return classification;
                    }
                    else {
                        classification = "Triângulo equilátero";
                        return classification;
                    }
                }
            }
        }
    }

    public  static void exercicio16(){
        // Classificação de um triângulo quanto aos seus ângulos.

        //Variáveis envolvidas
        double angulo_a, angulo_b, angulo_c;
        String classification;

        //Inicialização da leitura
        Scanner ler = new Scanner (System.in);

        //Leitura das variáveis
        System.out.println("Qual o valor do 1º ângulo, em graus ?");
        angulo_a = ler.nextDouble();
        System.out.println("Qual o valor do 2º ângulo, em graus ?");
        angulo_b = ler.nextDouble();
        System.out.println("Qual o valor do 3º ângulo, em graus ?");
        angulo_c = ler.nextDouble();

        //Processamento
        classification = getTriangule_anglesClassification(angulo_a, angulo_b, angulo_c);

        //Saída dos resultados
        System.out.println(classification);
    }

    public static String getTriangule_anglesClassification(double angulo_a, double angulo_b, double angulo_c) {
        String classification;

        double soma = angulo_a + angulo_b + angulo_c;

        if (angulo_a <= 0 || angulo_b <= 0 || angulo_c <= 0) {
        classification = "Erro. Valores de angulos são, obrigatoriamente, positivos.";
        return classification;
        } else {
            if (soma == 180) {
                if (angulo_a > 90 || angulo_b > 90 || angulo_c > 90) {
                    classification = "Triângulo obtusângulo";
                    return classification;
                } else {
                    if (angulo_a == 90 || angulo_b == 90 || angulo_c == 90) {
                        classification = "Triangulo retângulo";
                        return classification;
                    } else {
                        classification = "Triângulo acutângulo";
                        return classification;
                    }
                }
            } else {
                classification = "Erro. A soma dos angulos de um triângulo tem de ser igual a 180º.";
                return classification;
            }
        }
    }

    public static void exercicio17(){
        //Cálculo de chegada do comboio

        //Variáveis envolvidas
        int horas_partida, minutos_partida, duration_travel_hours, duration_travel_minuts;
        String resultado;

        //Inicialização da leitura
        Scanner ler = new Scanner(System.in);

        //Leitura das variáveis
        System.out.println("Horário de partida da viagem: horas ?");
        horas_partida = ler.nextInt();
        System.out.println("Horário de partida da viagem: minutos ?");
        minutos_partida = ler.nextInt();
        System.out.println("Duração da viagem: horas?");
        duration_travel_hours = ler.nextInt();
        System.out.println("Duração da viagem: minutos?");
        duration_travel_minuts = ler.nextInt();

        //Procedimento
        resultado = getHorarioChegadaTrain(horas_partida,minutos_partida,duration_travel_hours,duration_travel_minuts);
        //Saída dos dados
        System.out.println(resultado);
    }

    public static String getHorarioChegadaTrain(int horas_partida, int minutos_partida, int duration_travel_hours, int duration_travel_minuts){
        String day, resultado;
        int horas_chegada, minutos_chegada;

        if(horas_partida<0 || horas_partida >=24 || duration_travel_hours <0 || duration_travel_hours >24){
            resultado = "Erro. Horas inseridas erradas.";
            return resultado;
        }
        else {
            if (minutos_partida <0 || minutos_partida >60 || duration_travel_minuts <0 || duration_travel_minuts >60){
                resultado = "Erro. Minutos inseridos errados";
                return resultado;
            }
            else {
                horas_chegada = horas_partida + duration_travel_hours;
                minutos_chegada = minutos_partida + duration_travel_minuts;
            }
        }

    }
}
