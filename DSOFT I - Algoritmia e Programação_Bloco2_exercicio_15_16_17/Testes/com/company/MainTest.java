package com.company;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    //Testes Exercicio 15
    @org.junit.jupiter.api.Test
    void getTrianguleClassification_teste_equilatero() {
        //arrange
        double lado_a = 12;
        double lado_b = 9;
        double lado_c = 11;
        String expected = "Triângulo equilátero";

        //act
        String result = Main.getTrianguleClassification(lado_a,lado_b,lado_c);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getTrianguleClassification_teste_isosceles() {
        //arrange
        double lado_a = 5;
        double lado_b = 3;
        double lado_c = 3;
        String expected = "Triângulo isósceles";

        //act
        String result = Main.getTrianguleClassification(lado_a,lado_b,lado_c);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getTrianguleClassification_teste_escaleno(){
        //arrange
        double lado_a = 4;
        double lado_b = 4;
        double lado_c = 4;
        String expected = "Triângulo escaleno";

        //act
        String result = Main.getTrianguleClassification(lado_a,lado_b,lado_c);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getTrianguleClassification_teste_valor_nulo(){
        //arrange
        double lado_a = 0;
        double lado_b = 4;
        double lado_c = 4;
        String expected = "O lado do triângulo tem de tomar valor positivo, obrigatoriamente.";

        //act
        String result = Main.getTrianguleClassification(lado_a,lado_b,lado_c);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getTrianguleClassification_teste_valor_negativo(){
        //arrange
        double lado_a = 3;
        double lado_b = 4;
        double lado_c = -4;
        String expected = "O lado do triângulo tem de tomar valor positivo, obrigatoriamente.";

        //act
        String result = Main.getTrianguleClassification(lado_a,lado_b,lado_c);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getTrianguleClassification_teste_valor_combinação_errada(){
        //arrange
        double lado_a = 3;
        double lado_b = 7;
        double lado_c = 2;
        String expected = "A combinação dos comprimentos dos lados inseridos não impossiveis.";

        //act
        String result = Main.getTrianguleClassification(lado_a,lado_b,lado_c);

        //assert
        assertEquals(expected, result);
    }

    //Testes Exercicio16
    @org.junit.jupiter.api.Test
    void getTriangule_angle_Classification_teste_T_retângulo(){
        //arrange
        double angulo_a = 34;
        double angulo_b = 90;
        double angulo_c = 56;
        String expected = "Triangulo retângulo";

        //act
        String result = Main.getTriangule_anglesClassification(angulo_a,angulo_b,angulo_c);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getTriangule_angle_Classification_teste_T_obtusângulo(){
        //arrange
        double angulo_a = 32;
        double angulo_b = 110;
        double angulo_c = 38;
        String expected = "Triângulo obtusângulo";

        //act
        String result = Main.getTriangule_anglesClassification(angulo_a,angulo_b,angulo_c);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getTriangule_angle_Classification_teste_T_acutângulo(){
        //arrange
        double angulo_a = 65;
        double angulo_b = 30;
        double angulo_c = 85;
        String expected = "Triângulo acutângulo";

        //act
        String result = Main.getTriangule_anglesClassification(angulo_a,angulo_b,angulo_c);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getTriangule_angle_Classification_teste_soma_inf_180(){
        //arrange
        double angulo_a = 47;
        double angulo_b = 32;
        double angulo_c = 0;
        String expected =  "Erro. A soma dos angulos de um triângulo tem de ser igual a 180º.";

        //act
        String result = Main.getTriangule_anglesClassification(angulo_a,angulo_b,angulo_c);

        //assert
        assertEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void getTriangule_angle_Classification_teste_valores_negat(){
        //arrange
        double angulo_a = 90;
        double angulo_b = -110;
        double angulo_c = 200;
        String expected =  "Erro. Valores de angulos são, obrigatoriamente, positivos.";

        //act
        String result = Main.getTriangule_anglesClassification(angulo_a,angulo_b,angulo_c);

        //assert
        assertEquals(expected, result);
    }

    //Testes Exercicio 17

}